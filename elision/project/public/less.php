<?php

    $start_time = microtime(true);

    $css = $_GET['path'];
    $less = str_replace('.css', '.less', $css);
    $less = str_replace('.min', null, $less);
    $enable_minified = true;

    if(file_exists($less)) {
        // scan less files
        function scan_import($file) {
            if(!file_exists($file)) {
                die('File not found '.$file);
            }
            $file = realpath($file);
            $dir = dirname($file).'/';
            $resp = [$file];
            $body = file_get_contents($file);
            $body = explode('@import', $body);
            array_shift($body);
            if(count($body)) {
                foreach($body as $line) {
                    $line = explode(';', $line, 2)[0];
                    $line = trim($line, ' "');
                    $resp = array_merge($resp, scan_import($dir.$line));
                }
            }
            return $resp;
        };
        $all_in = scan_import($less);

        $max_less_ftime = PHP_INT_MIN;
        foreach($all_in as $a) {
            $fmt = filemtime($a);
            if($fmt > $max_less_ftime) {
                $max_less_ftime = $fmt;
            }
        }

        if (!is_file($css) || ($max_less_ftime > filemtime(__DIR__.'/'.$css))) {
            require __DIR__.'/tools/lessc.php';
            $compiller = new lessc;
            try {
                $compiller->compileFile($less, $css);
                $elapsed = round(microtime(true) - $start_time, 3);
                file_put_contents($css, "/*CSS generated time: {$elapsed} sec*/", FILE_APPEND);
                if($enable_minified) {
                    require __DIR__.'/tools/CssMin.php';
                    $minified = CssMin::minify(file_get_contents($css));
                    file_put_contents($css, $minified);
                }
            } catch(Exception $ex) {
                die($ex->getMessage());
            }
        }
    }

header('Content-Type: text/css');
$seconds_to_cache = 86400 * 365;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: {$ts}");
header("Pragma: cache");
header("Cache-Control: max-age={$seconds_to_cache}");
echo file_get_contents(__DIR__."/{$css}");
