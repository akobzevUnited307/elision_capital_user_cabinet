/*
Mikron 0.0.1
@since 2014-04-30
*/

var Mikron = {

    list_id_list: {},
    list_events: {
        before_load_edit_form: 1,
        before_load_edit_form: 2,
        onload_edit_form: 3,
        before_item_delete: 4
    },
    
    resetMikronListHandlers: function() {
		$('.mikron-list-ajax .mikron-paginator a').off('click').on('click', function(e){
			var list_id = $(this).closest('form').attr('id');
			var url = $(this).attr('href');
			$('#' + list_id).attr('action', url).submit();
			return false;
		});
        return false;
    },

    init: function(){

		Mikron.resetMikronListHandlers();

		$('.mikron-list-ajax').submit(function(){
			var form = $(this);
            var op = {
                // успешный HTTP ответ (статус 200)
                success: function(data, status, e) {
                	form.html(data);
                    form.find('table').hide().stop().fadeIn('slow');
                	return Mikron.resetMikronListHandlers();
                },
                headers: {
					'HTTP_X_REQUESTED_WITH': 'xmlhttprequest',
					'X-Requested-With': 'xmlhttprequest'
				},
                // ошибочный HTTP ответ
                error: function(jqXHR, textStatus, errorThrown) {
                    var e = 'Ошибка сервера №' + jqXHR.status + ': ' + jqXHR.statusText;
                    return CustomUI.showMessage(e, options.caption, 'error', {onclose: function(data) {
                            if(freezed_form_id && $('#'+freezed_form_id+' .popup-form-content').length) {
                                return CustomUI.showForm(freezed_form_id, true, true, {});
                            }
                        }, params: false});
                },
				beforeSend: function(xhr, a) {
					xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest');
					xhr.setRequestHeader('HTTP_X_REQUESTED_WITH', 'xmlhttprequest');
				}
            };
			$(this).ajaxSubmit(op);
			return false;
		});

    },

    /**
    * Добавление слушателя событий списка
    *
    * @param string list_id
    * @param callback callback
    *
    * @returns {Boolean}
    */
    addListHandler: function(list_id, callback) {
        if(!(list_id in this.list_id_list)) {
            this.list_id_list[list_id] = new Array();
        }
        // .unshift() aka Prepend: http://stackoverflow.com/questions/8073673/how-can-i-add-new-array-elements-at-the-top-of-an-array-in-javascript
        this.list_id_list[list_id].unshift(callback);
        // console.log(this.list_id_list);
        return true;
    },

    /**
    * Called by Mikron template automatically
    * 
    * @param string list_id
    * @param string event
    * @param Array params
    * 
    * @returns {Boolean}
    */
    raiseListHandlers: function(list_id, event, params) {
        if(!(list_id in this.list_id_list)) {
            return false;
        }
        var c = this.list_id_list[list_id]
        for (key in c) {
            if(c[key] instanceof Function) {
                if(c[key](list_id, event, params)) {
                    return true;
                    break;
                }
            }
        }
        return false;
    }

}

$(function($) {
    Mikron.init();
});