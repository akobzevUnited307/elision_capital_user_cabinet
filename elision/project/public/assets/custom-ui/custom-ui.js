/**
* https://jscompress.com/
* http://closure-compiler.appspot.com/home
* 
* События
* $('form-id').data('before_show', function(){})
* $('form-id').data('after_show', function(){})
* $('form-id').data('onclose', function(options){})
*/

var CustomUI = {
    // класс невалидного поля
    invalid_field_class: 'invalid_field',
    hide_on_shadow: false,
    // идентификаторы все открытых окон
    popup_form_opened_forms_id: [],
    popup_form_freezed_id: null,
    wait_url: null,
    wait_interval_id: null,
    wait_form_id: null,
    confirm_answer: null,
    flag_form_installed: 'CustomUI_flag_form_installed',
    shadow_id: '#shadow',
    id_increment: 1,
    lang: {
        close_form: 'Закрыть окно',
        cancel_action: 'Прекратить выполнение действия',
        txt_cancel_action: 'Прекратить выполнение действия',
        action_success: 'Действие успешно выполнено',
        wait_text: 'Пожалуйста подождите...',
        wait_caption: 'Получение данных',
        txt_continue: 'Продолжить',
        yes: 'Да',
        txt_cancel: 'Отмена',
        choose_file: 'Выбрать файл'
    },
    confirm_template: function() {
        return '<form role="alertdialog" class="popup-form animated zoomIn" id="%dynamic_id%" title="" style="display: none;"><div class="popup-form-content"></div><div class="popup-form-footer"><input type="submit" class="btn btn-secondary btn-flat" value=" ' + CustomUI.lang.txt_cancel + ' " title="' + CustomUI.lang.txt_cancel_action + '" onclick="$(this).closest(\'form\').data(\'CustomUI.confirm.answer\', false); CustomUI.hideForm(); return false;" name="cancel" /><input type="submit" value=" ' + CustomUI.lang.yes + ' " class="btn btn-primary" title="' + CustomUI.lang.txt_continue + '" onclick="$(this).closest(\'form\').data(\'CustomUI.confirm.answer\', true); CustomUI.hideForm(); return false;" name="yes" /></div></form>';
    },
    wait_image: '<div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
    close_button: '<img src="/assets/img/popup-close.png" />', // &times;
    createId: function(prefix){
		return (prefix || 'id') + '' + (this.id_increment++);
    },
    addMoreFileFieldEx: function(target, name, max_input_count, class_name, accept) {
        if (max_input_count > 0) {
            if ($(target).find('input[type=file]').length >= max_input_count) {
                // Нельзя загружать больше файлов чем указано
                return false;
            }
        }
        class_name = class_name || '';
        var accept_prop = accept || '';
        if (accept_prop) {
            accept_prop = 'accept="' + accept_prop + '"';
        }
        var f = '<input name="' + name + '" type="file" ' + accept_prop + ' class="gn-file-add ' + class_name + '" /><br />';
        $(target).append(f);
        CustomUI.centerForms();
        var inp = $(target).find('input[type=file]:last');
        inp.data('more_file_field_target', target);
        $(inp).on('change', function (e) {
            var target = $(e.target).data('more_file_field_target');
            if (target) {
                $(e.target).data('more_file_field_target', '');
                CustomUI.addMoreFileFieldEx(target, name, max_input_count, class_name, accept);
            }
            return false;
        });
        inp.styler({
            filePlaceholder: '',
            // Текст кнопки у загрузчика файлов
            fileBrowse: CustomUI.lang.choose_file
        });
        return false;
    },
    round: function precise_round(num, decimals) {
        num = parseFloat(num);
        var t = Math.pow(10, decimals);
        // Math.sign(num) есть не во всех браузерах
        var sign = typeof num === 'number' ? num ? num < 0 ? -1 : 1 : num === num ? 0 : NaN : NaN;
        return (Math.round((num * t) + (decimals > 0 ? 1 : 0) * (sign * (10 / Math.pow(100, decimals)))) / t).toFixed(decimals);
    },
    log: function (message) {
        // $('div#log').append(message + '<br />');
    },
    /**
    * Реализации функции print_r() из PHP
    * @param Array/Hashes/Objects
    * @param {Number} level
    * @returns {String}
    */
    dump: function (arr, level) {
        var dumped_text = '';
		if(!level) level = 0;
        var level_padding = '';
        for (var j = 0; j < level + 1; j++) {
            level_padding += '    ';
        }
        if (typeof (arr) == 'object') { // Array/Hashes/Objects 
            for (var item in arr) {
                var value = arr[item];
                if (typeof (value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += CustomUI.dump(value, level + 1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else {
            dumped_text = '===>' + arr + '<===(' + typeof (arr) + ')';
        }
        return dumped_text;
    },
    setHandlers: function(parent_selector){
    	
    	if(typeof parent_selector == 'undefined') {
			parent_selector = 'body';
    	}
    	var $parent = $(parent_selector);

	    this.setButtonHandlers();

	    /**
	    * перетаскивание форм за заголовок
	    */
        if(typeof $.fn.draggable !== 'undefined') {
	        $parent.find('.popup-form').draggable({
	            handle: '.popup-form-toolbox',
	            containment: '#shadow',
	            scroll: false
	        });
        }

	    $parent.find('.popup-ajax').customSubmit({
	        json: true
	    });

		/**
		* "Быстроссылка" из любого тега (например TR)
		*/
	    $parent.find('[data-href]').unbind('mousedown').on('mousedown', function (e) {
	        if (e.target.tagName != 'A' && e.target.tagName != 'I') {
                var url = $(this).data('href');
                if(typeof url == 'string') {
                    if(url.length > 0 && url != '#') {
                        if(e.button === 0) {
	                        location.href = url;
                            return false;
                        } else if(e.button === 1) {
                            var win = window.open(url, '_blank');
                            win.focus();
                            return false;
                        }
                    }
                }
	        }
	    });

		/**
		* Обработка элементов с атрибутом data-sort
		*/
	    $parent.find('[data-sort]').unbind('click').click(function () {
	        var dir = $(this).data('sort-dir');
	        if (dir == 'asc') {
	            dir = 'desc';
	        } else {
	            dir = 'asc';
	        }
	        var field = $(this).attr('data-sort');
	        $(this).closest('form').find('.sort_field').val(field);
	        $(this).closest('form').find('.sort_dir').val(dir);
	        $(this).closest('form').submit();
	    });

		/**
		* Обработка элементов с атрибутом data-ajax
		*/
	    $parent.find('[data-ajax]').unbind('click').click(function () {
	        var params = $(this).attr('data-ajax');
	        // результат
	        try {
	            var options = eval('(' + params + ')');
	            // default configuration properties
	            var defaults = {
	                url: '',
	                type: 'post',
	                refresh: false,
	                callback: false,
	                go: ''
	            };
	            var options = $.extend(defaults, options);
	            CustomUI.showForm('form-wait', false, true);
	            $.post(options.url, function (data) {
	                // результат
	                try {
	                    var obj = eval('(' + data + ')');
	                    if (options.callback) {
	                        CustomUI.hideForm();
	                        var callback = eval(options.callback);
	                        callback.call(obj);
	                        return false;
	                    }
	                    if (obj.status == 'success' && (options.refresh || options.go)) {
	                        window.location = options.go;
	                    } else {
	                        CustomUI.hideForm();
	                        alert(obj.message);
	                    }
	                }
	                catch (e) {
	                    CustomUI.hideForm();
	                    alert(e);
	                }
	            });
	        }
	        catch (e) {
	            alert(e);
	        }
	        return false;
	    });

    },
	/**
	* Нажатие кнопки на панели инструментов диалоговых окон
	*/
    setButtonHandlers: function () {
        /*$('.popup-form-button, .button-submit').unbind('click').click(function () {
            if (!$(this).hasClass('disabled')) {
                $(this).closest('form').submit();
            }
            return false;
        });*/
        $('.popup-form-button, .button-simple').unbind('click').click(function () {
            var dis = $(this).attr('disabled');
            if (dis != 'disabled') {
                // id - операция
                var id = $(this).attr('id');
                try {
                    var form = $('#form-' + id);
                    var func = form.data('before_show');
                    var ns = true;
                    if (func instanceof Function) {
                        var oc = form.data('_opencounter');
                        if (typeof oc == 'undefined') {
                            oc = 0;
                        } else {
                            oc++;
                        }
                        form.data('_opencounter', oc);
                        ns = func(this, oc);
                    }
                    if (ns) {
                        CustomUI.showForm('form-' + id, true, true, {});
                    }
                    return false;
                }
                catch (e) {
                    alert('Ошибка: ' + e)
                };
            }
            return false;
        });
        return false;
    },
    /**
    * Центрирование всех форм по центру сайта
    */
    centerForms: function() {
        /*for(i = 0; i < CustomUI.popup_form_opened_forms_id.length; i++) {
            var hide_id = CustomUI.popup_form_opened_forms_id[i];
            if($('#' + hide_id).is(':visible')) {
                $('#' + hide_id).css({left: function () {
                    return $(window).width() * 0.5 - $(this).outerWidth() * 0.5;
                }, top: function() {
                    return $(window).height() * 0.5 - $(this).outerHeight() * 0.5;
                }});
            }
        }
        */
        return false;
    },
	/**
	* Открытие формы
	*/
    showForm: function(id, can_close, apply_class, options) {
        var form = $('#' + id);
        form.addClass('active');
        CustomUI.can_close = can_close;
        CustomUI.log('showForm(\'#' + id + '\');');
        // Overlay
        var overlay = form.data('CustomUI.overlay');
        if(overlay) {
            overlay.show();
        } else {
            var overlay = {
                id: 'popup-overlay-' + id,
                options: options,
                show: function(){
                    $('#' + this.id).addClass('active');
                },
                hide: function(){
                    $('#' + this.id).removeClass('active');
                },
                create: function() {
                    var that = this;
                    var ov = $('<section id="' + this.id + '" class="popup-overlay"></section>');
                    ov.on('click', function(e){
                        if (e.target !== this) {
                            return;
                        }
                        if(that.options && that.options.clickonshadow) {
                            if (that.options.clickonshadow instanceof Function) {
                                that.options.clickonshadow(e, that);
                            }                
                        }
                    });
                    form
                        .data('CustomUI.overlay', this)
                        .wrap(ov);
                    this.show();
                }
            };
            overlay.create();
        }
        if ((typeof apply_class == 'undefined') || apply_class) {
            var no_cap = form.hasClass('popup-nocaption');
            if (!no_cap) {
                var title = form.attr('title');
				if(title == undefined) {title = form.data('title');}
                if(title == undefined) {title = '';}
                form.data('title', title);
                if (form.find('.popup-form-toolbox').length < 1) {
                    form.removeAttr('title').data('title', title);
                    if(!form.hasClass('popup-form-notoolbox')) {
                        var buttons = '';
                        if (can_close == null || can_close === true) {
                            buttons += '<a title="' + CustomUI.lang.close_form + '" href="#" onclick="return CustomUI.hideForm();" class="popup-close-button popup-toolbox-button">' + CustomUI.close_button + '</a>';
                        }
                        form.prepend('<div class="popup-form-toolbox"><table width="100%"><tr><td align="left"><h2 id="' + id + '-title">' + title + '</h2></td><td align="right">' + buttons + '</td></tr></table></div>');
                    }
                } else {
                    var h2 = form.find('.popup-form-toolbox h2');
                    if (h2 != 'undefined') {
                        h2.empty().append(title);
                    }
                }
            }
        }
        form.hide();
        setTimeout(function(){
            form.show();
        }, 1);
        /*
        // center form on form resize
        if(form.find('.resizer-frame').length < 1){
            form.prepend('<iframe class="resizer-frame" style="width:100%; height:100%; position:absolute; z-index:-1; border:0;" ></iframe>');
            var iframe = form.find('.resizer-frame')[0];
            if(iframe){
                var iframewindow= iframe.contentWindow? iframe.contentWindow : iframe.contentDocument.defaultView;
                $(iframewindow).on('resize', function(){CustomUI.centerForms();});
            }
        }*/
        /*
        form.css({margin: 0});
        var w = form.outerWidth();
        var h = form.outerHeight();
        var x = $(window).width() / 2 - w / 2;
        var y = $(window).height() / 2 - h / 2;
		*/
        CustomUI.hideForm();
        if(options) {
            form.data('options', options);
            if (options.wait_url) {
                CustomUI.wait_url = options.wait_url;
                if (CustomUI.wait_interval_id) {
                    clearInterval(CustomUI.wait_interval_id);
                }
                CustomUI.wait_interval_id = setInterval(function () {
                    $.getJSON(CustomUI.wait_url, function (data) {
                        var percent = ((data.value / data.total) * 100).toFixed(2);
                        var progress_bar = '<div class="progress"><div class="bar" style="width: ' + percent + '%;"></div></div>';
                        var message = data.message || options.wait_message;
                        var text = '<br>' + progress_bar + message;
                        $('#popup-form-wait_message').html(text);
                    });
                }, 500);
            }
        }
        CustomUI.popup_form_opened_forms_id.push(id);
		/*
        CustomUI.showShadow();
        form.data('shadow', $(CustomUI.shadow_id))
                .css({position: 'fixed', left: x, top: y, 'z-index': 3000})
                .show();
        var content_h = $(window).height() - 150;
        form.find('.popup-form-content').css({'max-height': content_h});
		*/
        // автофокус на первом элементе управления
        if (form.find('[data-first-focus]').length > 0) {
            form.find('[data-first-focus]').eq(0).focus();
        } else {
            if (form.find('input[type=text]:visible').length > 0) {
                form.find('input[type=text]:visible:first').focus();
            } else if (form.find('input[type=password]:visible').length > 0) {
                form.find('input[type=password]:visible:first').focus();
            } else {
                if (form.find('textarea:visible:first').length > 0) {
                    form.find('textarea:visible:first').focus();
                } else {
                    form.find('select:visible:first').focus();
                }
            }
        }
        // "колбэк" функция сразу после показа окна
        callback = form.data('after_show');
        if (callback instanceof Function) {
            callback(this);
        }
        $('body').addClass('noscroll');
        return CustomUI.centerForms();
    },
    
    showShadow: function () {
        $(CustomUI.shadow_id).show();
    },
    
    hideShadow: function () {
        $(CustomUI.shadow_id).hide();
    },

	/**
	* Закрытие всех открытых форм с генерацией события "onclose"
	* @since 2011-10-12
	*/
    hideForm: function(dontUsedVariable, dont_hide) {
        $('body').removeClass('noscroll');
        // preloader.off();
        dont_hide = (typeof dont_hide == 'boolean') ? dont_hide : false;
        var id_list = CustomUI.popup_form_opened_forms_id.slice(0);
        CustomUI.popup_form_opened_forms_id = [];
        while (hide_id = id_list.pop()) {
            var form = $('#' + hide_id);
            if (form.is(':visible')) {
                var options = form.data('options');
                var defaults = {
                    can_close: true,
                    onclose: false
                };
                var options = $.extend(defaults, options);
                if (options.can_close) {
                    CustomUI.hideShadow();
                    if(!dont_hide) {
                        form.removeClass('active');
                        var overlay = form.data('CustomUI.overlay');
                        if(overlay) {
                            overlay.hide();
                        }
                    }
                }
                var onclose = options['onclose'];
                if (onclose instanceof Function) {
                    var params = options['params'];
                    onclose(params);
                } else {
                    onclose = form.data('onclose');
                    if (onclose instanceof Function) {
                        var params = options['params'];
                        onclose(params);
                    }
                }
            }
        }
        return CustomUI.setButtonHandlers();
    },
	/**
	* Показ формы с сообщением
	* 
	* string message Текст сообщения
	* string title Заголовок окна
	* string icon стиль окна(info,error)
	* string options Массив с опциями окна (например событие "onclose")
	*/
    showMessage: function (message, title, icon, options) {
        if (icon == 'error') {
            if(typeof gmaback != 'undefined') {
            gmaback.js.showError(message, 4000);
            return false;
        }
        }
        CustomUI.log('showMessage(\'' + $('<div/>').text(message).html() + '\');');
        if (typeof title == 'undefined') {
            title = '';
        }
        title = title.replace(/<\/?[^>]+(>|$)/g, '');
        $('#form-msg-popup-window').attr('title', title);
        $('#form-msg-popup-window')
                .removeClass('popup-window-error')
                .removeClass('popup-window-info')
                .removeClass('popup-window-wait');

        if (icon == 'info') {
            $('#form-msg-popup-window').addClass('popup-window-info');
        }
        if (icon == 'error') {
            $('#form-msg-popup-window').addClass('popup-window-error');
        }
        if (icon == 'wait') {
            $('#form-msg-popup-window').addClass('popup-window-wait');
        }
        $('#form-msg-popup-window .popup-form-content').html(message);
        return CustomUI.showForm('form-msg-popup-window', true, true, options);
    },
    showWait: function (text, caption, wait_url) {
        CustomUI.log('showWait();');
        if(typeof caption == 'undefined') {
            caption = '';
        }
        caption = caption.replace(/<\/?[^>]+(>|$)/g, '');
        //if(CustomUI.popup_form_opened_forms_id.length < 1) {
        text = text ? text : 'Пожалуйста подождите...';
        caption = caption ? caption : 'Получение данных';
        return CustomUI.showMessage('<div style="text-align: center;" id="popup-form-wait_message">' + this.wait_image + '<br />' + text + '</div>', caption, 'wait', {wait_url: wait_url});
    },

    showConfirm: function(text, caption, ok, cancel) {
        var form_id = this.createId('confirm');
        var object = this;
        var $form = $(CustomUI.confirm_template()).clone();
        $form.attr('id', form_id);
        $form.attr('title', caption);
        $form.find('.popup-form-content').html(text);
        $form.data('CustomUI.confirm.ok', ok);
        $form.data('CustomUI.confirm.cancel', cancel);
        $form.data(
            'options', {
                onclose: function(e){
                    if($form.data('CustomUI.confirm.answer')) {
                        var f = $form.data('CustomUI.confirm.ok');
                        if(f instanceof Function) {
                            f();
                        }
                    } else {
                        var f = $form.data('CustomUI.confirm.cancel');
                        if(f instanceof Function) {
                            f();
                        }
                    }
                }
            }
        );
        $('body').prepend($form);
        CustomUI.showForm(form_id);
        return form_id;
    },

    showCustom: function(form_id, ok, cancel, options) {
        var object = this;
        var $form = $('#' + form_id);
        if(!$form.hasClass('active')) {
        	$form.addClass('active');
		}
        $form.data('CustomUI.confirm.ok', ok);
        $form.data('CustomUI.confirm.cancel', cancel);
        $form.data(
            'options', {
                onclose: function(e){
			        if($form.hasClass('active')) {
        				$form.removeClass('active');
					}
                    if($form.data('CustomUI.confirm.answer')) {
                        var f = $form.data('CustomUI.confirm.ok');
                        if(f instanceof Function) {
                        	f();
						}
                    }
                    else {
                        var f = $form.data('CustomUI.confirm.cancel');
                        if(f instanceof Function) {
                        	f();
						}
                    }
                }
            }
        );
        CustomUI.showForm(form_id, true, false, options);
        return form_id;
    }

}

$(function($) {

    /**
    * добавление затемнения (фон для окон)
    */
    $('body').prepend('<div id="shadow" style="display:none;"></div>');

    $('#shadow').on('click', function(){
        if(CustomUI.hide_on_shadow) {
            CustomUI.hideForm();
        }
    });

    /**
    * добавление окна сообщений
    */
    $('body').prepend('<form class="popup-form animated zoomIn" id="form-msg-popup-window" title="" style="display: none;"><div class="popup-form-content"></div></form>');
    $('body').prepend('<form class="popup-form" id="form-msg-popup-confirm" title="" style="display: none;"><div class="popup-form-content"></div><div class="popup-form-footer"><input type="submit" value=" Да " class="btn btn-primary" title="Продолжить" onclick="CustomUI.confirm_answer = true; CustomUI.hideForm(); return false;" name="yes" /> <input type="submit" class="btn btn-default" value=" Отмена " title="Прекратить выполнение действия" onclick="CustomUI.confirm_answer = false; CustomUI.hideForm(); return false;" name="cancel" /></div></form>');
    
    $(document).on('blur, change', '.' + CustomUI.invalid_field_class, function (e) {
        $(this).removeClass(CustomUI.invalid_field_class).removeClass('invalid');
        $(this).validationEngine('hide');
        if($(this).is('.select2')) {
            var el = $(this).closest('div.select2').find('input.select-dropdown');
            el.removeClass(CustomUI.invalid_field_class).removeClass('invalid');
            el.validationEngine('hide');
        }
    });

	/**
	* Закрытие всех окон по нажатию на клавишу [Esc]
	*/
    $(document).on('keyup', function(event) {
        try {
            if (event.keyCode == 27 && CustomUI.can_close !== false) { // 27 is keycode for [Esc] button on keyboard
                // debugger;
                if($('.popup-form.active').length > 0) {
                    CustomUI.hideForm();
                    event.preventDefault();
                    event.stopImmediatePropagation();
                }
            }
        } catch (e) {
            // do nothing
        }
    });
    
	/**
	* Центрование открытых/видимых форм при ресайзе окна браузера
	*/
	$(window).resize(function() {
	    CustomUI.centerForms();
	});

    $.fn.customFileInput = function (options) {
        // default configuration properties
        var defaults = {
            name: false,
            max_count: 5,
            class_name: '',
            accept: ''
        };
        var options = $.extend(defaults, options);
        this.each(function () {
            CustomUI.addMoreFileFieldEx(this, options.name, options.max_count, options.class_name, options.accept);
        });
    };

    $.fn.customSubmit = function(in_options) {

        // default configuration properties
        var defaults = {
            success: false,
            onclose: false,
            error: false,
            caption: '',
            show_wait: true,
            confirm: false,
            ajax: true,
            json: false,
            hideform_onerror: true,
            validate_function: false,
            wait_message: 'Пожалуйста, подождите...',
            wait_url: ''
        };
        this.each(function () {
            var options = $.extend(defaults, in_options);
            // check if listeneer already installed
            if(typeof $(this).data(CustomUI.flag_form_installed) == 'undefined') {
                $(this).data(CustomUI.flag_form_installed, true);
            } else {
                var id = $(this).attr('id');
                return false;
                // return alert('CustomUI.customSubmit() already installed for\nform #' + id);
            }
            $(this).find('.popup-form-button, .button-submit').unbind('click').click(function () {
                if(!$(this).hasClass('disabled')) {
                    $(this).closest('form').submit();
                }
                return false;
            });
            // не вызывать CustomUI.hideForm при ошибке
            if(typeof $(this).data('hideform_onerror') != 'undefined') {
                options.hideform_onerror = $(this).data('hideform_onerror');
            }
            // Clone object
            $(this).data('_options', jQuery.extend(true, {}, options));
            $(this).submit(
                function() {
                    $(this).find('.button-submit').addClass('disabled'); // .prop('disabled', 'disabled');
                    // Fix TinyMCE bug ("before ajax.submit()") http://blog.pavelb.ru/2012/07/tinymce-jquery-form-plugin.html
                    if (typeof tinymce === 'object') {
                        tinymce.triggerSave();
                    }
                    var options = $(this).data('_options');
                    options.target_form = $(this).attr('id');
                    try {
                        var freezed_form_id = $(this).hasClass('popup-form') ? $(this).attr('id') : false;
                        // show_wait
                        var sw = $(this).data('show_wait');
                        options.show_wait = (typeof sw != 'undefined') ? sw : options.show_wait;
                        // caption
                        options.caption = options.caption ? options.caption : $(this).data('title');
                        if(!options.caption) {
                            options.caption = $(this).attr('title');
                        }
                        // onclose
                        options.onclose = options.onclose ? options.onclose : $(this).data('onclose');
                        if(!options.onclose) {
                            options.onclose = defaults.onclose;
                        }
                        // success
                        options.success = options.success ? options.success : $(this).data('success');
                        if(!options.success) {
                            options.success = defaults.success;
                        }
                        // error
                        options.error = options.error ? options.error : $(this).data('error');
                        if(!options.error) {
                            options.error = defaults.error;
                        }
                        // confirm
                        options.confirm = options.confirm ? options.confirm : $(this).data('confirm');
                        if(!options.confirm) {
                            options.confirm = defaults.confirm;
                        }
                        // validate function
                        options.validate_function = options.validate_function ? options.validate_function : $(this).data('validate_function');
                        if(!options.validate_function) {
                            options.validate_function = defaults.validate_function;
                        }
                        $(this).data('options', options);
                        if(options.confirm) {
                            if (CustomUI.confirm_answer == null) {
                                var object = this;
                                $('#form-msg-popup-confirm .popup-form-content').html(options.confirm);
                                $('#form-msg-popup-confirm').attr('title', options.caption);
                                $('#form-msg-popup-confirm').data('options',
                                	{
	                                    onclose: function (e) {
	                                        if (CustomUI.confirm_answer) {
	                                            $(object).submit();
	                                        }
	                                        else {
	                                            CustomUI.confirm_answer = null;
	                                            return false;
	                                        }
	                                    }
                                    }
                                );
                                return CustomUI.showForm('form-msg-popup-confirm', false, true);
                            }
                            if(!CustomUI.confirm_answer) {
                                CustomUI.confirm_answer = null;
                                return false;
                            }
                            CustomUI.confirm_answer = null;
                        }
                        if (options.validate_function instanceof Function) {
                            if(!options.validate_function(this)) {
                                $('#' + options.target_form).find('.button-submit').removeClass('disabled');
                                return false;
                            }
                        }
                        if(options.ajax) {
                            // вывод информирующей формы с предложением дождаться серверной обработки формы
                            if(options.show_wait instanceof Function) {
                                options.show_wait(options);
                            } else if (options.show_wait) {
                                CustomUI.showWait(options.wait_message, options.caption, options.wait_url);
                            }
                            var op = {
                                // успешный HTTP ответ (статус 200)
                                success: function (data) {
                                    if(options.json) {
                                        // если в ответ должен прийти json объект
                                        var obj;
                                        try {
                                            // раскодируем сериализованный объект
                                            if (data instanceof Object) {
                                                obj = data;
                                            } else {
                                                obj = eval('(' + data + ')');
                                            }
                                            // на сервере произошла ошибка
                                            if(obj.status == 'redirect') {
                                                gmaback.js.showMessage(obj.message, 40000);
                                                location.href = obj.uri;
                                            } else if (obj.status != 'success') {
                                                // подсвечивание "плохо заполненных" полей
                                                if(obj.error_field_list instanceof Object) {
                                                    CustomUI.hideForm();
                                                    if (freezed_form_id) {
                                                        CustomUI.showForm(freezed_form_id, true, true, {});
                                                    }
                                                    var scroll = 0;
                                                    var has_scwizard = false;
                                                    var min_index_scwizard = 0;
                                                    var cur_index_scwizard = 0;
                                                    if($('#' + options.target_form).find('div.scwizard').length) {
                                                        has_scwizard = true; 
                                                        min_index_scwizard = $('#' + options.target_form).find('div[data-scwizard-page]').length;
                                                    }
                                                    // $('#' + options.target_form).validationEngine('hideAll');
                                                    $.each(obj.error_field_list, function (key, value) {
                                                        var field = $('#' + options.target_form).find('[name^="form[' + key + ']"]');
                                                        if(!field.length) return true;
                                                        field.removeClass(CustomUI.invalid_field_class)
                                                             .addClass(CustomUI.invalid_field_class);
                                                        if(field.hasClass('select2-hidden-accessible')) {
                                                            var field_id = field.attr('id');
                                                            field = field.closest('div').find('.select2-container');
                                                            field = field.attr('id', 'div_'+field_id);
                                                        }else if(field.hasClass('select2')) {
                                                            field = field.closest('div').find('.select-dropdown');
                                                        }
                                                        field.removeClass('invalid')
                                                             .addClass('invalid');
                                                        field.validationEngine('showPrompt', value ? value : 'Необходимо заполнить', 'error', 'bottomLeft', false);
                                                        if(scroll == 0) {
                                                            scroll = field.offset().top;
                                                        }
                                                        if(has_scwizard) {
                                                            if(field.closest('div[data-scwizard-page]').length) {
                                                                cur_index_scwizard = parseInt(field.closest('div[data-scwizard-page]').attr('data-scwizard-page'));
                                                                if(cur_index_scwizard < min_index_scwizard) {
                                                                    min_index_scwizard = cur_index_scwizard;
                                                                }
                                                            }
                                                        }
                                                    });
                                                    $('#' + options.target_form).find('.button-submit').removeClass('disabled');
                                                    if(scroll != 0) {
                                                        $('html,body').animate({scrollTop: scroll}, 600);
                                                        $(window).scrollTop(scroll);
                                                    }
                                                    if(has_scwizard) {
                                                        $('#' + options.target_form).find('div.scwizard').scWizard('showPage', min_index_scwizard);
                                                    }
                                                    return;
                                                } else {
                                                    throw obj.message;
                                                }
                                            }
                                            // если есть callback-функция для обработки статуса "успех"
                                            if(options.success instanceof Function) {
                                                // AngularJS
                                                var $scope = $('#' + options.target_form).data('Angular_scope');
                                                if(typeof $scope == 'undefined') {
                                                    options.success(obj ? obj : data, $scope);
                                                } else {
                                                    $scope.$apply(function(){
                                                        options.success(obj ? obj : data, $scope);
                                                    });                                                    
                                                }
                                            } else {
                                                // если в ответе отсутствует сообщение, присваиваем стандартное
                                                if(!obj.message) {
                                                    obj.message = CustomUI.lang.action_success;
                                                }
                                                // вывод информирующей формы
                                                CustomUI.hideForm();
                                                gmaback.js.showMessage(obj.message, 4000);
                                                // CustomUI.showMessage(obj.message, options.caption, 'info', {onclose: options.success, params: data});
                                            }
                                        } catch(e) {
                                            // preloader.off();
                                            $('#' + options.target_form).find('.button-submit').removeClass('disabled');
                                            // если есть callback-функция для обработки статуса "ошибка"
                                            if(options.error instanceof Function) {
                                                // AngularJS
                                                var $scope = $('#' + options.target_form).data('Angular_scope');
                                                if(typeof $scope == 'undefined') {
                                                    return options.error(obj ? obj : data, null);
                                                } else {
                                                    $scope.$apply(function(){
                                                        options.error(obj ? obj : data, $scope);
                                                    });                                                    
                                                }
                                            } else {
                                                // вывод информирующей формы
                                                if ((typeof obj.code != 'undefined') && (obj.code == 211)) {
                                                    location.href = '/index/logout/';
                                                    return false;
                                                }
                                                // if(!$('#' + options.target_form).hasClass('popup-form')) {
                                                if(options.hideform_onerror) {
                                                    CustomUI.hideForm();
                                                }
                                                if(typeof e == 'undefined') {
                                                    gmaback.js.showError(e, 4000);
                                                } else {
                                                    return CustomUI.showMessage(e, options.caption, 'error', {onclose: function(data) {
                                                    if(freezed_form_id && $('#'+freezed_form_id+' .popup-form-content').length) {
                                                    return CustomUI.showForm(freezed_form_id, true, true, {});
                                                    }
                                                    }, params: data});                                        
                                                }
                                                // gmaback.js.showError(e, 4000);
												/*
												    return CustomUI.showMessage(e, options.caption, 'error', {onclose: function(data) {
												    if(freezed_form_id && $('#'+freezed_form_id+' .popup-form-content').length) {
												    return CustomUI.showForm(freezed_form_id, true, true, {});
												    }
												    }, params: data});
												*/
                                            }
                                        }
                                    } else {
                                        CustomUI.hideForm();
                                        // если есть callback-функция для обработки статуса "успех"
                                        if(options.success instanceof Function) {
                                            options.success(data);
                                        }
                                    }
                                },
                                headers: {
                                    'HTTP_X_REQUESTED_WITH': 'xmlhttprequest',
                                    'X-Requested-With': 'xmlhttprequest'
                                },
                                // ошибочный HTTP ответ
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log('error');
                                    $('#' + options.target_form).find('.button-submit').removeClass('disabled');
                                    var e = 'Ошибка связи с сервером №' + jqXHR.status + ': ' + jqXHR.statusText;
                                    return CustomUI.showMessage(e, options.caption, 'error', {onclose: function (data) {
                                            if (freezed_form_id && $('#' + freezed_form_id + ' .popup-form-content').length) {
                                                return CustomUI.showForm(freezed_form_id, true, true, {});
                                            }
                                        }, params: false});
                                },
                                beforeSend: function (xhr, a) {
                                    xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest');
                                    xhr.setRequestHeader('HTTP_X_REQUESTED_WITH', 'xmlhttprequest');
                                }
                            };
                            // отправка формы на сервер
                            $(this).ajaxSubmit(op);
                            return false;
                        } else {
                            CustomUI.hideForm();
                            return true;
                        }
                    } catch(e) {
                        alert('eee: ' + e);
                        return false;
                    }
                }
            );
        });
    };

    CustomUI.setHandlers();

});
