'use strict';

var stub = {
    ms: {},
    getItem: function(key) {
        return key in this.ms ? this.ms[key] : null;
    },
    setItem: function(key, value) {
        this.ms[key] = value;
        return true;
    },
    removeItem: function(key) {
        var found = key in this.ms;
        if (found) {
            return delete this.ms[key];
        }
        return false;
    },
    clear: function() {
        this.ms = {};
        return true;
    }
};

var tracking = {
    listeners: {},
    listening: false,
    listen: function() {
        if (window.addEventListener) {
            window.addEventListener('storage', this.change, false);
        } else if (window.attachEvent) {
            window.attachEvent('onstorage', this.change);
        } else {
            window.onstorage = this.change;
        }
    },
    change: function(e) {
        if (!e) {
            e = window.event;
        }
        var all = window.tracking.listeners[e.key];
        if (all) {
            all.forEach(fire);
        }
        function fire(listener) {
            listener(JSON.parse(e.newValue), JSON.parse(e.oldValue), e.url || e.uri);
        }
    },
    on: function on (key, fn) {
        if (window.tracking.listeners[key]) {
            window.tracking.listeners[key].push(fn);
        } else {
            window.tracking.listeners[key] = [fn];
        }
        if (window.tracking.listening === false) {
            window.tracking.listen();
        }
    },
    off: function(key, fn) {
        var ns = this.listeners[key];
        if (ns.length > 1) {
            ns.splice(ns.indexOf(fn), 1);
        } else {
            this.listeners[key] = [];
        }
    }
};

var lstor = {
    ls : 'localStorage' in window && window.localStorage ? window.localStorage : stub,
    on: tracking.on,
    off: tracking.off,
    get: function(key) {
        return JSON.parse(this.ls.getItem(key));
    },
    set: function(key, value) {
        try {
            this.ls.setItem(key, JSON.stringify(value));
            return true;
        } catch (e) {
            return false;
        }
    },
    remove: function(key) {
        return this.ls.removeItem(key);
    },
    clear: function() {
        return this.ls.clear();
    }
};
