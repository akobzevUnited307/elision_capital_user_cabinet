// Anti XSRF
$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    jqXHR.setRequestHeader('X-Xsrftoken', getApp().XSRF);
});

// Chat
var prev_new_messages = {};
var update_events_in_process = false;

// Event sounds
var snd = {
    new_message_in_other_chat: new Audio('/assets/sound/new_message.mp3')
};
var need_sound_play = false;

$(function () {
    setInterval('updateevents()', 10000);
    updateevents();
    /*
        $('#chat_message_write').on('keydown', function (event) {
            if (event.keyCode == 13) { // 13 is keycode for [Enter] button on keyboard
                event.preventDefault();
                return false;
            }
        });
        $('#chat_message_write').on('keyup', function (event) {
            try {
                if (event.keyCode == 13) { // 13 is keycode for [Enter] button on keyboard
                    $(this).closest('form').submit();
                    event.preventDefault();
                }
            } catch (e) {
                // do nothing
            }
        });
    */
});

// подгрузить новые сообщения
function updateevents() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/admin/index/getupdates/',
        data: {
            session: str_session_id
        },
        success: function (data) {
            if (data.status == 'success') {
                var has_new_messages = false;
                $.each(data.data.event_list, function (key, value) {
                    var need = true;
                    if (key in prev_new_messages) {
                        if (prev_new_messages[key] >= value) {
                            need = false;
                        }
                    }
                    if (need) {
                        prev_new_messages[key] = value;
                        if (value > 0) {
                            // var $new = $('#menu-cabinet-main [data-event-selector=' + key + '] span.new');
                            var $new = $('[data-event-selector=' + key + '] span.new');
                            if ($new.length) {
                                if (need_sound_play) {
                                    $new.text(value).stop().fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
                                } else {
                                    $new.text(value).stop().show();
                                }
                                has_new_messages = true;
                            }
                        }
                    }
                });
                if (has_new_messages) {
                    if (need_sound_play) {
                        try {
                            snd.new_message_in_other_chat.play();
                        } catch (er) {
                            // do nothing
                        }
                    }
                }
                need_sound_play = true;
            } else if (data.status == 'redirect') {
                gmaback.js.showMessage(data.message, 2000, '', function (e) {
                    location.href = data.uri;
                });
            }
        }
    });
}