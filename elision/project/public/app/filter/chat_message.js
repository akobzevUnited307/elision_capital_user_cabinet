'use strict';

define(['app', 'service/utils'], function(app, utilsSrvc) {

    var injectParams = ['$sce', 'utilsSrvc'];
    var directive = function($sce, utilsSrvc) {
        return function(inputMD, highlightCode, smiles){
            var resultHTML = inputMD;
			if(highlightCode !== false) {
            	resultHTML = inputMD; // utilsSrvc.markdown.makeHTML(inputMD);
			} else {
                resultHTML = resultHTML
                    .replace(/&/g, "&amp;")
                    .replace(/</g, "&lt;")
                    .replace(/>/g, "&gt;")
                    .replace(/"/g, "&quot;")
                    .replace(/'/g, "&#039;");
            }
            /*(gmaback.emoji).forEach(function(v, k){
                var search = v.utf;
                var replacement = Emoji.getEmojiHTML(v.code, v.utf);
                htmlCode = htmlCode.split(search).join(replacement);
            });*/
            if(smiles) {
                for(var smile of smiles.list) {
                    var img = '<img align="absmiddle" class="smile" alt="' + smile.code + '" src="' + smile.path + '">';
                    resultHTML = resultHTML.replace(':' + smile.code + ':', img);
                }
            }
            return $sce.trustAsHtml(resultHTML);
        }
    };
    
    // Filter #1
    directive.$inject = injectParams;
    app.filter('chat_message', directive);

    // Filter #2
    var injectParams = ['$sce', 'utilsSrvc'];
    var html_code = function($sce, utilsSrvc) {
		return function(htmlCode){
			return $sce.trustAsHtml(htmlCode);
		}
    };
    html_code.$inject = injectParams;
    app.filter('html_code', html_code);

});