'use strict';
define(['app'], function(app) {
    var injectParams = [];
    var directive = function() {
		return function(date){
			var dt = new Date(date);
			if(isNaN(dt.valueOf())) {
                dt = new Date(date.replace(/-/g,"/"));
            }
			return date.toLocaleString(getApp().language.locale, {
				year: 'numeric',
				month: 'long',
				day: 'numeric'
			});
		}
    };
    directive.$inject = injectParams;
    app.filter('date_short', directive);
});