'use strict';

define(['app', 'service/utils'], function(app, utilsSrvc) {

    var injectParams = ['$sce', 'utilsSrvc'];
    var directive = function($sce, utilsSrvc) {
        return function(text, q){
            var i = text.toLowerCase().indexOf(q.toLowerCase());
            if(i >= 0) {
                var len = q.length;
                var html = text.substring(0, i) + '<strong>' + text.substring(i, i + len) + '</strong>' + text.substring(i + len, text.length);
                return $sce.trustAsHtml(html);
            }
            return $sce.trustAsHtml(text);
        }
    };

    directive.$inject = injectParams;
    app.filter('highliteSearch', directive);

});