'use strict';

define(['app', 'service/utils'], function(app, utilsSrvc) {

    var injectParams = ['$sce', 'utilsSrvc'];
    
    /**
    * Returns the text from a HTML string
    */
    var strip_tags = function($sce, utilsSrvc) {
        return function(inputHTML) {
            // Create a new div element
            var temporalDivElement = document.createElement('div');
            // Set the HTML content with the providen
            temporalDivElement.innerHTML = inputHTML;
            // Retrieve the text property of the element (cross-browser support)
            return temporalDivElement.textContent || temporalDivElement.innerText || '';
            // return $sce.trustAsHtml(resultHTML);
        }
    };
    
    // Filter #1
    strip_tags.$inject = injectParams;
    app.filter('strip_tags', strip_tags);

});