var gmaback;
var win;
var BrowserWindow;
var electron;

function isDesktop() {
    return (typeof process != 'undefined');
}

function isDev() {
    return isDesktop() ? (process.mainModule.filename.indexOf('app.asar') === -1) : (location.host.indexOf('.loc') >= 0);
}

/**
* DomainList
*/
var _domains = {
    web: 'http://dax.loc',
    static: 'http://dax.loc',
    api: 'http://dax.loc',
    avatar: 'http://dax.loc',
    daemon: 'ws://dax.loc:5559/ws/ws'
};

if (!isDev()) {
    if(location.hostname.indexOf('test.') == 0) {
        _domains.web = 'https://test.trustera.global';
        _domains.static = 'https://test.trustera.global';
        _domains.api = 'https://test.trustera.global';
        _domains.avatar = 'https://test.trustera.global';
        _domains.sticker = 'https://test.trustera.global';
        _domains.daemon = 'wss://test.trustera.global/ws/ws';
    } else {
        _domains.web = 'https://trustera.global';
        _domains.static = 'https://trustera.global';
        _domains.api = 'https://trustera.global';
        _domains.avatar = 'https://trustera.global';
        _domains.sticker = 'https://trustera.global';
        _domains.daemon = 'wss://trustera.global/ws/ws';
    }
}


window.Clipboard = (function(window, document, navigator) {
    var textArea, copy;
    function isOS() {
        return navigator.userAgent.match(/ipad|iphone/i);
    }
    function createTextArea(text) {
        textArea = document.createElement('textArea');
        textArea.value = text;
        document.body.appendChild(textArea);
    }
    function selectText() {
        var range, selection;
        if (isOS()) {
            range = document.createRange();
            range.selectNodeContents(textArea);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
            textArea.setSelectionRange(0, 999999);
        } else {
            textArea.select();
        }
    }
    function copyToClipboard() {        
        document.execCommand('copy');
        document.body.removeChild(textArea);
    }
    copy = function(text) {
        createTextArea(text);
        selectText();
        copyToClipboard();
    };
    return {
        copy: copy
    };
})(window, document, navigator);

function copyAddressToCB(id) {
    var copyText = document.getElementById(id);
    Clipboard.copy(copyText.value);
    gmaback.js.showMessage(getApp().lang.copied, 4000);
    return false;
}

/**
 * Application
 */
var $currentApp = {
    id: Routines.guid(),
    BTC_SATOCHI: 100000000,
    project_uuid: 'c0a0ffba-0a9c-4d63-89e2-ea8bb7356b28',
    is_desktop: isDesktop(),
    is_developer_host: isDev(),
    charts: function (a, b, c) {
        return getCharts(a);
    },
    domains: _domains,
    lang: {},
    language: {
        code: 'ru',
        locale: 'ru'
    },
    common_settings: {},
    // session
    is_logged: false,
    user_login: '',
    user: {}, // localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {},
    session_id: '', // localStorage.getItem('session_id') ? localStorage.getItem('session_id') : '',
    token_id: '', // localStorage.getItem('token_id') ? localStorage.getItem('token_id') : '',
    // features
    smile_enabled: true,
    messenger_enabled: true,
    message_forward_enabled: true,
    file_send_enabled: true,
    paste_image_enabled: true,
    player_enabled: true,
    audio_message_enabled: true,
    voice_call_enabled: true,
    create_group_enabled: true,
    tools_search_enabled: true,
    header_in_messenger: true,
    open_profile_in_popup: true,
    events_in_header: false,
    events_in_settings_window: false,
    lk_enabled: true,
    after_login_page: 'index.user', // page to open after seccussfully login
    messenger: {
        message_control_id: 'messagecontrol'
    },
    language: {
        locale: 'ru',
        code: 'ru',
        title: 'Русский',
    },
    languages: {
        current: null,
        list: [
            {locale: 'ru', code: 'ru', title: 'Русский', active: false},
            {locale: 'en', code: 'en', title: 'English', active: false},
        ],
        init: function() {
            var currentLang = localStorage.getItem('lang');
            if (!currentLang) {
                currentLang = 'ru';
            }
            for(var item of this.list) {
                item.active = item.code == currentLang;
                if(item.active) {
                    this.current = item;
                }
            }
        },
        change: function(code) {
            localStorage.setItem('lang', code);
            $.get('/app/common/setlang/', {code: code}, function(resp){
                location.reload();
            });
        }
    },
    constant: {
        PASSWORD_MIN_LENGTH: 8
    },
    // Дополнительные функции приложения
    feautures: {
    },
    isLogged: function() {
        return this.is_logged;
    },
    loginCallback: function(result) {
        console.log('loginCallback result', result);
        var currentLang = localStorage.getItem('lang');
        this.clearSession();
        localStorage.setItem('session_id', result.data.login_result.session_id);
        localStorage.setItem('token_id', result.data.login_result.token_id);
        localStorage.setItem('lang', result.data.login_result.user.lang_id);
        localStorage.setItem('user', JSON.stringify(result.data.login_result.user));
        this.session_id = result.data.login_result.session_id;
        this.token_id = result.data.login_result.token_id;
        // need for reload language
        if (currentLang) {
            if (currentLang != result.data.login_result.user.lang_id) {
                // location.reload();
                return;
            }
        }
        this.is_logged = false;
        this.isLogged();
        return gmaback.enter();
    },
    changeProfile: function(data) {
        if (!this.is_logged) {
            return false;
        }
        this.user.fio = data.fio;
        this.user.avatar = data.avatar_url;
        this.user.avatar_url = data.avatar_url;
        this.user.user_status_id = data.user_status_id;
        this.user.goldendax_ticket_id = data.goldendax_ticket_id;
		this.user.goldendax_ticket_dt_end = data.goldendax_ticket_dt_end;
        localStorage.setItem('user', JSON.stringify(this.user));
        return this.user;
    },
    changeAvatar: function(data) {
        if (!this.is_logged) {
            return false;
        }
        this.user.avatar_url = data;
        localStorage.setItem('user', JSON.stringify(this.user));
        return this.user;
    },
    gotoURL: function (url) {
        return gmaback.index_scope.pages.gotoURL(url);
    },
    clearSession: function(){
        this.is_logged = false;
        this.user = {};
        this.user_login = '';
        this.token_id = '';
        this.session_id = '';
        localStorage.removeItem('app_settings');
        localStorage.removeItem('session_id');
        localStorage.removeItem('token_id');
        localStorage.removeItem('user');
    },
    logout: function () {
        gmaback.index_scope.pages.setLogged(false);
        this.is_logged = false;
        this.clearSession();
        CustomUI.hideForm();
        return this.gotoURL('index.login');
    },
    isLogged: function () {
        if (this.is_logged) {
            return true;
        }
        var session_id = localStorage.getItem('session_id') ? localStorage.getItem('session_id') : false;
        if (session_id) {
            this.is_logged = localStorage.getItem('session_id') ? true : false;
            this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {};
            this.user_login = this.user.user_login;
            this.session_id = localStorage.getItem('session_id') ? localStorage.getItem('session_id') : '';
            this.token_id = localStorage.getItem('token_id') ? localStorage.getItem('token_id') : '';
            return true;
        }
        return false;
    },
    // Settings
    settings: {
        values: false,
        defaults: {
            theme: 'default',
            domains: _domains,
            events_desktop_notifications: false, // @todo
            hide_send_button: false, // @todo
            font: {
                size: 100
            }
        },
        save: function () {
            localStorage.setItem('appearance_values', JSON.stringify(this.values));
        },
        load: function () {   
            this.values = localStorage.getItem('appearance_values');
            if (this.values) {
                this.values = JSON.parse(this.values);
                if (this.values) {
                    this.values = $.extend(this.defaults, this.values);
                }
            }
            if (!this.values) {
                this.values = this.defaults;
            }

        }
    }
};

function getApp() {
    if (!$currentApp.settings.values) {
        $currentApp.settings.load();
    }
    return $currentApp;
}

function newDate(str_dt) {
	if (!str_dt) {
		return new Date();
	}
	var dt = new Date(str_dt);
	if (!isNaN(dt.getTime())) {
		return dt;
	}
	//ios device
	return new Date(str_dt.replace(/-/g, '/'));
}

// Detect current language
var lang_code = localStorage.getItem('lang');
if (['ru', 'en'].indexOf(lang_code) < 0) {
    lang_code = 'ru';
    localStorage.setItem('lang', lang_code);
}
getApp().language.locale = lang_code;

/**
 * Init RequireJS
 */
requirejs.config({
    baseUrl: './app',
    urlArgs: 'v=19',
    waitSeconds: 30,
    //  names and paths for used libraries and plugins
    paths: {
        // others
        angular: 'angular.min',
        lang: getApp().domains.api + '/lang_' + lang_code,
        gmaback: 'gmaback',
        // services
        pluginsSrvc: 'service/plugins',
        settingsSrvc: 'service/settings',
        userSrvc: 'service/user',
        // controllers
        indexCtrl: 'controller/index',
        referralCtrl: 'controller/referral',
        loginCtrl: 'controller/login',
        registrationCtrl: 'controller/registration',
        myprofileCtrl: 'controller/myprofile',
        newsCtrl: 'controller/news',
        modalCtrl: 'controller/modal',
        headerCtrl: 'controller/header',
        messengerCtrl: 'controller/messenger',
        playerCtrl: 'controller/player',
        footerCtrl: 'controller/footer',
        durationCtrl: 'controller/duration',
        testingCtrl: 'controller/testing',
        walletCtrl: 'controller/wallet',
        createWalletCtrl: 'controller/create_wallet'
    },
    // angular not supported, AMD by standart, because export angular variable to global scope
    shim: {
        'angular': {
            exports: 'angular'
        },
        // https://unpkg.com/@uirouter/angularjs@1.0.12/release/
        'angular-ui-router.min': [
            'lang',
            'angular',
            // 'plugin/load-image/load-image',
            // 'plugin/load-image/load-image-meta',
            // 'plugin/load-image/load-image-exif',
            // 'plugin/load-image/load-image-orientation'
        ]
    },
    // run application
    deps: isDesktop() ? ['app', 'indexCtrl', 'modalCtrl',  'newsCtrl', 'loginCtrl', 'registrationCtrl', 'footerCtrl', 'messengerCtrl' ]
					  : ['app', 'modalCtrl', 'headerCtrl', 'newsCtrl', 'footerCtrl', 'testingCtrl', 'messengerCtrl', 'myprofileCtrl' ]
});

$(function () {
    
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        // jqXHR.setRequestHeader('X-Xsrftoken', getApp().token_id);
        jqXHR.setRequestHeader('xsrf', getApp().token_id);
        jqXHR.setRequestHeader('X-Session-ID', getApp().session_id);
    });

    // Auto hide dropdown menus
    Routines.HideDropdown.setup();
    Routines.HideDropdown.add('#events_button,.extended-info,#favourites_button');

    $('body').on('click', 'span.likes-blog,span.dislikes-blog', function () {
        var el = $(this);
        if (el.hasClass('disabled')) {
            return false;
        }
        var user_activity_type_id = el.hasClass('fa-thumbs-down') ? 3 : 2;
        var entity_name = el.attr('entity_name');
        var entity_id = el.attr('entity_id');
        if (!entity_name || !entity_id) {
            return false;
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/app/action/like/',
            data: {
                'entity_name': entity_name,
                'entity_id': entity_id,
                'user_activity_type_id': user_activity_type_id,
                'form': 1,
                'session': getApp().session_id
            }, // i like it
            success: function (data) {
                if (data.status == 'success') {
                    if (el.hasClass('fa-heart-o')) {
                        el.removeClass('fa-heart-o').addClass('fa-heart');
                    } else if (el.hasClass('fa-heart')) {
                        el.removeClass('fa-heart').addClass('fa-heart-o');

                    } else if (el.hasClass('far') && el.hasClass('fa-thumbs-up')) {
                        el.removeClass('far').addClass('fa');
                        el.closest('.reply_box').find('span.dislikes-blog').addClass('disabled');
                    } else if (!el.hasClass('far') && el.hasClass('fa-thumbs-up')) {
                        el.addClass('far').removeClass('fa');
                        el.closest('.reply_box').find('span.dislikes-blog').removeClass('disabled');

                    } else if (el.hasClass('far') && el.hasClass('fa-thumbs-down')) {
                        el.removeClass('far').addClass('fa');
                        el.closest('.reply_box').find('span.likes-blog').addClass('disabled');
                    } else if (!el.hasClass('far') && el.hasClass('fa-thumbs-down')) {
                        el.addClass('far').removeClass('fa');
                        el.closest('.reply_box').find('span.likes-blog').removeClass('disabled');
                    }
                    el.siblings('span.count_like').html('' + parseInt(data.count));
                    el.closest('.like').attr('count_like', parseInt(data.count));
                }
            }
        });
    });

    $('body').on('click', '[data-user_activity_type_id]', function () {
        var el = $(this);
        if (el.hasClass('disabled')) {
            return false;
        }
        el.removeClass('zoomIn');
        el.addClass('disabled animated zoomOut');
        var user_activity_type_id = el.data('user_activity_type_id');
        var entity_name = el.data('entity_name');
        var entity_id = el.data('entity_id');
        if (!entity_name || !entity_id || !user_activity_type_id) {
            return false;
        }
        gmaback.utils_service.api.call('/app/action/action/', {
            'entity_name': entity_name,
            'entity_id': entity_id,
            'user_activity_type_id': user_activity_type_id,
            'form': 1,
            'session': getApp().session_id
        }, function (data) {
            el.removeClass('disabled');
            el.removeClass('zoomOut');
            el.addClass('zoomIn');
            if (data.status == 'success') {
                if (data.data.added == 1) {
                    el.addClass('active');
                } else {
                    el.removeClass('active');
                }
                if (user_activity_type_id == getApp().constant.USER_ACTIVITY_TYPE_ADD_TO_FAVOURITE) {
                    if (data.data.added == 1) {
                        gmaback.utils_service.favourites.append(data.data.info);
                    } else {
                        gmaback.utils_service.favourites.remove(data.data.info.id);
                    }
                }
                if (data.data.message) {
                    gmaback.js.showMessage(data.data.message, 2000);
                }
                el.find('.count').html('' + parseInt(data.data.count));
                el.data('cache', '').trigger('mouseenter');
                if(gmaback.index_scope) {
                    gmaback.index_scope.favourites.init(data.data.favourites);
                }
            }
        });
    });

});
// Я сейчас сделаю быстро - потом переделать!!



