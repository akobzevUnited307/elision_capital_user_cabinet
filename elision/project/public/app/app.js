'use strict';

define([
    'angular',
    'angularAMD.min',
    'lang',
    'angular-ui-router.min'

], function (ng, angularAMD, lang) {

    'use strict';

    var conf = getApp();
    getApp().language = lang.language;
    conf.lang = lang.values;

    // set CustomUI lang
    CustomUI.lang.close_form = getApp().lang.close;
    CustomUI.lang.cancel_action = getApp().lang.txt_cancel_action;
    CustomUI.lang.txt_cancel_action = getApp().lang.txt_cancel_action;
    CustomUI.lang.action_success = getApp().lang.operation_complete;
    CustomUI.lang.wait_text = getApp().lang.wait_text;
    CustomUI.lang.wait_caption = getApp().lang.wait_caption;
    CustomUI.lang.txt_continue = getApp().lang.continue;
    CustomUI.lang.yes = getApp().lang.yes;
    CustomUI.lang.txt_cancel = getApp().lang.cancel;
    CustomUI.lang.choose_file = getApp().lang.eds_check_description;

    CustomUI.close_button = '<i class="fal fa-times"></i>';

    var app = window.app = ng.module('mApp', ['ui.router'])
        .constant('AppConfig', conf);

    app.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $compileProvider) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tg|chrome-extension):/);

		if(!isDesktop()) {
	        // Routes
	        $stateProvider
	            	            
	        	.state('referral', angularAMD.route({
	                url: '/r/:user_login/',
	                controllerUrl: 'controller/referral',
	                controller: 'referralCtrl',
	                templateUrl: 'app/views/referral.html'
	            }))
	            
	            .state('index', angularAMD.route({
	                url: '/',
	                abstract: true,
	                controllerUrl: 'controller/index',
	                controller: 'indexCtrl',
	                templateUrl: 'app/views/index.html'
	            }))
	            .state('index.splash', angularAMD.route({
	                url: '',
	                controllerUrl: 'controller/splash',
	                controller: 'splashCtrl',
	                templateUrl: 'app/views/splash.html'
	            }))
                .state('index.user', angularAMD.route({
                    url: 'user/',
                    controllerUrl: 'controller/user',
                    templateUrl: 'app/views/user.html'
                }))

                /*// Wallet
                    .state('index.wallet', angularAMD.route({
                        abstract: true,
                        url: 'wallet/',
                        controllerUrl: 'controller/wallet',
                        controller: 'walletCtrl',
                        templateUrl: 'app/views/wallet/container.html'
                    }))
                    .state('index.wallet.view', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/wallet/index.html'
                    }))
                    .state('index.wallet.requisites', angularAMD.route({
                        url: 'requisites/{id:string}',
                        templateUrl: 'app/views/wallet/requisites.html'
                    }))
                    .state('index.wallet.cumulative', angularAMD.route({
                        url: 'cumulative/',
                        templateUrl: 'app/views/wallet/cumulative.html'
                    }))
                    .state('index.wallet.transfer', angularAMD.route({
                        url: 'transfer/',
                        templateUrl: 'app/views/wallet/transfer.html'
                    }))
                    .state('index.wallet.operation', angularAMD.route({
                        url: 'operation/{id:string}',
                        templateUrl: 'app/views/wallet/operation.html'
                    }))*/
                    
                // Wallet
                    .state('index.wallet', angularAMD.route({
                        abstract: true,
                        url: 'wallet/',
                        controllerUrl: 'controller/wallet',
                        controller: 'walletCtrl',
                        templateUrl: 'app/views/wallet/container.html'
                    }))
                    .state('index.wallet.view', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/wallet/index.html'
                    }))

                // Messenger
                    .state('index.messenger', {
                        url: 'messenger/{url:string}',
                        templateUrl: 'app/views/messenger/index.html'
                    })

                // My clients
                    .state('index.myclients', angularAMD.route({
                        url: 'myclients/',
                        controllerUrl: 'controller/myclients',
                        templateUrl: 'app/views/myclients.html'
                    }))

                /*// Team
                    .state('index.team', angularAMD.route({
                        abstract: true,
                        url: 'team/',
                        controllerUrl: 'controller/team',
                        controller: 'teamCtrl',
                        templateUrl: 'app/views/team/container.html'
                    }))
                    .state('index.team.stat', angularAMD.route({
                        url: '',
                        controllerUrl: 'controller/team',
                        controller: 'teamCtrl',
                        templateUrl: 'app/views/team/stat.html'
                    }))                    
                    .state('index.team.lead', angularAMD.route({
                        url: 'lead/',
                        controllerUrl: 'controller/partner',
                        controller: 'partnerCtrl',
                        templateUrl: 'app/views/team/lead.html'
                    }))
                                        
                    .state('index.team.partner', angularAMD.route({
                        url: 'partner/',
                        controllerUrl: 'controller/partner',
                        controller: 'partnerCtrl',
                        templateUrl: 'app/views/team/partner.html'
                    }))*/
                    
                    // Team
                    .state('index.team', angularAMD.route({
                        abstract: true,
                        url: 'team/',
                        controllerUrl: 'controller/team',
                        controller: 'teamCtrl',
                        templateUrl: 'app/views/team/container.html'
                    }))                                        
                    .state('index.team.partner', angularAMD.route({
                        url: '',
                        controllerUrl: 'controller/partner',
                        controller: 'partnerCtrl',
                        templateUrl: 'app/views/team/partner.html'
                    }))
                    .state('index.team.lead', angularAMD.route({
                        url: 'lead/',
                        controllerUrl: 'controller/partner',
                        controller: 'partnerCtrl',
                        templateUrl: 'app/views/team/lead.html'
                    }))
                // Career
                    .state('index.career', angularAMD.route({
                        abstract: true,
                        url: 'career/',
                        controllerUrl: 'controller/career',
                        controller: 'careerCtrl',
                        templateUrl: 'app/views/career/container.html'
                    }))
                    .state('index.career.index', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/career/index.html'
                    }))
                    .state('index.career.turnover', angularAMD.route({
                        url: 'turnover/',
                        templateUrl: 'app/views/career/turnover.html'
                    }))
                // Rewards
                    //     .state('index.rewards', angularAMD.route({
                    //     abstract: true,
                    //     url: 'rewards/',
                    //     controllerUrl: 'controller/rewards',
                    //     controller: 'rewardsCtrl',
                    //     templateUrl: 'app/views/rewards/container.html'
                    // }))
                    .state('index.rewards.index', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/rewards/index.html'
                    }))
                // Statistic
                    .state('index.stat', angularAMD.route({
                    	abstract: true,
                        url: 'statistic/',
                        controllerUrl: 'controller/team',
                        controller: 'teamCtrl',
                        templateUrl: 'app/views/statistic/container.html'
                    }))
                    .state('index.stat.view', angularAMD.route({
                    	url: '',
                        controllerUrl: 'controller/team',
                        controller: 'teamCtrl',
                        templateUrl: 'app/views/statistic/index.html'
                    }))
                    
                    .state('index.schedule', angularAMD.route({
                        url: 'schedule/',
                        controllerUrl: 'controller/schedule',
                        templateUrl: 'app/views/schedule.html'
                    }))
                    .state('index.social', angularAMD.route({
                        url: 'social/',
                        controllerUrl: 'controller/user',
                        templateUrl: 'app/views/social.html'
                    }))
                    .state('index.achievement', angularAMD.route({
                        url: 'achievement/',
                        controllerUrl: 'controller/user',
                        templateUrl: 'app/views/achievement.html'
                    }))
                    .state('index.presents', angularAMD.route({
                        url: 'presents/',
                        controllerUrl: 'controller/user',
                        templateUrl: 'app/views/presents.html'
                    }))
                    .state('index.in_developing', angularAMD.route({
                        url: 'indeveloping/',
                        controllerUrl: 'controller/user',
                        templateUrl: 'app/views/in_developing.html'
                    }))

                // Events
                    .state('index.events', angularAMD.route({
                        abstract: true,
                        url: 'events/',
                        controllerUrl: 'controller/events',
                        controller: 'eventsCtrl',
                        templateUrl: 'app/views/events/container.html'
                    }))
                    .state('index.events.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/events/list.html'
                    }))
                    .state('index.events.read', angularAMD.route({
                        url: '{id:string}/',
                        templateUrl: 'app/views/events/read.html'
                    }))
                    .state('index.events.tickets', angularAMD.route({
                        url: '{id:string}/tickets',
                        templateUrl: 'app/views/events/tickets.html'
                    }))
                    
                // Events history
                    .state('index.eventhistory', angularAMD.route({
                        abstract: true,
                        url: 'eventshistory/',
                        controllerUrl: 'controller/events',
                        controller: 'eventsCtrl',
                        templateUrl: 'app/views/events/container.html'
                    }))
                    .state('index.eventhistory.index', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/events/eventshistory.html'
                    }))
                    
                // Achievements
                    .state('index.achievements', angularAMD.route({
                        abstract: true,
                        url: 'achievements/',
                        controllerUrl: 'controller/events',
                        controller: 'eventsCtrl',
                        templateUrl: 'app/views/events/container.html'
                    }))
                    .state('index.achievements.index', angularAMD.route({
                        url: '',
                        controllerUrl: 'controller/achievements',
                        controller: 'achievementsCtrl',
                        templateUrl: 'app/views/events/achievements.html'
                    }))
                    
                // Lead
                    .state('index.lead', angularAMD.route({
                        abstract: true,
                        url: 'lead/',
                        controllerUrl: 'controller/lead',
                        controller: 'leadCtrl',
                        templateUrl: 'app/views/lead/container.html'
                    }))
                    .state('index.lead.index', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/lead/index.html'
                    }))
                    .state('index.lead.list', angularAMD.route({
                        url: 'list/',
                        templateUrl: 'app/views/lead/list.html'
                    }))
                    .state('index.lead.archive', angularAMD.route({
                        url: 'archive/',
                        templateUrl: 'app/views/lead/archive.html'
                    }))
                    .state('index.lead.view', angularAMD.route({
                        url: 'view/{id:string}/',
                        templateUrl: 'app/views/lead/view.html'
                    }))

                // News
                    .state('index.news', angularAMD.route({
                        abstract: true,
                        url: 'news/',
                        controllerUrl: 'controller/news',
                        controller: 'newsCtrl',
                        templateUrl: 'app/views/news/container.html'
                    }))
                    .state('index.news.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/news/index.html'
                    }))
                    .state('index.news.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/news/read.html'
                    }))

                // Bounty
                    .state('index.bounty', angularAMD.route({
                        abstract: true,
                        url: 'bounty/',
                        controllerUrl: 'controller/bounty',
                        controller: 'bountyCtrl',
                        templateUrl: 'app/views/bounty/container.html'
                    }))
                    .state('index.bounty.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/bounty/index.html'
                    }))
                    .state('index.bounty.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/bounty/read.html'
                    }))
                    .state('index.bounty.moderator', angularAMD.route({
                        url: 'moderator/',
                        templateUrl: 'app/views/bounty/moderator.html'
                    }))
                    
                // Lead bonus
                    .state('index.leaderbonus', angularAMD.route({
                        abstract: true,
                        url: 'leaderbonus/',
                        controllerUrl: 'controller/leader_bonus',
                        controller: 'leaderBonusCtrl',
                        templateUrl: 'app/views/leader_bonus/container.html'
                    }))
                    .state('index.leaderbonus.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/leader_bonus/list.html'
                    }))
                    .state('index.leaderbonus.view', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/leader_bonus/view.html'
                    }))

                // Promotions
                    .state('index.promotions', angularAMD.route({
                        abstract: true,
                        url: 'promotions/',
                        controllerUrl: 'controller/promotions',
                        controller: 'promotionsCtrl',
                        templateUrl: 'app/views/promotions/container.html'
                    }))
                    .state('index.promotions.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/promotions/index.html'
                    }))
                    .state('index.promotions.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/promotions/307IPhone.html'
                    }))

                // Competitions
                    .state('index.competition', angularAMD.route({
                        abstract: true,
                        url: 'competition/',
                        controllerUrl: 'controller/promotions',
                        controller: 'promotionsCtrl',
                        templateUrl: 'app/views/competition/container.html'
                    }))
                    .state('index.competition.quickstart', angularAMD.route({
                        url: 'quickstart',
                        templateUrl: 'app/views/competition/quickstart.html'
                    }))
                    .state('index.competition.beleader', angularAMD.route({
                        url: 'beleader',
                        templateUrl: 'app/views/competition/beleader.html'
                    }))

                // GD
                    .state('index.gd', angularAMD.route({
                        abstract: true,
                        url: 'gd/',
                        controllerUrl: 'controller/gd',
                        controller: 'gdCtrl',
                        templateUrl: 'app/views/gd/container.html'
                    }))
                    .state('index.gd.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/gd/index.html'
                    }))
                    .state('index.gd.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/gd/read.html'
                    }))
                    .state('index.gd.payment', angularAMD.route({
                        url: 'payment/{id:string}',
                        templateUrl: 'app/views/gd/payment.html'
                    }))

                // Education
                    .state('index.education', angularAMD.route({
                        abstract: true,
                        url: 'education/',
                        controllerUrl: 'controller/user',
                        controller: 'userCtrl',
                        templateUrl: 'app/views/education/container.html'
                    }))
                    .state('index.education.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/education/index.html'
                    }))
                    .state('index.education.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/education/read.html'
                    }))

                // Material
                    .state('index.material', angularAMD.route({
                        abstract: true,
                        url: 'material/',
                        controllerUrl: 'controller/material',
                        controller: 'materialCtrl',
                        templateUrl: 'app/views/material/container.html'
                    }))
                    .state('index.material.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/material/index.html'
                    }))
                    .state('index.material.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/material/read.html'
                    }))

                // Tools
                    .state('index.tools', angularAMD.route({
                        abstract: true,
                        url: 'tools/',
                        controllerUrl: 'controller/user',
                        controller: 'userCtrl',
                        templateUrl: 'app/views/tools/container.html'
                    }))
                    .state('index.tools.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/tools/index.html'
                    }))
                    .state('index.tools.read', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/tools/read.html'
                    }))

                // Remanagement
                    .state('index.remanagement', angularAMD.route({
                        abstract: true,
                        url: 'remanagement/',
                        controllerUrl: 'controller/user',
                        controller: 'userCtrl',
                        templateUrl: 'app/views/remanagement/container.html'
                    }))
                    .state('index.remanagement.index', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/remanagement/index.html'
                    }))
                    .state('index.remanagement.request', angularAMD.route({
                        url: 'request/',
                        templateUrl: 'app/views/remanagement/request.html'
                    }))
                    
                // FAQ
                    .state('index.faq', angularAMD.route({
                        abstract: true,
                        url: 'faq/',
                        controllerUrl: 'controller/user',
                        controller: 'userCtrl',
                        templateUrl: 'app/views/faq/container.html'
                    }))
                    .state('index.faq.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/faq/index.html'
                    }))
                    .state('index.faq.group', angularAMD.route({
                        url: '{id:string}',
                        templateUrl: 'app/views/faq/group.html'
                    }))
                    .state('index.faq.read', angularAMD.route({
                        url: 'question/{id:string}',
                        templateUrl: 'app/views/faq/read.html'
                    }))

                // Support
                    .state('index.support', angularAMD.route({
                        abstract: true,
                        url: 'support/',
                        controllerUrl: 'controller/user',
                        controller: 'userCtrl',
                        templateUrl: 'app/views/support/container.html'
                    }))
                    .state('index.support.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/support/index.html'
                    }))
                    .state('index.support.tickets', angularAMD.route({
                        url: 'tickets/',
                        templateUrl: 'app/views/support/tickets.html'
                    }))
                    .state('index.support.ticket', angularAMD.route({
                        url: 'ticket/{id:string}',
                        templateUrl: 'app/views/support/ticket.html'
                    }))
                    
                // Settings
                    .state('index.settings', angularAMD.route({
                        abstract: true,
                        url: 'settings/',
                        controllerUrl: 'controller/settings',
                        controller: 'settingsCtrl',
                        templateUrl: 'app/views/settings/container.html'
                    }))
                    .state('index.settings.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/settings/index.html'
                    }))
                    .state('index.settings.notification', angularAMD.route({
                        url: 'notification/',
                        templateUrl: 'app/views/settings/notification.html'
                    }))
                    .state('index.settings.confidentiality', angularAMD.route({
                        url: 'confidentiality/',
                        templateUrl: 'app/views/settings/confidentiality.html'
                    }))
                    .state('index.settings.loclang', angularAMD.route({
                        url: 'loclang/',
                        templateUrl: 'app/views/settings/loclang.html'
                    }))
                    .state('index.settings.payment', angularAMD.route({
                        url: 'payment/',
                        templateUrl: 'app/views/settings/payment.html'
                    }))
                
                // Withdrawal method
                    .state('index.withdrawal_method', angularAMD.route({
                        abstract: true,
                        url: 'withdrawalmethod/',
                        controllerUrl: 'controller/withdrawal_method',
                        controller: 'withdrawalMethodCtrl',
                        templateUrl: 'app/views/withdrawal_method/container.html'
                    }))
                    .state('index.withdrawal_method.index', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/withdrawal_method/index.html'
                    }))
                    
	            // Other pages
                    .state('index.transfer', angularAMD.route({
                        url: 'transfer/{id:string}',
                        controllerUrl: 'controller/transfer',
                        controller: 'transferCtrl',
                        templateUrl: 'app/views/transfer.html'
                    }))
                    .state('index.notifications', angularAMD.route({
                        url: 'notifications/',
                        controllerUrl: 'controller/user',
                        templateUrl: 'app/views/notifications.html'
                    })
                    )
                    .state('index.statistic', angularAMD.route({
                        url: 'statistic/',
                        controllerUrl: 'controller/user',
                        templateUrl: 'app/views/statistic.html'
                    }))
                    .state('index.agreement', angularAMD.route({
                        url: 'agreement/',
                        controllerUrl: 'controller/login',
                        templateUrl: 'app/views/agreement.html'
                    }))
	                .state('index.login', angularAMD.route({
	                    url: 'login/',
                        controllerUrl: 'controller/login',
	                    templateUrl: 'app/views/login.html'
	                }))
	                .state('index.registration', angularAMD.route({
	                    url: 'registration/',
                        controllerUrl: 'controller/registration',
	                    templateUrl: 'app/views/registration.html'
	                }))
                    .state('index.testing', angularAMD.route({
                        url: 'testing/',
                        // controllerUrl: 'controller/testing',
                        templateUrl: 'app/views/testing.html'
                    }))
                    .state('index.forpartner', angularAMD.route({
                        url: 'forpartner/',
                        controllerUrl: 'controller/forpartner',
                        templateUrl: 'app/views/forpartner.html'
                    }))
                    .state('index.profile', angularAMD.route({
                        url: 'profile/{user_login:string}',
                        controllerUrl: 'controller/profile',
                        templateUrl: 'app/views/profile.html'
                    }))
                    .state('index.myprofile', angularAMD.route({
                        url: 'myprofile/',
                        // controllerUrl: 'controller/myprofile',
                        templateUrl: 'app/views/myprofile.html'
                    }))

                    // Offices
                    .state('index.offices', angularAMD.route({
                        abstract: true,
                        url: 'offices/',
                        controllerUrl: 'controller/offices',
                        controller: 'officesCtrl',
                        templateUrl: 'app/views/offices/container.html'
                    }))
                    .state('index.offices.list', angularAMD.route({
                        url: '',
                        templateUrl: 'app/views/offices/index.html'
                    }))
                    .state('index.offices.my', angularAMD.route({
                        url: 'my/',
                        templateUrl: 'app/views/offices/my.html'
                    }))
                    .state('index.offices.manage', angularAMD.route({
                        url: 'manage/{id:[0-9]*}',
                        templateUrl: 'app/views/offices/manage.html'
                    }))
                    .state('index.offices.view', angularAMD.route({
                        url: '{id:[0-9]*}',
                        templateUrl: 'app/views/offices/view.html'
                    }))
                    .state('index.offices.program', angularAMD.route({
                        url: 'program/',
                        templateUrl: 'app/views/offices/program.html'
                    }))
                    .state('index.regionagio', angularAMD.route({
                        url: 'regionagio/',
                        controllerUrl: 'controller/offices',
                        templateUrl: 'app/views/regionagio.html'
                    }))
                    .state('index.howopen', angularAMD.route({
                        url: 'howopen/',
                        controllerUrl: 'controller/open-office/howopen',
                        templateUrl: 'app/views/open-office/howopen.html'
                    }))
                    .state('index.requestform', angularAMD.route({
                        url: 'requestform/',
                        controllerUrl: 'controller/open-office/requestform',
                        templateUrl: 'app/views/open-office/requestform.html'
                    }))
                    .state('index.thanksforrequest', angularAMD.route({
                        url: 'thanksforrequest/',
                        controllerUrl: 'controller/open-office/thanksforrequest',
                        templateUrl: 'app/views/open-office/thanksforrequest.html'
                    }))
                    // .state('index.userevent', angularAMD.route({
                    //     url: 'userevent/',
                    //     controllerUrl: 'controller/userevent',
                    //     templateUrl: 'app/views/events/userevent.html'
                    // }))
                    .state('index.userevent', angularAMD.route({
                        url: 'userevent/{id:string}',
                        controllerUrl: 'controller/userevent',
                        templateUrl: 'app/views/events/userevent.html'
                    }))
	            // 404
	                .state('404', {
	                    template: function(){
	                        return '404 not found';
	                    }
	                });
		}
    });

    app
        .filter('date', [function () {
            return function (str_date) {
                if (str_date) {
                    var date = new Date(str_date);
                    if (isNaN(date.valueOf())) {
                       //  debugger;
                        date = new Date(str_date.replace(/\s/, 'T') + ':00');
                    }
                    if (isNaN(date.valueOf())) {
                        date = new Date(str_date.replace(/-/g, "/"));
                    }
                    if (isNaN(date.valueOf())) {
                        return str_date;
                    }
                    return date.toLocaleString(getApp().language.locale, {
                        month: 'long',
                        day: 'numeric',
                        // year: 'numeric',
                        // hour: 'numeric',
                        // minute: 'numeric'
                    });
                }
            }
        }])

        .directive('imageonload', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.bind('load', function (e) {
                        scope._evt = e;
                        // call the function that was passed
                        scope.$apply(attrs.imageonload, e);
                    });
                }
            };
        })

        .filter('trust', ['$sce', function ($sce) {
            return function(htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            }
        }])

        .filter('imgSize', function () {
            return function (path, size) {
                if (path) {
                    if (!size) {
                        return path;
                    }
                    var tmp = path.split('/');
                    tmp.splice(-1, 0, size);
                    return tmp.join('/');
                }
            };
        })

        .filter('mycurrency', function () {
            return function(amount, fraction) {
                if (!isNaN(parseFloat(amount)) && isFinite(amount)) {
                    amount = parseFloat(amount);
                    if(isNaN(fraction)) {
                        fraction = 2;
                    }
                    if(fraction == 0) {
                        amount = Math.floor(amount);
                    } else {
                        amount = Math.floor(amount * Math.pow(10, fraction)) / Math.pow(10, fraction);
                    }
                    return amount.toLocaleString('ru-RU');
                } else {
                    return 0; // amount;
                }
            };
        })

        .directive('duration', function() {
            return {
                link: function($scope, el, attrs) {
                    $(el)
                        // сохраняем в форму, чтобы потом обращаться в событиях success и error
                        .data('Angular_scope', $scope)
                        .addClass('red-text');
                }
            }
        })

        .directive('art', function($parse) {
            return {
                link: function($scope, el, attrs) {
                    var content = $parse(attrs.content)($scope);
                    var root = $(el).get(0);
                    var x = document.createElement('article');
                    root.appendChild(x);
                    var shadow = x.attachShadow( { mode: 'closed' } );
                    content += '<link href="/assets/css/import/medium_article.min.css" rel="stylesheet">';
                    content = content.replace('link_color', '#fff');
                    shadow.innerHTML = content;
                    // var root = $(el).get(0).createShadowRoot();
                    //console.log($(el).get(0));
                    // var template = document.querySelector('#material2');
                    // content += '<style>img {max-width: 100% !important; height: auto !important;} figure{margin: 0 -1.25rem; background-color: rgba(0,0,0,.1);} span {width: auto !important;}</style>';
                    /*$(el)
                        // сохраняем в форму, чтобы потом обращаться в событиях success и error
                        .data('Angular_scope', $scope)
                        .addClass('red-text');
                    */
                }
            }
        })

        .filter('strReplace', function () {
            return function (input, from, to) {
                input = input || '';
                from = from || '';
                to = to || '';
                return input.replace(new RegExp(from, 'g'), to);
            };
        })

        .filter('ageFilter', function() {
            function calculateAge(birthday) { // birthday is a date
                var ageDifMs = Date.now() - birthday.getTime();
                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            }
            return function(birthdate) {
                try {
                    if(!birthdate) {
                        return '';
                    }
                    return calculateAge(new Date(birthdate));
                } catch(e) {
                    return birthdate;
                }
            };
        })

        .filter('noData',  ['$sce', function($sce) {
            return function(value) {
                // if(typeof value === 'number' && isFinite(value) &&Math.floor(value) === value) {
                if((typeof value === null) || (typeof value == 'object') || (value == '')) {
                    var htmlCode = '<span class="font-readonly">Нет данных</span>';
                    return $sce.trustAsHtml(htmlCode);
                } else {
                    return $sce.trustAsHtml(value + '');
                }
            };
        }])

        .directive('initBind', function ($compile) {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    attr.$observe('ngBindHtml', function () {
                        if (attr.ngBindHtml) {
                            $compile(element[0].children)(scope);
                        }
                    })
                }
            };
        })

        // messengerPlugin
        .directive('messengerPlugin', ['pluginsSrvc', function (pluginsSrvc) {
            function link(scope, element, attrs) {
                element.html(pluginsSrvc[attrs.name].run(JSON.parse(attrs.param)));
            }
            return {
                replace: true,
                link: link
            };
        }])

        .directive('customOnChange', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var onChangeHandler = scope.$eval(attrs.customOnChange);
                    element.bind('change', onChangeHandler);
                }
            };
        })

        .directive('popupForm', function () {
            return {
                link: function ($scope, el, attrs) {
                    $(el)
                        // сохраняем в форму, чтобы потом обращаться в событиях success и error
                        .data('Angular_scope', $scope)
                        .customSubmit({
                            onclose: $(el).data('onclose')
                        });
                }
            }
        })

        .directive('jsCrop', function () {
            return {
                link: function ($scope, el, attrs) {
                    var id = attrs.id,
                        title = attrs.jsCropTitle,
                        callback = attrs.jsCropCallback;
                    var jsc = new JSCropHelper(id, title, '').init().setCallback(function (ok, resp) {
                        return $scope.$apply(function () {
                            return $scope.$eval(callback, {
                                ok: ok,
                                resp: resp
                            });
                        });
                    });
                    el.data('JSCropHelper', jsc);
                }
            }
        })

        .directive('inputMask', function ($q) {
            return function (scope, element, attrs) {
                var attr = element.attr("input-mask");
                element.inputmask(attr, {
                    clearIncomplete: true
                });
            }
        })

        .directive('popupAjax', function () {
            return {
                link: function ($scope, el, attrs) {
                    $(el)
                        // сохраняем в форму, чтобы потом обращаться в событиях success и error
                        .data('Angular_scope', $scope)
                        .customSubmit({
                            json: true
                        });
                }
            }
        })

        .directive('datepicker', function() {
	        return {
	            link: function($scope, el, attrs) {
	                createDatepicker(el);
	            }
	        }
	    })

        .directive('showLikeUsers', function ($q) {
            return function (scope, element, attrs) {
                element.off('mouseenter').on('mouseenter', function (e) {
                    gmaback.likes.showLikeUsers(this, attrs.entityName, attrs.entityId);
                });
                element.off('mouseleave').on('mouseleave', function (e) {
                    gmaback.likes.hideLikeUsers(this);
                });
            }
        })

        .directive('materialSelect', function () {
            return {
                link: function ($scope, el, attrs) {
                    el.material_select();
                }
            }
        })

        .directive('materializeTextarea', function () {
            return {
                link: function ($scope, el, attrs) {
                    el.characterCounter();
                }
            }
        })

        .directive('myMobileTouchStart', [function () {
            return function (scope, elem, attrs) {
                elem.bind('touchstart mousedown', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    scope.$apply(attrs['myMobileTouchStart']);
                });
            }
        }])

        .filter('time_short', [function () {
            return function (date) {
                date = new Date(date);
                return date.toLocaleString(getApp().language.locale, {
                    hour: 'numeric',
                    minute: 'numeric'
                });
            }
        }])

        .filter('datetime', [function () {
            return function (str_date) {
                var date = new Date(str_date);
                if(isNaN(date.valueOf())) {
                    date = new Date(str_date.replace(/\s/, 'T') + ':00');
                }
                if(isNaN(date.valueOf())) {
                    date = new Date(str_date.replace(/-/g,"/"));
                }
                if(isNaN(date.valueOf())) {
                    return str_date;
                }
                return date.toLocaleString(getApp().language.locale, {
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric'
                });
            }
        }])

        .filter('date_weekday', [function () {
            return function (str_date) {
                // console.log('dt', str_date, getApp().language.locale);
                var date = new Date(str_date);
                if(isNaN(date.valueOf())) {
                    date = new Date(str_date.replace(/\s/, 'T') + ':00');
                }
                if(isNaN(date.valueOf())) {
                    date = new Date(str_date.replace(/-/g,"/"));
                }
                if(isNaN(date.valueOf())) {
                    return str_date;
                }
                return date.toLocaleString(getApp().language.locale, {
                    weekday: 'short'
                });
            }
        }])

        .filter('date_dmy_hm', [function () {
	        return function(str_date) {
	            // date = date.replace(/(:\d{2})([+-]\d{2})$/, '$1$200'); // firefox hack
	            //console.log('dt', str_date, getApp().language.locale);
	            if (!str_date) {
					return str_date;
	            }
	            var date = new Date(str_date);
	            if(isNaN(date.valueOf())) {
	                date = new Date(str_date.replace(/\s/, 'T') + ':00');
	            }
	            if(isNaN(date.valueOf())) {
	                return str_date;
	            }
	            return date.toLocaleString(getApp().language.locale, {
	                day: 'numeric',
	                month: 'numeric',
            		year: 'numeric',
	                hour: 'numeric',
	                minute: 'numeric'
	            });
	        }
	    }])
        .filter('date_short', [function () {
            return function(date) {
                var dt = new Date(date);
                if(isNaN(dt.valueOf())) {
                    dt = new Date(date.replace(/-/g,"/"));
                }
                return dt.toLocaleString(getApp().language.locale, {
                    year: 'numeric',
                    month: 'long'
                });
            }
        }])        
        .filter('date_notime', [function () {
            return function(date) {
                var dt = new Date(date);
                return dt.toLocaleString(getApp().language.locale, {
                    day: 'numeric',
                    year: 'numeric',
                    month: 'long'
                });
            }
        }])
        .filter('date_dmy', [function () {
            return function(date) {
                var dt = new Date(date);
                return dt.toLocaleString(getApp().language.locale, {
                    day: 'numeric',
                    year: 'numeric',
                    month: 'numeric'
                });
            }
        }])
        .filter('date_my', [function () {
            return function(date) {
                var dt = new Date(date);
                return dt.toLocaleString(getApp().language.locale, {
                    year: 'numeric',
                    month: 'numeric'
                });
            }
        }])

        .directive('myMobileTouchMove', [function () {
            return function (scope, elem, attrs) {
                elem.bind('touchmove mousemove', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    scope.$apply(attrs['myMobileTouchMove']);
                });
            }
        }])

        .directive('myMobileTouchEnd', [function () {
            return function (scope, elem, attrs) {
                elem.bind('touchend mouseup', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    scope.$apply(attrs['myMobileTouchEnd']);
                });
            }
        }])

        .directive('control', function () {
            var s;
            return {
                restrict: 'E',
                replace: true,
                // require: 'ngModel',
                templateUrl: function (elem, attrs) {
                    return attrs.template;
                }
            };
        })

        .directive('plugin', function () {
            var contentUrl = '<h2>Rec</h2>';
            return {
                restrict: 'E',
                replace: true,
                link: function (scope, element, attrs) {
                    // concatenating the directory to the ver attr to select the correct excerpt for the day
                    // contentUrl = 'content/excerpts/hymn-' + attrs.ver + '.html';
                    contentUrl = './app/plugin/' + attrs.name + '/template.html';
                },
                templateUrl: function (elem, attrs) {
                    return './app/plugin/' + attrs.name + '/template.html';
                }
            };
        });

    return angularAMD.bootstrap(app);
});