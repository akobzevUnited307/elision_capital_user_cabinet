'use strict';

define(['app'], function (app, emoji, calltools) {

    /**
     * gmaback.js
     * 2016-08-30 02:50 - 2017-02-28 17:31 - 2018-05-14
     * @type Object
     */
    gmaback = {
        // Controller scopes
        index_scope: null,
        header_scope: null,
        connected: false,
        debugMode: false,
        utils_service: null,
        contacts: false, // array if loaded
        user_id: null,
        socket: null,
        logged_id: null,
        prev_event: {},
        strings: [],
        TextDecoder: (window.TextDecoder != null) ? new window.TextDecoder('Latin1') : false,
        options: {
            app_server: getApp().domains.daemon,
            error: {
                ERROR_INVALID_SESSION                       : 401,
                ERROR_CALL_INVALID                          : 1,
                ERROR_CALL_BUSY                             : 2,
                ERROR_CALL_VOICE_DATA                       : 3,
                ERROR_CALL_INVALID_USER_NOT_IN_CHAT         : 4,
                ERROR_CALL_INVALID_USER_NOT_ACCEPT_INVITE   : 5
            },
            event: {
                command_msg_send: 1,
                command_msg_in: 2,
                ping: 3,
                pong: 4,
                command_connect_channel: 5, // connect to channel
                command_msg_get_prev: 6,
                command_error: 7, // какая-то ошибка
                command_channels: 8, // пришел список каналов(контактов)
                command_missed_update: 9, // обновление количества пропущенных сообщений в чате
                command_get_contacts: 10, // запрос списка контактов
                command_leave_room: 11, // вышел из чата
                command_call_in: 12, // звонок
                command_call_accept: 13, // принять звонок
                command_call_reject: 14, // отклонить звонок
                command_call_hangout: 15, // закончить звонок
                command_call_voice_data: 16, // звук
                command_crypt: 17, // pub_pem
                command_event_list: 18, // список событий (event_list)
                command_udp_port_voice: 19, // udp voice port number
                command_call_crypt: 20, // call crypt tumbler
                command_msg_delete: 31, // удалены сообщения в чате
                command_update_info: 32, // обновление информации о чате
                command_leave_chat: 33, // собеседник покинул чат(вышел из состава участников)
                command_add_member: 34, // в чат добавился новый участник
                command_msg_forward: 35, // пересылка сообщений
                cmd_call_ring: 36, // собеседнику доставлено событие входящего звонка
                cmd_msg_change_status: 37, // команда изменения статуса сообщений
                cmd_msg_data: 200 // команда с любыми данными
            },
        },
        crypt: {
            pub_pem: null,
            data: null,
        },
        connection_callback: function () {
            if (getApp().isLogged()) {
                // gmaback.index_scope.connection.setConnected(true);
                // for calc missed_messages
                gmaback.send(gmaback.options.event.command_get_contacts, {});
            }
        },
        isMasterTab: function () {
            return lstor.get('master_tab') == getApp().id;
        },
        statistic: {
            'inbound': 0,
            'outbound': 0,
            'incoming_events': 0,
            'outcount': 0,
        },
        likes: {
            entity: '',
            likesCount: -1,
            showLikeUsers: function (element, entityName, entityId) {
                var that = this;
                var user_activity_type_id = $(element).data('user_activity_type_id') || $(element).data('user_activity_type_id_noaction');
                if ($(element).find('.like_users_short').length == 0) {
                    $(element).append('<div class="like_users_short">like_users_short</div>');
                    setTimeout(function () {
                        $(element).find('.like_users_short').addClass('active');
                    }, 1);
                } else {
                    $(element).find('.like_users_short').addClass('active');
                }
                if(!$(element).data('cache')) {
                    console.log('load remote');
                    $.ajax({
                        url: getApp().domains.api + '/app/comment/likeusersshort/',
                        data: {
                            user_activity_type_id: user_activity_type_id,
                            avatar_domain: getApp().domains.avatar,
                            entity_name: entityName,
                            entity_id: entityId
                        },
                        type: 'POST'
                    }).done(function(p){
                        $(element).data('cache', true);
                        $(element).find('.like_users_short').html(p);
                    });
                } else {
                    console.log('from cache');
                }
            },
            hideLikeUsers: function (element) {
                $(element).find('.like_users_short').removeClass('active');
            }
        },
        js: {
            notify: {
                show: function (msg, title, icon) {
                    return Routines.Notification.show(msg, title, icon);
                }
            },
            formatDate: function (dt) {
                var replace_map = {
                    'mm': ('0' + (dt.getMonth() + 1)).slice(-2),
                    'dd': ('0' + dt.getDate()).slice(-2),
                    'yyyy': dt.getFullYear()
                };
                var string = new String(defDatepicker.format);
                return string.replace(/\bdd|mm|yyyy/g, function (match) { // be sure to add every char in the pattern
                    return replace_map[match];
                });
            },
            getFileIcon: function (filename) {
                var fileicons = ['ai', 'avi', 'css', 'csv', 'dbf', 'doc', 'docx',
                    'dwg', 'exe', 'fla', 'htm', 'html', 'iso', 'js', 'json', 'mp3',
                    'mp4', 'pdf', 'ppt', 'psd', 'rtf', 'svg',
                    'txt', 'xls', 'xlsx', 'xml', 'zip',
                    'jpg', 'png', 'gif'
                ];
                var extension = filename.split('.').pop();
                extension = extension.toLowerCase();
                if (extension == 'jpeg') {
                    extension = 'jpg'
                }
                if (fileicons.indexOf(extension) >= 0) {
                    return '/assets/img/extensions/' + extension + '.png';
                } else {
                    return '/assets/img/extensions/file.png';
                }
            },
            uint8ArrayToBase64: function (uint8array) {
                var binary = '';
                var len = uint8array.byteLength;
                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode(uint8array[i]);
                }
                return window.btoa(binary);
            },
            arrayBufferToBase64: function (buffer) {
                var bytes = new Uint8Array(buffer);
                return this.uint8ArrayToBase64(bytes);
            },
            base64ToUInt8Array: function (base64) {
                var binary_string = window.atob(base64);
                var len = binary_string.length;
                var bytes = new Uint8Array(len);
                for (var i = 0; i < len; i++) {
                    bytes[i] = binary_string.charCodeAt(i);
                }
                return bytes;
            },
            base64ToArrayBuffer: function (base64) {
                var arr = this.base64ToUInt8Array(base64);
                return arr.buffer;
            },
            bytesToUInt32: function (arr) {
                var view = new DataView(arr.buffer);
                return view.getUint32(0, false);
            },
            uint32toBytes: function (num) {
                var arr = new ArrayBuffer(4); // an Int32 takes 4 bytes
                var view = new DataView(arr);
                view.setUint32(0, num, false); // byteOffset = 0; litteEndian = false
                return new Uint8Array(arr);
            },
            getRandomInt: function (min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            },
            // Возвращает случайное число между min (включительно) и max (не включая max)
            getRandomArbitrary: function (min, max) {
                return Math.random() * (max - min) + min;
            },
            mergeTypedArrays: function (a, b) {
                // Checks for truthy values on both arrays
                if (!a && !b) throw 'Please specify valid arguments for parameters a and b.';
                // Checks for truthy values or empty arrays on each argument
                // to avoid the unnecessary construction of a new array and
                // the type comparison
                if (!b || b.length === 0) return a;
                if (!a || a.length === 0) return b;
                // Make sure that both typed arrays are of the same type
                if (Object.prototype.toString.call(a) !== Object.prototype.toString.call(b))
                    throw 'The types of the two arguments passed for parameters a and b do not match.';
                var c = new a.constructor(a.length + b.length);
                c.set(a);
                c.set(b, a.length);
                return c;
            },
            guid: function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
            },
            generateKey: function (length) {
                var text = '';
                var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                for (var i = 0; i < length; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            },
            showMessage: function (text, timer, v1, v2) { //callback) {
                
                return toastr.success(text);//, 'Miracle Max Says')

                var dialog_class = 'rounded';
                var options = {
                    html: text,
                    displayLength: timer

                };
                var classes = (typeof v1 == 'string') ? v1 : (typeof v2 == 'string' ? v2 : null);
                var completeCallback = (v1 instanceof Function) ? v1 : (v2 instanceof Function ? v2 : null);
                if (classes) {
                    options.classes = classes;
                }
                if (completeCallback) {
                    options.completeCallback = completeCallback;
                }
                M.toast(options);
                return false;
            },
            showError: function (text, timer, callback) {
                return toastr.error(text);//, 'Miracle Max Says')
                var dialog_class = 'red lighten-1 rounded';
                if (typeof timer == 'undefined') {
                    timer = 1500;
                }
                return this.showMessage(text, timer, dialog_class, callback);
            },
            escapeHtml: function (text) {
                var map = {
                    '&': '&amp;',
                    '<': '&lt;',
                    '>': '&gt;',
                    '"': '&quot;',
                    "'": '&#039;'
                };
                return text.replace(/[&<>"']/g, function (m) {
                    return map[m];
                });
            },
            pasteHtmlAtCaret: function (html) {
                var sel, range;
                if (window.getSelection) {
                    // IE9 and non-IE
                    sel = window.getSelection();
                    if (sel.getRangeAt && sel.rangeCount) {
                        range = sel.getRangeAt(0);
                        range.deleteContents();
                        // Range.createContextualFragment() would be useful here but is
                        // only relatively recently standardized and is not supported in
                        // some browsers (IE9, for one)
                        var el = document.createElement("div");
                        el.innerHTML = html;
                        var frag = document.createDocumentFragment(),
                            node, lastNode;
                        while ((node = el.firstChild)) {
                            lastNode = frag.appendChild(node);
                        }
                        range.insertNode(frag);
                        // Preserve the selection
                        if (lastNode) {
                            range = range.cloneRange();
                            range.setStartAfter(lastNode);
                            range.collapse(true);
                            sel.removeAllRanges();
                            sel.addRange(range);
                        }
                    }
                } else if (document.selection && document.selection.type != "Control") {
                    // IE < 9
                    document.selection.createRange().pasteHTML(html);
                }
            }
        },
        setServer: function (app_server) {
            this.options.app_server = app_server;
        },
        // Инициация соединения с сервером приложения по вебсокету
        enter: function (Callback) {
            if(!getApp().messenger_enabled) {
                return false;
            }
            lstor.on('close_tab', function (new_value, old_value, url) {
                // страшно, а вдруг что нибудь пропустится и мы останемся без мастер-вкладки =(
                // if(lstor.get('master_tab') == new_value) {
                lstor.set('master_tab', getApp().id);
                //}
            });
            lstor.set('master_tab', getApp().id);
            window.onbeforeunload = function () {
                lstor.set('close_tab', getApp().id);
            };
            this.connection_callback = (Callback instanceof Function) ? Callback : this.connection_callback;
            var AppServer = this.options.app_server;
            var ConnectTypeName = 'enter';
            this.strings[gmaback.options.event.connected] = '%time%: <sys>Вы успешно вошли в систему под именем %login%</sys>';
            this.strings[gmaback.options.event.user_joined] = '%time%: <sys>Пользователь %login% присоединился к системе.</sys>';
            this.strings[gmaback.options.event.user_exit] = '%time%: <err>Пользователь %login% покинул систему.</err>';
            // Создаем соединение с сервером
            try {
                if(this.socket != null) {
                    this.socket.socket.onclose = function(evt) {};
                    this.socket.socket.close();
                }
                if (window['WebSocket']) {
                    var connection_string = AppServer + '?SessionId=' + encodeURIComponent(getApp().token_id) + '&PlatformId=browser';
                    console.log('Connecting to WS: ' + connection_string);
                    var socket = new WebSocket(connection_string);
                    this.socket = {
                        'socket': socket,
                        'json': {
                            'socket': socket,
                            'send': function (data) {
                                // console.log('send data: ' + JSON.stringify(data));
                                this.socket.send(JSON.stringify(data));
                            }
                        }
                    }
                    socket.onopen = function () {
                        console.log('Websocket connected');
                        gmaback.connection_callback();
                        // Обработка пакета с сервера
                        this.onmessage = function (evt) {
                            gmaback.statistic.inbound += evt.data.length;
                            gmaback.statistic.incoming_events++;
                            var messages = JSON.parse(evt.data);
                            for (var i = 0; i < messages.length; i++) {
                                var msg = messages[i];
                                var e = msg.event;
                                var d = msg.data;
                                gmaback.onevent(e, d);
                            }
                        };
                        gmaback.connected = true;
                    };
                    socket.onclose = function (evt) {
                        var timeout = 3000;
                        gmaback.connected = false;
                        // gmaback.index_scope.connection.setConnected(false, timeout, evt);
                        console.log('Websocket connection interrupted');
                        // перезагрузка из-за разрыва со стороны сервера
                        setTimeout(function () {
                            gmaback.enter(function(){
                                // do nothing
                            });
                        }, timeout);
                    };
                    socket.onerror = function (evt) {
                        gmaback.connected = false;
                        // gmaback.js.showError('Connection error', 4000);
                        console.log('Socket connection lost', evt);
                    };
                } else {
                    alert('Sorry, your device does not supported');
                }
            } catch (e) {
                gmaback.js.showError('Нет связи с сервером ' + e, 4000);
            }
        },
        // Отправка команды на сервер
        send: function (event, data) {
            var packet = {
                'Name': event,
                'Params': data,
                'Id': CustomUI.createId(),
            };
            this.statistic.outbound += JSON.stringify(packet).length;
            if(this.socket) {
                this.socket.json.send(packet);
            }
        },
        // Команды
        commands: {
            addLog: function (text) {},
            addUser: function (login, id) {},
            removeUser: function (id) {},
            refreshUserList: function (list) {}
        },
        recalcMissedMessages: function () {
            var missed_messages = 0;
            for (var url in gmaback.channel_missed) {
                var mm = gmaback.channel_missed[url];
                if (Number.isInteger(mm)) {
                    missed_messages += mm;
                }
            }
            $('[data-event-selector=app_messenger] span.new').text(missed_messages ? missed_messages : '');
        },
        call: {
            data: {},
            isCrypt: false,
            crypt: null,
            tools: calltools,
            traff_in: 0,
            traff_out: 0,
            packets_in: 0,
            packets_out: 0,
            start_time: 0,
            packetNumber: 0,
            interval: null,
            // сервер для отправки голосовых пакетов
            voice_server: {
                ip: null, // string
                id: null, // string
                udp_port_number: null, // int
                socket: null, // websocket connection
                ws_url: null, // url
                connected: false, // bool
                statistic: {
                    'inbound': 0,
                    'outbound': 0,
                    'incoming_events': 0,
                    'outcount': 0,
                },
                // инициация соединения с голосовым сервером
                connect: function (server_list) {
                    // @smart detect server from this list
                    var call_server = server_list[0];
                    console.log('voice server:', call_server);
                    this.ip = call_server.ip;
                    this.id = call_server.id;
                    this.udp_port_number = call_server.port_number;
                    this.ws_url = call_server.ws_url;
                    this.ws = call_server.ip;
                    if (!window['WebSocket']) {
                        return gmaback.js.showError('Sorry, your device does not supported');
                    }
                    var connection_string = this.ws_url + '?SessionId=' + getApp().token_id + '&PlatformId=browser&ID=' + this.id;
                    console.log('voice websocket connection_string:', connection_string);
                    var socket = new WebSocket(connection_string);
                    this.socket = {
                        'socket': socket,
                        'json': {
                            'socket': socket,
                            'send': function (data) {
                                this.socket.send(JSON.stringify(data));
                            }
                        }
                    }
                    socket.onopen = function () {
                        console.log('Voice websocket connected');
                        // gmaback.connection_callback();
                        // Обработка пакета с сервера
                        this.onmessage = function (evt) {
                            gmaback.statistic.inbound += evt.data.length;
                            gmaback.statistic.incoming_events++;
                            var messages = JSON.parse(evt.data);
                            for (var i = 0; i < messages.length; i++) {
                                var msg = messages[i];
                                var e = msg.event;
                                var d = msg.data;
                                gmaback.onevent(e, d);
                            }
                        };
                        gmaback.call.voice_server.connected = true;
                    };
                    socket.onclose = function (evt) {
                        gmaback.call.voice_server.connected = false;
                        console.log('Voice websocket connection interrupted');
                        // gmaback.js.showError('Voice websocket connection interrupted');
                        // @todo Нужна ли перезагрузка из-за разрыва со стороны сервера?
                        // Просто вешаем трубку
                        gmaback.call.ihangout();
                    };
                    socket.onerror = function (evt) {
                        gmaback.call.voice_server.connected = false;
                        console.log('Voice websocket connection lost', 4000);
                    };
                },
                // Отправка команды на сервер
                send: function (event, data) {
                    if (!this.connected) {
                        return false;
                    }
                    var packet = {
                        'Name': event,
                        'Params': data,
                        'Id': CustomUI.createId(),
                    };
                    this.statistic.outbound += JSON.stringify(packet).length;
                    this.socket.json.send(packet);
                },
            },
            audio: {
                _audio: null,
                start: function () {
                    this._audio = new CallAudio();
                    this._audio.play();
                },
                stop: function () {
                    if (this._audio != null) {
                        this._audio.stop();
                    }
                },
                reset: function () {
                    if (this._audio != null) {
                        this._audio.stop();
                        this._audio = null;
                    }
                },
                voiceData: function (data) {
                    if (this._audio != null) {
                        this._audio.voiceData(data);
                    }
                }
            },
            reset: function () {
                console.log('*** reset data');
                this.data = null;
                this.isCrypt = false;
                gmaback.call.tools.callCrypt.reset();
                gmaback.header_scope.reinitCall();
            },
            incoming: function(data) {
                data = data[0];
                this.traff_in = 0;
                this.traff_out = 0;
                this.packets_in = 0;
                this.start_time = 0;
                this.packetNumber = 0;
                this.data = data;
                console.log('*** incoming', data);
                gmaback.call.tools.callCrypt.reset();
                gmaback.call.tools.callCrypt.init(function () {
                    gmaback.call.tools.callCrypt.setPartnerPublicKey(data.public_key);
                });
                /*$('#form-incoming-call').data('onclose', function(){
                gmaback.call.ihangout()
                });*/
                var description = data.channel_url;
                var logo = './assets/img/user_220x220.png';
                for (var contact of gmaback.messenger_service.channels.list) {
                    if (contact.id == data.channel_url) {
                        logo = contact.logo.replace('80x80', '220x220');
                        description = contact.title;
                    }
                }
                $('#form-incoming-call .channel_logo:not(.channel_logo_medium)').html('<img class="circle" src="' + logo + '" />');
                $('#form-incoming-call .channel_logo_medium').html('<img class="circle" src="' + logo.replace('220x220', '80x80') + '" />');
                $('#form-incoming-call .call_in_login').text(description);
                $('#form-incoming-call .info').removeClass('active');
                $('#form-incoming-call .popup-form-footer').show();
                $('#form-incoming-call .ihangout').hide();
                $('#form-incoming-call').data('onclose', false);
                this.hideForm();
                CustomUI.showForm('form-incoming-call', false);
                $('#form-incoming-call').data('onclose', function (data) {
                    gmaback.call.ireject();
                });
                if (gmaback.player_scope) {
                    gmaback.player_scope.player.setVolume(.2);
                }
                if (gmaback.isMasterTab()) {
                    audio.ring.play();
                }
            },
            call: function (channel) {
                this.hideForm();
                console.log('call');
                var data = {
                    channel_url: channel.id
                };
                this.data = data;
                this.traff_in = 0;
                this.traff_out = 0;
                this.packets_in = 0;
                var logo = channel.logo.replace('80x80', '220x220');
                var description = channel.title;
                gmaback.index_scope.call.outgoing.abonent.photo = logo;
                gmaback.index_scope.call.outgoing.abonent.photo_medium = logo.replace('220x220', '80x80');
                gmaback.index_scope.call.outgoing.abonent.name = description;
                $('#form-outcoming-call .popup-form-mini').removeClass('active');
                CustomUI.showForm('form-outcoming-call', false);
                $('#form-outcoming-call').data('onclose', function (data) {
                    gmaback.call.ireject();
                });
                gmaback.call.tools.callCrypt.reset();
                gmaback.call.tools.callCrypt.init(function () {
                    gmaback.send(gmaback.options.event.command_call_in, {
                        channel_url: channel.url,
                        uuid_string: gmaback.js.guid(),
                        public_key: gmaback.call.tools.callCrypt.getPublicKey() // gmaback.js.generateKey(512)
                    });
                });
            },
            voice_data: function (data) {
                // console.log('*** voice data');
                data = data[0];
                this.traff_in += data.data.length;
                this.packets_in++;
                this.tools.iaproc.voiceData(data.data);
                // this.audio.voiceData(data.data);
                $('#form-incoming-call .traff_in span').text(Math.round(this.traff_in / 1024));
                $('#form-incoming-call .packets_in span').text(this.packets_in_);
                $('#form-outcoming-call .traff_in span').text(Math.round(this.traff_in / 1024));
                $('#form-outcoming-call .packets_in span').text(this.packets_in);
            },
            ivoice_data: function (base64) {
                if (this.tools.test_user_name) {
                    if (getApp().user.fio == this.tools.test_user_name) {
                        return this.voice_data([{
                            data: base64
                        }]);
                    } else {
                        return false;
                    }
                }
                this.traff_out += base64.length;
                this.packets_out++;
                gmaback.call.voice_server.send(gmaback.options.event.command_call_voice_data, {
                    data: base64,
                    channel_url: this.data.channel_url
                });
                $('#form-incoming-call .traff_out span').text(Math.round(this.traff_out / 1024));
                $('#form-incoming-call .packets_out span').text(this.packets_out);
                $('#form-outcoming-call .traff_out span').text(Math.round(this.traff_out / 1024));
                $('#form-outcoming-call .packets_out span').text(this.packets_out);
            },
            iaccept: function () {
                console.log('iaccept');
                $('#form-incoming-call').data('onclose', false);
                CustomUI.hideForm(null, true);
                $('#form-incoming-call').addClass('mini animated zoomOut');
                setTimeout(function () {
                    $('#form-incoming-call').removeClass('zoomOut').addClass('bounceIn');
                    var x = $(window).width() - $('#form-incoming-call').width() - 10,
                        y = $(window).height() - $('#form-incoming-call').height() - 10;
                    $('#form-incoming-call').css({
                        left: x + 'px',
                        top: y + 'px'
                    });
                    $('#form-incoming-call').draggable({
                        handle: '.popup-form-mini',
                        containment: 'window',
                        scroll: false
                    });
                }, 300);
                if (gmaback.player_scope) {
                    gmaback.player_scope.player.setVolume(.1);
                }
                gmaback.send(gmaback.options.event.command_call_accept, {
                    channel_url: this.data.channel_url,
                    public_key: gmaback.call.tools.callCrypt.getPublicKey()
                });
                this.startAudio();
                this.startTimer();
            },
            accept: function (data) {
                console.log('accept');
                $('#form-outcoming-call').data('onclose', false);
                CustomUI.hideForm(null, true);
                $('#form-outcoming-call').addClass('mini animated zoomOut');
                setTimeout(function () {
                    $('#form-outcoming-call').removeClass('zoomOut').addClass('bounceIn');
                    var x = $(window).width() - $('#form-outcoming-call').width() - 10,
                        y = $(window).height() - $('#form-outcoming-call').height() - 10;
                    $('#form-outcoming-call').css({
                        left: x + 'px',
                        top: y + 'px'
                    });
                    $('#form-outcoming-call').draggable({
                        handle: '.popup-form-mini',
                        containment: 'window',
                        scroll: false
                    });
                }, 300);
                if (gmaback.player_scope) {
                    gmaback.player_scope.player.setVolume(.1);
                }
                gmaback.call.tools.callCrypt.setPartnerPublicKey(data.public_key);
                this.startAudio();
                this.startTimer();
            },
            toggleCrypt: function () {
                this.isCrypt = !this.isCrypt;
            },
            ireject: function () {
                console.log('ireject');
                if(this.data) {
                    this.hideForm();
                    gmaback.send(gmaback.options.event.command_call_reject, {
                        channel_url: this.data.channel_url
                    });
                    this.reset();
                }
            },
            reject: function (incoming) {
                console.log('reject');
                this.hideForm();
                this.reset();
            },
            ihangout: function () {
                console.log('ihangout');
                if(this.data) {
                    this.hideForm();
                    gmaback.send(gmaback.options.event.command_call_hangout, {
                        channel_url: this.data.channel_url
                    });
                    this.reset();
                }
            },
            hangout: function () {
                console.log('hangout');
                this.hideForm();
                this.reset();
            },
            startAudio: function () {
                console.log('startAudio');
                this.tools.GMAAudioEncoder.init();
                this.tools.audioQueue.start();
                audio.ring.stop();
                this.tools.t0 = performance.now();
                // захват звука
                this.tools.outaproc.start(this.data.channel_url);
                // генератор звука
                this.tools.iaproc.reset();
                this.tools.iaproc.start();
                this.tools.opus.reset();
                this.tools.opus.start();
                // CallAudio.reset();
                // CallAudio.start();
                // this.audio.reset();
                // this.audio.start();
            },
            stopAudio: function () {
                console.log('stopAudio');
                if (gmaback.player_scope) {
                    gmaback.player_scope.player.setVolume(1);
                }
                audio.ring.stop();
                // this.audio.stop();
                this.tools.iaproc.stop();
                this.tools.outaproc.stop();
                this.tools.opus.stop();
                // CallAudio.stop();
            },
            hideForm: function () {
                if (this.interval) {
                    clearInterval(this.interval);
                }
                this.stopAudio();
                $('#form-incoming-call').data('onclose', false);
                $('#form-outcoming-call').data('onclose', false);
                CustomUI.hideForm();
                $('#form-incoming-call').hide().addClass('zoomIn').removeClass('mini').removeClass('bounceIn');
                $('#form-outcoming-call').hide().addClass('zoomIn').removeClass('mini').removeClass('bounceIn');
            },
            startTimer: function () {
                if (this.interval) {
                    clearInterval(this.interval);
                }
                this.start_time = performance.now();
                this.interval = setInterval(function () {
                    var duration = Math.round((performance.now() - gmaback.call.start_time) / 1000);
                    var date = new Date(null);
                    date.setSeconds(duration); // specify value for SECONDS here
                    var duration_string = date.toISOString().substr(11, 8);
                    if (duration_string.substring(0, 3) == '00:') {
                        duration_string = duration_string.substring(3);
                    }
                    $('#form-incoming-call .duration span').text(duration_string);
                    $('#form-outcoming-call .duration span').text(duration_string);
                }, 500);
            }
        },
        // входящие сигналы с сервера
        onevent: function (event, data) {
            // console.log('<-command ' + event);
            // просто ping
            if (event == gmaback.options.event.ping) {
                return gmaback.send(gmaback.options.event.pong, []);

                // входящий голосовой вызов
            } else if (event == gmaback.options.event.command_call_in) {
                gmaback.call.incoming(data);

                // входящие звуковые пакеты от собеседника
            } else if (event == gmaback.options.event.command_call_voice_data) {
                gmaback.call.voice_data(data);

                // сервер для отправки голосовых пакетов
            } else if (event == gmaback.options.event.command_udp_port_voice) {
                gmaback.call.voice_server.connect(data);

                // собеседник принял звонок
            } else if (event == gmaback.options.event.command_call_accept) {
                data = data[0];
                gmaback.call.accept(data);

                // собеседник отклонил звонок
            } else if (event == gmaback.options.event.command_call_reject) {
                gmaback.call.reject(data);

                // публичный ключ для шифрования
            } else if (event == gmaback.options.event.command_call_hangout) {
                gmaback.call.hangout(data);

                // публичный ключ для шифрования
            } else if (event == gmaback.options.event.command_crypt) {
                gmaback.crypt.data = data[0];
                gmaback.crypt.pub_pem = gmaback.crypt.data.pub_pem;

                // список событий
            } else if (event == gmaback.options.event.command_event_list) {
                if (gmaback.index_scope) {
                    gmaback.index_scope.$apply(function () {
                        gmaback.index_scope.utilsSrvc.events.process(data);
                    });
                }
            
            // данные
            } else if (event == gmaback.options.event.cmd_msg_data) {
                if (gmaback.index_scope) {
                    gmaback.index_scope.$apply(function () {
                        gmaback.index_scope.utilsSrvc.data.process(data);
                    });
                }

                // какая-то ошибка
            } else if (event == gmaback.options.event.command_error) {
                if (data == 401) {
                    if (window.location.pathname == '/login/') {
                        // do nothing
                    } else {
                        return getApp().logout();
                    }
                } else {
                    var e = data.shift();
                    if (e.code == gmaback.options.error.ERROR_CALL_VOICE_DATA) {
                        // do nothing
                        return;
                    } else if (e.code == gmaback.options.error.ERROR_CALL_INVALID) {
                        gmaback.call.ireject();
                    } else if (e.code == gmaback.options.error.ERROR_CALL_INVALID_USER_NOT_IN_CHAT) {
                        gmaback.call.ireject();
                        return gmaback.js.showError(getApp().lang.error_call_invalid_user_not_in_chat, 4000);
                    } else if (e.code == gmaback.options.error.ERROR_CALL_INVALID_USER_NOT_ACCEPT_INVITE) {
                        gmaback.call.ireject();
                        return gmaback.js.showError(getApp().lang.error_call_invalid_user_not_accept_invite, 4000);
                    } else if (e.code == gmaback.options.error.ERROR_CALL_BUSY) {
                        return gmaback.js.showError(getApp().lang.user_busy, 4000);
                    }
                    if (e.code == 401) {
                        if (window.location.pathname != '/login/') {
                            getApp().logout();
                        }
                    } else {
                        return gmaback.js.showError(e.text, 4000);
                    }
                }

            } else if (event == gmaback.options.event.command_channels) {
                // пришел список контактов
                console.log('command_channels');
                gmaback.messenger_service.channels.setContacts(data);
                if (gmaback.messenger_scope) {
                    // gmaback.messenger_scope.$apply(function () {
                    //     gmaback.messenger_scope.channels.setContacts(data);
                    //});
                } else {
                    // calc missed messages
                    var missed_messages = 0;
                    data.forEach(function (m) {
                        gmaback.channel_missed[m.url] = m.missed_messages;
                    });
                    gmaback.recalcMissedMessages();
                }

            } else if (event < 1) {
                console.log(JSON.parse(data));

            } else if (event == gmaback.options.event.command_update_info) {
                // обновление свойст и параметров чата
                data.forEach(function (m) {
                    gmaback.messenger_scope.$apply(function () {
                        gmaback.messenger_scope.channels.info.cmdUpdate(m);
                        gmaback.messenger_scope.channels.sort();
                    });
                });
                /*
                gmaback.recalcMissedMessages();
                gmaback.messenger_scope.$apply(function(){
                    gmaback.messenger_scope.channels.updateMissed(data);
                    gmaback.messenger_scope.channels.sort();
                });*/

                // обновление количества пропущенных сообщений в чате
            } else if (event == gmaback.options.event.command_missed_update) {
                // calc missed messages
                data.forEach(function (m) {
                    gmaback.channel_missed[m.channel_url] = m.missed_messages;
                });
                gmaback.recalcMissedMessages();
                gmaback.messenger_scope.$apply(function () {
                    gmaback.messenger_scope.channels.updateMissed(data);
                    gmaback.messenger_scope.channels.sort();
                });

                /*// успешно присоединился к чату
                 } else if(event == gmaback.options.event.connected) {
                 gmaback.room_name = data.room_name;
                 gmaback.logged_id = data.id;
                 var user = gmaback.commands.addUser(data.login, data.id);
                 // добавляем в лог сообщение
                 gmaback.commands.addLog(this.strings[event]
                 .replace(/\%time\%/, data.time)
                 .replace(/\%login\%/, data.login));*/
             } else if(event == gmaback.options.event.cmd_msg_change_status) {
                var appElement = document.querySelector('[ng-controller=messengerCtrl]');
                var $scope = angular.element(appElement).scope();
                if(typeof $scope !== 'undefined') {
                    // если мессенджер открыт
                    $scope = $scope.$$childHead; // add this and it will work
                    $scope.$apply(function(){
                        data.forEach(function(m){
                            gmaback.messenger_scope.messages.changeMessagesStatus(m);
                        });
                    });
                }

            } else if (event == gmaback.options.event.command_msg_delete) {
                var appElement = document.querySelector('[ng-controller=messengerCtrl]');
                var $scope = angular.element(appElement).scope();
                if (typeof $scope !== 'undefined') {
                    // если мессенджер открыт
                    $scope = $scope.$$childHead; // add this and it will work
                    $scope.$apply(function () {
                        data.forEach(function (m) {
                            gmaback.messenger_scope.messages.deleteMessages(m);
                        });
                    });
                }
            } else if (event == gmaback.options.event.command_add_member) {
                var appElement = document.querySelector('[ng-controller=messengerCtrl]');
                var $scope = angular.element(appElement).scope();
                if (typeof $scope !== 'undefined') {
                    // если мессенджер открыт
                    $scope = $scope.$$childHead; // add this and it will work
                    $scope.$apply(function () {
                        for (var index in data) {
                            var m = data[index];
                            gmaback.messenger_scope.channels.addMember(m);
                        }
                    });
                }
            } else if (event == gmaback.options.event.command_leave_chat) {
                var appElement = document.querySelector('[ng-controller=messengerCtrl]');
                var $scope = angular.element(appElement).scope();
                if (typeof $scope !== 'undefined') {
                    // если мессенджер открыт
                    $scope = $scope.$$childHead; // add this and it will work
                    $scope.$apply(function () {
                        for (var index in data) {
                            var m = data[index];
                            gmaback.messenger_scope.channels.leaveChat(m);
                        }
                    });
                }
                // сообщение(я)
            } else if (event == gmaback.options.event.command_msg_in) {
                var appElement = document.querySelector('[ng-controller=messengerCtrl]');
                var $scope = angular.element(appElement).scope();
                var channel_url = data.length > 0 ? data[0].channel_url : '';
                var messengerIsOpen = (typeof $scope !== 'undefined');
                var channelIsOpen = messengerIsOpen && ($scope.channels.active.id == channel_url);
                // если мессенджер открыт
                if (messengerIsOpen) {
                    $scope = $scope.$$childHead; // add this and it will work
                    var $element = $('#chat_messages > div')[0];
                    var needScroll = channelIsOpen && ($element && ('scrollHeight' in $element));
                    if (needScroll) {
                        var scrollheight = $element.scrollHeight;
                        var scrolltop = $('#chat_messages > div').scrollTop();
                        scrollheight -= scrolltop;
                    }
                    $scope.$apply(function () {
                        $scope.messages.show_wait = false;
                        for(var m of data) {
                            var url = m.user_url.split('/');
                            var user_login = '';
                            if (url.length > 2) {
                                user_login = url[url.length - 2];
                            }
                            $scope.messages.add(m.channel_url, {
                                id: m.user_id,
                                user_login: user_login,
                                login: m.login,
                                avatar: m.user_avatar,
                                url: m.user_url
                            }, m.message, m.time, m.id, m.d, m.status_id);
                        }
                        gmaback.messenger_scope.channels.sort();
                        gmaback.recalcMissedMessages();
                        if((!channelIsOpen || !vis()) && data.length == 1) {
                            var m = data[0];
                            if(m.user_user_login != getApp().user.user_login) {
                                // Missed message
                                gmaback.addMissedMessage(m.channel_url, 1);
                                if(!gmaback.messenger_service.channels.channelIsMuted(m.channel_url)) {
                                    gmaback.js.notify.show(m.message, m.login || m.time, m.user_avatar);
                                }
                            }
                        }
                    });
                    if (needScroll) {
                        $element.scrollTop = $element.scrollHeight - scrollheight;
                    }
                // если мессенджер закрыт
                } else if(data.length == 1) {
                    // если мессенджер скрыт, то неможет быть такого, чтобы пришло несколько сообщений одним пакетом
                    var m = data[0];
                    if(m.user_user_login != getApp().user.user_login) {
                        // Missed message
                        gmaback.addMissedMessage(m.channel_url, 1);
                        if(!gmaback.messenger_service.channels.channelIsMuted(m.channel_url)) {
                            gmaback.js.notify.show(m.message, m.login || m.time, m.user_avatar);
                        }
                    }
                    gmaback.recalcMissedMessages();
                }
            }
        },
        addMissedMessage: function (channel_url, cnt) {
            if (!(channel_url in gmaback.channel_missed)) {
                gmaback.channel_missed[channel_url] = 0;
            }
            if (!$.isNumeric(gmaback.channel_missed[channel_url])) {
                gmaback.channel_missed[channel_url] = 0;
            }
            gmaback.channel_missed[channel_url]++;
        }
    };

    return gmaback;

});