Audio.prototype.stop = function() {
    this.pause();
    this.currentTime = 0.0;
}

class PlayerTrack {

    constructor(item) {
        this.player = false;
        this.item = item;
    }

    play() {
        if(!this.player) {
            this.player = new Player({});
            this.player.playlist.push(this);
        }
        this.player.play(0);
    }

    pause() {
        if(this.player) {
            this.player.pause();
        }
    }

    stop() {
        if(this.player) {
            this.player.stop();
        }
    }

}

class Player {

    setSettings(options) {
        this.options = $.extend(this.defaults, options);
        for(var k in this.options) {
            this[k] = this.options[k];
        }
        this.shuffle = this.options.shuffle;
        this.repeat = this.options.repeat;
        this.next_auto_play = this.options.next_auto_play;
    }

    constructor(options) {

        this.defaults = {
            playlist_mode: 1,
            shuffle: false,
            repeat: false,
            next_auto_play: true
        };

        this.setSettings(options);
        this.clear();

        this.audio = new Audio();
        this.audio.autoplay = false;
        this.audio.preload = false;
        this.audio.loop = false;
        this.audio.crossOrigin = 'anonymous';
        this.audio.crossorigin = 'anonymous';

        var that = this;

        function setState(code) {
            that.state = code;
            $(that).triggerHandler('stateChange', {code: code});
        }

        /**
        * Format the time from seconds to M:SS.
        * @param  {Number} secs Seconds to format.
        * @return {String}      Formatted time.
        */
        function formatTime(secs) {
            if(secs == Infinity) {
                return 'Infinity';
            }
            var minutes = Math.floor(secs / 60) || 0;
            var seconds = (Math.round(secs) - minutes * 60) || 0;
            return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
        }

        this.audio.addEventListener('ended', function(e){
            if(that.next_auto_play) {
                setState('ended');
                that.play(1);
            }
        });

        this.audio.addEventListener('error', function(e){
            if(that.next_auto_play) {
                setState('error');
                that.play(1);
            }
        });

        /**
        * https://developer.mozilla.org/ru/docs/Web/Guide/Events/Media_events
        */

        this.audio.addEventListener('pause', function(e){
            setState('pause');
        });

        this.audio.addEventListener('play', function(e){
            setState('play');
        });

        this.audio.addEventListener('playing', function(e){
            setState('play');
        });

        this.audio.addEventListener('waiting', function(e){
            setState('waiting');
        });

        this.audio.addEventListener('durationchange', function(e) {
            that.duration = this.duration;
            that.duration_format = formatTime(this.duration);
        }, false);

    }

    init() {
        this.clear();
    }

    clear() {
        this.state = 'new';
        this.duration = false;
        this.duration_format = '';
        this.playlist = [];
        this.play_history = [];
        this.current = false;
        this.index = -1;
        this.stop();
    }

    add(player_track) {
        player_track.player = this;
        this.playlist.push(player_track);
    }

    addToBegin(player_track) {
        player_track.player = this;
        this.playlist.unshift(player_track);
    }

    getPrev() {
        if(this.shuffle) {
            if(this.play_history.length > 0) {
                this.play_history.shift();
                return this.play_history.shift();
            } else {
                return this.getNext();
            }
        }
        this.index--;
        if(this.index < 0) {
            if(this.repeat) {
                this.index = this.playlist.length - 1;
            } else {
                this.index = 0;
                return false;
            }
        }
        return this.getCurrent();
    }

    getNext() {
        if(this.shuffle) {
            this.index = Math.floor(Math.random() * this.playlist.length);
            return this.getCurrent();
        }
        this.index++;
        if(this.index >= this.playlist.length) {
            if(this.repeat) {
                this.index = 0;
            } else {
                this.index = this.playlist.length - 1;
                return false;
            }
        }
        return this.getCurrent();
    }

    getCurrent() {
        if(this.audio) {
            if(this.playlist.length == 0) {
                return false;
            }
            if(this.index < 0) {
                this.index = 0;
            }
            var track = this.playlist[this.index];
            return track;
        }
    }

    play(move_step) {
        if (typeof move_step === 'undefined') {
            // Your variable is undefined
            move_step = 0;
        }
        if(this.audio) {
            if(move_step == 0) {
                var track = this.getCurrent();
            } else if(move_step > 0) {
                var track = this.getNext();
            } else if(move_step < 0) {
                var track = this.getPrev();
            }
            if(track) {
                console.log('Item', track.item.src[0]);
                this.play_history.unshift(track);
                this.current = track;
                var src = track.item.src[0];
                if(this.audio.src != src) {
                    this.audio.src = src;
                }
                this.audio.play();
            }
        }
    }

    pause() {
        if(this.audio) {
            this.audio.pause();
        }
    }

    stop() {
        if(this.audio) {
            this.audio.stop();
        }
    }

    next() {
        this.play(1);
    }

    prev() {
        this.play(-1);
    }

    isMuted() {
        if(this.audio) {
            return this.audio.muted;
        }
        return false;
    }

    isPaused() {
        if(this.audio) {
            return this.audio.paused;
        }
        return false;
    }

    isRepeat() {
        return this.repeat;
    }

    isShuffle() {
        return this.shuffle;
    }

    isPlaying() {
        // console.log([this.audio != false, this.audio.currentTime > 0, !this.audio.paused, !this.audio.ended, this.audio.readyState > 2]);
        return this.audio
            // && this.audio.currentTime > 0
            && !this.audio.paused
            && !this.audio.ended
            && this.audio.readyState > 2;
    }

    volume(value) {
        if(this.audio) {
            if(value < 0) {
                value = 0;
            }
            if(value > 1) {
                value = 1;
            }
            this.audio.volume = value;
            return this.audio.volume;
        }
        return 100;
    }

    toggleMute() {
        if(this.audio) {
            this.audio.muted = !this.audio.muted;
        }
    }

    toggleRepeat() {
        this.repeat = !this.repeat;
        this.options.repeat = this.repeat;
        $(this).triggerHandler('settingsChanged', this.getSettings());
    }

    toggleShuffle() {
        this.shuffle = !this.shuffle;
        this.options.shuffle = this.shuffle;
        $(this).triggerHandler('settingsChanged', this.getSettings());
    }

    togglePlaylistMode() {
        this.playlist_mode++;
        if(this.playlist_mode > 2) {
            this.playlist_mode = 1;
        }
        this.options.playlist_mode = this.playlist_mode;
        $(this).triggerHandler('settingsChanged', this.getSettings());
    }

    // перемотка
    seek(value) {
    }

    getSettings() {
        return this.options;
    }

    // Move to #index track
    skipTo(index) {
        if (typeof index === 'undefined') {
            return false;
        }
        if(this.playlist.length > 0) {
            if(index < 0) {
                index = 0;
            }
            if(index >= this.playlist.length) {
                index = this.playlist.length - 1;
            }
        } else {
            index = 0;
        }
        this.index = index;
        this.play(0);
    }

    /*
        var player = new Player({next_auto_play: true});
        player.playlist.push(new PlayerTrack({src: ['https://msg1.ru/rdo?http://winamp.xn--b1atgn2fc.xn--p1ai/mp3/Jingle/Music%20my%20All%20%E2%84%9614.mp3']}));
        player.playlist.push(new PlayerTrack({src: ['https://msg1.ru/rdo?http://air.radiorecord.ru:805/deep_320']}));

        var test = new Audio();
        test.src = 'https://msg1.ru/rdo?http://air.radiorecord.ru:805/deep_320';
        test.play();
        test.canPlayType('audio/ogg; codecs=vorbis')
    */

}

/**
* Routines static class
*/
var Routines = {
    niceScroll: function(selector, options) {
        $(selector).scrollify(options);
    },

    Notification: {
        id: 'NotificationPermission',
        init: function(){
            var that = this;
            if(Notification) {
                console.log('Notification.permission:', Notification.permission);
                if(Notification.permission == 'default') {
                    that.openPanel();
                } else {
                    that.hidePanel();
                }
            } else {
                console.error('Notification not supported');
            }
        },
        openPanel: function(){
            $('#' + this.id).show();
        },
        hidePanel: function(){
            $('#' + this.id).hide();
        },
        request: function(){
            var that = this;
            Notification.requestPermission().then(function(result) {
                if(result != 'default') {
                    that.hidePanel();
                }
            });
            return false;
        },
        show: function(msg, title, icon) {
            var that = this;
            console.log(msg, title, icon);
            if (gmaback.isMasterTab()) {
                if(getApp().common_settings.messages) {
                    audio.plop2.play();
                    if(!vis()) {
                        var was_show = false;
                        if(Notification) {
                            console.log('Notification.permission', Notification.permission);
                            if(Notification.permission == 'granted') {
                                was_show = true;
                                // If it's okay let's create a notification
                                var notification = new Notification(title, {
                                    body: msg,
                                    icon: icon ? icon : '/assets/img/logo/favicon.png'
                                });
                            } else if (Notification.permission == 'denied') {
                                // do nothing
                            } else if (Notification.permission == 'default') {
                                was_show = true;
                                Notification.requestPermission().then(function(result) {
                                    if(result != 'default') {
                                        // If it's okay
                                        that.hidePanel();
                                    }
                                    if(result == 'granted') {
                                        // If it's okay let's create a notification
                                        var notification = new Notification(title, {
                                            body: msg,
                                            icon: icon ? icon : '/assets/img/logo/favicon.png'
                                        });
                                    } else {
                                        // Show default insite notification
                                        gmaback.js.showMessage(msg, 10000);
                                    }
                                });
                            }
                        }
                        if(!was_show) {
                            gmaback.js.showMessage(msg, 10000);
                        }
                    }
                }
            }
        }
    },
    guid: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },
    morph: function (n, f1, f2, f5) {
        n = Math.abs(parseInt(n)) % 100;
        if (n > 10 && n < 20)
            return f5;
        n = n % 10;
        if (n > 1 && n < 5)
            return f2;
        if (n == 1)
            return f1;
        return f5;
    },
    printDuration: function(duration) {
        var hours = parseInt(duration / 3600, 10);
        var minutes = parseInt(duration / 60, 10);
        var seconds = duration % 60;
        var duration = [];
        if (hours > 0) {
            var hours_str = hours + this.morph(hours, getApp().lang['e_hours'], getApp().lang['e_hours_2'], getApp().lang['e_hours_3']);
            duration.push(hours_str);
        }
        if (minutes > 0) {
            var minutes_str = minutes + ' ' + ' ' + this.morph(minutes, getApp().lang['e_minutes'], getApp().lang['e_minutes_2'], getApp().lang['e_minutes_3']);
            duration.push(minutes_str);
        }
        if (seconds > 0) {
            var seconds_str = seconds + ' ' + this.morph(seconds, getApp().lang['e_seconds'], getApp().lang['e_seconds_2'], getApp().lang['e_seconds_3']);
            duration.push(seconds_str);
        }
        return getApp().lang['call_complete'] + ' ' + duration.join(' ');
    },
    HideDropdown: {
        list: [],
        setup: function(){
            var that = this;
            // Hide dropdown menu when click on outside active element
            $(document).off('mousedown').on('mousedown', function (e) {
                for(var item of that.list) {
                    var container = $(item.selector);
                    if (!container.is(e.target) // if the target of the click isn't the container...
                        &&
                        container.has(e.target).length === 0) // ... nor a descendant of the container
                    {
                        if(item.callback instanceof Function) {
                            item.callback(item.selector, e);
                        } else {
                            container.removeClass('active');
                        }
                    }
                }
            });
        },
        add: function(selector, callback)  {
            this.list.push({selector: selector, callback: callback});
        }
    },
    HTMLToText: function (elem, tab) {
        if (typeof tab == 'undefined') {
            tab = 0;
        }
        var t = '\t';
        var outText = '';
        for (var i = 0; i < elem.childNodes.length; i++) {
            var el = elem.childNodes[i];
            var nodeName = el.nodeName.toLowerCase();
            console.log('elem.childNodes', el, nodeName, el.nodeType);
            if (el.nodeType === 1 && nodeName == 'br') {
                outText += '\n';
            } else {
                if (nodeName == 'div') {
                    outText += '\n';
                }
                if (el.nodeType === 3 || el.nodeType === 4) {
                    outText += el.data.replace('<br>', '\n');
                } else if (nodeName == 'img') {
                    var src = el.getAttribute('src');
                    src = src.replace(getApp().domains.static, '');
                    if (src.indexOf('/assets/img/pixel.gif') >= 0) {
                        outText += el.getAttribute('alt');
                    } else if (src.indexOf('/assets/smiles') === 0 || src.indexOf('/assets/smiles') === 1) {
                        outText += ':' + el.getAttribute('alt') + ':';
                    }
                } else {
                    var t = Routines.HTMLToText(el, tab + 1);
                    outText += t;
                }
            }
        }
        return outText;
    },
    /**
    * PickaDate wrapper with pretty year selector
    * @type Object
    */
    getDefDatepicker: function() {
        return defDatepicker = {
            format: 'dd.mm.yyyy',
            monthsFull: [
                getApp().lang.jan_long,
                getApp().lang.fev_long,
                getApp().lang.mar_long,
                getApp().lang.apr_long,
                getApp().lang.may_long,
                getApp().lang.jun_long,
                getApp().lang.jul_long,
                getApp().lang.avg_long,
                getApp().lang.sep_long,
                getApp().lang.oct_long,
                getApp().lang.nov_long,
                getApp().lang.dec_long
            ],
            monthsShort: [
                getApp().lang.jan,
                getApp().lang.fev,
                getApp().lang.mar,
                getApp().lang.apr,
                getApp().lang.may,
                getApp().lang.jun,
                getApp().lang.jul,
                getApp().lang.avg,
                getApp().lang.sep,
                getApp().lang.oct,
                getApp().lang.nov,
                getApp().lang.dec
            ],
            weekdaysFull: [
                getApp().lang.sun_long,
                getApp().lang.mon_long,
                getApp().lang.tue_long,
                getApp().lang.wen_long,
                getApp().lang.thur_long,
                getApp().lang.fri_long,
                getApp().lang.sat_long
            ],
            weekdaysShort: [
                getApp().lang.sun,
                getApp().lang.mon,
                getApp().lang.tue,
                getApp().lang.wen,
                getApp().lang.thur,
                getApp().lang.fri,
                getApp().lang.sat,
            ],
            weekdaysLetter: [
                getApp().lang.sun,
                getApp().lang.mon,
                getApp().lang.tue,
                getApp().lang.wen,
                getApp().lang.thur,
                getApp().lang.fri,
                getApp().lang.sat,
            ],
            // Buttons
            today: getApp().lang.today,
            clear: getApp().lang.clear,
            close: getApp().lang.close,
            selectYears: 50,
            closeOnSelect: true,
            // Accessibility labels
            labelMonthNext: getApp().lang.next_month,
            labelMonthPrev: getApp().lang.prev_month,
            labelMonthSelect: getApp().lang.select_month,
            labelYearSelect: getApp().lang.select_year,
            onSet: function (e) {
                if (e.select) {
                    setTimeout(this.close, 0);
                } else {}
            }
        };
    }
};

/**
* Scrollify v0.1
*/
$.fn.scrollify = function(options) {

    var $list = this;

	if(!isDesktop()) {	
		var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	    if(isFirefox) {
	        return false;
	    }
	}

    if(typeof options === 'string') {
        if(options == 'off') {
            if($list.hasClass('scrollify')) {
                $list.find('._thumb').css({height: '0px', top: '0px'});
                return;
            }
        }
    }

    if(!$list.hasClass('scrollify')) {
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
            autohide: true
        }, options );
        $list.addClass('scrollify');
        if(!settings.autohide) {
            $list.addClass('scrollify-noautohide');
        }
        $list.data('settings', settings);
        // scroll resize DOMSubtreeModified mouseover touchmove wheel DOMMouseScroll mousewheel orientationchange touchend touchstart transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd
        $list.on('scroll resize DOMSubtreeModified mouseover', function(e){
        //$list.on('scroll resize DOMSubtreeModified', function(e){
            var el = $(this);
            var that = $(this);
            var $thumb = el.find('._thumb');
            if(e.type == 'DOMSubtreeModified') {
                var spt = that.data('scrollify_performance_timeout');
                if(spt === undefined || spt == null) {
                    that.data('scrollify_performance_timeout', setTimeout(function(){
                        $thumb.css({top: '0px'});
                        that.data('scrollify_performance_timeout', 'nope');
                        that.trigger('DOMSubtreeModified');
                    }, 300));
                    // console.log('DOMSubtreeModified');
                } else if (spt == 'nope') {
                    that.data('scrollify_performance_timeout', null);
                } else {
                    return;
                }
            }
            el = el.get(0);
            var scrollHeight = el.scrollHeight;
            var height = $(this).outerHeight();
            if(scrollHeight - height < 1) {
                $thumb.css({top: '0px'});
                if(!$list.hasClass('scrollify-noscroll')) {
                    $list.addClass('scrollify-noscroll');
                }
            } else {
                if($list.hasClass('scrollify-noscroll')) {
                    $list.removeClass('scrollify-noscroll');
                }
                var scrollTop = $(this).scrollTop();
                var thumb_height = Math.round(height / scrollHeight * height);
                $thumb.height(thumb_height + 'px');
                var vpos = scrollTop + scrollTop / (scrollHeight - height) * (height - thumb_height - 6);
                $thumb.css({top: vpos + 'px'});
            }
        });
        $list.append('<div class="_thumb"></div>').trigger('scroll');
    }
    return this;
}

// музыка, звуки http://soundimage.org/looping-music/
var audio = {
    plop2: new PlayerTrack({
        // incomming chat message
        src: ['./assets/sound/UI_Quirky7.mp3'],
        volume: 1,
        html5: true
    }),
    ring: new PlayerTrack({
        // incomming_call
        src: ['./assets/sound/ring.mp3'],
        volume: 1,
        loop: true,
        html5: true
    })
};

// Browser
function getBrowser() {
    var isIE = false;
    var isOpera = false;
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        isIE = true;
    }
    return {
        mozilla: false,
        iphone: false,
        ipad: false,
        opera: isOpera,
        msie: ('\v' == 'v') || isIE,
        version: 0,
    };
}
var browser = getBrowser();

function sprintf() {
    //  discuss at: http://locutus.io/php/sprintf/
    // original by: Ash Searle (http://hexmen.com/blog/)
    // improved by: Michael White (http://getsprink.com)
    // improved by: Jack
    // improved by: Kevin van Zonneveld (http://kvz.io)
    // improved by: Kevin van Zonneveld (http://kvz.io)
    // improved by: Kevin van Zonneveld (http://kvz.io)
    // improved by: Dj
    // improved by: Allidylls
    //    input by: Paulo Freitas
    //    input by: Brett Zamir (http://brett-zamir.me)
    //   example 1: sprintf("%01.2f", 123.1)
    //   returns 1: '123.10'
    //   example 2: sprintf("[%10s]", 'monkey')
    //   returns 2: '[    monkey]'
    //   example 3: sprintf("[%'#10s]", 'monkey')
    //   returns 3: '[####monkey]'
    //   example 4: sprintf("%d", 123456789012345)
    //   returns 4: '123456789012345'
    //   example 5: sprintf('%-03s', 'E')
    //   returns 5: 'E00'
    var regex = /%%|%(\d+\$)?([-+'#0 ]*)(\*\d+\$|\*|\d+)?(?:\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g
    var a = arguments
    var i = 0
    var format = a[i++]
    var _pad = function (str, len, chr, leftJustify) {
        if (!chr) {
            chr = ' '
        }
        var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0).join(chr)
        return leftJustify ? str + padding : padding + str
    }
    var justify = function (value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
        var diff = minWidth - value.length
        if (diff > 0) {
            if (leftJustify || !zeroPad) {
                value = _pad(value, minWidth, customPadChar, leftJustify)
            } else {
                value = [
                    value.slice(0, prefix.length),
                    _pad('', diff, '0', true),
                    value.slice(prefix.length)
                ].join('')
            }
        }
        return value
    }
    var _formatBaseX = function (value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
        // Note: casts negative numbers to positive ones
        var number = value >>> 0
        prefix = (prefix && number && {
            '2': '0b',
            '8': '0',
            '16': '0x'
        }[base]) || ''
        value = prefix + _pad(number.toString(base), precision || 0, '0', false)
        return justify(value, prefix, leftJustify, minWidth, zeroPad)
    }
    // _formatString()
    var _formatString = function (value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
        if (precision !== null && precision !== undefined) {
            value = value.slice(0, precision)
        }
        return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar)
    }
    // doFormat()
    var doFormat = function (substring, valueIndex, flags, minWidth, precision, type) {
        var number, prefix, method, textTransform, value
        if (substring === '%%') {
            return '%'
        }
        // parse flags
        var leftJustify = false
        var positivePrefix = ''
        var zeroPad = false
        var prefixBaseX = false
        var customPadChar = ' '
        var flagsl = flags.length
        var j
        for (j = 0; j < flagsl; j++) {
            switch (flags.charAt(j)) {
                case ' ':
                    positivePrefix = ' '
                    break
                case '+':
                    positivePrefix = '+'
                    break
                case '-':
                    leftJustify = true
                    break
                case "'":
                    customPadChar = flags.charAt(j + 1)
                    break
                case '0':
                    zeroPad = true
                    customPadChar = '0'
                    break
                case '#':
                    prefixBaseX = true
                    break
            }
        }
        // parameters may be null, undefined, empty-string or real valued
        // we want to ignore null, undefined and empty-string values
        if (!minWidth) {
            minWidth = 0
        } else if (minWidth === '*') {
            minWidth = +a[i++]
        } else if (minWidth.charAt(0) === '*') {
            minWidth = +a[minWidth.slice(1, -1)]
        } else {
            minWidth = +minWidth
        }
        // Note: undocumented perl feature:
        if (minWidth < 0) {
            minWidth = -minWidth
            leftJustify = true
        }
        if (!isFinite(minWidth)) {
            throw new Error('sprintf: (minimum-)width must be finite')
        }
        if (!precision) {
            precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined
        } else if (precision === '*') {
            precision = +a[i++]
        } else if (precision.charAt(0) === '*') {
            precision = +a[precision.slice(1, -1)]
        } else {
            precision = +precision
        }
        // grab value using valueIndex if required?
        value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++]
        switch (type) {
            case 's':
                return _formatString(value + '', leftJustify, minWidth, precision, zeroPad, customPadChar)
            case 'c':
                return _formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad)
            case 'b':
                return _formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
            case 'o':
                return _formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
            case 'x':
                return _formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
            case 'X':
                return _formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad).toUpperCase()
            case 'u':
                return _formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
            case 'i':
            case 'd':
                number = +value || 0
                // Plain Math.round doesn't just truncate
                number = Math.round(number - number % 1)
                prefix = number < 0 ? '-' : positivePrefix
                value = prefix + _pad(String(Math.abs(number)), precision, '0', false)
                return justify(value, prefix, leftJustify, minWidth, zeroPad)
            case 'e':
            case 'E':
            case 'f': // @todo: Should handle locales (as per setlocale)
            case 'F':
            case 'g':
            case 'G':
                number = +value
                prefix = number < 0 ? '-' : positivePrefix
                method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())]
                textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2]
                value = prefix + Math.abs(number)[method](precision)
                return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]()
            default:
                return substring
        }
    }
    return format.replace(regex, doFormat)
}

/**
* StorageWrapperProto
*/
var StorageWrapperProto = function (underlyingStorage) {
    return {
        key: function (n) {
            return underlyingStorage.key(n);
        },
        clear: function () {
            underlyingStorage.clear();
        },
        getItem: function (key) {
            try {
                return underlyingStorage.getItem(key);
            } catch (e) {
                window.console.error("Error reading from storage for key = " + key + ": " + e);
            }
        },
        setItem: function (key, value) {
            try {
                underlyingStorage.setItem(key, value);
            } catch (e) {
                window.console.error("Error writing to the storage for key = " + key + ": " + e);
            }
        },
        removeItem: function (key) {
            underlyingStorage.removeItem(key);
        }
    };
};
window.localStorageWrapper = Object.create(new StorageWrapperProto(window.sessionStorage ? window.localStorage : {}));

function cancelEvent(event) {
    event = (event || window.event);
    if (!event) return false;
    while (event.originalEvent) {
        event = event.originalEvent;
    }
    if (event.preventDefault) event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();
    if (event.stopImmediatePropagation) event.stopImmediatePropagation();
    event.cancelBubble = true;
    event.returnValue = false;
    return false;
}

function re(el) {
    el = ge(el);
    if (el && el.parentNode) el.parentNode.removeChild(el);
    return el;
}

function setContentEditableFocus(element_id) {
    var editable = document.getElementById(element_id);
    Emoji.editableFocus(editable, false, true);
}

function extractTextWithWhitespace(elems) {
    var ret = '',
        elem;
    for (var i = 0; elems[i]; i++) {
        elem = elems[i];

        // Get the text from text nodes and CDATA nodes
        if (elem.nodeType === 3 || elem.nodeType === 4) {
            ret += elem.nodeValue + "\n";

            // Traverse everything else, except comment nodes
        } else if (elem.nodeType !== 8) {
            ret += extractTextWithWhitespace2(elem.childNodes);
        }
    }
    return ret;
}

/**
 * scWizard
 */
(function ($) {
    /**
     * $('#wizard-payout').scWizard('init', {});
     * $('#wizard-payout').scWizard('showPage', 1);
     */
    var methods = {
        init: function (options) {
            // Создаём настройки по-умолчанию, расширяя их с помощью параметров, которые были переданы
            var settings = $.extend({
                current_page: 1,
                'onback': function (index) {
                    // do something a bit more specific here
                },
                'onnext': function (index) {
                    // do something a bit more specific here
                },
            }, options);
            // $(this).find('[data-scwizard-action]').addClass('active');
            return this.each(function () {

                /*$(this).find('input[type="text"],input[type="number"]').unbind('onchange').on('onchange', function (e) {
                 var is_dirty = $(this).val().trim().length > 0;
                 console.log(is_dirty);
                 if(is_dirty) {
                 $(this).closest('.input-field').addClass('is_dirty');
                 $(this).closest('.input-field2').addClass('is_dirty');
                 } else {
                 $(this).closest('.input-field').removeClass('is_dirty');
                 $(this).closest('.input-field2').removeClass('is_dirty');
                 }
                 });*/

                $(this).find('[data-scwizard-action]').on('click', function (e) {
                    if ($(this).hasClass('disabled')) {
                        return false;
                    }
                    var action = $(this).data('scwizard-action');
                    if (action == 'next') {
                        var $page = $(this).closest('[data-scwizard-page]');
                        var index = $page.data('scwizard-page') + 1;
                        if (settings.onnext instanceof Function) {
                            var new_index = settings.onnext(index);
                            if (new_index === false) {
                                return false;
                            } else if (new_index === true) {
                                // do nothing
                            } else if (new_index % 1 === 0) {
                                index = new_index;
                            }
                        }
                        // показать старницу
                        var $new_page = $(this).closest('.scwizard').find('[data-scwizard-page=' + index + ']');
                        // alert(index + ' ...' + $new_page.html());
                        if ($new_page.hasClass('popup-form')) {
                            CustomUI.showForm($new_page.attr('id'), false);
                        } else {
                            $(this).closest('.scwizard').find('[data-scwizard-page]').removeClass('active'); // .stop().removeClass('active').hide();
                            $new_page.addClass('active'); // .stop().fadeIn();
                            CustomUI.centerForms();
                        }
                    } else if (action == 'back') {
                        var $page = $(this).closest('[data-scwizard-page]');
                        if ($page.hasClass('popup-form')) {
                            CustomUI.hideForm('');
                        } else {
                            $(this).closest('.scwizard').find('[data-scwizard-page]').removeClass('active'); // .stop().hide();
                        }
                        var index = $page.data('scwizard-page') - 1;
                        var index2 = $(this).data('scwizard-backto');
                        if (typeof index2 != 'undefined') {
                            index = index2;
                        }
                        if (settings.onback instanceof Function) {
                            if (settings.onback(index) === false) {
                                return false;
                            }
                        }
                        // показать старницу
                        $(this).closest('.scwizard').find('[data-scwizard-page]:eq(' + index + ')').addClass('active'); // .stop().fadeIn();
                        CustomUI.centerForms();
                    }
                    // $('html, body').animate({scrollTop: $(this).closest('.scwizard').offset().top});
                    return false;
                });
            });
        },
        // показать старницу
        showPage: function (index) {
            return this.each(function () {
                $(this).closest('.scwizard').find('[data-scwizard-page]').removeClass('active'); // .stop().hide();
                $(this).closest('.scwizard').find('[data-scwizard-page]:eq(' + index + ')').addClass('active'); // .stop().fadeIn();
            });
        }
    };

    $.fn.scWizard = function (method) {
        // логика вызова метода
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.scWizard');
        }
    };

})(jQuery);

/**
 * Add JQuery functions
 */
$.fn.getScope = function () {
    // save a reference to a cloned element that can be measured
    // var scope = angular.element(this).controller;
    var resp = angular.element(this).scope();
    return resp;
};

$.fn.getHiddenOffsetWidth = function () {
    // save a reference to a cloned element that can be measured
    var $hiddenElement = $(this).clone().appendTo('body');
    // calculate the width of the clone
    var width = $hiddenElement.outerWidth();
    // remove the clone from the DOM
    $hiddenElement.remove();
    return width;
};

/**
* PickaDate wrapper with pretty year selector
* @type Object
*/
var gmaDatePicker = {
    createDatepicker: function($input) {
        return this.initDatePicker($input.attr('id'), {
            format: 'yyyy-mm-dd'
        });
    },
    initDatePicker: function (input_id, over_options) {
        var options = {
            format: 'yyyy-mm-dd',
            maxDate: new Date(),
            cur_: 'yyyy-mm-dd',
            onStart: function () {
                var date = new Date();
                this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
            },
            // onRender: function(){
            onDraw: function (e) {
                var select = e.calendars[0];
                var year = parseInt(select.year);
                var $picker_container = e.$modalEl.find('.datepicker-calendar-container');
                var sz = e.$modalEl.find('.datepicker-calendar-container .gma_picker_year').length;
                if (sz == 0) {
                    $picker_container.append('<ul class="gma_picker_year "><li></li></ul>');
                    var $gma_picker_year = $picker_container.find('ul.gma_picker_year');
                    $($gma_picker_year.get(0)).data('component', e);
                }
                $picker_container.find('.select-year').html('<div onclick="return gmaDatePicker.showSelectYear(this);" class="gma_picker gma_picker_select_year">' + year + '</div>');
                var $gma_picker_year = $picker_container.find('ul.gma_picker_year');
                gmaDatePicker.listYear(year, $gma_picker_year, select);
            }
        };
        var old_picker = $('#' + input_id).data('picker');
        if(old_picker) {
            return false;
            // @todo Not working
            // old_picker[0].destroy();
        }
        options = $.extend(Routines.getDefDatepicker(), options);
        options = $.extend(options, over_options);
        var picker = M.Datepicker.init($('#' + input_id), options);
        // var picker = $('#' + input_id).pickadate(options);
        $('#' + input_id).data('picker', picker);
        return picker;
    },
    setDate: function (el, date_obj, new_year) {
        var $parent = $(el).closest('ul.gma_picker_year');
        var comp = $parent.data('component');
        var year = new_year;
        var date = 1; // date_obj.date;
        if (comp.date) {
            date = comp.date.getDate();
        }
        var month = date_obj.month;
        if (date > 28) {
            date = 28;
        }
        var d = new Date(year, month, date);
        comp.setDate(d);
        // var comp = $parent.data('component');
        // comp.set('select', [year, month, date]);
        // comp.$node.pickadate('render');
        this.hideSelectYear($parent);
    },
    getYear: function () {
        year = new Date().getYear();
        if (year < 1900) {
            year += 1900;
        }
        return year;
    },
    listYear: function (year, $gma_picker_year, cur) {
        var year_range = 33;
        var current_year = cur.year;
        var date = cur.date;
        var month = cur.month;
        var current_as_string = '{date: ' + date + ', month: ' + month + ', year: ' + current_year + '}';
        var start_range_index = Math.floor(year_range / 2);
        var start_index = -start_range_index;
        var end_index = -start_range_index + year_range;
        var hideNext = false;
        if (year + end_index >= this.getYear()) {
            year = this.getYear() - Math.floor(year_range / 2);
            hideNext = true;
        }
        var first_year = (year - year_range);
        var year_list = '';
        year_list += '<li onclick="return gmaDatePicker.listYear(' + first_year + ', $(this).closest(\'ul.gma_picker_year\'), ' + current_as_string + ');"><i class="fa fa-chevron-left" aria-hidden="true"></i></li>';
        for (var i = start_index; i < end_index; i++) {
            var cl = '';
            if ((year + i) == current_year) {
                cl = 'current';
            }
            year_list += '<li onclick="return gmaDatePicker.setDate(this, ' + current_as_string + ', ' + (year + i) + ');" class="' + cl + '">' + (year + i) + '</li>';
        }
        if (!hideNext) {
            year_list += '<li onclick="return gmaDatePicker.listYear(' + (year + year_range) + ', $(this).closest(\'ul.gma_picker_year\'), ' + current_as_string + ');"><i class="fa fa-chevron-right" aria-hidden="true"></i></li>';
        }
        $gma_picker_year.html(year_list);
        return false;
    },
    showSelectYear: function (el) {
        var $parent = $(el).closest('.datepicker-calendar-container');
        $parent.find('.datepicker-table-wrapper,.datepicker-footer,.pika-single').hide();
        $parent.find('.gma_picker_year').addClass('active');
    },
    hideSelectYear: function ($el) {
        $parent = $el.closest('.datepicker-calendar-container');
        $parent.find('.datepicker-table-wrapper,.datepicker-footer,.pika-single').show();
        $parent.find('.gma_picker_year').removeClass('active');
    }
};

var QueryString = function () {
    // This function is anonymous, is executed immediately and 
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        // If first entry with this name
        if (typeof query_string[pair[0]] === 'undefined') {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === 'string') {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

/**
 * JSCropHelper
 */
var JSCropHelper = function (input_id, title, description) {
    var that = this;
    var form_id = 'form-' + input_id;
    var $form = null;
    var f = document.getElementById(form_id);
    if (f) {
        $form = $(f);
    } else {
        $form = $('#form-jscrop').clone();
        $form.attr('id', form_id);
        $form.appendTo('body');
    }
    $form.data('title', title);
    $form.data('JSCropHelper', this);
    this.form = $form;
    this.resp = {
        size: {
           // x: 0,
           // y: 0,
            w: 0,
            h: 0
        },
        image: null,
        file: null
    };
    // this.api = null; // thirdparty crop api
    this.api = { // thirdparty crop api
        img: $form.find('.msgCrop img')[0],
        parent: $form.find('.msgCrop')[0],
        options: {
            onChange: function (c) {}
        },
        drag: false,
        editorW: 0,
        editorH: 0,
        w: 0,
        h: 0,
        x: 0,
        y: 0,
        scale: 0,
        min_scale: 0,
        coordX: 0,
        coordY: 0,
        offsetX: 0,
        offsetY: 0,
        init: function (options) {
            // console.log('init', this.x);
            var that = this;
            var w = $(that.parent).width();
            var h = $(that.parent).height();
            that.options = options;
            that.editorW = w;
            that.editorH = h;
            var r = w / 2.05;
            // $(this.parent).append('<svg class="circle_mask"><defs><mask id="mask" x="0" y="0" width="500" height="361" style="position: absolute;"><rect x="0" y="0" width="500" height="361" fill="rgba(255, 255, 255, 0.7)"></rect><circle cx="250" cy="180.5" r="168" style="position: absolute;"></circle></mask></defs><rect x="0" y="0" width="500" height="361" mask="url(#mask)" fill-opacity="0.7" style="position: absolute;"></rect></svg>');
            $(this.parent).find('.circle_mask').remove();
            $(this.parent).append('<svg class="circle_mask"><defs><mask id="mask-' + options.form_id + '" x="0" y="0" width="' + w + '" height="' + h + '" style="position: absolute;"><rect x="0" y="0" width="' + w + '" height="' + h + '" fill="rgba(255, 255, 255, 0.7)"></rect><circle cx="' + w / 2 + '" cy="' + h / 2 + '" r="' + r + '" style="position: absolute;"></circle></mask></defs><rect x="0" y="0" width="' + w + '" height="' + h + '" mask="url(#mask-' + options.form_id + ')" fill-opacity="0.7" style="position: absolute;"></rect></svg>');
            $(this.img).on('mousedown touchstart', function (e) {
                $(that.img).css({
                    transition: ''
                });
                if (e.preventDefault) e.preventDefault();
                // calculate event X, Y coordinates
                that.offsetX = e.clientX || e.targetTouches[0].pageX;
                that.offsetY = e.clientY || e.targetTouches[0].pageY;
                that.coordX = that.x;
                that.coordY = that.y;
                that.drag = true;
            });
            $(this.img).on('mouseup touchend', function (e) {
                that.drag = false;
            });
            $(this.img).on('mousemove touchmove', function (e) {
                if (!that.drag) {
                    return false;
                }
                that.x = that.coordX + (e.clientX || e.targetTouches[0].pageX) - that.offsetX;
                that.y = that.coordY + (e.clientY || e.targetTouches[0].pageY) - that.offsetY;
                that.applyTransform();
            });
            // Zoom
            $(that.parent).find('.scale_plus').on('click', function (e) {
                var scale = that.scale + .05;
                scale = Math.min(scale, 2);
                scale = Math.max(scale, that.min_scale);
                // view center coord in original image size
                var px = ((that.x - that.editorW / 2) * -1) / that.scale / that.w;
                var py = ((that.y - that.editorH / 2) * -1) / that.scale / that.h;
                that.x = that.w * scale * px * -1 + that.editorW / 2;
                that.y = that.h * scale * py * -1 + that.editorH / 2;
                that.scale = scale;
                $(that.img).css({
                    transition: 'transform 200ms'
                });
                that.applyTransform();
            });
            $(that.parent).find('.scale_minus').on('click', function (e) {
                var scale = that.scale - .05;
                scale = Math.max(scale, that.min_scale);
                // view center coord in original image size
                var px = ((that.x - that.editorW / 2) * -1) / that.scale / that.w;
                var py = ((that.y - that.editorH / 2) * -1) / that.scale / that.h;
                that.x = that.w * scale * px * -1 + that.editorW / 2;
                that.y = that.h * scale * py * -1 + that.editorH / 2;
                that.scale = scale;
                $(that.img).css({
                    transition: 'transform 200ms'
                });
                that.applyTransform();
            });

            function onWheel(e) {
                e = e || window.event;
                // wheelDelta не дает возможность узнать количество пикселей
                var delta = e.deltaY || e.detail || e.wheelDelta;
                if (delta < 0) {
                    $(that.parent).find('.scale_plus').trigger('click');
                } else if (delta > 0) {
                    $(that.parent).find('.scale_minus').trigger('click');
                }
                e.preventDefault ? e.preventDefault() : (e.returnValue = false);
            }
            if (that.parent.addEventListener) {
                if ('onwheel' in document) {
                    // IE9+, FF17+, Ch31+
                    that.parent.addEventListener("wheel", onWheel);
                } else if ('onmousewheel' in document) {
                    // устаревший вариант события
                    that.parent.addEventListener("mousewheel", onWheel);
                } else {
                    // Firefox < 17
                    that.parent.addEventListener("MozMousePixelScroll", onWheel);
                }
            } else { // IE8-
                that.parent.attachEvent("onmousewheel", onWheel);
            }
        },
        applyTransform: function () {
            var that = this;
            if (that.x > 0) {
                that.x = 0;
            }
            if (that.y > 0) {
                that.y = 0;
            }
            if (that.x + that.w * that.scale < that.editorW) {
                that.x = that.editorW - that.w * that.scale;
            }
            if (that.y + that.h * that.scale < that.editorH) {
                that.y = that.editorH - that.h * that.scale;
            }
            // console.log(that.x+'x'+that.y);
            var transform = 'translate(' + that.x + 'px, ' + that.y + 'px) scale(' + that.scale + ')';
            var change = {
                x: parseInt((that.x * -1) / that.scale),
                y: parseInt((that.y * -1) / that.scale),
                w: that.editorW / that.scale,
                h: that.editorH / that.scale
            };
            that.options.onChange(change);
            $(that.img).css({
                transform: transform
            });
        },
        destroy: function () {},
        open: function (img) {
            var that = this;
            that.w = img.width;
            that.h = img.height;
            var mn = Math.min(that.w, that.h);
            that.min_scale = that.scale = that.editorW / mn;
            that.x = (that.editorW - that.w * that.scale) / 2;
            that.y = (that.editorH - that.h * that.scale) / 2;
            that.applyTransform();
        }
    };
    this.api.init({
        form_id: form_id,
        onChange: function (c) {
            return that.change(c);
        }
    });
    this.callback = null; // success callback function
    this.$file_input = null;
    this.options = {
        form_id: form_id, // 'form-jscrop',
        target_id: '.target_avatar',
        minSize: [16, 16],
        borderWidth: 1 // in percent
    };
    description = typeof description == 'undefined' ? getApp().lang.select_avatar_desc : description;
    $form.find('.select_avatar_desc').text(description);
    // img in crop form
    this.$target = $form.find(this.options.target_id).get(0); // document.getElementById(this.target_id);
    this.$file_input = $('#' + input_id);
    this.$file_input.change(function () {
        return that.processFileInput(this);
    });
    return this;
};

JSCropHelper.prototype.init = function () {
    var that = this;
    this.$target.onload = function () {
        console.log('onload2', that.$target);
        CustomUI.showForm(that.options.form_id);
        $('#' + that.options.form_id).data('onclose', function () {
            return that.destroy(true);
        });
        that.api.open(that.$target);
        CustomUI.centerForms();
    };
    return this;
};

JSCropHelper.prototype.setTarget = function (target) {
    this.$target = target;
    return this;
};

JSCropHelper.prototype.setCallback = function (callback) {
    this.callback = callback;
    return this;
};

// input[type="file"] onchange event
JSCropHelper.prototype.processFileInput = function (input) {
    this.resp.size = {
        x: 0,
        y: 0,
        w: 0,
        h: 0
    };
    this.resp.image = null;
    var that = this;
    console.log('change');
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log('onload1');
            that.resp.image = e.target.result;
            $(that.$target).attr('src', that.resp.image);
        };
        that.resp.file = input.files[0];
        reader.readAsDataURL(that.resp.file);
    }
    return false;
};

// thirdparty crop api change selected area calback
JSCropHelper.prototype.change = function (c) {
    this.resp.size = {
        x: parseInt(c.x),
        y: parseInt(c.y),
        w: parseInt(c.w),
        h: parseInt(c.h)
    };
};

// apply crop area and run callback function
JSCropHelper.prototype.cancel = function () {
    CustomUI.hideForm();
    if (typeof this.callback === 'function') {
        this.callback(false, this.resp);
    }
    return false;
};

// apply crop area and run callback function
JSCropHelper.prototype.finish = function () {
    try {
        var hideForm = true;
        $('#' + this.options.form_id).data('onclose', null);
        if (typeof this.callback === 'function') {
            hideForm = false;
            CustomUI.hideForm();
            this.callback(true, this.resp);
            this.destroy(hideForm);
            return this.api.destroy();
        }
        var $form = this.$file_input.closest('form');
        var crop_inp = $form.find('[name="crop"]');
        if (crop_inp.length == 0) {
            $form.append('<input type="hidden" name="crop" />');
            crop_inp = $form.find('[name="crop"]');
        }
        console.log(JSON.stringify(this.resp.size));
        crop_inp.val(JSON.stringify(this.resp.size));
        $form.submit();
        this.destroy(hideForm);
        this.api.destroy();
        $(this.$target).attr('src', ''); //.css({width: 'auto', height: 'auto'});
    } catch (e) {
        console.log(e);
    }
    return CustomUI.hideForm();
};

// очистить форму обрезки
JSCropHelper.prototype.destroy = function (hideForm) {
    console.log('destroy', hideForm);
    this.api.destroy();
    $(this.$target).attr('src', ''); //.css({width: 'auto', height: 'auto'});
    this.$file_input.val('');
    if (hideForm) {
        return CustomUI.hideForm();
    }
    return false;
};

// кнопка выбора другого изображения
JSCropHelper.prototype.selectAnotherPicture = function () {
    this.destroy(true);
    this.$file_input.val('').trigger('click');
    return false;
};