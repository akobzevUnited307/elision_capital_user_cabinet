'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', '$state', 'utilsSrvc', '$timeout', 'eventsSrvc'];

    var controller = function($scope, $state, utilsSrvc, $timeout, eventsSrvc) {
        $scope.loading = true;
        $scope.events = utilsSrvc.events.init();
    };

    controller.$inject = injectParams;

    app.controller('eventCtrl', controller);

});