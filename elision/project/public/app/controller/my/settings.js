'use strict';

define(['app', 'service/utils', 'settingsSrvc', 'filter/highlite_search'], function (app) {

    var injectParams = ['$scope', 'utilsSrvc', '$timeout', '$templateCache', '$state', 'settingsSrvc'];
    var mySettingsCtrl = function ($scope, utilsSrvc, $timeout, $templateCache, $state, settingsSrvc) {

        var msg1_theme_current = getApp().settings.values.theme;

        $scope.app = getApp();

        // Мои фотки
        $scope.myphotos = {
            list: [],
            newphotos: {
                list: [],
                add: function(item){
                    this.list.push(item);
                },
                remove: function(code){
                    for (var i = 0; i < this.list.length; i++) {
                        var file = this.list[i];
                        if(file.result.code == code) {
                            this.list.splice(i, 1);
                            break;
                        }
                    }
                },
                reset: function(){
                    this.list = [];
                }
            },
            resetNewPhotos: function(){
                this.newphotos.reset();
            },
            eventAboutNewPhotos: function(){
                if(this.newphotos.list.length > 0) {
                    var list = [];
                    for(var p of this.newphotos.list) {
                        list.push(p.result);
                    }
                    utilsSrvc.api.call('/app/photos/eventaboutnewphotos/', {list: list}, function(result){});
                }
            },
            showForm: function($event){
                var that = this;
                that.resetNewPhotos();
                $event.preventDefault();
                return CustomUI.showForm('form-myphotos', true, true, {
                    onclose: function(e){
                        that.eventAboutNewPhotos();
                    }
                });
            },
            updateList: function(list){
                // $('#myphotos').scrollify();
                $('#form-myphotos .photo_list').scrollify();
                this.list = list;
            }
        };

        // settings
        $scope.settings = {
            form: {
                lang_code: localStorage.getItem('lang')
            },
            edit: {
                loading: false,
                dictionary: {
                    dating_purpose_: [],
                    gender: [],
                    language: [],
                },
                lang: {
                    q: '',
                    itemIsVisible: function(item) {
                        return this.q.trim() == '' || item.title.toLowerCase().indexOf(this.q.toLowerCase()) >= 0;
                    },
                    add: function(item) {
                        this.q = '';
                        $scope.settings.edit.form.language_list.push(item);
                    },
                    remove: function(index, item) {
                        $scope.settings.edit.form.language_list.splice(index, 1);
                    },
                    getList: function(){
                        var list = [];
                        for(var item of $scope.settings.edit.dictionary.language) {
                            var exists = false;
                            for(var o of $scope.settings.edit.form.language_list) {
                                if(o.id == item.id) {
                                    exists = true;
                                }
                            }
                            if(!exists) {
                                list.push(item);
                            }
                        }
                        return list;
                    }
                },
                hobby: {
                    instance: false,
                    add: function($event){
                        $event.preventDefault();
                        var tag = this.instance.input.value.trim();
                        if(tag.length == 0) {
                            return false;
                        }
                        $scope.settings.edit.form.hobby_list.push({
                            tag: tag
                        });
                        this.instance.input.value = '';
                        // this.save();
                    },
                    remove: function($index, chip) {
                        $scope.settings.edit.form.hobby_list.splice($index, 1);
                        // this.save();
                    },
                    init: function(){
                        var that = this;
                        if(!that.instance) {
                            var container = $('#form-saveprofile .hobby');
                            that.instance = {
                                container: container,
                                input: container.find('input')[0],
                            };
                            console.log('that.instance', that.instance);
                            that.instance.input.addEventListener('keypress', function (e) {
                                if (e.keyCode === 13) {
                                    $scope.$apply(function(){
                                        that.add(e);
                                    });
                                }
                            });
                        }
                    }
                },
                form: {
                    fio: '',
                    geo: '',
                    dating_purpose_id: '',
                    birthday: '',
                    gender_id: '',
                    language_list: [],
                    hobby_list: [],
                    about: '',
                    social_list: []
                },
                load: function(){
                    this.hobby.init();
                    utilsSrvc.api.call('/app/user/loadeditinfo/', {}, function(resp){
                        // dictionaries
                        $scope.settings.edit.dictionary = resp.data.dictionary;
                        // values
                        var u = resp.data.profile;
                        $scope.settings.edit.form.gender_id                     = u.profile.gender_id + '';
                        $scope.settings.edit.form.dating_purpose_id             = u.profile.dating_purpose_id + '';
                        $scope.settings.edit.form.dating_purpose_gender_id      = u.profile.dating_purpose_gender_id + '';
                        $scope.settings.edit.form.gender_id                     = u.profile.gender_id + '';
                        $scope.settings.edit.form.hobby_list                    = u.hobby;
                        $scope.settings.edit.form.social_list                   = u.social_list;
                        $scope.settings.edit.form.about                         = u.profile.about;
                        $scope.settings.edit.form.fio                           = u.profile.fio;
                        $scope.settings.edit.form.geo                           = u.profile.geo;
                        $scope.settings.edit.form.birthday                      = u.profile.birthday;
                        $scope.settings.edit.form.language_list                 = [];
                        for(var d of $scope.settings.edit.dictionary.language) {
                            for(var lid of u.language_list) {
                                if(d.id == lid.id) {
                                    $scope.settings.edit.form.language_list.push(d);
                                }
                            }
                        }
                        $timeout(function(){
                            $('#form-my .select3').formSelect();
                            $('#form-my .collapsible').collapsible();
                        });
                    });
                },
                save: function(){
                    var that = this;
                    localStorage.setItem('myinfo-hobby', angular.toJson(this.list));
                    // utilsSrvc.api.call('/app/user/savehobby/', this.form, function(resp){
                    utilsSrvc.api.call('/app/user/saveprofile/', this.form, function(result) {
                        console.log('changeProfile', result.data);
                        if(gmaback.main_scope) {
                            try {
                                gmaback.main_scope.profile.update(result);
                            } catch(e) {
                                // do nothing
                            }
                        }
                        // getApp().changeProfile(result.data);
                        gmaback.js.showMessage(result.message, 4000);
                        CustomUI.hideForm();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            }
        };

        if ($state.current.name == 'settings.work') {
            $templateCache.remove('/my/settings/work/');
        } else if ($state.current.name == 'settings.form') {
            $templateCache.remove('/my/settings/form/');
        }

        // Профиль
        $scope.profile = getApp().user;
        $scope.updateProfile = function (profile_data) {
            utilsSrvc.my.profile.fio = profile_data.fio;
            utilsSrvc.my.profile.avatar = profile_data.avatar;
            utilsSrvc.my.profile.city = profile_data.city;
            utilsSrvc.my.profile.direction_id = profile_data.direction_id;
            utilsSrvc.my.profile.direction_id_list = profile_data.direction_id_list;
            utilsSrvc.my.profile.info_science_degree = profile_data.info_science_degree;
            utilsSrvc.my.profile.info_skills = profile_data.info_skills;
            utilsSrvc.my.profile.phone_is_public = profile_data.phone_is_public;
            utilsSrvc.my.profile.hobby_id_list = profile_data.hobby_id_list;
            $('#profile-image').attr('src', profile_data.avatar);
        };

        /**
        * F.A.Q.
        */
        $scope.faq = {
            list: [],
            loading: false,
            load: function(){
                // console.log('Events.loadFullList');
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadfaq/', {}, function(result) {
                    that.list = result.data;
                    for(var i in that.list) {
                        if(i == 0) {
                            that.list[i].active = true;
                        } else {
                            that.list[i].active = false;
                        }
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },
            select: function(item) {
                for(var o of this.list) {
                    o.active = false;
                }
                item.active = true;
            }
        };

        /**
        * Support
        */
        $scope.support = {
            loading: false,
            answer: '',
            form: {
                name: '',
                email: '',
                message: ''
            },
            reset: function(){
                this.form.name = getApp().user.fio;
                this.form.email = getApp().user.email;
                this.form.message = '';
                this.answer = '';
            },
            send: function() {
                // console.log('Events.loadFullList');
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/writetosupport/', that.form, function(result) {
                    that.reset();
                    that.answer = 'Спасибо за обращение!<br>Наши технические специалисты свяжутся с Вами в течение 24 часов';
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

        // Редактор профиля
        $scope.edit_profile = {
            loading: false,
            submit: function () {
                var that = this;
                that.loading = true;
                var form = {
                    fio: $scope.profile.fio,
                    birthday: $scope.profile.birthday
                };
                utilsSrvc.api.call('/app/user/saveprofile', form, function(result) {
                    that.loading = false;
                    getApp().changeProfile(result.data.profile);
                    $scope.profile = getApp().user;
                    gmaback.js.showMessage(result.message, 4000);
                });
                return false;
            }
        };

        // Просмотр чужого профиля
        $scope.showMyForm = function ($event, tab_id) {
            $('#form-my').addClass('fullsize');
            $scope.profile = getApp().user;
            angular.element('#modalCtrl').scope().init_settings = true;
            $('#events_button').removeClass('active');
            if ($event) {
                $event.preventDefault();
            }
            $scope.myphotos.resetNewPhotos();
            CustomUI.showForm('form-my', true, true, {
                onclose: function(e){
                    $scope.myphotos.eventAboutNewPhotos();
                }
            });
            $timeout(function() {
                $scope.tabs.select(tab_id);
                gmaDatePicker.createDatepicker($('#birthday'));
                $scope.faq.load();
                $scope.support.reset();
                $('#form-my').removeClass('fullsize');
            });
            return false;
        }

        $scope.redirect = function (url, data) {
            if (!data) {
                data = {};
            }
            $state.go(url, data, {
                reload: true
            });
        }

        // Форма смены пароля
        $scope.changePassword = {
            visible: false,
            loading: false,
            form: {
                password1: '',
                password2: ''
            },
            toggle: function () {
                this.visible = !this.visible;
                return false;
            },
            submit: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/settings/changepassword', this.form, function (result) {
                    that.loading = false;
                    that.toggle();
                    that.form.password1 = '';
                    that.form.password2 = '';
                    gmaback.js.showMessage(result.message, 4000);
                });
                return false;
            }
        };

        $scope.changeAvatar = function (ok, resp, open_settings_form) {
            if (ok) {
                var fd = new FormData();
                fd.append('crop', JSON.stringify(resp.size));
                fd.append('form[avatar]', resp.file);
                utilsSrvc.api.call('/app/user/saveavatar/', fd, function (result) {
                    getApp().changeProfile(result.data);
                    $scope.profile = getApp().user;
                });
            }
            if(open_settings_form) {
                return CustomUI.showForm('form-my');
            }
        }

        // Work experience (Резюме -> Опыт работы)
        $scope.experience = {
            saving: false,
            loading: true,
            list: [],
            load: function () {
                $scope.loading = true;
                utilsSrvc.api.call('/app/settings/loadresume/', {}, function (result) {
                    $scope.experience.loading = false;
                    $scope.experience.list = result.data.resume.work_places;
                });
            },
            form: {
                position_title: '',
                organization_title: '',
                dt_start: '',
                dt_end: '',
                responsibility: ''
            },
            showAdd: function () {
                CustomUI.showForm('form-addexperience');
            },
            submit: function () {
                $scope.saving = true;
                utilsSrvc.api.call('/app/settings/addexperience/', this.form, function (result) {
                    $scope.saving = false;
                    $scope.experience.list.unshift(result.data.item); // ($scope.tasks.offset, 1);
                }, function (result) {
                    $scope.saving = false;
                    gmaback.js.showError(result.message, 4000);
                });
                CustomUI.hideForm();
            },
            cancel: function () {
                CustomUI.hideForm();
            }
        };

        $scope.getProfileType = function () {
            return sessionStorage.getItem('profile_type');
        }

        $scope.email = {
            open: function () {
                CustomUI.showForm('form-email');
            }
        };

        $scope.phone = {
            open: function () {
                CustomUI.showForm('form-phone');
            }
        };

        if ($state.current.name == 'settings.work') {
            $scope.experience.load();
        }

    };

    mySettingsCtrl.$inject = injectParams;
    app.controller('mySettingsCtrl', mySettingsCtrl);

});