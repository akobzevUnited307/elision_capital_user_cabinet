'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$timeout'];
    var myclientsCtrl = function ($scope, utilsSrvc, $state, $timeout) {

        // Constants
        $scope.lang = getApp().lang;
        $scope.BTC_SATOCHI = 100000000;
        $scope.app = getApp();

        // myclients
        $scope.myclients = {
            init: function() {
				$scope.requests.loadLogin();
                $scope.requests.load();
                $scope.methods.no_collapsed = localStorage.getItem('no_collapsed') == null ? true : JSON.parse(localStorage.getItem('no_collapsed'));
            }
        };

        // Необработанные заявки
        $scope.requests = {
            loading: false,
			total_count: 12,
			user_login_gd: null,
			user_login_proauto: null,
			not_user_login_proauto: null,
			user_login_trustera: null,
            repsonalConditions: {
                item: null,
                open: function(item) {
                    this.item = item;
                    return CustomUI.showForm('form-repsonalConditions');
                }
            },
            inwork: {
                // list: [],
                list: [
                    {fio: 'Ержан Абдуллин', age: 45, dt: '2019-11-17 14:23:00', collapsed: true, dream_status_id: 1, dream_status_title: 'Новая заявка', message: 'Проведены тесты по стабилизации работы ключевого оборудования', instagram: 'maxgalkinru', phone: '+79521478555', phone_provider: 'Билайн (Казахстан)', technical_info: {browser: 'Vivaldi Browser', ip: '12.145.14.44', geo: 'Kazanhstan, Aktobe'}, dream: {target: 'Стабильность (5)', description: 'Шикарная квартира, машина мечты и финансовая независимость'}},
                    {fio: 'Николай Иванов', age: 50, dt: '2019-11-16 17:23:15', collapsed: true, dream_status_id: 1, dream_status_title: 'Новая заявка', message: 'Проведены тесты по стабилизации работы ключевого оборудования', instagram: 'maxgalkinru', phone: '+79521478555', phone_provider: 'Билайн (Казахстан)', technical_info: {browser: 'Vivaldi Browser', ip: '12.145.14.44', geo: 'Kazanhstan, Aktobe'}, dream: {target: 'Стабильность (5)', description: 'Шикарная квартира, машина мечты и финансовая независимость'}},
                    {fio: 'Луиза Великолепная', age: 18, dt: '2019-11-16 20:15:47', collapsed: true, dream_status_id: 1, dream_status_title: 'Новая заявка', message: 'Проведены тесты по стабилизации работы ключевого оборудования', instagram: 'maxgalkinru', phone: '+79521478555', phone_provider: 'Билайн (Казахстан)', technical_info: {browser: 'Vivaldi Browser', ip: '12.145.14.44', geo: 'Kazanhstan, Aktobe'}, dream: {target: 'Стабильность (5)', description: 'Шикарная квартира, машина мечты и финансовая независимость'}}
                ],
                // удалить заявку
                deleteItem: function(item) {
                    var that = $scope.requests;
	                that.loading = true;
	                utilsSrvc.api.call('/app/dream/deleterequest', {id: item.id}, function(result) {
                		gmaback.js.showMessage('Заявка удалена', 3000);
	                    that.inwork.list = that.inwork.list.filter(function(req) {
							return req.id != item.id;
	                    });
	                }, null, null, function () {
	                    that.loading = false;
	                });
                },
                // обработать заявку
                addLead: function(item) {
                    var that = $scope.requests;
	                that.loading = true;
	                utilsSrvc.api.call('/app/dream/addlead', item, function(result) {
                		gmaback.js.showMessage('Заявка обработана', 3000);
	                    that.inwork.list = that.inwork.list.filter(function(req) {
							return req.id != item.id;
	                    });
	                    $scope.lead.load();
	                }, null, null, function () {
	                    that.loading = false;
	                });
                }
            },
            load: function() {
                /*var that = this;
                that.loading = true;
                $timeout(function(){
                    that.loading = false;
                }, 1000);
                */
                var that = this;
				that.loading = true;
				that.tat = $scope.app.user;
                utilsSrvc.api.call('/app/dream/getmy', {}, function(result) {
                    that.total_count = result.data.total_count;
					that.inwork.list = result.data.inwork.list;
                }, null, null, function () {
                    that.loading = false;
                });
			},
			loadLogin: function() {
				var that = this;
				// that.loading = true;
				utilsSrvc.api.call('/app/user/getUserLoginForRef', {}, function(result) {
					that.user_login_gd = result.data.user_login_gd;
					that.user_login_proauto = result.data.user_login_proauto;
					if (!result.data.user_login_proauto) {
						that.not_user_login_proauto = 1;
					}
					that.user_login_trustera = result.data.user_login;
                }, null, null, function () {
                    that.loading = false;
                });
			}
        };

        // startEducation  
        $scope.startEducation = {
            open: function() {
                CustomUI.showForm('form-dreamlinks');
            }
        };
        
        //startWebinar
        $scope.startWebinar = {
            open: function() {
                CustomUI.showForm('form-webinarlinks');
            }
        };

        // selectEvent  
        $scope.selectEvent = {
            open: function() {
                // CustomUI.showForm('form-selectEvent');
                gmaback.js.showMessage('Временное недоступно', 3000);
            }
        };

        // addLead  
        $scope.addLead = {
            open: function() {
                // CustomUI.showForm('form-addLead');
                gmaback.js.showMessage('Временное недоступно', 3000);
            }
        };

        $scope.openHowWorkWithClients = function(){
            return CustomUI.showForm('form-how_work_with_clients');
        };

        // инструкция по работе с заявками
        $scope.openLeadInstruction = function(){
            return CustomUI.showForm('form-leadInstruction');
        };
        
        $scope.lead = {
			data: {},
            loading: false,
            form: {
				fio: '',
				phone: '',
				lead_source_id: 1,
				comment: ''
            },
        	fio: '',
        	delete_id: '',
            init: function () {
            	var that = this;
				Highcharts.setOptions({
        			lang:{
            			rangeSelectorZoom: ''
					},
					global: {
						useUTC: false
					}
				});
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/lead/getlist/', {}, function(result) {
                	that.data.grow = result.data;
	            }, null, null, function(){
		            that.loading = false;
		        });
			},
            openAddLead: function(){
                var that = this;
                that.form = {
					fio: '',
					phone: '',
					lead_source_id: 1,
					comment: ''
	            };
                return CustomUI.showForm('form-addlead');
            },
            addLeadIsOK: function() {
				var that = this;
				return that.form.fio && that.form.phone && that.form.lead_source_id;
            },
            addlead: function() {
				var that = this;
				utilsSrvc.api.call('/app/lead/addlead/', that.form, function(result) {
					CustomUI.hideForm();
					if (result.data.duplicated) {
						CustomUI.showForm('form-addleadduplicated')
					}
					else {
						gmaback.js.showMessage('Лид добавлен', 5000);
                		that.load();
					}
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            hideForm: function() {
				CustomUI.hideForm();
            },
            setisarch: function(item) {
				var that = this;
				utilsSrvc.api.call('/app/lead/setisarch/', {id: item.id}, function(result) {
					gmaback.js.showMessage('Лид перемещен в ' + (item.is_arch ? 'активные' : 'архив'), 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            openDeleteLead: function(id){
                var that = this;
                that.delete_id = id;
                return CustomUI.showForm('form-deletelead');
            },
            deleteLead: function() {
				var that = this;
				if (!that.delete_id) {
					return;
				}
				utilsSrvc.api.call('/app/lead/deletelead/', {id: that.delete_id}, function(result) {
					that.delete_id = '';
					CustomUI.hideForm();
					gmaback.js.showMessage('Лид удален', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            }
        };
        
        $scope.safeApply = function(fn) {
   			var phase = this.$root.$$phase;
   			if(phase == '$apply' || phase == '$digest') {
   				if(fn && (typeof(fn) === 'function')) {
   					fn();
   				}
   			} else {
   				this.$apply(fn);
   			}
		};
		
		$scope.search = function (item) {
            if (!item.is_arch && (!$scope.lead.fio || (item.fio.toLowerCase().indexOf($scope.lead.fio) != -1))) {
			    return true;
			}
			return false;
		};
		
		$scope.archsearch = function (item) {
            if (item.is_arch && (!$scope.lead.fio || (item.fio.toLowerCase().indexOf($scope.lead.fio) != -1))) {
			    return true;
			}
			return false;
		};
		
		$scope.leadlist = {
			data: {},
            loading: false,
            form: {
				fio: '',
				phone: '',
				comment: ''
            },
        	fio: '',
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/lead/getleadlist/', {}, function(result) {
                	that.data.grow = result.data;	                       
	            }, null, null, function(){
		            that.loading = false;
		        });
			},
            openAddLead: function(){
                var that = this;
                that.form = {
					fio: '',
					phone: '',
					comment: ''
	            };
                return CustomUI.showForm('form-addleadlist');
            },
            openEditLeadList: function(lead){
                var that = this;
                that.form = {
                	id: lead.id,
					fio: lead.fio,
					phone: lead.phone,
					comment: lead.comment
	            };
                return CustomUI.showForm('form-editleadlist');
            },
            addLeadIsOK: function() {
				var that = this;
				return that.form.fio;
            },
            addlead: function() {
				var that = this;
				utilsSrvc.api.call('/app/lead/addleadlist/', that.form, function(result) {				
					CustomUI.hideForm();
					if (result.data.duplicated) {
						CustomUI.showForm('form-addleadlistduplicated')
					}
					gmaback.js.showMessage('Контакт добавлен', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            addLeadFromLeadList: function(item) {
				var that = this;
				utilsSrvc.api.call('/app/lead/addleadfromleadlist/', {id: item.id}, function(result) {
					gmaback.js.showMessage('Лид перемещен в активные', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            editLeadList: function() {
				var that = this;
				utilsSrvc.api.call('/app/lead/editleadlist/', that.form, function(result) {
					CustomUI.hideForm();
					gmaback.js.showMessage('Лид изменен', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            }
        };
        
        $scope.searchleadlist = function (item) {
            if (!$scope.leadlist.fio || (item.fio.toLowerCase().indexOf($scope.leadlist.fio) != -1)) {
			    return true;
			}
			return false;
		};
		
		$scope.lead_view = {
			data: {},
			form: {
				lead_id: '',
				lead_touch_type_id: 2,
				dt: '',
				comment: ''
			},
            loading: false,
            load: function() {
                var that = this;
                that.loading = true;
                that.initDatepicker();
                that.form = {
					lead_id: '',
					lead_touch_type_id: 2,
					dt: '',
					comment: ''
				};
                if (!$stateParams.id) {
                    return;
                }
                utilsSrvc.api.call('/app/lead/getleadbyid/', {id : $stateParams.id}, function(result) {
                	that.data = result.data;
                	if (!that.lead_touch_types) {
		                utilsSrvc.api.call('/app/lead/getleadtouchtypes/', {}, function(result) {
                			that.lead_touch_types = result.data;
		                }, null, null, function(){
			                that.loading = false;
			            });
					}
	            }, null, null, function(){
		            that.loading = false;
		        });
			},
			initDatepicker: function(value) {
				var that = this;
                // max date
                var dt = new Date();
                dt.setYear(dt.getFullYear() - 18);
                // start date
                var dt_start = new Date();
                dt_start.setYear(dt_start.getFullYear() - 18);
                var startDate = value ? new Date(value) : dt_start;
                $('#input-dt').datepicker({
                    language: 'ru', 
                    autoClose : true,
                    timepicker: true,
                    dateFormat: 'yyyy-mm-dd',
                    onSelect: function(fd, d, picker) {
                        if(d - startDate == 0) {
                            return;
                        }
                        $timeout(function(){
						    that.form.dt = fd;
						});
                    }
                }).data('datepicker').selectDate(startDate);
            },
            openAddComment: function(){
                var that = this;
                return CustomUI.showForm('form-addcomment');
            },
            addcomment: function() {
				var that = this;
                that.loading = true;
                if (!that.data.id) {
					return;
                }
                utilsSrvc.api.call('/app/lead/editleadcomment/', {id : that.data.id, comment: that.data.comment}, function(result) {
                	CustomUI.hideForm();
                	that.load();
	            }, null, null, function(){
		            that.loading = false;
		        });
            },
            openAddLeadTouch: function(){
                var that = this;
                that.form.lead_id = that.data.id;
                return CustomUI.showForm('form-addleadtouch');
            },
            addLeadTouchIsOK: function() {
				var that = this;
				return that.form.lead_id && that.form.lead_touch_type_id && that.form.dt;
            },
            addleadtouch: function() {
				var that = this;
                that.loading = true;
                if (!that.data.id) {
					return;
                }
                utilsSrvc.api.call('/app/lead/addleadtouch/', that.form, function(result) {
                	CustomUI.hideForm();
                	that.form = {
						lead_id: that.data.id,
						lead_touch_type_id: 2,
						dt: '',
						comment: ''
					};
                	that.load();
	            }, null, null, function(){
		            that.loading = false;
		        });
            },
            setisarch: function(item) {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('/app/lead/setisarch/', {id: item.id}, function(result) {
					gmaback.js.showMessage('Лид перемещен в ' + (item.is_arch ? 'активные' : 'архив'), 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            deletelead: function(id) {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('/app/lead/deletelead/', {id: id}, function(result) {
					gmaback.js.showMessage('Лид удален', 5000);
                	$state.go('index.lead.archive');
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            deleteLeadTouch: function(id) {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('/app/lead/deleteleadtouch/', {id: id}, function(result) {
					gmaback.js.showMessage('Касание удалено', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
        };               
    
	    $scope.methods = {
    		no_collapsed: true,
			collapse: function () {
				var that = this;
				that.no_collapsed = !that.no_collapsed;
				localStorage.setItem('no_collapsed', that.no_collapsed);
			}
		};
		$scope.methodspro = {
    		no_collapsed: true,
			collapse: function () {
				var that = this;
				that.no_collapsed = !that.no_collapsed;
				localStorage.setItem('no_collapsed', that.no_collapsed);
			}
	    };

    };

    myclientsCtrl.$inject = injectParams;
    app.controller('myclientsCtrl', myclientsCtrl);

});