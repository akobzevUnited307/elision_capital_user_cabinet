'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$timeout', '$stateParams'];
    var walletCtrl = function ($scope, utilsSrvc, $state, $timeout, $stateParams) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        $scope.BTC_SATOCHI = 100000000;
        $scope.MIN_WITHDRAWAL_BTC_SATOSHI = 1000000;
        $scope.MIN_WITHDRAWAL_USDT = getApp().constant.MIN_WITHDRAWAL_USDT;

        // Партнерский рублевой счёт
        $scope.partnerRub = {
            wallet: null,
            // внесение средств
            fill: {
                open: function() {
                    this.setPage(1);
                    return CustomUI.showForm('form-partnerRub-fill');
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                setPage: function(number) {
                    this.page = number;
                },
            },
            // Перевод другому 
            transfer: {
            	loading: false,
                min_usd: 1,
                show_telegram: false,
                show_password: false,
                telegram_exists: false,
                searchUser: null,
                form: {
                    wallet_number: '',
                    number: '',
                    password: '',
                    amount_dax: ''
                },
                open: function() {
                    this.setPage(1);
                    var that = this;
                    that.loading = true;
                    that.searchUser = null;
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    that.form.number = '';
                    that.form.comment = '';
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                        that.form.amount_dax = ''; //$scope.walletView.wallet.amount / $scope.BTC_SATOCHI;
                        that.form.wallet_number = $scope.wallet.item.number;
                        CustomUI.showForm('form-partnerRub-transfer');
                        }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                telegramprepareconfirm: function() { 
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {}, function(result) {
                        that.telegram_exists = false;
                        that.show_telegram = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                setPage: function(number) {
                    this.page = number;
                },
                getInUSD: function() {
                    var resp = this.form.amount_dax * $scope.view.info.dax_rate_current;
                    if(resp > this.min_usd - 1 && resp < this.min_usd) {
                        resp = this.min_usd - 1;
                    }
                    return resp;
                },
                numberChange: function() {
                    var that = this;
                    that.searchUser = null;
                    if (!that.form.number) {
                        return;
                    }
                    
                    utilsSrvc.api.call('/app/qwertypay/getgduserbynumberloginphone/', { number: that.form.number }, function(result) {
                        that.searchUser = result.data;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                checkMin: function() {
                    return this.getInUSD() >= this.min_usd;
                },
                isOk: function() {
                    return this.checkMin() && (this.form.amount_dax <= $scope.wallet.item.amount / $scope.BTC_SATOCHI) && this.searchUser && (this.form.password || this.form.pin);
                },
                isOkTelegram: function() {
                    return this.checkMin() && (this.form.amount_dax <= $scope.wallet.item.amount / $scope.BTC_SATOCHI) && this.searchUser;
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    that.form.password = that.form.pin || that.form.password;
                    utilsSrvc.api.call('/app/user/createtransfer/', {
                            wallet_number: that.form.wallet_number,
                            to_user_id: that.searchUser.id,
                            password: that.form.password,
                            amount_dax: Math.round(that.form.amount_dax * $scope.BTC_SATOCHI),
                            comment: that.form.comment, 
                            project_id: 2
                        }, function(result) {
                        gmaback.js.showMessage('Перевод успешно осуществлён', 4000);
                        CustomUI.hideForm();
                        $scope.wallet.data = null;
                        $scope.wallet.load();
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                }
            },
            // Перевод в Trustera
            partnerToMain: {
	            loading: false,
	            wallet: null,
	            max_amount: 0,
	            form: {
	                wallet_number: '',
	                amount_dax: ''
	            },
	            open: function () {
            		var that = this;
            		that.show_telegram = false;
			        that.show_password = false;
			        that.telegram_exists = false;
			        that.form.password = '';
			        that.form.pin = '';
	                that.loading = true;
            		utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
            			that.telegram_exists = result.telegram_exists;
                		that.show_password = !result.telegram_exists;
                		that.setWallet();
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
	            },
	            telegramprepareconfirm: function() { 
            		var that = this;
	                that.loading = true;
            		utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {project_id: 2}, function(result) {
            			that.telegram_exists = false;
                		that.show_telegram = result.telegram_exists;
                		that.show_password = !result.telegram_exists;
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
	            },
	            setWallet: function() {
	            	var w = $scope.wallet.item;
	                this.max_amount = w.unlocked_amount_dax / $scope.BTC_SATOCHI;
	                this.wallet = w;
	                this.form.wallet_number = w.number;
	                this.form.amount_dax = '';
	            },
	            allAmount: function() {
	                this.form.amount_dax = this.wallet.unlocked_amount_dax / $scope.BTC_SATOCHI;
	            },
	            submit: function() {
	                var that = this;
	                that.loading = true;
	                that.form.password = that.form.pin || that.form.password;
	                utilsSrvc.api.call('/app/user/movepartnertomain/', {
                		wallet_number: that.form.wallet_number, 
                		amount_dax: Math.round(that.form.amount_dax * $scope.BTC_SATOCHI),
                        password: that.form.password,
                        project_id: 2,
	                }, function(result) {
	                    gmaback.js.showMessage(getApp().lang.paid_success, 4000);
	                    CustomUI.hideForm();
	                    $scope.wallet.data = null;
                        $scope.wallet.load();
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
	            }
	        },
            // Вывод средств
            withdrawalBtc: {
                open: function() {
                    var that = this;
                    that.loading = false;
                    that.setPage(1);
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    that.form.dax_wallet_id = $scope.wallet.item.id;
                    CustomUI.showForm('form-partnerRub-withdrawalBtc');
                    if(!this.current) {
                        this.check();
                    }
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                    // that.confirm();
                    that.loading = false;
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                setPage: function(number) {
                    this.page = number;
                },
                max_dax:        0,
                rates:          [],
                currency:       {},
                info:           false,
                loading:        false,
                sent:           false,
                current:        false,
                enabled:        false,
                check_enabled:  true,
                form: {
                    amount: 0,
                    target_amount: 0, // сколько человек получит выбранной валюты
                    currency_id: 0,
                    dax_rate: 0,
                    withdrawal_wallet_id: '',
                    password: '',
                    dax_wallet_id: 0
                },
                init: function () {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/getmain/', {}, function(result) {
                        that.info = result.data;
                        that.form.dax_rate = that.info.dax_rate_current;
                        //$scope.buyincity.load();
                        that.sent = false;
                        that.max_dax = that.info.withdrawal_available / $scope.BTC_SATOCHI;
                        that.max_dax = Math.round(that.max_dax * 100) / 100;
                        that.form.amount = 0;
                        that.rates = [];
                        for (var cur of that.info.dax_rates) {
                            that.rates.push({
                                id: cur.id,
                                code: cur.code,
                                title: cur.title,
                                value: cur.rate // cur.amount / that.max_dax
                            });
                        }
                        that.setCurrency(4);
                        utilsSrvc.api.call('/app/user/getwithdrawalwalletlist/', {system_type_id: 2}, function(result) {   
                            that.withdrawal_wallets = result.data;
                        }, null, null, function (err) {
                            console.log('err', err);
                        });
                        utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function(result) {
                            if (result.data.current && result.data.current.length) {
                                that.current = result.data.current[0];
                            } else {
                                that.current = result.data.current;
                            }
                            if (!that.current) {
                                if (result.data.enabled == 1) {
                                    that.enabled = true;
                                } else {
                                    that.check_enabled = false;
                                }
                                $timeout(function() {                        
                                    $('#withdrawal-wallet').select2({
                                        templateResult: that.formatSelection,
                                        templateSelection: that.formatSelection,
                                        minimumResultsForSearch: -1
                                    });
                                });
                            } else {
								if(that.current.currency_id == 4){
                                    that.current.currency_short_title = 'USDT';
                                    that.current.explorerHref = 'https://www.blockchain.com/ru/btc/address/';
                                    that.current.wallet_type_number = $scope.wallet.item.number;
                                    that.current.main_sum = $scope.wallet.item.amount;
                                }
                            }
                        });
                    }, null, null, function (err) {
                        that.loading = false;
                        console.log('err', err);
                    });
                },
                setCurrency: function(currency_id) {
                    this.form.currency_id = currency_id;
                    for (var r of this.rates) {
                        if (r.id == currency_id) {
                            this.currency = r;
                        }
                    }
                    this.changeAmount();
                },
                addressIsCorrect: function () {
                    var valid = false;
                    if (this.form.currency_id == 4) {
                        // BTC
                        valid = (/^(1|3)[0-9a-zA-Z]{32,33}$/.test(this.form.address));
                        // valid = (/^(1)[0-9a-zA-Z]{32,33}$/.test(this.form.address));
                    } else if (this.form.currency_id == 5) {
                        // ETH
                        valid = (/^0x[0-9a-f]{40}$/.test(this.form.address) || /^0x?[0-9A-F]{40}$/.test(this.form.address));
                    }
                    return valid;
                },
                changeAmount: function () {
                    this.form.amount = (this.form.amount || '0').toString().replace(',', '.');
                    for (var r of this.rates) {
                        if (r.id == this.form.currency_id) {
                            this.form.target_amount = this.form.amount * r.value;
                        }
                    }
                },
                changeWallet: function () {
                    var that = this;
                    that.withdrawal_wallet = that.withdrawal_wallets.find(function(item) {
                        return item.id == that.form.withdrawal_wallet_id;
                    });
                },
                getRate: function() {
                    for (var r of this.rates) {
                        if (r.id == this.form.currency_id) {
                            return 1 / r.value;
                        }
                    }
                },
                confirm: function () {
                    var that = this;
                    //that.loading = true;
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    CustomUI.showForm('form-withdrawal');
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                telegramprepareconfirm: function() { 
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {}, function(result) {
                        that.telegram_exists = false;
                        that.show_telegram = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                submit: function () {
                    var that = this;
                    that.form.password = that.form.pin || that.form.password;
                    utilsSrvc.api.call('/app/user/withdrawal/', this.form, function (result) {
                    	that.close();
                        CustomUI.showMessage('Заявка успешно зарегистрирована', getApp().lang.withdrawal_funds);
                        utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function(result) {
                            that.sent = false;
                            that.current = false;
                            that.enabled = false;
                            that.check_enabled = true;
                            that.init();
                        });
                    });
                },
                check: function () {
                    var that = this;
                    utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function (result) {
                        console.info('Withdrawal check result', result);
                        if (result.data.current && result.data.current.length) {
                            that.current = result.data.current[0];
                        } else {
                            that.current = result.data.current;
                        }
                        if (result.data.enabled == 1) {
                            that.enabled = true;
                        } else {
                            that.check_enabled = false;
                        }
                    });
                },
                // партнерская программа
                partner_program: {
                    loading: false,
                    start: function () {
                        // Пользователь запустил перерасчет партнерской программы
                        var that = this;
                        that.loading = true;
                        utilsSrvc.api.call('/app/user/withdrawalpartnercalc/', {}, function (result) {
                            that.loading = false;
                            $scope.withdrawal.enabled = true;
                        }, function (result) {
                            // error
                            that.loading = false;
                            gmaback.js.showError('Произошла ошибка: ' + result.message, 4000);
                        });
                    }
                },
                formatSelection: function (optionElement) {
                    try { JSON.parse(optionElement.text) }
                    catch (ex) {
                        var $state = $('<div style="padding-top: 20px;text-transform: uppercase;">' + optionElement.text + '</div>');
                        return $state;
                    }
                    var item = JSON.parse(optionElement.text);
                    var html = '';
                    if (item.icon) {
                        html = '<img src="' + item.icon + '" style="margin-top: 10px;margin-right: 15px;width: 40px;height: 40px;">';
                    }
                    var $state = $('<div style="display:flex;align-items: center;">' + html + '<div><div style="padding-top: 10px;text-transform: uppercase;">Мой кошелек ' + item.title + '</div><strong style="text-transform: none;">' + item.address + '</strong></div></div>');
                    return $state;
                }
            },
            // вывод
            withdrawal: {
                page: 1,
                loading: false,
                form: {
                    amount: '',
                    ip_title: '',
                    inn: '',
                    rs_number: '',
                },
                open: function() {
                    this.setPage(1);
                    return CustomUI.showForm('form-partnerRub-withdrawal');
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                reset: function() {
                    this.form.amount = '';
                    this.form.ip_title = '';
                    this.form.inn = '';
                    this.form.rs_number = '';
                },
                setPage: function(number) {
                    this.page = number;
                },
                isAllow: function() {
                    return $scope.partnerRub.wallet && $scope.partnerRub.wallet.amount_rub > 300000000000;
                },
                formFilled: function() {
                    return false;
                    /*
                    return  this.form.amount &&
                            this.form.ip_title &&
                            this.form.inn &&
                            this.form.rs_number;
                    */
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/wallet/withdrawalpartnerrub/', that.form, function(result) {
                        that.reset();
                        that.close();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            }
        };

        // wallet
        $scope.wallet = {
            loading: false,
            operations_loading: false,
            data: null,
            slider: null,
            item: null, // текущий открытый кошелёк
            ref_history_list: null,
            op_more: {
                all: 0,
                visible: 0,
                reset: function(all) {
                    this.all = all;
                    this.visible = 0;
                    this.show();
                },
                enabled: function() {
                    return this.visible < this.all;
                },
                show: function() {
                    this.visible += 100;
                    if(this.visible > this.all) {
                        this.visible = this.all;
                    }
                }
            },
            open: function(){
                var that = this;
                if(this.data) {
                    for(var w of that.data.dax_wallets) {
                        if(w.number == $stateParams.id) {
                            that.item = w;
                            break;
                        }
                    }
                } else {
                    this.load(function(){
                        that.open();
                    });
                }
            },
            slider_callback: function (index) {
                var that = this;
                if (that.data && that.data.dax_wallets && that.data.dax_wallets.length > 0) {
                    that.item = that.data.dax_wallets[index];
                    that.operations_loading = true;
                    utilsSrvc.api.call('/app/user/loadwalletpagedata/', {dax_wallet: {number: that.item.number, operations: true}}, function(result) {
                        for(var w of result.data.dax_wallets) {
                            if(w.number == that.item.number) {
                                that.item.allOperations = w.operations;
                                that.item.operations = w.operations;
                                that.op_more.reset(that.item.allOperations.length);
                                $scope.operationsfilter.init();
                            }
                        }
                    }, null, null, function(){
                        that.operations_loading = false;
                    });
                }
            },
            load: function(callback){
                var that = this;
                console.log('wallets', that.data);
                if(that.data) {
                    $timeout(function(){
                        that.slider = multiItemSlider('.slider', that.slider_callback);
                    });
                    return;
                }
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadwalletpagedata/', {}, function(result) {
                    if (result.data && result.data.dax_wallets && result.data.dax_wallets.length > 0) {
                        result.data.dax_wallets = result.data.dax_wallets.filter(function(item) {
                        	if (item.dax_wallet_type_id == 1) {
								that.main_number = item.number;
                        	}
                            return item.dax_wallet_type_id == 3;
                        });
                    }
                    that.data = result.data;
                    that.slider_callback(0);
                    
                    for(var w of that.data.dax_wallets) {
                        if(w.dax_wallet_type_id == 3) {
                            $scope.partnerRub.wallet = w;
                        }
                    }
                    // wallet.data.dax_wallets
                    
                    utilsSrvc.api.call('/app/user/getRate', {}, function(result){
                        $scope.app.rate = result.data;
                    }, null, null, null);
                    
                    $timeout(function(){
                        try {
                            that.slider = multiItemSlider('.slider', that.slider_callback);
                        } catch(e) {
                            // do nothing
                            console.error(e);
                        }
                    });
                    if (typeof callback === 'function') {
                        callback(that.data)
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },

            showMoreInfo: function (ref_history_list) {
                this.ref_history_list = ref_history_list;
                return CustomUI.showForm('form-more-by-ref');
            }, 
            showMoreInfoClose: function () {
                    return CustomUI.hideForm('');
            }        
        };


        // Страница выбранного одного кошелька
        $scope.walletView = {
            loading: false,
            wallet: null,
            debt: null,
            operations: {
                sz: 100,
                len: 0,
                visible_count: 0,
                reset: function() {
                    this.len = $scope.walletView.wallet.operations.length;
                    this.visible_count = this.sz;
                    if(this.visible_count > this.len) {
                        this.visible_count = this.len;
                    }
                },
                moreAvailable: function() {
                    return this.visible_count < this.len;
                },
                more: function() {
                    this.visible_count += this.sz;
                    if(this.visible_count > this.len) {
                        this.visible_count = this.len;
                    }
                }
            },
            init: function(load_debt) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/user/loadwalletpagedata', {dax_wallet: {number: $stateParams.number, operations: true}, load_debt: load_debt}, function(result) {
                    that.debt = result.data.debt;
                    for(var w of result.data.dax_wallets) {
                        if(w.number == $stateParams.number) {
                            that.wallet = w;
                            $scope.wallet_setting.form.wallet_id = w.id;
                            $scope.wallet_setting.form.title_orig = w.title;
                            $scope.wallet_setting.form.title = w.title;
                            that.wallet.allOperations = that.wallet.operations;
                            that.percent = 0;
                            that.all_percent = 0;
                            for(var i = w.operations.length - 1; i >= 0; i--) {
                                var item = w.operations[i];
                                if (item.dax_wallet_operation_type_id == 28 ) {
                                    that.percent = Math.round(item.amount / (item.balance - item.amount) * 100 * 100) / 100;
                                    item.percent = that.percent;
                                    that.all_percent += that.percent;
                                } else {
                                    item.percent = 0;
                                }
                            }
                            utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function(result) {
                                that.loading = false;
                                if (result.data.current && result.data.current.length) {
                                    that.withdrawal = result.data.current[0];
                                } else {
                                    that.withdrawal = result.data.current;
                                }
                            });

                            $interval(function () {
                                var date1 = newDate();
                                var date2 = newDate('12-01-2020');
                                var diff = date2.getTime() - date1.getTime();
                                that.days = diff / (1000 * 3600 * 24) + 1;
                            }, 1000);

                            that.operations.reset();
                            $scope.investToMain.setWallet(w);
                            $scope.trustSettings.init();
                            that.loadoffer();
                        }
                    }
                    if(that.wallet) {
                        $scope.operationsfilter.init();
                        that.makeChart();
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },
            loadoffer: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/common/loadoffer/', {dax_wallet_invest_type_id: that.wallet.dax_wallet_invest_type_id}, function(result) {
                    if ($state.current.name == 'navigation.wallet_view_offer' && that.wallet.is_offer && that.wallet.is_condition) {
                        $state.go('navigation.wallet_view', {number: that.wallet.number});
                        return;
                    }
                    that.offer_wallet_en = result.data.offer_wallet_en;
                    that.offer_wallet_ru = result.data.offer_wallet_ru;
                    that.condition_wallet_en = result.data.condition_wallet_en;
                    that.condition_wallet_ru = result.data.condition_wallet_ru;
                    $(document).ready(function(){
                        $('.tabs').tabs();
                    });
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
            makeChart: function() {
                var that = this;
                that.seriesData = that.wallet.operations.filter(function (item) {
                    return item.dax_wallet_operation_type_id == 28;
                }).sort((a,b) => (a.dt > b.dt) ? 1 : ((b.dt > a.dt) ? -1 : 0));
                that.setSeriesData();

                $scope.chart = Highcharts.chart('chart-profit', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        series: {
                            allowPointSelect: true,
                            stacking: 'normal'
                        }
                    },
                    xAxis: {
                        categories: that.tmpIntervalCategories,
                        labels: {
                            autoRotation: [-90],
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Montserrat, sans-serif, Arial'
                            },
                            formatter: function () {
                                return this.value;
                            }
                        }
                    },
                    yAxis: {
                        title: false
                    },
                    tooltip: {
                        shared: true,
                        useHTML: true,
                        headerFormat: '',
                        pointFormatter: function () {
                            var percent = this.y.toFixed(2);
                            return '<div style="display:flex;text-transform: uppercase;align-items: center;font-weight: bold;font-family: Montserrat, sans-serif, Arial;"><div style="flex: 1;font-size:11px;"><div style="color:#384db9">' + this.category + '</div><div>Процент доходности</div></div><div style="font-size:30px;margin-left: 10px;">' + percent + '</div></div>';
                        },
                        valueDecimals: 2
                    },
                    credits: {
                        enabled: true
                    },
                    series: [{
                        data: that.tmpIntervalData,
                        color: '#929bcc',
                        name: 'Процент доходности',
                        stack: 'total',
                        showInLegend: false,
                        states: {
                            hover: {
                                color: '#253899',
                                borderColor: 'gray'
                            }
                        }
                    }
                    ]
                });
                if (that.wallet.dax_wallet_type_id != 2 ||
                    !that.wallet.operations.filter(function (item) {
                        return item.dax_wallet_operation_type_id == 28;
                    }).length) {
                    $('#chart-profit-container').remove();
                } else {
                    $('#chart-profit-container').css({"visibility": "visible"});
                }
            },
            setSeriesData: function(interval) {
                var that = this;
                var intervalData = [];
                that.seriesData.map(function(value, index, array) {
                    d = newDate(value['dt']);
                    d = (d.getFullYear()-1970)*12 + d.getMonth();
                    intervalData[d] = intervalData[d]||[];
                    intervalData[d].push(value);
                });
                that.tmpIntervalData = [];
                that.tmpIntervalCategories = [];
                intervalData.forEach(function(intervalItem) {
                    if (intervalItem.length) {
                        var dt = intervalItem[0].dt;
                        var percent = 0;
                        intervalItem.forEach(function (item) {
                            percent += item.percent;
                        });
                        that.tmpIntervalData.push(percent);
                        that.tmpIntervalCategories.push(newDate(dt).toLocaleString($scope.app.languages.current.code, {
                            year: 'numeric',
                            month: 'long'
                        }));
                    }
                });
            },
            getTrustDtUnlock: function () {
                var dt_unlock = newDate();
                dt_unlock = dt_unlock.setDate(dt_unlock.getDate() + 365);
                var dt = newDate(this.wallet.dax_wallet_trust_settings.capital_withdrawal_date);

                if(dt > dt_unlock) {
                    dt_unlock = dt;
                }
                return newDate(dt_unlock).toLocaleDateString();
            },
            confirmAccept: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/wallet/confirmaccept', {id: that.wallet.id}, function (resp) {
                    that.init();
                }, null, null, function () {
                    that.loading = false;
                });
            },
            initOffer: function() {
                var that = this;
                $timeout(function() {
                    $(window).scrollTop(0);
                }, 1000);
            },
            initBuy: function () {
                mercuryoWidget.run({
                    widgetId: '85251fd9-8602-452b-b90b-6e897ad2c129',
                    host: document.getElementById('mercuryo-widget'),
                    type: 'buy',
                    currencies: ['BTC'],
                    fixCurrency: true,
                    height: '1500px',
                    fiatCurrencyDefault: 'USD',
                    address: $scope.wallet.data.buy_wallet.address,
                    fiatAmount: 500,
                    hideAddress: true,
                    lang: $scope.app.languages.current.code
                });
            }
        }

        // transfer
        $scope.transfer = {
            loading: false,
            confirm_flag: false,
            data: {
                amount: '',
                comm: '',
            },            
            load: function(){
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/paidtxn/', {}, function(result) {
                    
                }, null, null, function(){
                    that.loading = false;
                });
            },
            confirm: function() {
                var that = this;
                if (that.data.amount > 0) {
                    that.confirm_flag = true;
                    that.data.comm = that.data.amount * 0.03;
                    that.data.full_amount = that.data.amount - that.data.comm;
                } else {
                    gmaback.js.showError('Некорректная сумма', 4000);
                }
            },
            submit: function() {
                $state.go('index.transfer', {id: 378});
            }
        };
        
        /**
        * operationsfilter
        */
        $scope.operationsfilter = {
            start_date: null,
            finish_date: null,
            dax_wallet_operation_type_id: [],
            dax_wallet_operation_types: [],
            init: function() {
                var that = this;
                $scope.wallet.item.operations.forEach(function(operation) {
                    if (!that.dax_wallet_operation_types.find(function(item) {
                        return operation.dax_wallet_operation_type_id == item.dax_wallet_operation_type_id;
                    })) {
                        that.dax_wallet_operation_types.push({
                            dax_wallet_operation_type_id: operation.dax_wallet_operation_type_id,
                            dax_wallet_operation_type_title: operation.dax_wallet_operation_type_title,
                        });
                    }
                });
                setTimeout(function() {                    
                    $('.myfiltercheckbox:checkbox').each(function () {
                        $(this).prop('checked', true);
                      });
                }, 1000);
                that.start_date = null;
                 that.finish_date = null;
            },
            clear: function() {
                var that = this;
                $('.myfiltercheckbox:checkbox').each(function () {
                    $(this).prop('checked', true);
                });
                
                that.start_date = null;
                that.finish_date = null;
                that.filter();
            },
            changeDate: function() {
                var that = this;
                if (that.start_date && that.finish_date &&
                    that.start_date.getTime() > that.finish_date.getTime()) {
                    that.start_date = that.finish_date;
                }
            },
            filter: function() {
                var that = this;
                
                that.dax_wallet_operation_type_id = [];
                /*$('.myfiltercheckbox:checkbox:checked').each(function () {
                   if (this.checked && $(this).val()) {
                       that.dax_wallet_operation_type_id.push(parseInt($(this).val()));
                   }
                  });*/
                
                $scope.wallet.item.operations = $scope.wallet.item.allOperations;
                $scope.wallet.item.operations = $scope.wallet.item.operations.filter(function(item) {
                    if (that.start_date && that.start_date.getTime() >= newDate(item.dt).getTime()) {
                        return false;
                    }
                    if (that.finish_date && that.finish_date.getTime() <= newDate(item.dt).getTime()) {
                        return false;
                    }
                    if (that.dax_wallet_operation_type_id.length) {
                        if (that.dax_wallet_operation_type_id.indexOf(item.dax_wallet_operation_type_id) < 0) {
                            return false;
                        }
                    }
                    
                    return true;
                });
                $scope.wallet.op_more.reset();
                CustomUI.hideForm();
            },
            open: function() {
                CustomUI.showForm('form-operationsfilter');
            }
        };

        //$scope.walletView.init();


    };

    walletCtrl.$inject = injectParams;
    app.controller('walletCtrl', walletCtrl);

});