'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var rewardsCtrl = function ($scope, utilsSrvc, $state) {
    	
    	$scope.lang = getApp().lang;
        $scope.BTC_SATOCHI = 100000000;
        $scope.app = getApp();
        $scope.date = newDate();                 
        $scope.pages.setURL($state.current.name);
        $scope.my = utilsSrvc.my;
        $scope.rewards = {
			loading: false,
            data: false,
			days: [],
            loadTurnover: function () {	
				var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadforcareerdata/', {}, function(result) {
                    that.turnover = result.data;                    
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            loadBonusInfo: function () {	
				var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadforbonusdata/', {}, function(result) {
                    that.data = result.data;                    
                }, null, null, function(){
	                that.loading = false;
	            });
            }
        }
    }
    rewardsCtrl.$inject = injectParams;
    app.controller('rewardsCtrl', rewardsCtrl);
   
});