'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {
    
    var injectParams = ['$scope', 'utilsSrvc', '$state', '$timeout', '$stateParams'];
    var withdrawalMethodCtrl = function ($scope, utilsSrvc, $state, $timeout, $stateParams) {
        
       $scope.withdrawal = {
            loading: false,
            list: [],
            currency_list: [],
            form: {
                currency_id: 4, 
                title: '',
                address: '',
                system_type_id: 2
            },
            wallet: null,
            load: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/getwithdrawalwalletlist/', {system_type_id: 2}, function(result) {   
                    that.list = result.data;
                    that.currency_list = $scope.app.currency;
                }, null, null, function (err) {
                    that.loading = false;
                    console.log('err', err);
                });
            },
            openAdd: function() {
                var that = this;
                CustomUI.showForm('form-addwallet');
                /*$timeout(function() {
                    $('#currency').select2({
                        templateResult: that.formatSelection,
                        templateSelection: that.formatSelection,
                        minimumResultsForSearch: -1
                    });
                    if (that.form.currency_id) {
                        $('#currency').val(that.form.currency_id);
                        $('#currency').trigger('change');
                    }
                });*/
            },
            openRemove: function(wallet) {
                var that = this;
                that.wallet = wallet;
                CustomUI.showForm('form-removewallet');
            },
            formatSelection: function (optionElement) {
                try { JSON.parse(optionElement.text) }
                catch (ex) {
                    var $state = $('<div style="padding-top: 20px;text-transform: uppercase;">' + optionElement.text + '</div>');
                    return $state;
                }
                var item = JSON.parse(optionElement.text);
                var html = '';
                if (item.index) {
                    html = '<img src="assets/img/logo/cryptocoins/' + item.index + '.png" style="margin-right: 15px;width: 40px;height: 40px;">';
                }
                var $state = $('<div style="display:flex;align-items: center;height: 100%;">' + html + '<div><strong style="text-transform: uppercase;">' + item.title + '</strong></div></div>');
                return $state;
            },
            addressIsCorrect: function () {
                var valid = false;
                if (this.form.currency_id == 4) {
                    // BTC
                    valid = (/^(1|3)[0-9a-zA-Z]{32,33}$/.test(this.form.address));
                } else if (this.form.currency_id == 5) {
                    // ETH
                    valid = (/^0x[0-9a-f]{40}$/.test(this.form.address) || /^0x?[0-9A-F]{40}$/.test(this.form.address));
                }
                return valid;
            },
            submit: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/savewithdrawalwallet/', that.form, function(result) {
                    that.form.title = '';
                    that.form.address = '';
                    CustomUI.hideForm();
                    that.load();
                }, null, null, function (err) {
                    that.loading = false;
                    console.log('err', err);
                });
            },
            remove: function() {                
                var that = this;
                if (!that.wallet) {
                    return;
                }
                that.loading = true;
                utilsSrvc.api.call('/app/user/removewithdrawalwallet/', {id: that.wallet.id}, function(result) {
                    that.wallet = null;
                    CustomUI.hideForm();
                    that.load();
                }, null, null, function (err) {
                    that.loading = false;
                    console.log('err', err);
                });
            }
        };
        
    };
    withdrawalMethodCtrl.$inject = injectParams;
    app.controller('withdrawalMethodCtrl', withdrawalMethodCtrl);
    
});