'use strict';

define(['app', 'lang', 'service/utils'], function (app, lang) {

    var injectParams = ['$scope', 'utilsSrvc', '$window', '$timeout', '$interval'];

    var testingCtrl = function ($scope, utilsSrvc, $window, $timeout, $interval) {

        $scope.conf = getApp();
        $scope.lang = lang.values;
        $scope.loading = false;

        $scope.testing = {
            loading: true,
            result: true,
            test: [],
            current: null,
            number: null,
            errQuestionArr: [],
            countdown: {
                dt: 0,
                timer: null,
                interval: 0,
                hours: 0,
                minutes: 0,
                seconds: 0,
                update: function() {
                    var that = this;
                    var date1 = new Date(that.dt);
                    date1.setHours(date1.getHours() + 2);
                    var date2 = new Date();
                    // that.interval = Math.round(Math.abs(date1 - date2) / 1000);
                    that.interval = Math.round(Math.round(date1 - date2) / 1000);
                    // get total days between two dates
                    // var days = Math.floor(res / 86400);
                    // get hours
                    that.hours = Math.floor(that.interval / 3600) % 24;
                    // get minutes
                    that.minutes = Math.floor(that.interval / 60) % 60;
                    if(that.minutes < 10) {
                        that.minutes = '0' + that.minutes;
                    }
                    // get seconds
                    that.seconds = Math.round(that.interval % 60);
                    if(that.seconds < 10) {
                        that.seconds = '0' + that.seconds;
                    }
                },
                setup: function(dt) {
                    var that = this;
                    this.dt = dt;
                    this.timer = $interval(function() {
                        that.update();
                    }, 1000);
                    that.update();
                }
            },
            tryMore: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/testing/deleteresult/', {}, function (result) {
                    // that.result = result.data;
                    that.load();
                }, null, null, function () {
                    that.loading = false;
                });
            },
            load: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/testing/gettest/', {}, function (result) {
                    that.test = result.data.test;
                    that.result = result.data.result;
                    if (!result.data.result) {
                        that.selectQuestion(0);
                    } else {
                        that.countdown.setup(that.result.dt);
                    }
                    if(that.result) {
                        that.result.questions.forEach(function(item) {
                            var arrayItem = {};
                            if (!item.is_correct) {
                                arrayItem.answer = item.title;
                                item.answers.forEach(function(item2) {
                                    if (item2.active) {
                                        arrayItem.choosen_answer = item2.title;
                                    }
                                    if (item2.is_correct) {
                                        arrayItem.correct_answer = item2.title;
                                    }
                                });
                                that.errQuestionArr.push(arrayItem);
                            }
                        });
                    }
                }, null, null, function () {
                    that.loading = false;
                });
            },
            selectQuestion: function (index) {
                index = parseInt(index);
                this.number = index + 1;
                this.current = this.test.questions[index];
            },
            selectAnswer: function (index) {
                index = parseInt(index);

                // выбор активного ответа
                for (var i in this.current.answers) {
                    this.current.answers[i].active = i == index;
                }

                this.current.is_answered = true;

                // Найти следующий неотвеченный вопрос
                for (var i in this.test.questions) {
                    if (i >= this.number) {
                        if (!this.test.questions[i].is_answered) {
                            return this.selectQuestion(i);
                        }
                    }
                }
                // Найти следующий неотвеченный вопрос с начала
                // for(var i in this.current.questions) {
                //     if(!this.current.questions[i].is_answered) {
                //         return this.selectQuestion(i);
                //     }
                // }
                this.finish();

            },
            finish: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/testing/sendresult/', this.test, function (result) {
                    // that.result = result.data;
                    that.load();
                }, null, null, function () {
                    that.loading = false;
                });
            }
        };

        $scope.testing.load();

    };

    testingCtrl.$inject = injectParams;

    app.controller('testingCtrl', testingCtrl);

});