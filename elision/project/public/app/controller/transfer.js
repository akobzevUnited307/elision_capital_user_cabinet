'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var transferCtrl = function ($scope, utilsSrvc, $state) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

    };

    transferCtrl.$inject = injectParams;
    app.controller('transferCtrl', transferCtrl);

});