'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', '$state', '$stateParams', '$window', 'utilsSrvc', '$timeout'];

    var registrationCtrl = function ($scope, $state, $stateParams, $window, utilsSrvc, $timeout) {

        $scope.app = getApp();
        $scope.lang = $scope.app.lang;
          
        // inviter
        $scope.inviter = {
            user: false,
            confirmed: false,
            showChoose: false,
            confirm: function() {
                this.confirmed = true;
            },
            unconfirm: function() {
                this.confirmed = false;
            },
            remove: function() {
                this.user = null;
            },
            chooseMyself: function() {
                this.showChoose = true;
            },
            cancelChooseMyself: function() {
                this.showChoose = false;
            },
            setUser: function(data) {
                $scope.registration.form.follower_code = data.user_login;
                // $('#inviter-id').val(data.user_login);
                this.user = data;
                this.confirm();
                this.cancelChooseMyself();
            },
            search: {
                result: 1,
                login: '',
                loading: false,
                run: function(){
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/getinviter/', {user_login: this.login}, function(result){
                        that.loading = false;
                        if(result.data.result == 2) {
                            that.result = 2;
                        } else {
                            that.result = 1;
                            $scope.inviter.setUser(result.data.user);
                        }
                    }, function(){
                        that.result = 2;
                    });
                },
                reset: function(){
                    this.result = 1;
                }
            }
        };
        
        // user
        $scope.user = {
            social: {
                profile: $stateParams.socialProfile ? $stateParams.socialProfile : null, // token, email, fio, avatar
                cancel: function() {
                    this.profile = null;
                }
            },
            simple: {
                profile: {
                    first_name: null,
                    last_name: null,
                    phone: null,
                    email: null,
                    pin_code: null
                }
            }
        };

        // registration
        $scope.registration = {
            loading: false,
            agree_ref: false,
            form: {
                user_login: null,
                project_id: 2,
                user_login: null,
                // fname: null,
                // lname: null,
                phone: null,
                email: null,
                pwd1: null,
                pwd2: null,
                country_id: '',
                region_id: '',
                first_name: null,
                last_name: null,
                code_confirm: null,
                telegram_code: null,
                is_not_usa: null,
                accept: null
            },
            submit: function() {
                var that = this;
                /*if(!that.form.region_id) {
                    gmaback.js.showError('Пожалуйста выберите страну и регион', 4000);
                    return false;
                }*/
                that.loading = true;
                that.form.is_not_usa = that.form.is_not_usa ? 1 : 0;
                utilsSrvc.api.call('/app/registration/run/', that.form, function(result) {
                    getApp().loginCallback(result);
                    $state.go('index.user');
                    that.bepartner();
                }, null, null, function(){
                    that.loading = false;
                });
            },
            bepartner: function() {
                utilsSrvc.api.call('/app/buy/bepartner/', {}, function(result) {
                    getApp().changeProfile(result.data.user);
                }, null, null, function () {
                });
            },
            agreeRef: function () {
            	var that = this;
				that.agree_ref = true;
                CustomUI.hideForm();
                that.next();
            },
            back: function () {
            	CustomUI.hideForm();
            },
            next: function () {
                var that = this;                                
                if (!that.form.is_not_usa) {
                    gmaback.js.showError('Вы не подтвердили, что не являетесь гражданином США', 4000);
                    return;
                }
                if (!$scope.inviter.user && !that.agree_ref) {
                    CustomUI.showForm('form-ref');
					return false;
                }
                that.form.email = that.form.email || '';
                CustomUI.showWait('Пожалуйста, подождите', 'Проверка');
                utilsSrvc.api.call('/app/user/checkphoneemailproavto/', {
                        phone: that.form.phone,
                        email: that.form.email,
                        user_login: that.form.user_login,
                        first_name: that.form.first_name,
                        last_name: that.form.last_name,
                        project_id: 2
                    }, function(result) {
                    CustomUI.hideForm();
                    that.next_step = true;
                    $timeout(function(){
                        $('select').select2();
                        $('#select-region_id2').select2();
                    });
                }, null, null, function(){
                    CustomUI.hideForm();
                });
            }
        };

        // Region
        $scope.region = {
            list: [],
            loading: false,
            load: function(country_id) {
                var filter = {country_id: country_id};
                var that = this;
                that.loading = true;
                $scope.registration.form.region_id = '';
                utilsSrvc.api.call('/app/common/dictionaryregion/', filter,
                    function(result) {
                        that.list = result.data.list;
                        if(that.list.length == 1) {
                            $scope.registration.form.region_id = '' + that.list[0].id;
                        }
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    }, function(result) {
                        that.loading = false;
                    }
                );
            }
        };

        // Country changed
        $scope.countryChange = function() {
            $scope.region.load($scope.registration.form.country_id);
        };

        
        // Data ...
        $scope.data = {
            countries: [],
            loading: false,
            load: function(){
                var that = this;
                that.loading = true;
                var filter = {};
                utilsSrvc.api.call('/app/common/loadregistrationpagedata/', filter, function(result) {
                    that.countries = result.data.countries;
                }, function(err) {
                    if(err.code == 952) {
                        $state.go('index.user');
                    } else {
                        gmaback.js.showError(err.message, 4000);
                    }
                }, function() {
                }, function() {
                    that.loading = false;
                });
            }
        };
        
        var user_login = localStorage.getItem('user_login');
        if (user_login) {
            $scope.inviter.search.login = user_login;
            $scope.inviter.search.run();
        }

    };

    registrationCtrl.$inject = injectParams;
    app.controller('registrationCtrl', registrationCtrl);
});