'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var scheduleCtrl = function ($scope, utilsSrvc, $state) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

    };

    scheduleCtrl.$inject = injectParams;
    app.controller('scheduleCtrl', scheduleCtrl);

});