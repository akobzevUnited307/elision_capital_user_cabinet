'use strict';

define(['app', 'service/utils', 'service/messenger'], function(app) {

    var injectParams = ['$scope', '$state', '$stateParams', '$location', '$timeout', 'utilsSrvc', 'messengerSrvc'];
    var controller = function ($scope, $state, $stateParams, $location, $timeout, utilsSrvc, messengerSrvc) {

        gmaback.messenger_scope = $scope;
        $scope.$on('$destroy', function () {
            gmaback.messenger_scope = null;
            $scope.messenger.chat = {
                url: '',
                messages: []
            };
            if ($scope.currentContact) {
                gmaback.send(gmaback.options.event.command_leave_room, $scope.currentContact.url);
            }
        });

        $scope.messenger = messengerSrvc;
        $scope.logged_id = getApp().user.id;
        $scope.currentContact = null;
        $scope.obj = {
            messageText: ''
        };
        setTimeout(function() {
            gmaback.send(gmaback.options.event.command_get_contacts);
        }, 500);

        // Watch
        $scope.$watch('messenger.chat.messages.length', function(newVal, oldVal){
            setTimeout(function() {
                var scrollinDiv = $('.right-header-contentChat');
                if (scrollinDiv.length && $scope.messenger.needToScroll) {
                    scrollinDiv[0].scrollTop = scrollinDiv[0].scrollHeight;
                }
            }, 50);            
        }, true);

        // ...
        if ($stateParams.url) {
            $scope.$watch('messenger.contacts.length', function(newVal, oldVal){
                $scope.messenger.contacts.forEach(function(contact) {
                    if (contact.url == $stateParams.url) {
                        $scope.currentContact = contact;
                        $scope.enterToChat(contact);
                        return;
                    }
                });
                var unregister = $scope.$watch('messenger.chat.contacts', function () {
                    unregister();
                });    
            }, true);
        }

        // waitUntil ...
        function waitUntil() {  
            setTimeout(function() {
                var contentChat = $('#right-header-contentChat');
                if (contentChat.length) {
                    contentChat.scroll(function() {
                        var pos = contentChat.scrollTop();
                        if (pos == 0) {                            
                            if ($scope.messenger.chat.messages.length > 0) {
                                var message_id = $scope.messenger.chat.messages[0].id;
                                $scope.messenger.getPreviousMessages($scope.currentContact.url, message_id);
                            }
                        }
                    });
                }
                else {
                    waitUntil();
                }
            }, 500);
        }
        waitUntil();

        // enterToChat ...
        $scope.enterToChat = function(contact){ 
            $stateParams.url = contact.url;
            $scope.currentContact = contact;
            $scope.messenger.enterToChat(contact.url);
            $location.path('/messenger/' + contact.url);
        }

        // backContacts ...
        $scope.backContacts = function() {
            $scope.currentContact = false;
        }

        // sendMessage ...
        $scope.sendMessage = function(){            
            if ($scope.obj.messageText.trim().length === 0) {
                return;
            }
            $scope.messenger.sendMessage($scope.obj.messageText);
            $scope.obj.messageText = '';
        }

        // getClass ...
        $scope.getClass = function (path, url) {
            return ($location.path().replace(path, '') == url) ? 'left-chat-li-active' : '';
        }
    };

    controller.$inject = injectParams;

    app.controller('messengerCtrl', controller);

});