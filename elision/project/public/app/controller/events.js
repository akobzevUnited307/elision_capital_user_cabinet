'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', '$stateParams', 'utilsSrvc', '$state', '$timeout', '$interval'];
    var eventsCtrl = function ($scope, $stateParams, utilsSrvc, $state, $timeout, $interval) {

        // $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        
        $scope.event_pages = {
            isActive: function(state_name) {
                return $state.current.name.indexOf(state_name) == 0;
            }
        };
        

        // event_list
        $scope.event_list = {
            loading: false,
            office_loading: false,
            mode: 'list',
            data: false,
            event_cases_visible_count: false,
            offices: false,
            currentWeekDate: false,
            week: false,
            monthName: false,
            setMode: function(mode) {
                this.mode = mode;
            },
            load: function() {
                var that = this;  
                that.loading = true;                
                utilsSrvc.api.call('/app/events/getofficialeventcaselist', {}, function(result) {
                    that.data = result.data.list;
                    that.event_cases_visible_count = result.data.visible_count;
                    that.currentWeekDate = {
                        dt: new Date()
                    };
                    that.currentWeekDate.dt.setHours(0, 0, 0, 0);
                    that.currentWeekDate.dt = new Date(that.currentWeekDate.dt);
                    that.setWeek();
                }, null, null, function(){   
                    that.loading = false;
                });
            },
            loadoffices: function() {
                var that = this;  
                that.office_loading = true;                
                utilsSrvc.api.call('/app/office/getlist', { }, function(result){
                    that.offices = result.data.result;
                }, null, null, function(){
                    that.office_loading = false;
                });
            },
            setWeek: function () {    
                var that = this;  
                that.loading = true;
                var d = new Date(that.currentWeekDate.dt.valueOf());
                d.setHours(0,0,0,0);
                d = new Date(d);
                var monday = that.getMonday(d);                                       
                var days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
                var months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
                that.week = [];
                for (var i = 0; i < 7; i++) {
                    var dt = new Date(monday.valueOf());
                    dt.setDate(dt.getDate() + i);
                    that.week.push({ 
                        dt: dt, 
                        dayName: days[i],
                        hourList: [],
                        active: false
                    });
                    if (dt.setMinutes(0,0,0) == that.currentWeekDate.dt.setMinutes(0,0,0)) {
                        that.currentWeekDate.hourList = [];
                    }
                }
                var lastDay = that.week[that.week.length - 1].dt;
                that.monthName = months[lastDay.getMonth()] + ' ' + lastDay.getFullYear();
                that.week.forEach(function(dt) {
                    var tmpDate = new Date(dt.dt.valueOf());
                    tmpDate.setHours(tmpDate.getHours() + 8);
                    dt.hourList = [];
                    for (var i = 0; i < 15; i++) {
                        var date = new Date(tmpDate.valueOf());
                        date.setHours(date.getHours() + i);
                        dt.hourList.push({ 
                            dt: date
                        });
                        if (dt.dt.setMinutes(0,0,0) == that.currentWeekDate.dt.setMinutes(0,0,0)) {
                            that.currentWeekDate.hourList.push({ 
                                dt: date
                            });
                        }
                    }
                    if (dt.dt.setMinutes(0,0,0) == that.currentWeekDate.dt.setMinutes(0,0,0)) {
                        dt.active = true;
                    }
                    that.data.forEach(function(item) {
                        dt.hourList.forEach(function(hour) {  
                            var dt1 = new Date(hour.dt);
                            var dt2 = new Date(item.dt.replace(/-/g,"/"));
                            if(dt1.setMinutes(0,0,0) == dt2.setMinutes(0,0,0)) {
                                hour.data = item;
                            }
                        });
                        var dt2 = new Date(item.dt.replace(/-/g,"/"));
                        if (that.currentWeekDate.dt.setMinutes(0,0,0) == dt2.setHours(0,0,0,0)) {
                            that.currentWeekDate.hourList.forEach(function(hour) {  
                                var dt1 = new Date(hour.dt);
                                var dt2 = new Date(item.dt.replace(/-/g,"/"));
                                if(dt1.setMinutes(0,0,0) == dt2.setMinutes(0,0,0)) {
                                    hour.data = item;
                                }                            
                            });
                        }
                    });             
                });      
                that.loading = false;        
            },
            toLeft: function () {  
                var that = this;    
                that.currentWeekDate.dt.setDate(that.currentWeekDate.dt.getDate() - 7);
                that.setWeek();
            },
            toRight: function () {
                var that = this;    
                that.currentWeekDate.dt.setDate(that.currentWeekDate.dt.getDate() + 7);
                that.setWeek();
            },
            getMonday: function (d) {
                d = new Date(d.valueOf());
                var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
                return new Date(d.setDate(diff));
            },
            setCurrentDay: function (day) {
                var that = this;
                that.currentWeekDate.dt = day.dt;
                var days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
                that.currentWeekDate.hourList = [];
                
                var tmpDate = new Date(day.dt.valueOf());
                tmpDate.setHours(tmpDate.getHours() + 8);
                
                that.week.forEach(function(dt) {
                    dt.active = false;
                    if (dt.dt.setMinutes(0,0,0) == that.currentWeekDate.dt.setMinutes(0,0,0)) {
                        dt.active = true;
                    }
                });
                
                for (var i = 0; i < 15; i++) {
                    var date = new Date(tmpDate.valueOf());
                    date.setHours(date.getHours() + i);
                    that.currentWeekDate.hourList.push({ 
                        dt: date
                    });       
                }
                
                that.data.forEach(function(item) {  
                    var dt2 = new Date(item.dt.replace(/-/g,"/"));
                    if (that.currentWeekDate.dt.setMinutes(0,0,0) == dt2.setHours(0,0,0,0)) {
                        that.currentWeekDate.hourList.forEach(function(hour) {  
                            var dt1 = new Date(hour.dt);
                            var dt2 = new Date(item.dt.replace(/-/g,"/"));
                            if(dt1.setMinutes(0,0,0) == dt2.setMinutes(0,0,0)) {
                                hour.data = item;
                            }                            
                        });
                    }
                });
            },
            getEventClass: function (event) {
                var cls = 'event-type-';
                switch (event.event_type_id) {
                    case 1:
                        cls += 'meet';
                        break;
                    case 2:
                        cls += 'training';
                        break;
                    case 3:
                        cls += 'seminar';
                        break;
                    case 4:
                        cls += 'training-dax';
                        break;
                    default:
                        cls = '';
                        break;
                }
                return cls;
            }                     
        };
        
        // allTickets
        $scope.allTickets = {
            loading: true,
            list: [],
            open: function() {
                CustomUI.showForm('form-allTickets');
                var that = this;  
                that.loading = true;
                utilsSrvc.api.call('/app/events/loadalltickets', { official_event_case_id: $stateParams.id }, function(result){
                    that.list = result.data.list;
                }, null, null, function(){
                    that.loading = false;
                });
            },
        };

        // registerVisit
        $scope.registerVisit = {
            loading: false,
            error: '',
            loading_ticket_info: false,
            inited: false,
            error: null,
            ticket_info: null,
            form: {
                official_event_case_id: '',
                follower_id: '',
                ticket_number: ''
            },
            reset: function() {
                console.log('reset');
                this.form.official_event_case_id = '';
                this.form.follower_id = '';
                this.form.ticket_number = '';
                this.ticket_info = null;
            },
            loadTicketInfo: function(ticket_number) {
                var that = this;  
                that.loading_ticket_info = true;
                utilsSrvc.api.call('/app/events/loadticketinfo', {ticket_number: ticket_number}, function(result) {
                    that.ticket_info = result.data;
                }, function(err) {
                    that.error = err.message;
                }, null, function(){
                    that.loading_ticket_info = false;
                });
            },
            clearTicket: function(ticket_number) {
                var that = this;  
                utilsSrvc.api.call('/app/events/clearticket', {ticket_number: that.form.ticket_number}, function(result){
                    gmaback.js.showMessage('Билет аннулирован', 4000);
                    that.reset();
                }, null, null, function(){
                    that.loading_ticket_info = false;
                });
            },
            open: function() {
                var that = this;  
                that.reset();
                if(!that.inited) {
                    that.inited = true;
                    $scope.$watch('registerVisit.form.ticket_number', function() {
                        if(that.form.ticket_number.length == 9) {
                            that.loadTicketInfo(that.form.ticket_number);
                        } else {
                            that.error = null;
                            that.ticket_info = null;
                        }
                    });
                }
                CustomUI.showForm('form-registerVisit');
                $timeout(function(){
                    $('#input-ticket-number').focus();
                });
            },
            submit: function() {
                var that = this;  
                that.loading = true;
                that.form.official_event_case_id = $stateParams.id;
                utilsSrvc.api.call('/app/events/registervisit', that.form, function(result){
                    // that.form.cnt = '';
                    CustomUI.hideForm();
                    gmaback.js.showMessage('Билет зарегистрирован', 4000);
                    that.open();
                    $scope.createTicket.loadStat();
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

        // createTicket
        $scope.createTicket = {
            stat: [],
            loading_stat: true,
            loading: false,
            filter: '',
            form: {
                cnt: ''
            },
            speakerForm: {
                number: ''
            },
            loadStat: function() {
                var that = this;
                that.loading_stat = true;
                utilsSrvc.api.call('/app/events/ticketsstat', { official_event_case_id: $stateParams.id }, function(result){
                    that.stat = result.data.stat;
                    that.stat.newbie_cnt = that.stat.users.filter(function(item) {
                        return item.is_newbie;
                    }).length;
                    that.stat.invest_cnt = that.stat.users.filter(function(item) {
                        return item.is_user && !item.is_partner && !item.need_blue_badge;
                    }).length;
                    that.stat.partner_cnt = that.stat.users.filter(function(item) {
                        return item.is_user && item.is_partner;
                    }).length;
                    that.stat.elite_cnt = that.stat.users.filter(function(item) {
                        return item.need_blue_badge;
                    }).length;
                    that.stat.newbie_used_cnt = that.stat.users.filter(function(item) {
                        return item.is_newbie && item.is_used;
                    }).length;
                    that.stat.invest_used_cnt = that.stat.users.filter(function(item) {
                        return item.is_user && !item.is_partner && !item.need_blue_badge && item.is_used;
                    }).length;
                    that.stat.partner_used_cnt = that.stat.users.filter(function(item) {
                        return item.is_user && item.is_partner && item.is_used;
                    }).length;
                    that.stat.elite_used_cnt = that.stat.users.filter(function(item) {
                        return item.need_blue_badge && item.is_used;
                    }).length;
                }, null, null, function(){
                    that.loading_stat = false;
                });
            },
            open: function() {
                CustomUI.showForm('form-createTicket');
            },
            openSpeaker: function() {
                CustomUI.showForm('form-addSpeaker');
            },
            numberChange: function() {
                var that = this;
                that.searchUser = null;
                if (!that.speakerForm.number) {
                    return;
                }                
                utilsSrvc.api.call('/app/events/getuserbynumberloginphone/', { number: that.speakerForm.number, official_event_case_id: $stateParams.id }, function(result) {
                    that.searchUser = result.data;
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
            search: function(item) {
                var filter = $scope.createTicket.filter;
			    if (!filter || (item.number.toLowerCase().indexOf(filter.toLowerCase()) != -1 || 
			    	(item.fio || '').toLowerCase().indexOf(filter.toLowerCase()) != -1 || 
			    	(item.phone || '').toLowerCase().indexOf(filter.toLowerCase()) != -1)) {
				    return true;
				}
			    return false;
			},
			clear: function() {
				this.filter = '';
			},
            submit: function() {
                var that = this;  
                that.loading = true;
                utilsSrvc.api.call('/app/events/createtickets', { official_event_case_id: $stateParams.id, cnt: that.form.cnt }, function(result){
                    that.stat = result.data;
                    that.form.cnt = '';
                    CustomUI.hideForm();
                }, null, null, function(){
                    that.loading = false;
                });
            },
            addSpeaker: function() {
                var that = this;  
                that.loading = true;
                utilsSrvc.api.call('/app/events/addspeaker', { official_event_case_id: $stateParams.id, user_id: that.searchUser.id }, function(result){
                    that.speakerForm.number = '';
                    that.searchUser = null;
                    CustomUI.hideForm();
                    that.loadStat();
                }, null, null, function(){
                    that.loading = false;
                });
            }            ,
            removeSpeaker: function(id) {
                var that = this;  
                that.loading = true;
                utilsSrvc.api.call('/app/events/removespeaker', { id: id }, function(result){
                    that.loadStat();
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };
        
        // showVideoNotEnabled
        $scope.showVideoNotEnabled = function(){
            gmaback.js.showMessage('На данное мероприятие трансляция недоступна', 4000);
        };
        
        // event
        $scope.event = {
            loading: false,
            data: false,
            office: false,
            load: function() {
                var that = this;  
                that.loading = true;           
                if (!$stateParams.id) {
                    return;
                }
                utilsSrvc.api.call('/app/events/getofficialeventcase', { id: $stateParams.id }, function(result){
                    that.data = result.data;
                    var dateFuture = new Date(that.data.dt.replace(/-/g,"/"));
                    var dateFutureEnd = new Date(that.data.dt_end.replace(/-/g,"/"));
					var dateNow = new Date();
					that.data.showBtnIGo = dateNow < dateFuture;
					that.data.showBtnBuy = dateNow < dateFutureEnd;
					
                    if ($scope.app.user) {
                        that.data.showBtnIGo = that.data.showBtnIGo && !that.data.user_list.find(function(item) {
                            return $scope.app.user.id == item.user_id;
                        }); 
                        that.data.showBtnIGo = that.data.showBtnIGo && !that.data.user_ticket_list.find(function(item) {
                            return $scope.app.user.id == item.user_id;
                        });
                    }
					
					that.data.custom_address_orig = that.data.custom_address;
					that.data.dt_minutes = 0;
					that.data.dt_hours = 0;
					that.data.dt_days = 0;
					if (dateFuture > dateNow) {					
						var seconds = Math.floor((dateFuture - (dateNow))/1000);
						that.data.dt_minutes = Math.floor(seconds/60);
						that.data.dt_hours = Math.floor(that.data.dt_minutes/60);
						that.data.dt_days = Math.floor(that.data.dt_hours/24);
						that.data.dt_hours = that.data.dt_hours-(that.data.dt_days*24);
						that.data.dt_minutes = that.data.dt_minutes-(that.data.dt_days*24*60)-(that.data.dt_hours*60);                    
					}
                    that.loadoffice(that.data.office_id);
                }, null, null, function(){
                    that.loading = false;
                });
            },
            numberWithSpace: function (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
            },
            download2: function() {
		       for(var i=0; i<arguments.length; i++) {
		         var iframe = $('<iframe style="visibility: collapse;"></iframe>');
		         $('body').append(iframe);
		         var content = iframe[0].contentDocument;
		         var form = '<form action="' + arguments[i] + '" method="GET"></form>';
		         content.write(form);
		         $('form', content).submit();
		         setTimeout((function(iframe) {
		           return function() { 
		             iframe.remove(); 
		           }
		         })(iframe), 2000);
		       }
			},
            free: function() {
                var that = this;  
                that.loading = true;
                utilsSrvc.api.call('/app/events/freeofficialeventcase', { id: $stateParams.id }, function(result){
                    that.data.user_list.push(result.data);
                    that.data.showBtnIGo = false;
                    gmaback.js.showMessage($scope.app.lang.save_success, 4000);
                }, null, null, function(){
                    that.loading = false;
                });
            },
            // покупка билета на мероприятие
            buy: {
                order: {
                    visible: false,
                    loading: false,
                    foruser: null,
                    form: {
                        id: null,
                        forme: '1',
                        forregistereduser: '1',
                        foruserid: '',
                        foruser_number: '',
                        guest_im_inviter: '1',
                        guest_inviter_userid: '',
                        product_type: 'official_event_case',
                        // fio: '',
                        first_name: '',
                        last_name: '',
                        phone: '',
                        product_id: '',
                        official_event_case_price_id: '',
                        ticket_count: 1,
                        official_event_case_id: ''
                    },
                    reset: function() {
                        this.form.id = '';
                        this.form.forme = '1';
                        this.form.forregistereduser = '1';
                        this.form.foruserid = '';
                        this.form.foruser_number = '';
                        this.form.guest_im_inviter = '1';
                        this.form.guest_inviter_userid = '';
                        this.form.product_type = 'official_event_case';
                        this.form.first_name = '';
                        this.form.last_name = '';
                        this.form.phone = '';
                        this.form.product_id = '';
                        this.form.official_event_case_price_id = '';
                        this.form.official_event_case_id = '';
                        this.form.is_debt = '';
                        this.form.ticket_count = 1;
                        this.foruser = null;
                    },
                    show: function(price) {
                        var that = this;
                        that.loading = true;
                        that.form.official_event_case_id = $scope.event.data.id;
                        if (price) {
                            that.form.product_id = price.product_id;
                        	that.form.official_event_case_price_id = price.id;
						}
                        utilsSrvc.api.call('/app/qwertypay/createorder/', that.form, function(result) {                        	
                        	if (result.data == true) {
								CustomUI.hideForm();
								$scope.event.load();
                                that.reset();
                                return;
                        	}
                            that.form.id = result.data.order_id;
                        	var exists = $scope.event.data.price_list.find(function(item) {
								return item.is_active == 1;
                        	});
                        	if (!exists || !(price && price.qwertypay_ticket_id)) {
								CustomUI.hideForm();
								$scope.event.load();
                                that.reset();
								gmaback.js.showMessage('Билет приобретен', 4000);
                        	} else {                            
                        		that.visible = true;
							}
                        	
                        }, null, null, function(){
                            that.loading = false;
                        });
                        return false;
                    },
                    saveTicketData: function(price) {
                        var that = this;
                        that.loading = true;
                        that.form.official_event_case_id = $scope.event.data.id;
                        utilsSrvc.api.call('/app/qwertypay/saveticketdata/', that.form, function(result) {
							CustomUI.hideForm();
							$scope.event.load();
                            that.reset();
                        }, null, null, function(){
                            that.loading = false;
                        });
                        return false;
                    },
                    canBy: function() {
                        if(this.form.forme == '1') {
                            if($scope.event.data) {
                                return !$scope.event.data.my_ticket;
                            } else {
                                return false;
                            }
                        } else if(this.form.forme == '2') {
                            if(this.form.forregistereduser == '1') {
                                return !!$scope.event.buy.order.foruser && $scope.event.buy.order.foruser.ok;
                            	// return this.form.foruserid != '';
                            } else {
                                if(this.form.first_name && this.form.last_name && this.form.phone) {
                                    if(this.form.guest_im_inviter == '0') {
                                    	// return this.form.guest_inviter_userid != '';
                                        return !!$scope.event.buy.order.guest_inviter_user;
                                    } else {
                                        return true;
                                    }
                                } else {
                                    return false;
                                }
                            }
                        } else if(this.form.forme == '3') {
                        	return true;
						}
                    },
                    buyForChanged: function () {
						this.form.ticket_count = 1;
                    },
                    foruseridChange: function() {
		                var that = this;
		                that.foruser = null;
		                if (!that.form.foruserid) {
		                    return;
		                }		                
            			utilsSrvc.api.call('/app/qwertypay/getuserbynumberloginphone/', { number: that.form.foruserid }, function(result) {
                            that.form.foruser_number = result.data.number;
            				that.foruser = result.data;
            			}, null, null, function(err) {
			                that.loading = false;
			            });
		            },
		            guestInviterUseridChange: function() {
		                var that = this;    
		                that.guest_inviter_user = null;
		                if (!that.form.guest_inviter_userid) {
		                    return;
		                }		            
            			utilsSrvc.api.call('/app/qwertypay/getuserbynumberloginphone/', { number: that.form.guest_inviter_userid }, function(result) {
            				that.guest_inviter_user = result.data;
            			}, null, null, function(err) {
			                that.loading = false;
			            });
		            },
		            clickPayBtn: function() {
						CustomUI.hideForm();
		            	$scope.event.load();
		            }
                },
                open: function(){
                    this.order.reset();
                    this.order.visible = false;
                    return CustomUI.showForm('form-buy-ticket');
                },
                openDebt: function(){
                    this.order.reset();
                    this.order.visible = false;
                    this.order.form.is_debt = true;
                    return CustomUI.showForm('form-buy-ticket-debt');
                },
                openSaveData: function(item){
                    this.order.reset();
                    this.order.form.official_event_case_ticket_id = item.id;
                    this.order.visible = false;
                    return CustomUI.showForm('form-buy-ticket-preorder');
                },
                submit: function(){
                    CustomUI.hideForm();
                    alert('ok');
                }
            },
            loadoffice: function(office_id) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/office/getlist/', {id: office_id}, function(result) {
                	$interval(function() {
                		that.data.my_unpaided_ticket_order.forEach(function (item) {
                			that.getTimeToStart(item);
                		});
                	}, 1000);
                    $timeout(function(){
                        var officeArr = that.buildOfficeObject(result.data.result);
                        that.office = officeArr[0];
                        var myMap, layer;
                        var points_data = [];
                        var points = [[parseFloat(that.office.lot), parseFloat(that.office.lan)]];
                        var points_raw = [];
                        var points_title_data = [];
                        points_data[points_data.length] = '<div class="point-baloon"><h2>Адрес: Ямашева, д. 102А, п. 1</h2><table class="point-baloon-table"><tr><td><a class="fancybox" href="/data/imedia_kaz/01081049-10003650.jpg"><img src="/data/imedia_kaz/01081049-10003650.jpg" /></a><span class="code128">2112142331112</span></td><td>Код лифта:<br><strong>10003650</strong><br><br>Район:<br><strong>Элитка Ново</strong><br><br>Код тиража:<br><strong>01081049</strong><br><br>Дата:<br><strong>1 Апреля 2019, 08:02</strong></td></tr></table></div>';
                        points_raw[points_raw.length] = {"id":"12769","code":"10003650","photo":null,"desc":null,"dist":"\u042d\u043b\u0438\u0442\u043a\u0430 \u041d\u043e\u0432\u043e","street":"\u042f\u043c\u0430\u0448\u0435\u0432\u0430","house":"102\u0410","porch":"1","way_id":"77","way_step":"100","type":null,"cord_x":that.office.lot,"cord_y":that.office.lan,"status_id":0,"msg":null,"last_action_time":"1 \u0410\u043f\u0440\u0435\u043b\u044f 2019, 08:02"};
                        /**
                        * http://openlayers.org/en/latest/examples/
                        * http://openlayers.org/en/latest/apidoc/
                        * http://korona.geog.uni-heidelberg.de/tiles/roads/x=41719&y=20476&z=16
                        **/
                        myMap = new ol.Map({
                            target: 'map',
                            layers: [
                                new ol.layer.Tile({
                                    source: new ol.source.OSM()
                                })
                            ],
                            view: new ol.View({
                                // projection: 'EPSG:4326',
                                center: ol.proj.fromLonLat([parseFloat(that.office.lot), parseFloat(that.office.lan)]),
                                zoom: 15
                            })
                        });
                        const overlay = new ol.Overlay({
                            element: document.getElementById('popup-container'),
                            positioning: 'bottom-center',
                            offset: [0, -10],
                            autoPan: true
                        });
                        myMap.addOverlay(overlay);
                        var styles = {
                            'route': new ol.style.Style({
                                stroke: new ol.style.Stroke({
                                    width: 3, color: [237, 212, 0, 0.8]
                                })
                            }),
                            'red': new ol.style.Style({image: new ol.style.Icon({anchor: [0.5, 0.5], src: '/assets/img/map_point.png'})}),
                            'geoMarker': new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 7,
                                    snapToPixel: false,
                                    fill: new ol.style.Fill({color: 'black'}),
                                    stroke: new ol.style.Stroke({
                                        color: 'white', width: 1
                                    })
                                })
                            })
                        };
                        var layers = [];
                        // Зелёные
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                /*// hide geoMarker if animation is active
                                if (animating && feature.get('type') === 'geoMarker') {
                                    return null;
                                }*/
                                return styles[feature.get('type')];
                            }
                        }));
                        // Красные
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                return styles[feature.get('type')];
                            }
                        }));
                        // Жёлтые
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                return styles[feature.get('type')];
                            }
                        }));
                        // Серые
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                return styles[feature.get('type')];
                            }
                        }));

                        for(var i = 0, len = points.length; i < len; i++) {
                            var s = points_raw[i].status_id;
                            var type = 'red';
                            var marker = new ol.Feature({
                                type: type,
                                index: i,
                                data: points_raw[i],
                                geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(points_raw[i].cord_x), parseFloat(points_raw[i].cord_y)]))
                            });
                            layers[s].getSource().addFeature(marker);
                        }
                        myMap.addLayer(layers[1]);
                        myMap.addLayer(layers[2]);
                        myMap.addLayer(layers[3]);
                        myMap.addLayer(layers[0]);
                        myMap.on('click', function(e) {
                            let markup = '';
                            myMap.forEachFeatureAtPixel(e.pixel, function(feature) {
                                if(markup == '') {
                                    markup += `${markup && '<hr>'}<table>`;
                                    const properties = feature.getProperties();
                                    console.log(properties);
                                    var index = properties.index;
                                    markup += points_data[index];
                                }
                            }, {hitTolerance: 1});
                            if (markup) {
                                document.getElementById('popup-content').innerHTML = markup;
                                overlay.setPosition(e.coordinate);
                            } else {
                                overlay.setPosition();
                            }
                        });
                    });
                }, null, null, function(){
                    that.loading = false;
                });
            },
            buildOfficeObject: function (officesArr) {
                officesArr.forEach(function(of) {
                    if(of.contact_list) {
                        of.contact_list.forEach(function (contact) {
                            if (contact.office_contact_type_id == 1) {
                                of.address = contact.value;
                            } else if (contact.office_contact_type_id == 2) {
                                of.number = contact.value;
                            } else if (contact.office_contact_type_id == 3) {
                                of.email = contact.value;
                            } else if (contact.office_contact_type_id == 4) {
                                of.telegram = contact.value;
                            } else if (contact.office_contact_type_id == 5) {
                                of.whatsapp = contact.value;
                            } else if (contact.office_contact_type_id == 6) {
                                of.contact_person = contact.value;
                            }
                        });
                    }
                    if(of.social_list) {
                        of.social_list.forEach(function (social) {
                            if (social.social_type_id == 1) {
                                of.vk = social.value;
                            } else if (social.social_type_id == 2) {
                                of.facebook = social.value;
                            } else if (social.social_type_id == 5) {
                                of.twitter = social.value;
                            } else if (social.social_type_id == 7) {
                                of.youtube = social.value;
                            } else if (social.social_type_id == 8) {
                                of.instagram = social.value;
                            }
                        });
                    }
                });

                return officesArr;
            },
            getTimeToStart: function (item) {
				var that = this;
				var nowDate = new Date();
				var achiveDate = new Date(item.dt); //Задаем дату, к которой будет осуществляться обратный отсчет
				achiveDate = new Date(achiveDate.setHours(achiveDate.getHours() + 1));
				var result = (achiveDate - nowDate) + 1000;
				if (result < 0) {
				    var elmnt = document.getElementById('timer' + item.id);
        			return '';
				}
				var seconds = Math.floor((result/1000) % 60);
				var minutes = Math.floor((result/1000/60) % 60);
				if (seconds < 10) seconds = '0' + seconds;
				if (minutes < 10) minutes = '0' + minutes;
				var elmnt = document.getElementById('timer' + item.id);
				return minutes + ' мин ' + seconds + ' сек';
			},
			saveAddress: function () {
				var that = this;  
                that.loading = true;
                utilsSrvc.api.call('/app/events/savecustomaddress', { id: $stateParams.id, custom_address: that.data.custom_address }, function(result){
                    that.load();
                }, null, null, function(){
                    that.loading = false;
                });
			}
        };       
    };

    eventsCtrl.$inject = injectParams;
    app.controller('eventsCtrl', eventsCtrl);

});