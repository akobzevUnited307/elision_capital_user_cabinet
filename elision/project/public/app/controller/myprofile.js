'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams'];
    var myprofileCtrl = function ($scope, utilsSrvc, $state, $stateParams) {

        $scope.pages.setURL($state.current.name);        
    };

    myprofileCtrl.$inject = injectParams;
    app.controller('myprofileCtrl', myprofileCtrl);

});