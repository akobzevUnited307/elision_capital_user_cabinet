'use strict';

define(['app'], function(app) {
    
    var injectParams = ['$scope', '$state', 'utilsSrvc', '$stateParams', '$timeout', '$window'];
    var createwalletCtrl = function ($scope, $state, utilsSrvc, $stateParams, $timeout, $window) {
        
        // Constants
        $scope.lang = getApp().lang;
        $scope.app = getApp();
        $scope.BTC_SATOCHI = 100000000;
        $scope.MIN_WITHDRAWAL_BTC_SATOSHI = 1000000;

        // Страница кошелька
        $scope.wallet = {
            loading: false,
            data: {},
            list: {
                main: [],
                invest: [],
            },
            operations: {
                total: 0,
                visible_count: 0,
                showMore: function() {
                    this.visible_count += 50;
                    if(this.visible_count > this.total) {
                        this.visible_count = this.total;
                    }
                }
            },
            init: function() {                           
                this.load();
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/user/loadwalletpagedata', {}, function(result){
                    that.data = result.data;
                    getApp().changeVerify(that.data.is_verificated);
                    that.list.main = [];
                    that.list.invest = []; 
                    for(var w of that.data.dax_wallets) {
                        if(w.dax_wallet_type_id == 1) {
                            that.list.main.push(w);
                        } else if(w.dax_wallet_type_id == 2) {
                            that.list.invest.push(w);
                        }
                    }    
                    $scope.walletView.init();
                }, null, null, function(){
                    that.loading = false;
                });
            },
            copyToClipboard: function (){
                var that = this;
                const el = document.createElement('textarea');
                el.value = that.data.buy_wallet.address;               
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                document.body.removeChild(el);
                gmaback.js.showMessage('Текст скопирован', 4000);
            },
            getNowYear: function() {
                var currentTime = new Date();
                var year = currentTime.getFullYear();
                return year; 
             },
        };


        // $scope.elision_wallet = {
        //     loading: false,
        //     load:function() {
        //         debugger;
        //         var that = this;
        //         that.loading = true;
        //         utilsSrvc.api.call('app/wallet/loadelisionwallets', {}, function(result) {
        //            that.list = result.data;
        //         }, null, null, function(){
        //             that.loading = false;
        //         });
        //     }
        // }

        // Страница выбранного одного кошелька
        $scope.walletView = {
            loading: false,
            wallet: null,
            init: function() {
                var that = this;
                that.loading = true; 
                var wallet = $scope.wallet.list.main[0];
                utilsSrvc.api.call('app/user/loadwalletpagedata', {dax_wallet: {number: wallet.number, operations: true}}, function(result) {
                    for(var w of result.data.dax_wallets) {
                        if(w.number == wallet.number) {
                            that.wallet = w;                   
                        }
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            }
        }

        // Buy DAX
        $scope.buy = {
            loading: false,
            loading_submit: false,
            can_create_intro_dax_wallet: false,
            can_create_intro_plus_dax_wallet: false,
            quotes: [],
            btc_quote_rate: null,
            dax_in_usd: 2.0,
            form: {
                from_amount: '1',
                to_amount: '500',
                from_currency_code: 'btc',
            },
            getCurrentQuote: function () {
                for (var q of this.quotes) {
                    if (q.code == this.form.from_currency_code) {
                        return q;
                    }
                }
                return {};
            },
            init: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/buy/loadinfo', {}, function (resp) {
                    that.quotes = resp.data.quotes;
                    that.can_create_intro_dax_wallet = resp.data.can_create_intro_dax_wallet;
                    that.can_create_intro_plus_dax_wallet = resp.data.can_create_intro_plus_dax_wallet;
                    that.btc_in_usd = resp.data.btc_in_usd; 
                    that.changeToAmount();
                    var btc_quote = that.quotes.filter(function(item) {
                        return item.code == 'btc';
                    });
                    if (btc_quote.length) {
                        that.btc_quote_rate = btc_quote[0].rate;
                    }
                }, null, null, function() {
                    that.loading = false;
                });
            },
            changeFromAmount: function() {
                this.form.to_amount = Math.round(this.form.from_amount * this.btc_in_usd * 100000000) / 100000000;                                
            },
            changeToAmount: function() {
                this.form.from_amount = Math.round(this.form.to_amount / this.btc_in_usd * 100000000) / 100000000;
            },
            changeCurrency: function() {
                this.changeFromAmount();
            },
            submit: function() {
                var that = this;
                that.loading_submit = true;
                var form = {
                    form: {
                        currency_from: this.form.from_currency_code.toUpperCase(),
                        from_amount: this.form.from_amount,
                        to_amount: this.form.to_amount
                    }
                };
                utilsSrvc.api.call('app/exchange/transfer', form, function (resp) {
                    // Success
                    var txn = resp.data.txn;
                    $('#resp_address').val(txn.address);
                    $('#resp_message').html(txn.message);
                    $('#form-submit a.button-submit.disabled').removeClass('disabled');
                    $('#resp_give').text(resp.data.give);
                    $('#resp_receive').html(resp.data.receive_text);
                    $('#resp_qrcode_url .img_shadow').html('<img src="' + txn.qrcode_url + '">');
                    $('#resp_confirms_needed').closest('p').show();
                    if (txn.confirms_needed) {
                        $('#resp_confirms_needed').text(txn.confirms_needed);
                    } else {
                        $('#resp_confirms_needed').closest('p').hide();
                    }
                    $('#resp_timeout_string').text(txn.timeout_string);
                    if (resp.data.currency_from == 'XRP') {
                        $('#tag').show().find('span').text(txn.tag);
                    } else {
                        $('#tag').hide();
                    }
                    CustomUI.centerForms();
                    CustomUI.hideForm();
                    preloader.off();
                    CustomUI.showForm('form-result', true, true, {
                        'onclose': function () {
                            window.location.reload();
                        }
                    });
                }, function (resp) {
                    CustomUI.hideForm();
                    preloader.off();
                    return CustomUI.showMessage(resp.message, 'Error', 'error', {});
                }, null, function () {
                    that.loading_submit = false;
                });

            },
            toAccounts: function () {
                CustomUI.hideForm();
                $state.go('navigation.user_view', {code: $scope.app.user.user_login});
            }
        };

        // startCreate
        $scope.startCreate = {
			loading: false,
            stages: [true, false],
			createGenesis: false,
			closeIntro: new Date() >= new Date('2020-09-01 00:00:00'.replace(/-/g, "/")),  
			createIntro: false,
            createIntroPlus: false,
			settingsTrust: false,
			moreGenesis: false, // 1
			moreElision: false, // 2
			moreIntelex: false, // 3
            clickGenesis: function() {
                var that = this;
                that.createGenesis = true;
                $scope.createGenesis.form.dax_wallet_invest_type_id = '1';
                $scope.createGenesis.invest_tarif_text = $scope.lang.tarif + ' Standard';                
                $scope.createGenesis.conditions = [
	                $scope.lang.genesis_open_conditions_txt_5,
					$scope.lang.genesis_open_conditions_txt_10,
					$scope.lang.genesis_open_conditions_txt_7,
					$scope.lang.genesis_open_conditions_txt_11,
					$scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
            clickIntro: function() {
                var that = this;
                that.createIntro = true;
                that.createIntroPlus = false;
                that.createTrust = false;
                $scope.createGenesis.form.dax_wallet_invest_type_id = '3';
                $scope.createGenesis.invest_tarif_text = $scope.lang.tarif + ' Intro';
                $scope.createGenesis.conditions = [
	                $scope.lang.genesis_open_conditions_txt_5,
					$scope.lang.genesis_open_conditions_txt_6,
					$scope.lang.genesis_open_conditions_txt_7,
					$scope.lang.genesis_open_conditions_txt_8,
					$scope.lang.genesis_open_conditions_txt_9
                ];
                that.create();
            },
            clickIntroPlus: function() {
                var that = this;
                that.createIntroPlus = true;
                that.createIntro = false;
                that.createTrust = false;
                $scope.createGenesis.form.dax_wallet_invest_type_id = '4';
                $scope.createGenesis.invest_tarif_text = $scope.lang.tarif + ' Intro+';
                $scope.createGenesis.conditions = [
                    $scope.lang.genesis_open_conditions_txt_5,
                    $scope.lang.genesis_open_conditions_txt_6,
                    $scope.lang.genesis_open_conditions_txt_7,
                    $scope.lang.genesis_open_conditions_txt_8,
                    $scope.lang.genesis_open_conditions_txt_9
                ];
                that.create();
            },
			clickTrust: function() {
                /*gmaback.js.showError('Открытие нового счета Genesis с данным тарифом временно невозможно', 4000);
                return;*/
                
				var that = this;
                that.createIntro = false;
                that.createIntroPlus = false;
                that.createTrust = true;
				$scope.createGenesis.form.dax_wallet_invest_type_id = '2';
				$scope.createGenesis.invest_tarif_text = $scope.lang.tarif + ' Trust';
				$scope.createGenesis.conditions = [
	                $scope.lang.genesis_open_conditions_txt_5,
					$scope.lang.genesis_open_conditions_txt_12,
					$scope.lang.genesis_open_conditions_txt_13,
					$scope.lang.genesis_open_conditions_txt_14,
					$scope.lang.genesis_open_conditions_txt_15,
					$scope.lang.genesis_open_conditions_txt_16
                ];
				that.create();
			},
			create: function() {
				var that = this;
				that.stages = [false, false, false, false];
				$scope.createGenesis.init();
			},
			openStage: function(index) {
                for(var i in this.stages) {
                    this.stages[i] = i == index;
                }
				if (index == 0) {
					that.moreGenesis = false;
					that.moreElision = false;
					that.moreIntelex = false;
				}
            },
            clickDescription: function($event) {
            	var block = $($event.target);
            	var className = 'invest-description-text';
				if (block.hasClass(className)) {
					block.removeClass(className);
				} else {
					block.addClass(className);
				}
				$('#invest-description-text').height(block.height());
            },
			loadWalletElision: function() {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('app/wallet/getElisionList', {}, function (resp) {
                    that.list_elision = resp.data;
				}, function(reason) {
                    gmaback.js.showError(reason.message, 4000);
                }, null, null, function() {
                    that.loading = false;
                });
			}, 
			moreDetails: function(index) {
				var that = this;
				that.moreGenesis = index == 1;
				that.moreElision = index == 2;
				that.moreIntelex = index == 3;
			},
			checkMore: function() {
				var that = this;
				if (that.moreGenesis || that.moreElision || that.moreIntelex) {
					return true;
				} else {
					return false;
				}
			}
        }

		$scope.startCreateElision = {
			loading: false,
			createNeural: false,
			createEclipse: false,
			createEnergon: false,
			createSoller: false,
			createForever: false,
			createInfinite: false,
			createDelorean: false,
			createNext: false,
			createDrive: false,
			createEver: false,
			createLife: false,
			createSerenity: false,
			el_stages : [true, false, false, false, false],
			show_telegram: false,
			show_password: false,
			fname: '',
			flag: false,
			title: '',
			id: 0,
			month_percent: 0,
			min_deposit: 0,
			max_deposit: 0,
			month_period: 0,
			tariff_delay_day: 0,
			correct_sum: 0,   // 0 ничего не ввели, 1 - меньше сумма, 2 - больше, 3 - недостаточно на счету
			form: {
				amount: 0,
				min_sum: 0,
				max_sum: 0,
				profit_percent: 0,
				title: '',
				validity: 1,
                payment_method_id: '',
                elision_invest_type_id: '1',
                password: ''
            },
			clickPortfolio: function(tariff, index) {
				var that = this;
				for(var i in this.el_stages) {
                    this.el_stages[i] = i == index;
                }
				
				for (var q of $scope.startCreate.list_elision.elision_capital) {
					if (q.title == tariff) {
						that.id = q.id;
                        that.title = q.title;
						that.month_percent = q.profit_percent;
						that.min_deposit = q.minimum_sum / $scope.BTC_SATOCHI;
						that.max_deposit = q.max_limit / $scope.BTC_SATOCHI;
						that.month_period = q.validity;
						that.tariff_delay_day = q.tariff_delay_day;
					}
                }

				that.fname = 'that.click' + tariff+'()';
				eval(that.fname);
			},
            clickNeural: function() {
                var that = this;
                that.createNeural = true;
				// createNeural = false;
				that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '1';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Neural';                
                $scope.createElision.conditions = [
					'Получаете 2% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickEclipse: function() {
                var that = this;
                that.createEclipse = true;
				that.createNeural = false;
				// that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '2';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Eclipse';                
                $scope.createElision.conditions = [
					'Получаете 2.2% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickEnergon: function() {
                var that = this;
                that.createEnergon = true;
				that.createNeural = false;
				that.createEclipse = false;
				// that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '3';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Energon';                
                $scope.createElision.conditions = [
					'Получаете 2.5% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickSoller: function() {
                var that = this;
                that.createSoller = true;
				that.createNeural = false;
				that.createEclipse = false;
				that.createEnergon = false;
				// that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '4';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Soller';                
                $scope.createElision.conditions = [
					'Получаете 2.7% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickForever: function() {
                var that = this;
                that.createForever = true;
				that.createNeural = false;
				that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				// that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '5';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Forever';                
                $scope.createElision.conditions = [
					'Получаете 3% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickInfinite: function() {
                var that = this;
                that.createInfinite = true;
				that.createNeural = false;
				that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				// that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '6';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Infinite';                
                $scope.createElision.conditions = [
					'Получаете 3.3% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickDelorean: function() {
                var that = this;
                that.createDelorean = true;
				that.createNeural = false;
				that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
				// that.createDelorean = false;
                $scope.createElision.portfolio_type_id = '7';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Delorean';                
                $scope.createElision.conditions = [
					'Получаете 3.5% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickNext: function() {
                var that = this;
                that.createEclipse = false;
				that.createNeural = false;
				// that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = true;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '8';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Next';                
                $scope.createElision.conditions = [
					'Получаете 5% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickDrive: function() {
                var that = this;
                that.createEclipse = false;
				that.createNeural = false;
				// that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = true;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '9';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Drive';                
                $scope.createElision.conditions = [
					'Получаете 6% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickEver: function() {
                var that = this;
                that.createEclipse = false;
				that.createNeural = false;
				// that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = true;
				that.createLife = false;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '10';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Ever';                
                $scope.createElision.conditions = [
					'Получаете 7% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickLife: function() {
                var that = this;
                that.createEclipse = false;
				that.createNeural = false;
				// that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = true;
				that.createSerenity = false;
                $scope.createElision.portfolio_type_id = '11';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Life';                
                $scope.createElision.conditions = [
					'Получаете 7.5% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			clickSerenity: function() {
                var that = this;
                that.createEclipse = false;
				that.createNeural = false;
				// that.createEclipse = false;
				that.createEnergon = false;
				that.createSoller = false;
				that.createForever = false;
				that.createInfinite = false;
				that.createDelorean = false;
				that.createNext = false;
				that.createDrive = false;
				that.createEver = false;
				that.createLife = false;
				that.createSerenity = true;
                $scope.createElision.portfolio_type_id = '12';
                $scope.createElision.portfolio_tarif_text = $scope.lang.tarif + ' Serenity';                
                $scope.createElision.conditions = [
					'Получаете 8% прибыли в месяц'
	                // $scope.lang.genesis_open_conditions_txt_5,
					// $scope.lang.genesis_open_conditions_txt_10,
					// $scope.lang.genesis_open_conditions_txt_7,
					// $scope.lang.genesis_open_conditions_txt_11,
					// $scope.lang.genesis_open_conditions_txt_9
                ];
                
                that.create();
            },
			create: function() {
				$scope.createElision.init();
			},
			openStage: function(index) {
				var that = this;
                for(var i in this.el_stages) {
                    this.el_stages[i] = i == index;
                }
                if (index == 3) {
					var that = this;
					that.show_telegram = false;
			        that.show_password = false;
			        that.telegram_exists = false;
			        that.form.password = '';
			        that.form.pin = '';
	                that.loading = true;
            		utilsSrvc.api.call('/app/user/prepareconfirm/', {}, function(result) {
            			that.telegram_exists = result.telegram_exists;
                		that.show_password = !result.telegram_exists;
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
                }                
                if (index == 5 || index == 4) {
                    $timeout(function() {
                        $(document).ready(function(){
                            $('.tabs').tabs();
                        }); 
                    }, 1000);
                    if (index == 5) {
                        $(window).scrollTop(0);
                    }
                }
				if (index == 0) {
					$scope.startCreate.moreGenesis = false;
					$scope.startCreate.moreElision = false;
					$scope.startCreate.moreIntelex = false;
				}
			}, 
			chooseCorridor: function() {
				var that = this;
				that.loading = true;
				that.form.min_sum = that.min_deposit;
				that.form.max_sum = that.max_deposit;
				that.form.profit_percent = that.month_percent;
				that.form.title = that.title;
				that.form.validity = that.month_period;
				that.form.elision_invest_type_id = that.id;
				$scope.startCreateElision.loadelisionwalletdeposit();
	            // that.selectDebt({value: 0});
	            $('#payment_method_id').trigger('change');
				that.openStage(3);
            },
			loadelisionwalletdeposit: function() {
                var that = this;
                that.loading = true; 
                utilsSrvc.api.call('app/user/loadelisionwalletdeposit', {el_portfolio_id: that.id}, function(result) {
                    that.dep_list = result.data; 
                }, null, null, function(){
                    that.loading = false;
                });
            },
			changeAmount: function() {
				var that = this;
				if($scope.walletView.wallet.amount < that.form.amount) {
					that.flag = true;
				} else {
					that.flag = false;
				}
			},
			submit: function() {
				var that = this;
                that.loading = true;
                that.form.password = that.form.pin || that.form.password;
                utilsSrvc.api.call('/app/wallet/createwalletelision/', {
                    timezone_offset:            (new Date()).getTimezoneOffset() * -60,
                	elision_invest_type_id:     that.form.elision_invest_type_id,
                	password:                   that.form.password, 
                	amount:                     Math.round(that.form.amount * $scope.BTC_SATOCHI)
                }, function(result) {
                    gmaback.js.showMessage($scope.lang.ok_wallet_create, 4000);
                    $scope.buy.toAccounts();
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
			checkPin: function() {
				var that = this;
				// that.password = that.form.pin || that.form.password;
				if (that.form.pin >= 1000 && that.form.pin <= 9999) {
						return true;
				} else if (that.form.password.length > 0) {
					 return true;
				}
				return false;
			},
			formIsFilled: function() {
				var that = this; 
				// if (that.form.max_sum == 0) {
				// 	that.correct_sum = 0;
				// 	return true;
				// }
				if (that.form.amount < that.form.min_sum) {
					that.correct_sum = 1;
				} else
				if (that.form.amount > that.form.max_sum && that.form.max_sum != 0) {
					that.correct_sum = 2;
				} else if($scope.walletView.wallet.amount < that.form.amount * $scope.BTC_SATOCHI) {
					that.correct_sum = 3;
				} else {
					that.correct_sum = 0;
					return true;
				}
			}
		}

		$scope.createElision = {
			loading:false,
			init: function() {
				var that = this;
				$scope.startCreateElision.el_stages[1] = true;
			}
		}

        // Создание счета
        $scope.createGenesis = {
            loading: false,
            stages: [false, false, false, false, false],
            dax_wallet_trust_settings: {},
            invest_corridors: [],
            invest_corridors_trust: [],
            invest_corridors_intro: [],
            invest_corridors_intro_plus: [],
            invest_corridors_rate: 12.5,
            invest_corridor_step: 1000,
            countries: [],
            genders: [],
            relatives: [],
            calc_amount: 0,
            show_telegram: false,
            show_password: false,
            show_sendtelegrampin: true,
            getInvestCorridors: function(breakFnName) {
                /*
                that.invest_corridors.push({
                    amount: i * that.invest_corridor_step,
                    corridor: i * that.invest_corridor_step * that.invest_corridors_rate
                });
                */
                // Intro
                if($scope.startCreate.createIntro) {
                    return this.invest_corridors_intro;
                // Intro+
                } else if($scope.startCreate.createIntroPlus) {
                    return this.invest_corridors_intro_plus;
                // Trust
                } else if($scope.startCreate.createTrust) {
                    return this.invest_corridors_trust;
                // Others
                } else {
                    return this.invest_corridors;
                }
            },
            form: {
                amount: 0,
                corridor: 0,
                payment_method_id: '',
                dax_wallet_invest_type_id: '1',
                password: ''
            },
            settingsTrust: {
            	group1: 1,
	            is_allow_withdraw_profit: false,
	            withdraw_profit_type_id: null,
	            is_withdraw_profit_percent: null,
	            group5: null,
	            is_withdraw_profit_period: null,
	            group7: null,
	            group8: null,
	            is_birth_new_family_member: null,
	            new_family_member_gender_id: null,
	            new_family_member_relate_id: null,
	            is_new_family_member_fact_birth: null,
	            account_owner_id: null,
	            document_type_id: 1,
	            is_delay_withdraw_profit: null,
	            is_time_frame: true,
	            is_third_party_access: false,
	            check3: true,
	            check4: false,
	            check5: true,
	            check6: null,
	            check7: null,
	            error3litso18year: false,
	            error3litso21year: false,
	            errorage: false,
            	today: new Date(new Date().setHours(0,0,0,0)),
            	dateYear: null,
            	dateMonth: null,
                capital_withdrawal_date: null,
            	capital_withdrawal_date1: null,
            	capital_withdrawal_date2: null,
            	firstWithdrawalDateYear: null,
            	firstWithdrawalDate1: null,
            	firstWithdrawalDate2: null,
            	firstWithdrawalDate: null,
            	birthday: null,
            	withdraw_profit_percent: null,
            	withdraw_profit_limit: null,
            	withdraw_profit_period_months: null,
            	period_day: null,
            	last_name: null,
            	last_name_lat: null,
            	first_name: null,
            	first_name_lat: null,
            	middle_name: null,
            	middle_name_lat: null,
            	gender_id: null,
            	country_id: null,
            	relative_id: null,
            	doc_series: null,
            	doc_number: null,
            	new_family_member_age: null,
            	withdraw_profit_period_start: null,
            	doc_info: null,
            	new_family_member_info: null,
            	account_owner_info: null,
            	individual_conditions: null,
            },
            changeDate: function(breakFnName) {
				this.settingsTrust.capital_withdrawal_date = null;
				/*if (this.settingsTrust.capital_withdrawal_date1 &&
					this.settingsTrust.capital_withdrawal_date1 < this.settingsTrust.min_date) {
					this.settingsTrust.capital_withdrawal_date1 = this.settingsTrust.min_date;
				}*/
				if (this.settingsTrust.dateYear &&
					this.settingsTrust.dateYear < 2) {
						this.settingsTrust.dateYear = 2;
				}
				if (this.settingsTrust.dateMonth &&
					this.settingsTrust.dateMonth < 0) {
						this.settingsTrust.dateMonth = 0;
				}
            	if (this.settingsTrust.group1 == 1) {
					this.settingsTrust.capital_withdrawal_date2 = null;
					this.settingsTrust.dateYear = null;
					this.settingsTrust.dateMonth = null;
				} else if (this.settingsTrust.group1 == 2) {
					this.settingsTrust.capital_withdrawal_date1 = null;
				}
				else {
					this.settingsTrust.capital_withdrawal_date1 = null;
					this.settingsTrust.capital_withdrawal_date2 = null;					
					this.settingsTrust.dateYear = null;
					this.settingsTrust.dateMonth = null;
					return;
				}
            	
            	if (isNaN(parseInt(this.settingsTrust.dateYear)) || isNaN(parseInt(this.settingsTrust.dateMonth))) {
            		this.settingsTrust.capital_withdrawal_date2 = null;
            	} else {
                    this.settingsTrust.capital_withdrawal_date2 = new Date(new Date().setHours(0,0,0,0));
					this.settingsTrust.capital_withdrawal_date2 = new Date(this.settingsTrust.capital_withdrawal_date2.setFullYear(this.settingsTrust.capital_withdrawal_date2.getFullYear() + parseInt(this.settingsTrust.dateYear)));
					this.settingsTrust.capital_withdrawal_date2 = new Date(this.settingsTrust.capital_withdrawal_date2.setMonth(this.settingsTrust.capital_withdrawal_date2.getMonth() + parseInt(this.settingsTrust.dateMonth)));
                    this.settingsTrust.capital_withdrawal_date2 = new Date(this.settingsTrust.capital_withdrawal_date2.setDate(this.settingsTrust.capital_withdrawal_date2.getDate() + 1));
				}
				if (this.settingsTrust.group1 == 1) {
					this.settingsTrust.capital_withdrawal_date = this.settingsTrust.capital_withdrawal_date1;
				} else if (this.settingsTrust.group1 == 2) {
					this.settingsTrust.capital_withdrawal_date = this.settingsTrust.capital_withdrawal_date2;
				}
				
				if (!this.settingsTrust.capital_withdrawal_date) {
					return;
				}
				this.changeIsAllowWithdrawProfit();
				this.changeBirthday(breakFnName);
            },
            changeBirthday: function(breakFnName) {
            	if (this.settingsTrust.birthday &&
					this.settingsTrust.birthday > new Date()) {
					this.settingsTrust.birthday = new Date(new Date().setHours(0,0,0,0));
				}
            	if (!this.settingsTrust.capital_withdrawal_date && this.settingsTrust.birthday) {
            		var ageDifMs = Date.now() - this.settingsTrust.birthday.getTime();
			   		var ageDate = new Date(ageDifMs); // miliseconds from epoch
   					var years = Math.abs(ageDate.getUTCFullYear() - 1970);
   					   					
   					if (years < 18) {
   						var birth18 = this.settingsTrust.birthday.getTime();
						birth18 = new Date(birth18);
						birth18 = birth18.setFullYear(birth18.getFullYear() + 18);						
   						this.settingsTrust.capital_withdrawal_date = new Date(birth18);
   						if (this.settingsTrust.capital_withdrawal_date < this.settingsTrust.min_date) {
							this.settingsTrust.capital_withdrawal_date = this.settingsTrust.min_date;
						}
   						this.settingsTrust.capital_withdrawal_date1 = this.settingsTrust.capital_withdrawal_date;
   						this.settingsTrust.is_time_frame = true;
   						this.settingsTrust.group1 = 1;
   						this.settingsTrust.is_allow_withdraw_profit = false;
   						this.changeIsAllowWithdrawProfit();
					}
				}
            	if (this.settingsTrust.capital_withdrawal_date && this.settingsTrust.birthday) {
					var ageDifMs = Date.now() - this.settingsTrust.birthday.getTime();
			   		var ageDate = new Date(ageDifMs); // miliseconds from epoch
   					var years = Math.abs(ageDate.getUTCFullYear() - 1970);
   					   					
   					if (years < 18) {
						var birth18 = this.settingsTrust.birthday.getTime();
						birth18 = new Date(birth18);
						birth18 = birth18.setFullYear(birth18.getFullYear() + 18);
						if (this.settingsTrust.capital_withdrawal_date.getTime() < birth18) {
							this.settingsTrust.error3litso18year = true;
						} else {
							this.settingsTrust.error3litso18year = false;
   						}
   					} else {
						this.settingsTrust.error3litso18year = false;
   					}
   					if (years < 21) {
						var birth21 = this.settingsTrust.birthday.getTime();
						birth21 = new Date(birth21);
						birth21 = birth21.setFullYear(birth21.getFullYear() + 21);
						if (this.settingsTrust.capital_withdrawal_date.getTime() < birth21) {
							this.settingsTrust.error3litso21year = true;
						} else {
							this.settingsTrust.error3litso21year = false;
   						}
   					} else {
						this.settingsTrust.error3litso21year = false;
   					}
				} else {
					this.settingsTrust.error3litso18year = false;
					this.settingsTrust.error3litso21year = false;
   				}
   				if (!breakFnName || breakFnName != 'changeGroup8') {
   					this.changeGroup8('changeGroup8');
				}
            },
            changeAge: function() {
				if (this.settingsTrust.new_family_member_age) {					
					if (this.settingsTrust.new_family_member_age < 0) {
						this.settingsTrust.new_family_member_age = 0;
					}
					
					if (parseInt(this.settingsTrust.new_family_member_age) < 18) {
						this.settingsTrust.errorage = true;
					} else {
						this.settingsTrust.errorage = false;
					}
				} else {
					this.settingsTrust.errorage = false;
				}
            },
            changeIsTimeFrame: function (breakFnName) {
				var that = this;
				if (that.settingsTrust.is_time_frame) {
					that.settingsTrust.group1 = 1;
					that.settingsTrust.is_allow_withdraw_profit = false;
				} else {
					that.settingsTrust.group1 = null;
					that.settingsTrust.is_allow_withdraw_profit = null;
				}
            	that.settingsTrust.is_withdraw_profit_percent = null;
            	that.settingsTrust.is_delay_withdraw_profit = null;            		
            	that.settingsTrust.group5 = null;            		
            	that.settingsTrust.is_withdraw_profit_period = null;            		
            	that.settingsTrust.group7 = null;
				that.changeDate(breakFnName);
				that.changeIsAllowWithdrawProfit();
            },
            changeIsAllowWithdrawProfit: function() {
            	if (this.settingsTrust.is_allow_withdraw_profit == true) {
					this.settingsTrust.withdraw_profit_type_id = 1;
					this.settingsTrust.is_delay_withdraw_profit = false;
					this.settingsTrust.is_withdraw_profit_period = false;
				} else {
					this.settingsTrust.withdraw_profit_type_id = null;
					this.settingsTrust.is_delay_withdraw_profit = null;
					this.settingsTrust.is_withdraw_profit_period = null;
					this.settingsTrust.firstWithdrawalDate = this.settingsTrust.capital_withdrawal_date;
					this.settingsTrust.firstWithdrawalDate1 = this.settingsTrust.firstWithdrawalDate;
				}				
				this.changeWithdrawProfitTypeId();
				this.changeIsDelayWithdrawProfit();
				this.changeIsWithdrawProfitPeriod();
            },
            changeWithdrawProfitTypeId: function() {
            	if (this.settingsTrust.withdraw_profit_type_id == 1) {
					this.settingsTrust.is_withdraw_profit_percent = null;
					this.settingsTrust.withdraw_profit_percent = 100;
            	} else if (this.settingsTrust.withdraw_profit_type_id == 2) {
            		this.settingsTrust.is_withdraw_profit_percent = true;
				} else {
					this.settingsTrust.is_withdraw_profit_percent = null;
				}
				this.changeIsWithdrawProfitPercent();
			},
            changeIsWithdrawProfitPercent: function() {
				if (this.settingsTrust.is_withdraw_profit_percent == true) {
					this.settingsTrust.withdraw_profit_limit = null;
				} else if (this.settingsTrust.is_withdraw_profit_percent == false) {
					this.settingsTrust.withdraw_profit_percent = null;
				} else if (this.settingsTrust.withdraw_profit_type_id == 1) {
					this.settingsTrust.withdraw_profit_limit = null;
				} else {
					this.settingsTrust.withdraw_profit_limit = null;
					this.settingsTrust.withdraw_profit_percent = null;
				}
            },
            changeIsDelayWithdrawProfit: function() {
				if (this.settingsTrust.is_delay_withdraw_profit == true) {
					this.settingsTrust.group5 = 1;
				} else {
					this.settingsTrust.group5 = null;
				}
				this.changeFirstWithdrawalDate();
            },
            changeWithdrawProfitPercent: function() {
				if (this.settingsTrust.withdraw_profit_percent &&
					this.settingsTrust.withdraw_profit_percent < 0) {
					this.settingsTrust.withdraw_profit_percent = 0;
				}
            },
            changeWithdrawProfitLimit: function() {
				if (this.settingsTrust.withdraw_profit_limit &&
					this.settingsTrust.withdraw_profit_limit < 0) {
					this.settingsTrust.withdraw_profit_limit = 0;
				}
            },
            changeFirstWithdrawalDate: function() {
            	if (this.settingsTrust.firstWithdrawalDate1 &&
					this.settingsTrust.firstWithdrawalDate1 < new Date()) {
					this.settingsTrust.firstWithdrawalDate1 = new Date(new Date().setHours(0,0,0,0));
				}
				if (this.settingsTrust.firstWithdrawalDateYear &&
					this.settingsTrust.firstWithdrawalDateYear < 0) {
						this.settingsTrust.firstWithdrawalDateYear = 0;
				}
				if (this.settingsTrust.group5 == 1) {
					this.settingsTrust.firstWithdrawalDate2 = null;
					this.settingsTrust.firstWithdrawalDateYear = null;
				} else if (this.settingsTrust.group5 == 2) {
					this.settingsTrust.firstWithdrawalDate1 = null;
				} else if (this.settingsTrust.is_delay_withdraw_profit == false) {
					this.settingsTrust.firstWithdrawalDate = new Date(this.settingsTrust.today);
					this.settingsTrust.firstWithdrawalDate1 = this.settingsTrust.firstWithdrawalDate;
				} else if (this.settingsTrust.is_allow_withdraw_profit == false) {					
					this.settingsTrust.firstWithdrawalDate2 = null;
					this.settingsTrust.firstWithdrawalDateYear = null;
				} else {
					this.settingsTrust.firstWithdrawalDate = new Date(this.settingsTrust.today);
					this.settingsTrust.firstWithdrawalDate1 = this.settingsTrust.firstWithdrawalDate;
					this.settingsTrust.firstWithdrawalDate2 = null;
					this.settingsTrust.firstWithdrawalDateYear = null;
					return;
				}
            	
            	if (!this.settingsTrust.firstWithdrawalDateYear) {
            		this.settingsTrust.firstWithdrawalDate2 = null;
            	} else {
            		this.settingsTrust.firstWithdrawalDate2 = new Date(new Date().setHours(0,0,0,0));
					this.settingsTrust.firstWithdrawalDate2 = new Date(this.settingsTrust.firstWithdrawalDate2.setFullYear(this.settingsTrust.firstWithdrawalDate2.getFullYear() + parseInt(this.settingsTrust.firstWithdrawalDateYear)));
				}
				if (this.settingsTrust.group5 == 1) {
					this.settingsTrust.firstWithdrawalDate = this.settingsTrust.firstWithdrawalDate1;
				} else if (this.settingsTrust.group5 == 2) {
					this.settingsTrust.firstWithdrawalDate = this.settingsTrust.firstWithdrawalDate2;
				}
				this.changeWithdrawProfitPeriod();
            },
            changeIsWithdrawProfitPeriod: function() {
				if (this.settingsTrust.is_withdraw_profit_period == true) {
					this.settingsTrust.group7 = 1;
				} else {
					this.settingsTrust.group7 = null;
				}
				this.changeWithdrawProfitPeriod();
            },            
            changeWithdrawProfitPeriod: function() {
            	if (this.settingsTrust.withdraw_profit_period_months_text &&
            		this.settingsTrust.withdraw_profit_period_months_text < 0) {
					this.settingsTrust.withdraw_profit_period_months_text = 0;
            	}
            	if (this.settingsTrust.withdraw_profit_period_day &&
            		this.settingsTrust.withdraw_profit_period_day < 0) {
					this.settingsTrust.withdraw_profit_period_day = 0;
            	}
            	
				if (this.settingsTrust.group7 == 1) {					
					this.settingsTrust.withdraw_profit_period_months_text = null;;
					this.settingsTrust.withdraw_profit_period_day = null;
					var d = new Date(this.settingsTrust.firstWithdrawalDate.getTime());
				    d.setDate(1);
				    this.settingsTrust.withdraw_profit_period_start = d;
				    this.settingsTrust.withdraw_profit_period_months = 1;
				} else if (this.settingsTrust.group7 == 2) {
					if (!this.settingsTrust.withdraw_profit_period_day || !this.settingsTrust.withdraw_profit_period_months_text) {
						this.settingsTrust.withdraw_profit_period_start = null;
						return;
					}
					var d = new Date(this.settingsTrust.firstWithdrawalDate.getTime());
					d.setDate(this.settingsTrust.withdraw_profit_period_day);
					if (this.settingsTrust.withdraw_profit_period_day > new Date().getDate()) {
						d.setMonth(d.getMonth() - 1);
					}
					this.settingsTrust.withdraw_profit_period_start = d;
					this.settingsTrust.withdraw_profit_period_months = this.settingsTrust.withdraw_profit_period_months_text;
				} else if (this.settingsTrust.group7 == 3) {					
					this.settingsTrust.withdraw_profit_period_months_text = null;;
					this.settingsTrust.withdraw_profit_period_day = null;
					var d = new Date(this.settingsTrust.firstWithdrawalDate.getTime());
					this.settingsTrust.withdraw_profit_period_start = d;
					this.settingsTrust.withdraw_profit_period_months = 12;
				} else {
					this.settingsTrust.withdraw_profit_period_start = null;
					this.settingsTrust.withdraw_profit_period_months = null;
					this.settingsTrust.withdraw_profit_period_months = null;
					this.settingsTrust.withdraw_profit_period_months_text = null;;
					this.settingsTrust.withdraw_profit_period_day = null;
				}
            },
            changeIsThirdPartyAccess: function() {
				if (this.settingsTrust.is_third_party_access) {
					this.settingsTrust.document_type_id = 1;
					this.settingsTrust.check5 = true;
					this.settingsTrust.group8 = 1;
				}
				else {
					this.settingsTrust.document_type_id = null;
					this.settingsTrust.check5 = null;
					this.settingsTrust.group8 = null;
					this.settingsTrust.last_name = null;
            		this.settingsTrust.last_name_lat = null;
            		this.settingsTrust.first_name = null;
            		this.settingsTrust.first_name_lat = null;
            		this.settingsTrust.middle_name = null;
            		this.settingsTrust.middle_name_lat = null;
            		this.settingsTrust.gender_id = null;
            		this.settingsTrust.country_id = null;
            		this.settingsTrust.relative_id = null;
            		this.settingsTrust.doc_series = null;
            		this.settingsTrust.doc_number = null;
            		this.settingsTrust.doc_info = null;
            		this.settingsTrust.birthday = null;
				}
				this.changeCheck5();
				this.changeGroup8();
            },
            changeCheck5: function () {
				if (!this.settingsTrust.check5) {
					this.settingsTrust.relative_id = null;
				}
            },
            changeGroup8: function(breakFnName) {
            	if (this.settingsTrust.birthday) {
            		if (this.settingsTrust.check7) {
						this.settingsTrust.check6 = true;
            		}
            		if (this.settingsTrust.check6 || this.settingsTrust.check7) {
						if (this.settingsTrust.check6 == true) {
   							var birth21 = new Date(this.settingsTrust.birthday.getTime());
   							this.settingsTrust.capital_withdrawal_date = new Date(birth21.setFullYear(birth21.getFullYear() + 21));
   							if (this.settingsTrust.capital_withdrawal_date < this.settingsTrust.min_date) {
								this.settingsTrust.capital_withdrawal_date = this.settingsTrust.min_date;
							}
   							this.settingsTrust.capital_withdrawal_date1 = this.settingsTrust.capital_withdrawal_date;
						} 
						if (this.settingsTrust.check7 == true) {
							var now = new Date(new Date().setHours(0,0,0,0));
   							this.settingsTrust.capital_withdrawal_date = new Date(now.setFullYear(now.getFullYear() + 50));
   							if (this.settingsTrust.capital_withdrawal_date < this.settingsTrust.min_date) {
								this.settingsTrust.capital_withdrawal_date = this.settingsTrust.min_date;
							}
   							this.settingsTrust.capital_withdrawal_date1 = this.settingsTrust.capital_withdrawal_date;
   							var birth21 = new Date(this.settingsTrust.birthday.getTime());
   							this.settingsTrust.firstWithdrawalDate = new Date(birth21.setFullYear(birth21.getFullYear() + 21));
   							this.settingsTrust.firstWithdrawalDate1 = this.settingsTrust.firstWithdrawalDate;
   							this.settingsTrust.is_allow_withdraw_profit = true;
   							this.settingsTrust.withdraw_profit_type_id = 2;
   							this.settingsTrust.is_withdraw_profit_percent = true;
   							this.settingsTrust.withdraw_profit_percent = 50;
   							this.settingsTrust.is_delay_withdraw_profit = false;
   							this.settingsTrust.group5 = 1;
   							this.settingsTrust.is_withdraw_profit_period = false;
   							this.settingsTrust.group7 = 1;
   							this.changeWithdrawProfitPeriod();
						}
   						this.settingsTrust.is_time_frame = true;
   						this.settingsTrust.group1 = 1;
						this.changeBirthday(breakFnName);
					}
				}
            },
            changeCheck4: function () {
				if (!this.settingsTrust.check4) {
					this.settingsTrust.is_birth_new_family_member = null;
				} else {					
					this.settingsTrust.is_birth_new_family_member = true;
				}
				this.changeIsBirthNewFamilyMember();
            },
            changeIsBirthNewFamilyMember: function () {
				if (this.settingsTrust.is_birth_new_family_member == true) {
					this.settingsTrust.new_family_member_gender_id = 0;
					this.settingsTrust.new_family_member_relate_id = 1;
					this.settingsTrust.is_new_family_member_fact_birth = true;
					this.settingsTrust.account_owner_id = 1;
				} else {
					this.settingsTrust.new_family_member_gender_id = null;
					this.settingsTrust.new_family_member_relate_id = null;
					this.settingsTrust.is_new_family_member_fact_birth = null;
					this.settingsTrust.account_owner_id = null;
					this.settingsTrust.new_family_member_info = null;
				}
				if (this.settingsTrust.is_birth_new_family_member != false) {
					this.settingsTrust.individual_conditions = null;
				}
				
				this.changeIsNewFamilyMemberFactBirth();
				this.changeAccountOwnerId();
            },
            changeIsNewFamilyMemberFactBirth: function () {
				if (this.settingsTrust.is_new_family_member_fact_birth != false) {
					this.settingsTrust.new_family_member_age = null;
				}
				this.changeAge();
            },
            changeAccountOwnerId: function() {
				if (this.settingsTrust.account_owner_id != 4) {
					this.settingsTrust.account_owner_info = null;
				}
            },
            debt_table: [
                // { value: 10, title: '10%', active: false },
                { value: 25, title: '25%', active: false },
                { value: 50, title: '50%', active: false },
                { value: 75, title: '75%', active: false },
                { value: 100, title: '100%', active: false }
            ],
            init: function() {
            	var that = this;
				that.stages[0] = true;
				var today = new Date(that.settingsTrust.today);
				that.settingsTrust.min_date = new Date(today.setFullYear(today.getFullYear() + 2));
				that.settingsTrust.min_date = new Date(that.settingsTrust.min_date.setDate(that.settingsTrust.min_date.getDate() + 1));
				that.settingsTrust.capital_withdrawal_date = that.settingsTrust.min_date;
				that.settingsTrust.capital_withdrawal_date1 = that.settingsTrust.capital_withdrawal_date;
				that.settingsTrust.firstWithdrawalDate = that.settingsTrust.capital_withdrawal_date;
				that.settingsTrust.firstWithdrawalDate1 = that.settingsTrust.firstWithdrawalDate;
				that.calc_amount = that.invest_corridor_step;
				that.form.amount = that.calc_amount - that.calc_amount % that.invest_corridor_step;
				that.form.corridor = that.form.amount * that.invest_corridors_rate;
				that.can_create_wallet_debt = true;
                // Create corridor arrays
				for(var i = 1; i < 4; i++) {
                    // - for Others
                    that.invest_corridors.push({
                        amount: i * that.invest_corridor_step,
                        corridor: i * that.invest_corridor_step * that.invest_corridors_rate
                    });
				}
				for(var i = 1; i < 3; i++) {
                    // - for Trust
                    that.invest_corridors_trust.push({
                        amount: (i * that.invest_corridor_step) * 2,
                        corridor: (i * that.invest_corridor_step * that.invest_corridors_rate) * 2
                    });
				}
                // Коридоры для Intro счёта
                that.invest_corridors_intro.push({
                    amount: 100,
                    corridor: 1250
                });
                that.invest_corridors_intro_plus.push({
                    amount: 200,
                    corridor: 2500
                });
				utilsSrvc.api.call('/app/common/loadcountrylist/', {}, function(result) {
                    that.countries = result.data.list;
                }, function() {
                    that.loading = false;
                });
                utilsSrvc.api.call('/app/common/loadgenderlist/', {}, function(result) {
                    that.genders = result.data.list;
                }, function() {
                    that.loading = false;
                });
                utilsSrvc.api.call('/app/common/loadrelativelist/', {}, function(result) {
                	result.data.list.forEach(function(item) {
                		if (item.id <= 8) {
							that.relatives.push(item);
						}
                	});
                }, function() {
                    that.loading = false;
                });
                utilsSrvc.api.call('/app/common/loadoffer/', {dax_wallet_invest_type_id: that.form.dax_wallet_invest_type_id}, function(result) {
                    that.offer_wallet_en = result.data.offer_wallet_en;
                    that.offer_wallet_ru = result.data.offer_wallet_ru;
                    that.condition_wallet_en = result.data.condition_wallet_en;
                    that.condition_wallet_ru = result.data.condition_wallet_ru;
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
            calc: {
                data: false,
                show: function () {
                    return CustomUI.showForm('form-calcyield');
                },
                init: function() {
                    var that = this;
                    that.data = {
                        amount: 500,
                        refund_amount: '',
                        growth_usd: '',
                        growth_perc: '',
                        term: '',
                        invest_total_amount_usd_for_agio: 0,
                        need_pay_agio: 0,
                        value_amount_usd_max: 0,
                        agio_amount_usd: 0,
                        total_amount: 0
                    };
                    that.onChange();
                },
                onChange: function () {
                    var that = this;
                    that.loading = true;
                    //that.data.amount = that.data.amount || 0;    
                    var tmpAmount = parseFloat(that.data.amount);
                    if (isNaN(tmpAmount)) {
                        that.data.refund_amount = '';
                        that.data.growth_usd = '';
                        that.data.growth_perc = '';
                        that.data.term = '';                     
                        that.data.invest_total_amount_usd_for_agio = '';
                        that.data.need_pay_agio = '';
                        that.data.value_amount_usd_max = '';
                        that.data.agio_amount_usd = '';
                        that.data.total_amount = '';                        
                        return;
                    }
                    utilsSrvc.api.call('app/user/calcinvest', {amount_usd: that.data.amount}, function (resp) {
                        that.data.refund_amount = resp.data.refund_amount;
                        that.data.growth_usd = resp.data.growth_usd;
                        that.data.growth_perc = resp.data.growth_perc;
                        that.data.term = resp.data.term;                        
                        that.data.invest_total_amount_usd_for_agio = resp.data.invest_total_amount_usd_for_agio;
                        that.data.need_pay_agio = resp.data.need_pay_agio;
                        that.data.value_amount_usd_max = resp.data.value_amount_usd_max;
                        that.data.agio_amount_usd = resp.data.agio_amount_usd;
                        that.data.total_amount = resp.data.total_amount;
                        /*
                        that.data.grow_perc_in_month = resp.data.grow_perc_in_month;
                        that.data.usd__rate = resp.data.usd__rate;
                        that.data.btc__rate = resp.data.btc__rate;*/
                    }, null, null, function () {
                        that.loading = false;
                    });
                }
            },
            openStage: function(index) {
                var that = this;
                for(var i in this.stages) {
                    this.stages[i] = i == index;
                }
                if (index == 1 && $scope.createGenesis.form.dax_wallet_invest_type_id == '2') {
					$scope.startCreate.settingsTrust = true;
				}
                if (index == 2) {
					var that = this;
					that.show_telegram = false;
			        that.show_password = false;
			        that.telegram_exists = false;
			        that.form.password = '';
			        that.form.pin = '';
	                that.loading = true;
            		utilsSrvc.api.call('/app/user/prepareconfirm/', {}, function(result) {
            			that.telegram_exists = result.telegram_exists;
                		that.show_password = !result.telegram_exists;
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
                }                
                if (index == 3 || index == 4) {
                    $timeout(function() {
                        $(document).ready(function(){
                            $('.tabs').tabs();
                        }); 
                    }, 1000);
                    if (index == 4) {
                        $(window).scrollTop(0);
                    }
                }
				if (index == 0) {
					$scope.startCreate.moreGenesis = false;
					$scope.startCreate.moreElision = false;
					$scope.startCreate.moreIntelex = false;
				}
            },            
            resizeWindow: function() {
                // выставление id
                var i = 0;
                if ($('#offer_ru') && $('#offer_ru').children('p').length && $('#offer_ru').children('p')[0].id) {
                    // do nothing
                } else {                
                    $('#offer_ru').find('p').each(function() {
                        this.setAttribute("id", "offer_ru_" + i++);
                    });                
                }
                var i = 0;
                if ($('#offer_en') && $('#offer_en').children('p').length && $('#offer_en').children('p')[0].id) {
                    // do nothing
                } else {                
                    $('#offer_en').find('p').each(function() {
                        this.setAttribute("id", "offer_en_" + i++);
                    });                
                }
                var i = 0;
                if ($('#condition_ru') && $('#condition_ru').children('p').length && $('#condition_ru').children('p')[0].id) {
                    // do nothing
                } else {                
                    $('#condition_ru').find('p').each(function() {
                        this.setAttribute("id", "condition_ru_" + i++);
                    });                
                }
                var i = 0;
                if ($('#condition_en') && $('#condition_en').children('p').length && $('#condition_en').children('p')[0].id) {
                    // do nothing
                } else {                
                    $('#condition_en').find('p').each(function() {
                        this.setAttribute("id", "condition_en_" + i++);
                    });                
                }
                
                // автовысота
                var i = 0;
                while ($("#offer_ru_" + i).length !== 0 && $("#offer_en_" + i).length !== 0) {
                    $("#offer_ru_" + i).height('auto');
                    $("#offer_en_" + i).height('auto');
                    if ($("#offer_ru_" + i).height() > $("#offer_en_" + i).height()) {
                        $("#offer_en_" + i).height($("#offer_ru_" + i).height());
                    } else {
                        $("#offer_ru_" + i).height($("#offer_en_" + i).height());
                    }
                    i++;
                };
                var i = 0;
                while ($("#condition_ru_" + i).length !== 0 && $("#condition_en_" + i).length !== 0) {
                    $("#condition_ru_" + i).height('auto');
                    $("#condition_en_" + i).height('auto');
                    if ($("#condition_ru_" + i).height() > $("#condition_en_" + i).height()) {
                        $("#condition_en_" + i).height($("#condition_ru_" + i).height());
                    } else {
                        $("#condition_ru_" + i).height($("#condition_en_" + i).height());
                    }
                    i++;
                };
            },
            telegramprepareconfirm: function() { 
            	var that = this;
                that.loading = true;
            	utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {}, function(result) {
            		that.telegram_exists = false;
                	that.show_telegram = result.telegram_exists;
                	that.show_password = !result.telegram_exists;
					$scope.startCreateElision.telegram_exists = false;
					$scope.startCreateElision.show_telegram = result.telegram_exists;
                	$scope.startCreateElision.show_password = !result.telegram_exists;
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
            hideSettingsTrust: function() {
            	var that = this;
            	if (that.settingsTrust.group1) {
					if (!that.settingsTrust.capital_withdrawal_date) {
						gmaback.js.showError('Заполните поля времени заморозки капитала', 4000);
						return;
            		} else if (that.settingsTrust.capital_withdrawal_date < that.settingsTrust.min_date) {
            			gmaback.js.showError('Срок работы депозита на счете не может быть меньше 2х лет', 4000);
						return;
            		}
            		//that.settingsTrust.capital_withdrawal_date = that.settingsTrust.min_date;
            	}
            	if (that.settingsTrust.error3litso18year || that.settingsTrust.errorage) {
            		gmaback.js.showError('У Вас остались ошибки при заполнении настроек', 4000);
					return;
            	}
            	if (that.settingsTrust.is_allow_withdraw_profit) {
					if (!that.settingsTrust.withdraw_profit_percent &&
						!that.settingsTrust.withdraw_profit_limit) {
						gmaback.js.showError('Заполните поля частичного снятия прибыли', 4000);
						return;
            		}
            	}
            	if (that.settingsTrust.is_delay_withdraw_profit) {
					if (!that.settingsTrust.firstWithdrawalDate) {
						gmaback.js.showError('Заполните поля отсрочки для первого снятия прибыли', 4000);
						return;
					}
            	}
            	if (that.settingsTrust.is_withdraw_profit_period) {
					if (!that.settingsTrust.withdraw_profit_period_months ||
						!that.settingsTrust.withdraw_profit_period_start) {
						gmaback.js.showError('Заполните поля периода снятия прибыли', 4000);
						return;
					}
            	}
            	if (that.settingsTrust.is_third_party_access) {
					if (!that.settingsTrust.last_name ||
            			!that.settingsTrust.last_name_lat ||
            			!that.settingsTrust.first_name ||
            			!that.settingsTrust.first_name_lat ||
            			!that.settingsTrust.middle_name ||
            			!that.settingsTrust.middle_name_lat ||
            			!that.settingsTrust.gender_id ||
            			!that.settingsTrust.country_id ||
            			!that.settingsTrust.document_type_id ||
            			!that.settingsTrust.doc_series ||
            			!that.settingsTrust.doc_number ||
            			!that.settingsTrust.doc_info ||
            			!that.settingsTrust.birthday) {
						gmaback.js.showError('Заполните поля третьего лица', 4000);
						return;
            		}
            		
					if (that.settingsTrust.check5 && !that.settingsTrust.relative_id) {
						gmaback.js.showError('Заполните поля родственной связи', 4000);
						return;            			
					}
            	}
            	if (that.settingsTrust.is_birth_new_family_member) {
					if (!that.settingsTrust.new_family_member_gender_id ||
						!that.settingsTrust.new_family_member_relate_id ||
						!that.settingsTrust.account_owner_id) {
						gmaback.js.showError('Заполните поля особых условий при рождении нового члена семьи', 4000);
						return;            			
					}
						
					if (that.settingsTrust.is_new_family_member_fact_birth == false) {
						if (!that.settingsTrust.new_family_member_age) {
							gmaback.js.showError('Заполните поле достижения определенного возраста', 4000);
							return;            			
						}
					}
					
					if (that.settingsTrust.account_owner_id == 4) {
						if (!that.settingsTrust.account_owner_info) {
							gmaback.js.showError('Заполните требования к иному лицу', 4000);
							return;
						}
					}
            	} else if (that.settingsTrust.is_birth_new_family_member == false) {
            		if (!that.settingsTrust.individual_conditions) {
						gmaback.js.showError('Заполните индивидуальные условия трастового счета', 4000);
						return;
            		}
				}
            	if (that.settingsTrust.withdraw_profit_limit) {
            		that.settingsTrust.withdraw_profit_limit = that.settingsTrust.withdraw_profit_limit * $scope.BTC_SATOCHI;
				}
            	that.dax_wallet_trust_settings = {
					capital_withdrawal_date:        that.settingsTrust.capital_withdrawal_date,
					first_withdrawal_date:          that.settingsTrust.firstWithdrawalDate,
					withdraw_profit_percent:        that.settingsTrust.withdraw_profit_percent,
					withdraw_profit_limit:          that.settingsTrust.withdraw_profit_limit,
					withdraw_profit_period_months:  that.settingsTrust.withdraw_profit_period_months,
					withdraw_profit_period_start:   that.settingsTrust.withdraw_profit_period_start,
					is_third_party_access:          that.settingsTrust.is_third_party_access,
					last_name:                      that.settingsTrust.last_name,
					last_name_lat:                  that.settingsTrust.last_name_lat,
					first_name:                     that.settingsTrust.first_name,
					first_name_lat:                 that.settingsTrust.first_name_lat,
					middle_name:                    that.settingsTrust.middle_name,
					middle_name_lat:                that.settingsTrust.middle_name_lat,
					birthday:                       that.settingsTrust.birthday,
					gender_id:                      that.settingsTrust.gender_id,
					country_id:                     that.settingsTrust.country_id,
					relative_id:                    that.settingsTrust.relative_id,
					document_type_id:               that.settingsTrust.document_type_id,
					doc_series:                     that.settingsTrust.doc_series,
					doc_number:                     that.settingsTrust.doc_number,
					doc_info:                       that.settingsTrust.doc_info,
					is_birth_new_family_member:     that.settingsTrust.is_birth_new_family_member,
					new_family_member_gender_id:    that.settingsTrust.new_family_member_gender_id,
					new_family_member_relate_id:    that.settingsTrust.new_family_member_relate_id,
					new_family_member_age:          that.settingsTrust.new_family_member_age,
					new_family_member_info:         that.settingsTrust.new_family_member_info,
					account_owner_id:               that.settingsTrust.account_owner_id,
					account_owner_info:             that.settingsTrust.account_owner_info,
					individual_conditions:          that.settingsTrust.individual_conditions
            	};
            	that.form.dax_wallet_invest_type_id = '2';
				$scope.startCreate.settingsTrust = false;
            },
            chooseCorridor: function(item) {
				var that = this;
				that.loading = true;
				that.form.amount = item.amount;
				that.form.corridor = item.corridor;
				
				if (that.form.dax_wallet_invest_type_id == '3' ||
                    that.form.dax_wallet_invest_type_id == '4') {
					that.form.payment_method_id = '2';
				}
				else if (this.form.corridor != this.invest_corridors_rate * this.invest_corridor_step) {
					that.form.payment_method_id = '1';
				}
				else {
					that.form.payment_method_id = '';
				}
	            that.selectDebt({value: 0});
	            $('#payment_method_id').trigger('change');
				that.openStage(2);
            },
            /*changeCorridor: function() {
				var that = this;
				that.loading = true;
				that.form.amount = that.calc_amount - that.calc_amount % that.invest_corridor_step;
				that.form.corridor = that.form.amount * that.invest_corridors_rate;
            },*/
            selectDebt: function(d) {
                for(var v of this.debt_table) {
                    v.active = v.value == d.value;
                }
                this.form.pay_debt_percent = d.value;
            },
            formIsFilled: function() {
            	if (this.form.payment_method_id == '3') {
            		return true;
            	} else if (this.form.corridor && this.form.payment_method_id) {
                    if(this.form.payment_method_id == '2') {
                        return this.form.pay_debt_percent > 0 && $scope.walletView.wallet.amount >= this.form.amount * $scope.BTC_SATOCHI;
                    } else {
                        return $scope.walletView.wallet.amount >= this.form.amount * $scope.BTC_SATOCHI;
                    }
                
				} else if (this.form.corridor != this.invest_corridors_rate * this.invest_corridor_step) {
					return $scope.walletView.wallet.amount >= this.form.amount * $scope.BTC_SATOCHI;
            	} else {
                    return false;
                }
            },
            /*changeAmount: function() {
                var that = this;
                $timeout(function(){
                    if (that.form.amount != that.invest_corridor_step) {                    
	                    that.form.payment_method_id = '';
	                    that.selectDebt({value: 0});
	                    $('#payment_method_id').trigger('change');
					}
                });
            },*/
            paymentMethodChange: function() {
                var that = this;
                that.selectDebt({value: 0});
                that.can_create_wallet_debt = true;
                if (that.form.payment_method_id == '2') {
                	that.loading = true;
	                utilsSrvc.api.call('/app/wallet/cancreatewalletdebt/', {dax_wallet_invest_type_id: that.form.dax_wallet_invest_type_id}, function(result) {
                		that.can_create_wallet_debt = result.data;
	                }, null, null, function() {
	                    that.loading = false;
	                });
				}
            },
            submit: function() {
				var that = this;
                that.loading = true;
                that.form.password = that.form.pin || that.form.password;
                utilsSrvc.api.call('/app/wallet/createwallet/', {
                    timezone_offset:            (new Date()).getTimezoneOffset() * -60,
                	amount:                     that.form.amount,
                	invest_coridor:             that.form.corridor,
                	payment_method_id:          that.form.payment_method_id,
                	dax_wallet_invest_type_id:  that.form.dax_wallet_invest_type_id,
                	pay_debt_percent:           that.form.pay_debt_percent,
                	password:                   that.form.password, 
                	amount:                     Math.round(that.form.amount * $scope.BTC_SATOCHI),
                	dax_wallet_trust_settings:  that.dax_wallet_trust_settings
                }, function(result) {
                    gmaback.js.showMessage($scope.lang.ok_wallet_create, 4000);
                    $scope.buy.toAccounts();
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
            purchase: {
                loading: false,
                response: null,
                continue: function() {
                    var that = this;
                    that.loading = true;
                    that.response = null;
                    var form = $scope.createGenesis.form;
                    var dax_wallet_trust_settings = $scope.createGenesis.dax_wallet_trust_settings;
                    utilsSrvc.api.call('/app/wallet/purchase/', {
                        timezone_offset:            (new Date()).getTimezoneOffset() * -60,
                        amount:                     form.amount,
                        invest_coridor:             form.corridor,
                        payment_method_id:          form.payment_method_id,
                        dax_wallet_invest_type_id:  form.dax_wallet_invest_type_id,
                        pay_debt_percent:           form.pay_debt_percent,
                        password:                   form.password, 
                        amount:                     Math.round(form.amount * $scope.BTC_SATOCHI),
                        dax_wallet_trust_settings:  dax_wallet_trust_settings
                    }, function(result) {
                        that.response = result.data;
                        /*result.data.list.forEach(function(item) {
                            if (item.id <= 8) {
                                that.relatives.push(item);
                            }
                        });
                        */
                        CustomUI.showForm('form-createGenesis-etr', true, true, {onclose: function(){
                            $scope.buy.toAccounts();
                        }});
                    }, function(resp) {
                        that.loading = false;
                        CustomUI.hideForm();
                        gmaback.js.showError(resp.message, 4000);
                    }, function() {
                        // that.loading = false;
                    });
                    CustomUI.showForm('form-createGenesis-etr');
                }
            },
            showBuyCalc: function () {
                return CustomUI.showForm('form-buy-calc');
            }
        }

    };

    createwalletCtrl.$inject = injectParams;
    app.controller('createwalletCtrl', createwalletCtrl);

});