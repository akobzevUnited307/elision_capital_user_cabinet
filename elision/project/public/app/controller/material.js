'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams'];
    var materialCtrl = function ($scope, utilsSrvc, $state, $stateParams) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

        $scope.material = {
            list: null,
            item: null,
            loading: false,
            loadItem: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/content/loadinfoitem/', {url: $stateParams.id}, function(result) {
                    that.item = result.data;
                }, null, null, function(){
                    that.loading = false;
                });
            },
            loadList: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/content/loadinfos/', {project_uuid: getApp().project_uuid}, function(result) {
                    that.list = result.data;
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

    };

    materialCtrl.$inject = injectParams;
    app.controller('materialCtrl', materialCtrl);

});