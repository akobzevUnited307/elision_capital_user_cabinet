'use strict';
    
define(['app', 'service/utils'], function(app) {
    var injectParams = ['$scope', '$state', '$stateParams', 'utilsSrvc', '$timeout', '$location'];
    var forpartnerCtrl = function($scope, $state, $stateParams, utilsSrvc, $timeout, $location) {

        // Constants
        $scope.lang = getApp().lang;
        $scope.BTC_SATOCHI = 100000000;
        $scope.app = getApp();
        $scope.date = new Date();

        $scope.getRefURL = function() {
            return window.location.origin + '/r/' + getApp().user.login + '/';
        };

        // Клуб GoldenDAX
        $scope.career = {
            loading: false,
            current: {
                dt_to: '2019-05-12'
            },
            load: function () {
                var that = this;
                that.loading = true;
                that.buy.reset();
                utilsSrvc.api.call('/app/buy/loadgoldendaxinfo', {}, function (result) {
                    that.current = result.data.current;
                    that.buy.plan_list = result.data.plan_list;
                    $scope.sub_count = result.data.sub_count;
                }, null, null, function () {
                    that.loading = false;
                });
            },
            buy: {
                price_list: [],
                plan_list: [],
                current_price: false,
                show_confirm: false,
                loading: false,
                wallet: false,
                form: {
                    plan_id: false,
                    price_id: false
                },
                reset: function () {
                    // reset state
                    this.loading = false;
                    this.wallet = false;
                    this.show_confirm = false;
                    // reset form
                    this.form.plan_id = false;
                    this.form.price_id = false;
                },
                selectPlan: function (plan) {
                    this.price_list = plan.price_list;
                    this.form.plan_id = plan.id;
                },
                selectPrice: function (price) {
                    if (this.current_price) {
                        this.current_price.active = false;
                    }
                    price.active = true;
                    this.current_price = price;
                    this.form.price_id = price.id;
                    $('.career_month_list').find('input[type=radio]').each(function (index, element) {
                        $(element).prop("checked", false);
                    });                    
                    $('.career_month_list').find('input#' + price.id).each(function (index, element) {
                        $(element).prop("checked", true);
                    });  
                },
                showConfirm: function () {
                	var that = this;
                    that.show_confirm = true;
                    that.show_telegram = false;
			        that.show_password = false;
			        that.telegram_exists = false;
			        that.form.password = '';
			        that.form.pin = '';
	                that.loading = true;
            		utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
            			that.telegram_exists = result.telegram_exists;
                		that.show_password = !result.telegram_exists;
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
                },
	            telegramprepareconfirm: function() { 
            		var that = this;
	                that.loading = true;
            		utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {}, function(result) {
            			that.telegram_exists = false;
                		that.show_telegram = result.telegram_exists;
                		that.show_password = !result.telegram_exists;
	                }, null, null, function(err) {
	                    that.loading = false;
	                });
	            },
                getPrice: function () {
                    return this.current_price;
                },
                submit: function () {
                    var that = this;
                    that.loading = true;
                    that.form.password = that.form.pin || that.form.password;
                    utilsSrvc.api.call('/app/buy/buygoldendax', this.form, function (result) {
                        if (result.data.is_complete == 1) {
                            $scope.career.load();
                            that.reset();
                            CustomUI.hideForm();
                            getApp().changeProfile(result.data.user);
                        } else {
                            that.wallet = result.data.wallet;
                        }
                    }, null, null, function () {
                        that.loading = false;
                    });
                },
                show: function () {
                    this.reset();
                    return CustomUI.showForm('form-buy-goldendax');
                }
            }
        }

        if(getApp().is_logged) {
            $scope.career.load();
        }

    }
    forpartnerCtrl.$inject = injectParams;
    app.controller('forpartnerCtrl', forpartnerCtrl);
});