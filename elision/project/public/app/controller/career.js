'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var careerCtrl = function ($scope, utilsSrvc, $state) {
    	
    	$scope.lang = getApp().lang;
        $scope.BTC_SATOCHI = 100000000;
        $scope.app = getApp();
        $scope.date = newDate();                 
        $scope.pages.setURL($state.current.name);
        $scope.my = utilsSrvc.my;
    	
        // Career status
        $scope.careerStatus = {
            loading: false,
            data: false,
            days: [],
            load: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/forpartner/loadcareerstatus', {}, function (result) {
                    if(result.data) {
                        result.data.side_volume_leaders.isComplete = true;
                        result.data.total_fact_percent = 0;
                        if (result.data.side_volume_leaders.length) {
	                        that.command = {
								progress: 0,
	                            fact: 0,
	                            max: 0,
	                            progress: 0,
	                            is_limited: false
	                        };
						}
                        for(var lead of result.data.side_volume_leaders) {
                            if(!lead.percent.ok){
                                result.data.side_volume_leaders.isComplete = false;
                            }
                            result.data.total_fact_percent += lead.percent.fact;
                            that.command.fact += lead.percent.fact;
                            that.command.max += lead.percent.max;
                            that.command.is_limited = that.command.is_limited || lead.percent.is_limited;
                        }
                        if (result.data.next_career) {
                        	that.command.progress = that.command.fact / result.data.next_career.value_limit_command * 100;
						}
                        that.data = result.data;
                        // calc percent
                        //that.data.value_amount_command.percent = Math.round(that.data.value_amount_command.fact / that.data.value_amount_command.min * 100);
                        //if(that.data.value_amount_command.percent > 100) {
                        //    that.data.value_amount_command.percent = 100;
                        //}
						if (!that.data.side_volume_leaders.length) {
							return;
						}
						new Highcharts.Chart({
					        chart: {
					            renderTo: 'volume-chart',
					            type: 'pie',
					            backgroundColor: '#343740',
					            height: 305
					        },
					        title: {
					            text: ''
					        },
					        yAxis: {
					            title: {
					                text: ''
					            }
					        },
					        plotOptions: {
					            pie: {
					                shadow: false,
					                borderColor: '#42454e',
					                colors: ['#8f3a41', '#197a98', '#607e3e']
					            }
					        },
					        legend: {
		            			enabled: false
					        },
					        tooltip: {
					            formatter: function() {
					                return '<b>'+ this.point.name +'</b>: '+ this.y;
					            }
					        },
					        series: [{
					            name: '',
					            data: that.data.side_volume_leaders.map(function(item) {
									return [item.fio, item.percent.fact];
								}),
					            size: '165',
					            innerSize: '70%',
					            showInLegend:true,
					            dataLabels: {
					                enabled: false
					            }
					        }]
					    });						
                    }
                }, null, null, function () {
                    that.loading = false;
                });
            },
            loadTurnover: function () {					
				var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadforcareerdata/', {}, function(result) {
                    that.turnover = result.data;
                    
                    
                    new Highcharts.Chart({
				        chart: {
				            renderTo: 'turnover-chart',
				            type: 'pie',
				            backgroundColor: '#343740'
				        },
				        title: {
				            text: ''
				        },
				        yAxis: {
				            title: {
				                text: ''
				            }
				        },
				        plotOptions: {
				            pie: {
				                shadow: false,
				                borderColor: '#42454e',
				                colors: ['#304483', '#997e2a']
				            }
				        },
				        legend: {
		            		enabled: false
				        },
				        tooltip: {
				            formatter: function() {
				                return '<b>'+ this.point.name +'</b>: '+ this.y.toFixed(2) +' %';
				            }
				        },
				        series: [{
				            name: '',
				            data: [["Trustera", that.turnover.trustera_turnover / that.turnover.total_turnover * 100],["Проавто", 100 - that.turnover.trustera_turnover / that.turnover.total_turnover * 100]],
				            size: '100%',
				            innerSize: '70%',
				            showInLegend:true,
				            dataLabels: {
				                enabled: false
				            }
				        }]
				    });
                    
                }, null, null, function(){
	                that.loading = false;
	            });
			}
        }

        $scope.methods = {
            no_collapsed: true,
            collapse: function () {
                var that = this;
                that.no_collapsed = !that.no_collapsed;
                localStorage.setItem('no_collapsed', that.no_collapsed);
            }
        };
        
        // Career status
        $scope.turnover = {
            loading: false,
            list: [],
            load: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/forpartner/loadturnover', {}, function (result) {
                	that.list = result.data;
                }, null, null, function () {
                    that.loading = false;
                });
            }
        }
    };

    careerCtrl.$inject = injectParams;
    app.controller('careerCtrl', careerCtrl);

});