'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$window'];

    var controller = function ($scope, utilsSrvc, $window) {
        $scope.gotoUrl = function(url) {
            $window.location.href = url;
        }
    };

    controller.$inject = injectParams;

    app.controller('errorCtrl', controller);

});