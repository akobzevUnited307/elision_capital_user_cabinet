'use strict';

define(isDesktop() ? ['app', 'lang', 'service/utils', 'modalCtrl', 'footerCtrl', 'headerCtrl']
                   : ['app', 'lang']
, function(app, lang) {
    'use strict';

    var injectParams = ['$scope', '$state', 'utilsSrvc', '$timeout', '$location', '$sce', '$rootScope'];

    var indexCtrl = function ($scope, $state, utilsSrvc, $timeout, $location, $sce, $rootScope) {
        
        $scope.app = getApp(); 
        $scope.lang = lang.values;
        $scope.$state = $state;

        $scope.$on("$locationChangeStart", function (event, next, current) {  
            CustomUI.hideForm();
        });

        // getStateName ...
        $scope.getStateName = function(){
            return $state.current.name.split('.').join('_');
        };
        
        $scope.go = function(path) {
        	$location.path(path);
		};
		
		$scope.getPremiumDate = function() {
			if ($scope.app.user.goldendax_ticket_id) {
				if ($scope.app.user.goldendax_ticket_dt_end) {
					var dt = new Date($scope.app.user.goldendax_ticket_dt_end);
					var now = new Date();
					if (dt > now) {
						
						// year
						var diff = Math.abs(dt.getTime() - now.getTime()) / 1000;
					    diff /= (60 * 60 * 24);
					    if (diff >= 365) {
							return Math.floor(diff / 365) + 'г';
					    }						
						// month
						if (diff >= 30) {
							return Math.floor(diff / 30) + 'м';
					    }					    
					    // day
						if (diff > 1) {
							return Math.floor(diff) + 'д';
					    }
					    // hour
					    diff = Math.abs(dt.getTime() - now.getTime()) / 1000;
					    diff /= (60 * 60);
						if (diff >= 0) {
							return Math.floor(diff) + 'ч';
					    }
					}
				}
			}
			return false;
		}
		
		$scope.isLeap = function (year) {
		  return new Date(year, 1, 29).getDate() === 29;
		}

        $scope.lang = lang.values;
        gmaback.index_scope = $scope;
        $scope.conf = getApp();

        // Appearance
        $scope.appearance = getApp().settings.values;
        $scope.is_logged = getApp().isLogged();
        $scope.utilsSrvc = utilsSrvc;

        $scope.temp = {
            lang: {
                list: [
                    {id: 'ru', title: 'Русский'},
                    {id: 'en', title: 'English'},
                ],
                change: function(item) {
                    // changeLang
                    var code = item.id;
                    localStorage.setItem('lang', code);
                    utilsSrvc.api.call('/app/settings/setlang/', {
                        code: code
                    }, function(result) {
                        location.reload();
                    });
                },
                getCurrent: function() {
                    for(var item of this.list) {
                        if(item.id == $scope.conf.language.code) {
                            return item;
                        }
                    }
                }
            }
        };

        // Dictionaries
        $scope.dictionary = {
            dictionaries: [],
            init: function(dictionaries) {
                console.log('dictionaries', dictionaries);
                this.dictionaries = dictionaries;
            },
            getItem: function(dictionary_name, id) {
                for(var dic in this.dictionaries) {
                    if(dic == dictionary_name) {
                        for(var item of this.dictionaries[dic]) {
                            if(item.id == id) {
                                return item.title;
                                break;
                            }
                        }
                    }
                }
                return id + '';
            }
        };

        // Common settings ...
        $scope.commonsettings = {
            loading: false,
            form: {
                show_me_on_search: true,
                messages: true,
                likes: true,
                lastchance: true,
                matches: true,
            },
            set: function(data) {
                for(var k in data) {
                    this.form[k] = data[k];
                    getApp().common_settings[k] = data[k];
                }
            },
            save: function() {
                return false;
                var that = this;
                that.loading = true;
                var form = {};
                for(var k in this.form) {
                    form[k] = this.form[k] ? 1 : 0;
                }
                $timeout(function(){
                    utilsSrvc.api.call('/app/user/savecommonsettings/', that.form, function(result) {
                        getApp().common_settings = result.data;
                        console.log('app/user/savecommonsettings', result);
                    }, null, null, function(){
                        that.loading = false;
                    });
                });
                console.log(that.form);
                return false;
            }
        };

        // GetHeaderData
        /*utilsSrvc.api.call('/app/user/getheaderdata/', {}, function (result) {
            if (result.data) {
                $scope.dictionary.init(result.data.dictionary);
                $scope.commonsettings.set(result.data.commonsettings);
            }
        });*/

        // Notification ...
        $scope.Notification = {
            init: function() {
                Routines.Notification.init();
            }
        };

        // Date picker
        $scope.datePicker = function(selector) {
             gmaDatePicker.createDatepicker($(selector));
        };

        // Change language
        $scope.changeLang = function($element) {
            var code = $('#changelang').val();
            localStorage.setItem('lang', code);
            utilsSrvc.api.call('/app/settings/setlang/', {
                code: code
            }, function (result) {
                location.reload();
            });
        }

        // Pages
        $scope.pages = {
            url: isDesktop() ? ($scope.is_logged ? 'index.messenger' : 'index.login') : $state.current.name,
            isActive: function(state_name) {
                return $state.current.name.indexOf(state_name) == 0;
            },
            setLogged: function (new_value) {
                $scope.is_logged = new_value;
            },
            setURL: function (new_value) {
                this.url = new_value;
            },
            gotoURL: function(url, options, options2) {
                CustomUI.hideForm();
                options = options || {};
                options2 = options2 || {};
                this.url = url;
                if(!isDesktop()) {
                    $state.go(url, options, options2);
                }
                return false;
            }
        };

        $scope.goLogin = function () {
            if(!isDesktop()) {
                return $scope.pages.gotoURL('index.login');
            }
            return false;
        };

        $scope.logout = function () {
            return getApp().logout();
        };
        
        // Avatar
        $scope.avatar = {
            show: function () {
                $('#input-user-avatar').click();
                return false;
            },
            callback: function (ok, resp) {
                if (ok) {
                    var form = new FormData();
                    // form.append('session_id', utilsSrvc.user.getSessionId());
                    form.append('crop', JSON.stringify(resp.size));
                    //form.append('crop', '{"w":271,"h":182}');
                    form.append('file', resp.file);
                    form.append('system_type_id', 2);
                    utilsSrvc.api.call('/app/user/saveavatar/', form, function (result) {
                        CustomUI.hideShadow();
                        $scope.app.user.avatar = result.data.avatar;
                        getApp().user.avatar = result.data.avatar;
                        //$scope.profile.data.avatar_path = result.data.avatar;
                        // $('#my_profile_button img').attr('src', result.data.avatar);
                        $('.user-menu-header img').attr('src', result.data.avatar);
                        getApp().changeProfile(result.data);
                    });
                }
                return CustomUI.showForm('form-profile');
            }
        };
        
        $scope.newDate = function(str_dt) {
            if (!str_dt) {
                return new Date();
            }
            var dt = new Date(str_dt);
            if (!isNaN(dt.getTime())) {
                return dt;
            }
            //ios device
            return new Date(str_dt.replace(/-/g, '/'));
        }
        
        if ($scope.app.isLogged() && $state.current.name == 'index.splash') {
        	$scope.pages.gotoURL('index.user');
        }

    };

    indexCtrl.$inject = injectParams;
    app.controller('indexCtrl', indexCtrl);

});