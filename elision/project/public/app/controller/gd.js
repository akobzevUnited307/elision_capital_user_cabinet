'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var gdCtrl = function ($scope, utilsSrvc, $state) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        
        // Клуб GoldenDAX
        $scope.career = utilsSrvc.career.load();
        
        if(getApp().is_logged) {
            $scope.career.load();
        }
        
        $scope.datediff = function (dt) {
            dt = new Date(dt);
            var now = new Date();
            return Math.abs(Math.round((dt-now)/(1000*60*60*24)));
        }
        
        $scope.checkCurrent = function (plan_id) {
            return $scope.career.current.find(function(item) { return item.id == plan_id })
        }

    };

    gdCtrl.$inject = injectParams;
    app.controller('gdCtrl', gdCtrl);

});