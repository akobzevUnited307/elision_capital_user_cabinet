'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', '$stateParams', 'utilsSrvc', '$state'];
    var usereventCtrl = function ($scope, $stateParams, utilsSrvc, $state) {
        //$scope.pages.setURL($state.current.name);
        $scope.app = getApp();

        $scope.event_list = {
            loading: false,
            data: false,
            load: function() {
                var that = this;
                that.loading = true;

                utilsSrvc.api.call('/app/events/getuservisitedevent', {user_id: $stateParams.id}, function(result){
                    that.data = result.data;
                }, null, null, function(){
                    that.loading = false;
                });
            },
        };
    };

    usereventCtrl.$inject = injectParams;
    app.controller('usereventCtrl', usereventCtrl);

});