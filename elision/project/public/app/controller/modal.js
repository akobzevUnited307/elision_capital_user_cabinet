'use strict';

define(['app', 'lang', 'service/utils'], function (app, lang) {

    var injectMenuParams = ['$scope', 'utilsSrvc', '$state', '$timeout'];
    var modalCtrl = function ($scope, utilsSrvc, $state, $timeout) {

        $scope.lang = lang.values;
        $scope.init_settings = false;

        $scope.tabs = {
            instance: false,
            init: function () {
                var options = {
                    onShow: function (elem) {
                        var e = $(elem);
                        var current_tab_id = $(elem).attr('id');
                        if (current_tab_id == 'my_tab_events') {
                            e.trigger('click');
                        } else if (current_tab_id == 'my_tab_favourites') {
                            e.trigger('click');
                        }
                    }
                };
                this.instance = new M.Tabs(document.getElementById('my-tabs'), options);
            },
            select: function (tab_id) {
                if (!this.instance) {
                    this.init();
                }
                var that = this;
                $timeout(function () {
                    that.instance.select(tab_id);
                    that.instance.updateTabIndicator();
                });
            }
        };

        // user ...
        $scope.user = {
            profile_loading: true,
            loading: false,
            view: false,
            // добавление в контакты
            addToContacts: function() {
                var that = this;
                that.loading = true;
                messengerSrvc.call('addcontact', {user_login: that.view.profile.user_login}, function (result) {
                    var chat_id = result.data;
                    var msg_scope = gmaback.messenger_scope;
                    msg_scope.forms.toggle('chat');
                    msg_scope.channels.select(chat_id);
                    CustomUI.hideForm();
                }, null, function(){
                    that.loading = false;
                });
            },
            show: function (user_login) {
                var that = this;
                this.profile_loading = true;
                preloader.on();
                console.log('modalCtrl, user_login:', user_login);
                utilsSrvc.api.call('/app/user/getpublic/', {
                    user_login: user_login
                }, function (result) {
                    console.log('user/getpublic', result);
                    preloader.off();
                    that.profile_loading = false;
                    that.view = result.data;
                    CustomUI.showForm('form-userView');
                    $('#form-userView .popup-form-toolbox h2').text(that.view.profile.title);
                    $timeout(function () {
                        CustomUI.centerForms();
                    });
                }, function (result) {
                    preloader.off();
                    gmaback.js.showError(result.message, 4000);
                });
                $scope.user.friend.load(user_login);

            }
        }

        // События
        $scope.events = utilsSrvc.events.init();

    };

    modalCtrl.$inject = injectMenuParams;
    app.controller('modalCtrl', modalCtrl);

});