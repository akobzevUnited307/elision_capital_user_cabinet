'use strict';

define(['app', 'lang', 'service/utils'], function (app, lang) {

    var injectParams = ['$scope', 'utilsSrvc', '$window', '$timeout', '$interval'];

    var requestformCtrl = function ($scope, utilsSrvc, $window, $timeout, $interval) {

        $scope.lang = $scope.app.lang;

        // Заявка на открытие офиса
        $scope.request = {
            form: {
                title: '',
                fio: '',
                number: '',
                what_city: '',
                about_you: '',
                country_id: '',
                region_id: '',
                city_id: ''
            },
            sendToBot: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/office/sendtobot/', that.form, function (result) {
                    // that.result = result.data;
                    $scope.pages.gotoURL('index.thanksforrequest');
                }, null, null, function (error) {
                    that.loading = false;
                });
            }
        };

        // Data
        $scope.data = {
            countries: [],
            loading: false,
            load: function(){
                var that = this;
                that.loading = true;
                var filter = {};
                utilsSrvc.api.call('/app/common/getcountries/', filter, function(result) {
                    that.countries = result.data.list.countries;
                }, null, function() {
                }, function() {
                    that.loading = false;
                });
            }
        };

        // Region
        $scope.region = {
            list: [],
            loading: false,
            load: function(country_id) {
                var that = this;
                if(!country_id) {
                    that.list = [];
                    return;
                }
                $scope.city.reset();
                var filter = {country_id: country_id};
                that.loading = true;
                $scope.request.form.region_id = '';
                utilsSrvc.api.call('/app/common/dictionaryregion/', filter,
                    function(result) {
                        that.list = result.data.list;
                        if(that.list.length == 1) {
                            $scope.request.form.region_id = '' + that.list[0].id;
                            $timeout(function(){
                                $scope.regionChange();
                            });
                        }
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    }, function(result) {
                        that.loading = false;
                    }
                );
            }
        };

        // City
        $scope.city = {
            list: [],
            loading: false,
            reset: function() {
                this.list = [];
                $scope.request.form.city_id = '';
            },
            load: function(region_id) {
                var that = this;
                if(!region_id) {
                    that.list = [];
                    return;
                }
                var filter = {region_id: region_id};
                that.loading = true;
                $scope.request.form.city_id = '';
                utilsSrvc.api.call('/app/common/dictionarycity/', filter,
                    function(result) {
                        that.list = result.data.list;
                        if(that.list.length == 1) {
                            $scope.request.form.city_id = '' + that.list[0].id;
                        }
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    }, function(result) {
                        that.loading = false;
                    }
                );
            }
        };

        // Country changed
        $scope.countryChange = function() {
            $scope.region.load($scope.request.form.country_id);
        };

        // Region changed
        $scope.regionChange = function() {
            $scope.city.load($scope.request.form.region_id);
        };

        // Data load
        $scope.data.load();

        $timeout(function(){
            $('select').select2();
        });
    };

    requestformCtrl.$inject = injectParams;

    app.controller('requestformCtrl', requestformCtrl);
});