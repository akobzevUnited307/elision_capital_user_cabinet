'use strict';

define(['app', 'lang', 'service/utils'], function (app, lang) {

    var injectParams = ['$scope', 'utilsSrvc', '$window', '$timeout', '$interval'];

    var thanksforrequestCtrl = function ($scope, utilsSrvc, $window, $timeout, $interval) {
        $scope.info = {
            loading: true,
            user: {},
            load: function () {
                var that = this;
                that.loading = true;
                that.user = getApp().user;
            },
        }
    };

    thanksforrequestCtrl.$inject = injectParams;

    app.controller('thanksforrequestCtrl', thanksforrequestCtrl);
});