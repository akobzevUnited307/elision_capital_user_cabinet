'use strict';

define(['app', 'lang', 'service/utils'], function (app, lang) {

    var injectParams = ['$scope', 'utilsSrvc', '$window', '$timeout', '$interval'];

    var howopenCtrl = function ($scope, utilsSrvc, $window, $timeout, $interval) {

    };

    howopenCtrl.$inject = injectParams;

    app.controller('howopenCtrl', howopenCtrl);

});