'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$timeout'];
    var partnerCtrl = function ($scope, utilsSrvc, $state, $timeout) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        
        // Реферальная структура
        $scope.referealTree = {
            loading: false,
            partner_dax_wallet: null,
            column: 'item.user.fio',
            search_str: '',
            tree: [],
            list: [],    
            sortItems: [
            	{id: 1, title: 'Сортировка: По глубине линии'},
            	{id: 2, title: 'Сортировка: По наличию премиум аккаунта'},
            	{id: 3, title: 'Сортировка: По наличию верификации'},
            	{id: 4, title: 'Сортировка: По ФИО'},
            	{id: 5, title: 'Сортировка: По логину'},
            	{id: 6, title: 'Сортировка: По уровню статуса'},
            	{id: 7, title: 'Сортировка: По участию в кооперативе'},
            	{id: 8, title: 'Сортировка: По инвестиционному счету'},
            	{id: 9, title: 'Сортировка: По баллансу счетов'},
            	{id: 10, title: 'Сортировка: По контрактному баллансу счетов'},
            	{id: 11, title: 'Сортировка: По стране'},
            	{id: 12, title: 'Сортировка: По количеству дней с момента регистрации'}
            ],
            sortItemId: null,
            sortDirection: 1,
            expandAll: function() {
                function expand(i) {
                    if(i.sub_count > 0) {
                        i.expanded = true;
                        $timeout(function(){
                            for(var k in i.referal_list) {
                                expand(i.referal_list[k]);
                            }
                        });
                    }
                }
                for(var item of this.tree.referal_list) {
                    expand(item);
                }
                this.treeExpanded = true;
            },
            collapseAll: function() {
                function collapse(i) {
                    if(i.sub_count > 0) {
                        i.expanded = false;
                        $timeout(function(){
                            for(var k in i.referal_list) {
                                collapse(i.referal_list[k]);
                            }
                        });
                    }
                }
                for(var item of this.tree.referal_list) {
                    collapse(item);
                }
                this.treeExpanded = false;
            },
			search: function(item) {
                var filter = $scope.referealTree.search_str;
			    if (!filter || (item.user.user_login && item.user.user_login.toLowerCase().indexOf(filter.toLowerCase()) != -1 || 
			    	item.user.fio && item.user.fio.toLowerCase().indexOf(filter.toLowerCase()) != -1)) {
				    return true;
				}
			    return false;
			},
			changeSort: function () {
				var that = this;
				that.loading = true;                     
                that.sortTree(that.tree);
            	that.list.sort(function (a,b) {
            		if (that.sortItemId == 1) {
                        if (a.depth < b.depth) return 1 * that.sortDirection;
                        if (a.depth > b.depth) return -1 * that.sortDirection;
                        if (a.depth == b.depth) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 2) {
						if (a.user.is_premium && !b.user.is_premium) {
            				return -1 * that.sortDirection;
						}
						if (!a.user.is_premium && b.user.is_premium) {
            				return 1 * that.sortDirection;
						}
						if (!a.user.is_premium && !b.user.is_premium) {
            				return that.careerSort(a, b);
						}
            		}
            		if (that.sortItemId == 3) {
						if (a.user.is_verificated && !b.user.is_verificated) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.is_verificated && b.user.is_verificated) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.is_verificated && !b.user.is_verificated) {
                            return that.careerSort(a, b);
                        }
            		}
            		if (that.sortItemId == 4) {
						if (a.user.fio < b.user.fio) return 1 * that.sortDirection;
                        if (a.user.fio > b.user.fio) return -1 * that.sortDirection;
                        if (a.user.fio == b.user.fio) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 5) {
						if (a.user.user_login < b.user.user_login) return 1 * that.sortDirection;
                        if (a.user.user_login > b.user.user_login) return -1 * that.sortDirection;
                        if (a.user.user_login == b.user.user_login) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 6) {
            			if (a.user.user_career && !b.user.user_career) {
            				return -1 * that.sortDirection;
						}
						if (!a.user.user_career && b.user.user_career) {
            				return 1 * that.sortDirection;
						}
						if (!a.user.user_career && !b.user.user_career) {
            				return that.careerSort(a, b);
						}
						if (a.user.user_career.id < b.user.user_career.id) return 1 * that.sortDirection;
                        if (a.user.user_career.id > b.user.user_career.id) return -1 * that.sortDirection;
                        if (a.user.user_career.id == b.user.user_career.id) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 7) {
						if (a.user.proauto_car && !b.user.proauto_car) {
            				return -1 * that.sortDirection;
						}
						if (!a.user.proauto_car && b.user.proauto_car) {
            				return 1 * that.sortDirection;
						}
						if (!a.user.proauto_car && !b.user.proauto_car) {
            				return that.careerSort(a, b);
						}
						if (a.user.proauto_car.proauto_car_status_id < b.user.proauto_car.proauto_car_status_id) return 1 * that.sortDirection;
                        if (a.user.proauto_car.proauto_car_status_id > b.user.proauto_car.proauto_car_status_id) return -1 * that.sortDirection;
                        if (a.user.proauto_car.proauto_car_status_id == b.user.proauto_car.proauto_car_status_id) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 8) {
                        if (a.user.invest_wallet_exist && !b.user.invest_wallet_exist) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.invest_wallet_exist && b.user.invest_wallet_exist) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.invest_wallet_exist && !b.user.invest_wallet_exist) {
                            return that.careerSort(a, b);
                        }
						if (a.user.invest_wallet_balance < b.user.invest_wallet_balance) return 1 * that.sortDirection;
                        if (a.user.invest_wallet_balance > b.user.invest_wallet_balance) return -1 * that.sortDirection;
                        if (a.user.invest_wallet_balance == b.user.invest_wallet_balance) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 9) {
                        if (a.user.sum_amount && !b.user.sum_amount) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.sum_amount && b.user.sum_amount) {
                            return 1 * that.sortDirection;
                        }   
                        if (!a.user.sum_amount && !b.user.sum_amount) {
                            if (a.user.investor_status.id < b.user.investor_status.id) return 1 * that.sortDirection;
                            if (a.user.investor_status.id > b.user.investor_status.id) return -1 * that.sortDirection;
                            if (a.user.investor_status.id == b.user.investor_status.id) return that.careerSort(a, b);
                        }
						if (parseInt(a.user.sum_amount) < parseInt(b.user.sum_amount)) return 1 * that.sortDirection;
                        if (parseInt(a.user.sum_amount) > parseInt(b.user.sum_amount)) return -1 * that.sortDirection;
                        if (parseInt(a.user.sum_amount) == parseInt(b.user.sum_amount)) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 10) {
						if (parseInt(a.user.value_limit_command) < parseInt(b.user.value_limit_command)) return 1 * that.sortDirection;
                        if (parseInt(a.user.value_limit_command) > parseInt(b.user.value_limit_command)) return -1 * that.sortDirection;
                        if (parseInt(a.user.value_limit_command) == parseInt(b.user.value_limit_command)) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 11) {
						if (a.user.country_alpha2 < b.user.country_alpha2) return 1 * that.sortDirection;
                        if (a.user.country_alpha2 > b.user.country_alpha2) return -1 * that.sortDirection;
                        if (a.user.country_alpha2 == b.user.country_alpha2) return that.careerSort(a, b);
            		}
            		if (that.sortItemId == 12) {
                        if (a.user.days < b.user.days) return 1 * that.sortDirection;
                        if (a.user.days > b.user.days) return -1 * that.sortDirection;
                        if (a.user.days == b.user.days) return that.careerSort(a, b);
            		}
                    if (!that.sortItemId) {
                        return that.careerSort(a, b);
                    }
            	});
            	that.loading = false;
			},
            careerSort: function (a, b) {
                var that = this;
                if (a.user.user_career && !b.user.user_career) {
                    return -1;
                }
                if (!a.user.user_career && b.user.user_career) {
                    return 1;
                }
                if (!a.user.user_career && !b.user.user_career) {
                    return that.valueCommandSort(a, b);
                }
                return (a.user.user_career.id < b.user.user_career.id ? 1 : a.user.user_career.id > b.user.user_career.id ? -1 : that.valueCommandSort(a, b));
            },
            valueCommandSort: function (a, b) {
                var that = this;
                return (parseInt(a.user.value_command) < parseInt(b.user.value_command) ? 1 : parseInt(a.user.value_command) > parseInt(b.user.value_command) ? -1 : that.valueLimitCommandSort(a, b));
            },
            valueLimitCommandSort: function (a, b) {
                return (parseInt(a.user.value_limit_command) < parseInt(b.user.value_limit_command) ? 1 : parseInt(a.user.value_limit_command) > parseInt(b.user.value_limit_command) ? -1 : 0);
            },
			reverseSort: function () {
                var that = this;
				that.sortDirection *= -1;
				that.changeSort();
			},      
            sortTree: function(tree) {
                var that = this;
                tree.referal_list.forEach(function (item) {
                    that.sortTree(item);
                });
                tree.referal_list.sort(function (a,b) {
                    if (that.sortItemId == 1) {
                        if (a.depth < b.depth) return 1 * that.sortDirection;
                        if (a.depth > b.depth) return -1 * that.sortDirection;
                        if (a.depth == b.depth) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 2) {
                        if (a.user.is_premium && !b.user.is_premium) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.is_premium && b.user.is_premium) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.is_premium && !b.user.is_premium) {
                            return that.careerSort(a, b);
                        }
                    }
                    if (that.sortItemId == 3) {
                        if (a.user.is_verificated && !b.user.is_verificated) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.is_verificated && b.user.is_verificated) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.is_verificated && !b.user.is_verificated) {
                            return that.careerSort(a, b);
                        }
                    }
                    if (that.sortItemId == 4) {
                        if (a.user.fio < b.user.fio) return 1 * that.sortDirection;
                        if (a.user.fio > b.user.fio) return -1 * that.sortDirection;
                        if (a.user.fio == b.user.fio) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 5) {
                        if (a.user.user_login < b.user.user_login) return 1 * that.sortDirection;
                        if (a.user.user_login > b.user.user_login) return -1 * that.sortDirection;
                        if (a.user.user_login == b.user.user_login) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 6) {
                        if (a.user.user_career && !b.user.user_career) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.user_career && b.user.user_career) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.user_career && !b.user.user_career) {
                            return that.careerSort(a, b);
                        }
                        if (a.user.user_career.id < b.user.user_career.id) return 1 * that.sortDirection;
                        if (a.user.user_career.id > b.user.user_career.id) return -1 * that.sortDirection;
                        if (a.user.user_career.id == b.user.user_career.id) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 7) {
                        if (a.user.proauto_car && !b.user.proauto_car) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.proauto_car && b.user.proauto_car) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.proauto_car && !b.user.proauto_car) {
                            return that.careerSort(a, b);
                        }
                        if (a.user.proauto_car.proauto_car_status_id < 5 && b.user.proauto_car.proauto_car_status_id < 5) {
                            return that.careerSort(a, b);
                        }
                        if (a.user.proauto_car.proauto_car_status_id < b.user.proauto_car.proauto_car_status_id) return 1 * that.sortDirection;
                        if (a.user.proauto_car.proauto_car_status_id > b.user.proauto_car.proauto_car_status_id) return -1 * that.sortDirection;
                        if (a.user.proauto_car.proauto_car_status_id == b.user.proauto_car.proauto_car_status_id) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 8) {
                        if (a.user.invest_wallet_exist && !b.user.invest_wallet_exist) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.invest_wallet_exist && b.user.invest_wallet_exist) {
                            return 1 * that.sortDirection;
                        }
                        if (!a.user.invest_wallet_exist && !b.user.invest_wallet_exist) {
                            return that.careerSort(a, b);
                        }
                        if (a.user.invest_wallet_balance < b.user.invest_wallet_balance) return 1 * that.sortDirection;
                        if (a.user.invest_wallet_balance > b.user.invest_wallet_balance) return -1 * that.sortDirection;
                        if (a.user.invest_wallet_balance == b.user.invest_wallet_balance) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 9) {
                        if (a.user.sum_amount && !b.user.sum_amount) {
                            return -1 * that.sortDirection;
                        }
                        if (!a.user.sum_amount && b.user.sum_amount) {
                            return 1 * that.sortDirection;
                        }   
                        if (!a.user.sum_amount && !b.user.sum_amount) {
                            if (a.user.investor_status.id < b.user.investor_status.id) return 1 * that.sortDirection;
                            if (a.user.investor_status.id > b.user.investor_status.id) return -1 * that.sortDirection;
                            if (a.user.investor_status.id == b.user.investor_status.id) return that.careerSort(a, b);
                        }
                        if (parseInt(a.user.sum_amount) < parseInt(b.user.sum_amount)) return 1 * that.sortDirection;
                        if (parseInt(a.user.sum_amount) > parseInt(b.user.sum_amount)) return -1 * that.sortDirection;
                        if (parseInt(a.user.sum_amount) == parseInt(b.user.sum_amount)) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 10) {
                        if (parseInt(a.user.value_limit_command) < parseInt(b.user.value_limit_command)) return 1 * that.sortDirection;
                        if (parseInt(a.user.value_limit_command) > parseInt(b.user.value_limit_command)) return -1 * that.sortDirection;
                        if (parseInt(a.user.value_limit_command) == parseInt(b.user.value_limit_command)) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 11) {
                        if (a.user.country_alpha2 < b.user.country_alpha2) return 1 * that.sortDirection;
                        if (a.user.country_alpha2 > b.user.country_alpha2) return -1 * that.sortDirection;
                        if (a.user.country_alpha2 == b.user.country_alpha2) return that.careerSort(a, b);
                    }
                    if (that.sortItemId == 12) {
                        if (a.user.days < b.user.days) return 1 * that.sortDirection;
                        if (a.user.days > b.user.days) return -1 * that.sortDirection;
                        if (a.user.days == b.user.days) return that.careerSort(a, b);
                    }
                    if (!that.sortItemId) {
                        return that.careerSort(a, b);
                    }
                });
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/getRate', {}, function(result){
                    $scope.app.rate = result.data;                    
                    utilsSrvc.api.call('/app/user/loadforpartnerdata', {}, function(result) {
                        that.tree = {
                            referal_list: result.data.referals.tree
                        };
                        that.list = [];
                        var itemToList = function(item) {
                            var new_item = JSON.parse(JSON.stringify(item));
                            new_item.referal_list = null;
                            that.list.push(new_item);
                            if(new_item.sub_count) {
                                for(var ref_user of item.referal_list) {
                                    itemToList(ref_user);
                                }
                            }
                        }
                        for(var ref_user of that.tree.referal_list) {
                            itemToList(ref_user);
                        }
                        that.changeSort();
                        // Make regions
                        /*that.regions = {};
                        for(var u of that.list) {
                            var region_id = u.user.region_id;
                            if(region_id in that.regions) {
                               // do nothing
                            } else {
                                that.regions[region_id] = {
                                    id: region_id,
                                    title: u.user.region_title,
                                    list: []
                                };
                            }
                            that.regions[region_id].list.push(u);
                        }
                        that.regions = Object.values(that.regions);
                        that.regions.sort(function (a,b) {
            				return a.list.length > b.list.length ? -1 : 1;
            			});
                        // alert(JSON.stringify(that.regions).length);
                        that.sortRegion();*/
                        $timeout(function () {
							var el = document.querySelector("#list-container");
							var x = 0, y = 0, top = 0, left = 0;

							var draggingFunction = (e) => {
							    document.addEventListener('mouseup', () => {
							        document.removeEventListener("mousemove", draggingFunction);
							    });

							    el.scrollLeft = left - e.pageX + x;
							    el.scrollTop = top - e.pageY + y;
							};

							el.addEventListener('mousedown', (e) => {
							    e.preventDefault();

							    y = e.pageY;
							    x = e.pageX;
							    top = el.scrollTop;
							    left = el.scrollLeft;

							    document.addEventListener('mousemove', draggingFunction);
							});
                        });
                    }, null, null, function(){
                        that.loading = false;
                    });
                }, null, null, null);
            }
        };

        function dist(x1, y1, x2, y2) {
            var a = x1 - x2;
            var b = y1 - y2;

            return Math.sqrt( a*a + b*b );
        }

        // Message
        $scope.message = {
            goToChat: function(user_login) {
                var that = this;
                that.loading = true;
                var CONTACT_TYPE_USER = 1
                var form = {
                    contact_type_id: CONTACT_TYPE_USER,
                    user_login: user_login
                }
                utilsSrvc.api.call('/app/messenger/addcontact', form, function(result){      
                    var app = $scope.app;
                    if (result.data) {
                        if ($scope.app.is_developer_host) {
                            window.open('dax.loc/messenger/' + result.data);
                        }
                        else {
                            window.open('https://trustera.global/messenger/' + result.data);
                        }
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

        // Клуб GoldenDAX
        $scope.career = utilsSrvc.career.load();

        // Career status
        $scope.careerStatus = {
            loading: false,
            data: false,
            days: [],
            load: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/forpartner/loadcareerstatus', {}, function (result) {
                    if(result.data) {
                        result.data.side_volume_leaders.isComplete = true;
                        for(var lead of result.data.side_volume_leaders) {
                            if(!lead.percent.ok){
                                result.data.side_volume_leaders.isComplete = false;
                            }
                        }
                        // calc days
                        that.days = [];
                        for(var i = 0; i < result.data.period.max; i++) {
                            var item = {i: i, fill: i < result.data.period.value, class: i < result.data.period.value ? 'ok' : 'clock'};
                            if(i == result.data.period.value - 1) {
                                if(!result.data.period.ok) {
                                    item.class = 'exclamation';
                                }
                            }
                            that.days.push(item);
                        }
                        that.data = result.data;
                        // calc percent
                        that.data.value_amount_command.percent = Math.round(that.data.value_amount_command.fact / that.data.value_amount_command.min * 100);
                        if(that.data.value_amount_command.percent > 100) {
                            that.data.value_amount_command.percent = 100;
                        }
                    }
                }, null, null, function () {
                    that.loading = false;
                });
            }
        }
        
    };

    partnerCtrl.$inject = injectParams;
    app.controller('partnerCtrl', partnerCtrl);

});