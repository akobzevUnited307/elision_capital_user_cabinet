'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams'];
    var newsCtrl = function ($scope, utilsSrvc, $state, $stateParams) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

        $scope.news = {
            list: null,
            item: null,
            loading: false,
            loadItem: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/content/loadnewsitem/', {url: $stateParams.id}, function(result) {
                    that.item = result.data;
                }, null, null, function(){
                    that.loading = false;
                });
            },
            loadList: function(callback) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/content/loadnews/', {project_uuid: getApp().project_uuid}, function(result) {
                    that.list = result.data;
                    if (typeof callback === 'function') {
                        callback(that.list)
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

    };

    newsCtrl.$inject = injectParams;
    app.controller('newsCtrl', newsCtrl);

});