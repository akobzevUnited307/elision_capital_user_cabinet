'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams', '$timeout'];
    var promotionsCtrl = function ($scope, utilsSrvc, $state, $stateParams, $timeout) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        
        /*if($scope.app.user.user_status_id != 4) {
            return $state.go('index.user', {});
        }*/

        /**
        * Новые промоушены
        */
        $scope.competition = {
            list: null,
            loading: false,
            loadList: function(callback) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/promotion/getInfoIphone/', {}, function(result) {
                    that.list = result.data;
                    if (typeof callback === 'function') {
                        callback(that.list);
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

        /**
        * Quick Start
        */
        $scope.quickstart = {
            user_groups: [],
            item: null,
            loading: false,
            loading_start: false,
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/competition/loaditem/', {code: 'quickstart'}, function(result) {
                        that.item = result.data;
                        if(that.item.info) {
                            that.setUserGroups(that.item.info.users);
                        }
                    }, null, null, function(){
                        that.loading = false;
                });
            },
            setUserGroups: function(users) {
                this.user_groups = [];
                this.user_groups.push({users: [], bonus_usdt: 100, ok: false});
                this.user_groups.push({users: [], bonus_usdt: 200, ok: false});
                this.user_groups.push({users: [], bonus_usdt: 300, ok: false});
                if(this.item) {
                    var i = -1;
                    for(var k in users) {
                        if(k % 2 == 0) {
                            i++;
                            var bonus_usdt = 100;
                            switch(i) {
                                case 0: { bonus_usdt = 100; break; }
                                case 1: { bonus_usdt = 200; break; }
                                default: { bonus_usdt = 300; break; }
                            }
                            if(i + 1 > this.user_groups.length) {
                                this.user_groups.push({users: [], bonus_usdt: bonus_usdt, ok: false});
                            }
                        }
                        this.user_groups[i].users.push(users[k]);
                        if(this.user_groups[i].users.length == 2) {
                            this.user_groups[i].ok = true;
                        }
                    }
                }
            },
            start: function() {
                var that = this;
                CustomUI.showConfirm('Вы действительно хотите активировать промоушен?', 'Внимание', function(){
                    that.loading_start = true;
                    utilsSrvc.api.call('/app/competition/quickstartstart/', {}, function(result) {
                        that.item = result.data;
                        if(that.item.info) {
                            that.setUserGroups(that.item.info.users);
                        }
                    }, null, null, function(){
                        that.loading_start = false;
                    });
                });
            }
        };

        /**
        * Be Leader
        */
        $scope.beleader = {
            loading: false,
            item: null,
            loading_start: false,
            load: function(code) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/competition/loaditem/', {code: 'beleader'}, function(result) {
                        that.item = result.data;
                    }, null, null, function(){
                        that.loading = false;
                });
            },
            start: function() {
                var that = this;
                CustomUI.showConfirm('Вы действительно хотите активировать промоушен?', 'Внимание', function(){
                    that.loading_start = true;
                    utilsSrvc.api.call('/app/competition/beleaderstart/', {}, function(result) {
                        that.item = result.data;
                        CustomUI.showMessage('Промоушен успешно активирован', 'Поздравляем!');
                    }, null, null, function(){
                        that.loading_start = false;
                    });
                });
            }
        };

        /**
        * Промоушены
        */
        $scope.promotion = {
            list: null,
            item: null,
            loading: false,
            loading_finish: false,
            countries: [],
            form: {
                fio: null,
                country_id: null,
                region: null,
                city: null,
                address: null,
                phone: null,
                promo_iphone_id:null,
            },
            tabs: {
                index: 0,
                list: [
                    {title: 'Прогресс и билеты'},
                    {title: 'Информация'}
                ],
                select: function(index) {
                    this.index = index;
                    for(var k in this.list) {
                        this.list[k].active = k == index;
                    }
                },
                init: function() {
                    this.select(0);
                }
            },
            loadItem: function() {
                var that = this;
                console.log('prom.id', $stateParams.id);
                this.loadList(function(list){
                    for(var item of list) {
                        if(item.id == $stateParams.id) {
                            that.item = item;
                            break;
                        }
                    }
                });
            },
            loadList: function(callback) { 
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/promotion/getlist/', {}, function(result) {
                    that.list = result.data;
                    if (typeof callback === 'function') {
                        callback(that.list)
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },
            showExtendForm: function() {
                CustomUI.hideForm();
                CustomUI.showForm('form-promotion-extend');
            },
            closeExtend: function() {
                CustomUI.hideForm();
            },
            countryChange: function(id) {
                var that = this;
                that.form.country_id = id; 
            },
            extendPromotion: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/promotion/extendPromotionIPhone/', {}, function(result) {
                    that.data = result.data;
                    that.closeExtend();
                    $scope.competition.loadList();
                }, function() {
                    that.loading = false;
                });
            },
            showGetGift: function(id) {
                var that = this;
                that.form.promo_iphone_id = id;
                CustomUI.showForm('form-promotion-congratulation');
            },
            FillIn: function() {
                CustomUI.hideForm();
                CustomUI.showForm('form-promotion-anketa');
            },
            sendAnketa: function() {
                var that = this;
                CustomUI.hideForm();
                utilsSrvc.api.call('/app/promotion/setUserPromoInfo/', {form: this.form}, function(result) {
                    that.data2 = result.data;
                    $scope.competition.loadList();
                    gmaback.js.showMessage('Данные отправлены!', 4000);
                }, function() {
                    that.loading = false;
                });
                CustomUI.showForm('form-promotion-last');
            },
            loadCountries: function(){
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/common/getcountrylist/', {}, function(result) {
                    that.countries = result.data;
                }, function() {
                    that.loading = false;
                });
            },
            activatePromotion: function(promotion_id) {
                var that = this;
                CustomUI.showConfirm('Вы действительно хотите активировать промоушен?', 'Активация', function(){
                    that.loading = true;
                    utilsSrvc.api.call('/app/promotion/activate/', {promotion_id: promotion_id}, function(result) {
                        that.list = result.data;
                        for(var item of that.list) {
                            if(item.id == promotion_id) {
                                that.item = item;
                                break;
                            }
                        }
                    }, null, null, function(){
                        that.loading = false;
                    });
                });
            },
            finish: function(promotion_id) {
                var that = this;
                that.loading_finish = true;
                utilsSrvc.api.call('/app/promotion/finish/', {promotion_id: promotion_id}, function(result) {
                    that.list = result.data.list;
                    for(var item of that.list) {
                        if(item.id == promotion_id) {
                            that.item = item;
                            break;
                        }
                    }
                    CustomUI.hideForm();
                    var message = 'Вы успешно завершили промоушен.<br>Ваш зачислен бонус в размере ' +
                                   result.data.bonus_amount_dax_string + ' DAX ($' +
                                   result.data.bonus_amount_usd_string + ')';
                    if(result.data.continue) {
                        message += '<br>Открыт новый промоушен.';
                    }
                    CustomUI.showMessage(message, 'Поздравляем!');
                }, null, null, function(){
                    that.loading_finish = false;
                });
            }
        };

        /**
        * Congress Golden DAX MRIYA 2020
        */
        $scope.promotionMriya2020 = {
            data: null,
            loading: false,
            need_price: 0,
            withdrawalBonus: {
                loading: false,
                submit: function() {
                    var that = this;
                    CustomUI.showMessage('Вывод бонусов невозможен, т.к. промоушен завершен', 'Внимание', 4000);
                    /*
                    CustomUI.showConfirm('Вы подтверждаете отправку ' + (Math.round($scope.promotionMriya2020.data.tickets.wallet_amount.bonus_withdrawal_limit / 100000000 * 100) / 100) + ' трэвел бонус на расчётный счёт', 'Подтверждение', function() {
                        that.loading = true;
                        utilsSrvc.api.call('/app/wallet/withdrawalbonus/', {amount: $scope.promotionMriya2020.data.tickets.wallet_amount.bonus_withdrawal_limit}, function(result) {
                            gmaback.js.showMessage('Бонусные средства успешно отправлены на расчётный счёт', 4000);
                            $scope.promotionMriya2020.load();
                        }, null, null, function(){
                            that.loading = false;
                        });
                    });
                    */
                }
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/promotion/loadmriya2020/', {}, function(result) {
                    that.data = result.data;
                    for(var ticket of that.data.tickets.price_list) {
                        if(ticket.is_active) {
                            that.need_price = ticket.amount * 100000000;
                        }
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },
            canBuy: function() {
                var that = this;
                if(!this.data) {
                    return false;
                }
                return this.need_price <= that.data.tickets.wallet_amount.main + that.data.tickets.wallet_amount.bonus;
            },
            buyTicketDebt: {
                submit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/promotion/buyticketmriya2020fordebt/', {}, function(result) {
                        gmaback.js.showMessage('Билет успешно приобретён в рассрочку', 4000);
                        CustomUI.hideForm();
                        $scope.promotionMriya2020.load();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            },
            buyTicket: {
                form: {},
                page: 1,
                setPage: function(index) {
                    this.page = index;
                },
                open: function() {
                    this.setPage(1);
                    return CustomUI.showForm('form-promotion-mriya2020-buy_ticket');
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/promotion/buyticketmriya2020/', {}, function(result) {
                        CustomUI.hideForm();
                        $scope.promotionMriya2020.load();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            },
            tickets: {
                item: null,
                loading: false,
                is_accept: false,
                photo1: null,
                photo2: null,
                openView: function(ticket) {
                    this.item = ticket;
                    return CustomUI.showForm('form-mriya2020-ticket-view');
                },
                openEdit: function(ticket) {
                    this.item = ticket;
                    this.photo1 = null;
                    this.photo2 = null;
                    return CustomUI.showForm('form-mriya2020-ticket-edit');
                },
                canSave: function() {
                    if(!this.is_accept) {
                        return false;
                    }
                    if(!this.item.last_name || !this.item.first_name || !this.item.middle_name || !this.item.sex || !this.item.birthday || !this.item.passport_number) {
                        return false;
                    }
                    return true;
                },
                selectPhoto: function(id) {
                    $('#input-photo-passport-' + id).trigger('click');
                },
                isFirstTicket: function() {
                    var that = this;
                    var length = $scope.promotionMriya2020.data.tickets.list.length;
                    var ticket = $scope.promotionMriya2020.data.tickets.list[length - 1];
                    return ticket.id == that.item.id;
                },
                processCFAttachImage: function(id, files) {
                    var that = this;
                    console.log('files', files[0]);
                    $timeout(function(){
                        var file = files[0];
                        if(!file.type.startsWith('image/')) {
                            return gmaback.js.showError('Необходимо выбрать изображение', 3000);
                        }
                        if(id == 1) {
                            that.photo1 = file;
                        }
                        if(id == 2) {
                            that.photo2 = file;
                        }
                        $('#input-photo-passport-' + id).val('');
                    });
                },
                save: function() {
                    var that = this;
                    that.loading = true;
                    var form = new FormData();
                    form.append('passport_photo_1', this.photo1);
                    form.append('passport_photo_2', this.photo2);
                    for(var k in that.item) {
                        if(k == 'birthday') {
                            var v = $('#input-birthday').val();
                            if(!v) {
                                return gmaback.js.showError('Пожалуйста укажите дату рождения', 4000);
                            }
                            form.append(k, v);
                        } else {
                            form.append(k, that.item[k]);
                        }
                    }
                    utilsSrvc.api.call('/app/promotion/savemriya2020ticketdata/', form, function(result) {
                        gmaback.js.showMessage('Данные успешно сохранены', 4000);
                        $scope.promotionMriya2020.load();
                        CustomUI.hideForm();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            }
        };

        /**
        * Bon Voyage Golden DAX 2020
        */
        $scope.promotionBonVoyage2020 = {
            data: null,
            loading: false,
            need_price: 0,
            withdrawalBonus: {
                loading: false,
                submit: function() {
                    var that = this;
                    CustomUI.showMessage('Вывод бонусов невозможен, т.к. промоушен завершен', 'Внимание', 4000);
                    /*
                    CustomUI.showConfirm('Вы подтверждаете отправку ' + (Math.round($scope.promotionBonVoyage2020.data.tickets.wallet_amount.bonus_withdrawal_limit / 100000000 * 100) / 100) + ' трэвел бонус на расчётный счёт', 'Подтверждение', function() {
                        that.loading = true;
                        utilsSrvc.api.call('/app/wallet/withdrawalbonus/', {amount: $scope.promotionBonVoyage2020.data.tickets.wallet_amount.bonus_withdrawal_limit}, function(result) {
                            gmaback.js.showMessage('Бонусные средства успешно отправлены на расчётный счёт', 4000);
                            $scope.promotionBonVoyage2020.load();
                        }, null, null, function(){
                            that.loading = false;
                        });
                    });
                    */
                }
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/promotion/loadBonVoyage2020/', {}, function(result) {
                    that.data = result.data;
                    for(var ticket of that.data.tickets.price_list) {
                        if(ticket.is_active) {
                            that.need_price = ticket.amount * 100000000;
                        }
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },
            canBuy: function() {
                var that = this;
                if(!this.data) {
                    return false;
                }
                return this.need_price <= that.data.tickets.wallet_amount.main + that.data.tickets.wallet_amount.bonus;
            },
            buyTicketDebt: {
                submit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/promotion/buyticketBonVoyage2020fordebt/', {}, function(result) {
                        gmaback.js.showMessage('Билет успешно приобретён в рассрочку', 4000);
                        CustomUI.hideForm();
                        $scope.promotionBonVoyage2020.load();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            },
            buyTicket: {
                form: {},
                page: 1,
                setPage: function(index) {
                    this.page = index;
                },
                open: function() {
                    this.setPage(1);
                    return CustomUI.showForm('form-promotion-Bonvoyage2020-buy_ticket');
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/promotion/buyticketBonVoyage2020/', {}, function(result) {
                        CustomUI.hideForm();
                        $scope.promotionBonVoyage2020.load();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            },
            tickets: {
                item: null,
                loading: false,
                is_accept: false,
                photo1: null,
                photo2: null,
                openView: function(ticket) {
                    this.item = ticket;
                    return CustomUI.showForm('form-BonVoyage2020-ticket-view');
                },
                openEdit: function(ticket) {
                    this.item = ticket;
                    this.photo1 = null;
                    this.photo2 = null;
                    return CustomUI.showForm('form-BonVoyage2020-ticket-edit');
                },
                canSave: function() {
                    if(!this.is_accept) {
                        return false;
                    }
                    if(!this.item.last_name || !this.item.first_name || !this.item.middle_name || !this.item.sex || !this.item.birthday || !this.item.passport_number) {
                        return false;
                    }
                    return true;
                },
                selectPhoto: function(id) {
                    $('#input-photo-passport-' + id).trigger('click');
                },
                isFirstTicket: function() {
					var that = this;
					var length = $scope.promotionBonVoyage2020.data.tickets.list.length;
					var ticket = $scope.promotionBonVoyage2020.data.tickets.list[length - 1];
					return ticket.id == that.item.id;
                },
                processCFAttachImage: function(id, files) {
                    var that = this;
                    console.log('files', files[0]);
                    $timeout(function(){
                        var file = files[0];
                        if(!file.type.startsWith('image/')) {
                            return gmaback.js.showError('Необходимо выбрать изображение', 3000);
                        }
                        if(id == 1) {
                            that.photo1 = file;
                        }
                        if(id == 2) {
                            that.photo2 = file;
                        }
                        $('#input-photo-passport-' + id).val('');
                    });
                },
                save: function() {
                    var that = this;
                    that.loading = true;
                    var form = new FormData();
                    form.append('passport_photo_1', this.photo1);
                    form.append('passport_photo_2', this.photo2);
                    for(var k in that.item) {
                        if(k == 'birthday') {
                            var v = $('#input-birthday').val();
                            if(!v) {
                                return gmaback.js.showError('Пожалуйста укажите дату рождения', 4000);
                            }
                            form.append(k, v);
                        } else {
                            form.append(k, that.item[k]);
                        }
                    }
                    utilsSrvc.api.call('/app/promotion/saveBonVoyage2020ticketdata/', form, function(result) {
                        gmaback.js.showMessage('Данные успешно сохранены', 4000);
                        $scope.promotionBonVoyage2020.load();
                        CustomUI.hideForm();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            }
        };

    };

    promotionsCtrl.$inject = injectParams;
    app.controller('promotionsCtrl', promotionsCtrl);

});