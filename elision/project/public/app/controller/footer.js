'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'playerSrvc', 'utilsSrvc'];
    var controller = function ($scope, playerSrvc, utilsSrvc) {
        $scope.$watch(function () { return playerSrvc.play; }, function(newVal, oldVal) {
            $scope.player = newVal;
        });
    };

    controller.$inject = injectParams;
    app.controller('footerCtrl', controller);

});