'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$timeout'];
    var dashboardCtrl = function ($scope, utilsSrvc, $state, $timeout) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

        // getRefURL
        $scope.getRefURL = function() {
            // return window.location.origin + '/r/' + getApp().user.user_login + '/';
            return getApp().domains.web + '/r/' + getApp().user.user_login + '/';
        };

        // hello ...
        $scope.hello = {
            loading: false,
            loading_activate_demo: false,
            candidat: {
                loading: false,
                loading_becandidat: false,
                result: null,
                form: {
                    accept: false,
                },
                check: function(send_msg_if_not) {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/checktelegrampartnerrequirements/', {send_msg_if_not: send_msg_if_not}, function(result) {
                        that.result = result.data;
                        if(!that.result.ok) {
                            $timeout(function() {
                                that.check(false);
                            }, 1000);
                        }
                    }, null, null, function(){
                        that.loading = false;
                    });
                },
                beCandidat: function() {
                    var that = this;
                    that.loading_becandidat = true;
                    // that.loading_activate_demo = true;
                    utilsSrvc.api.call('/app/user/becandidat/', {}, function(result) {
                        $scope.hello.load();
                        getApp().changeProfile(result.data.user);
                    }, null, null, function(){
                        that.loading_becandidat = false;
                    });
                }
            },
            requirements: {
                visible: false,
                ok: false,
                user_status_id: null,
                i_can_testing: null,
                list: {
                    is_min_amount: true,
                    is_visit_event: false,
                    is_get_training: false,
                    is_get_testing: false,
                },
                show: function() {
                    this.visible = true;
                }
            },
            showVisitEventDetails: function() {
                return CustomUI.showForm('form-visit_event-details');
            },
            showTrainingDetails: function() {
                return CustomUI.showForm('form-training-details');
            },
            showTestingDetails: function() {
                return CustomUI.showForm('form-testing-details');
            },
            showBePartner: function() {
                return CustomUI.showForm('form-bepartner');
            },
            /*bePartner: function() {
                var that = this;
                that.loading_activate_demo = true;
                utilsSrvc.api.call('/app/buy/bepartner/', {}, function(result) {
                    that.load();
                    getApp().changeProfile(result.data.user);
                    CustomUI.hideForm();
                }, null, null, function(){
                    that.loading_activate_demo = false;
                });
            },*/
            openTesting: function() {
                var that = this;
                return CustomUI.showForm('form-testing', true, true, {
                    onclose: function(){
                        that.load();
                    }
                });
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadgoldendatxinfo/', {}, function(result) {
                    that.requirements.list = result.data.requirements.list;
                    that.requirements.i_can_testing = result.data.requirements.i_can_testing;
                    that.requirements.min_amount_usd = result.data.requirements.min_amount_usd;
                    that.requirements.amount_usd = result.data.requirements.amount_usd;
                    that.requirements.ok = result.data.requirements.ok;
                    that.requirements.user_status_id = result.data.requirements.user_status_id;
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };
        
        $scope.remanagement = {
            loading: false,
            confirm_flag: false,
            data: {
                
            },            
            load: function(){
                var that = this;
                that.loading = true;
                that.loading = false;
            },
            confirm: function() {
                that.confirm_flag = true;
            }
        };

    };

    dashboardCtrl.$inject = injectParams;
    app.controller('dashboardCtrl', dashboardCtrl);

});