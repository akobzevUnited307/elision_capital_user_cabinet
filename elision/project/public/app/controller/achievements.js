'use strict';

define(['app', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams'];
    var achievementsCtrl = function ($scope, utilsSrvc, $state, $stateParams) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

        /**
        * Achievements
        */
        $scope.achievements = {
            loading: false,
            data: null,
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/achievements/load/', {}, function(result) {
                    that.data = result.data;
                    that.data.academy.list.forEach(function(item) {
                        if (item.lessons) {
                            item.lessons = Object.values(item.lessons);
                        }
                    });
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

    };

    achievementsCtrl.$inject = injectParams;
    app.controller('achievementsCtrl', achievementsCtrl);

});