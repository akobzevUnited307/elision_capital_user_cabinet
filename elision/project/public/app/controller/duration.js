'use strict';

define([
    'app', 'lang', 'service/utils'
], function (app, lang) {
    'use strict';

    var injectParams = ['$scope', 'utilsSrvc', '$interval', '$timeout'];
    var durationCtrl = function ($scope, utilsSrvc, $interval, $timeout) {
        $scope.pin_send_result = '';
        $scope.interval = null;
        $scope.$confirm_method_box = null;
        $scope.sendPinCode = function(e, user_pin_type_id) {
            var $btn = $(e.target);
            var $confirm_box = $btn.closest('.action-confirm-box');
            if ($btn.hasClass('disabled')) {
                return false;
            }
            $scope.$confirm_method_box = $confirm_box.find('[data-confirm-method-item=' + user_pin_type_id + ']');
            $scope.pin_send_result = '<i class="fa fa-spinner fa-pulse fa-1x"></i>&nbsp;&nbsp;Отправка.';
            $scope.$confirm_method_box.find('.send-pin-button').addClass('disabled');
            $confirm_box.find('[data-confirm-method]').addClass('disabled').removeClass('underline').removeProp('href').addClass('black-text');
            // Отсылаем параметры
            utilsSrvc.api.call('/app/pin/sendpin/', {user_pin_type_id: user_pin_type_id, form: 1, session: getApp().session_id}, function(result){
                // выводим ответ скрипта
                $scope.pin_send_result = result.data.message;
                $scope.$confirm_method_box.find('.pin_code').show().focus();
                $scope.$confirm_method_box.find('.send-pin-button').hide();
                $scope.$confirm_method_box.find('.confirm-pin-button').removeClass('disabled');
                if($scope.interval) {
                    $interval.cancel($scope.interval);
                }
                $scope.$on('$destroy', function() {
                    if($scope.interval) {
                        $interval.cancel($scope.interval);
                    }
                });
                $scope.interval = $interval(function(){
                    console.log('durationCtrl.interval');
                    $scope.$confirm_method_box.find('[data-duration]').each(function (index) {
                        var timer = $(this).data('duration');
                        try {
                            var days = parseInt(timer / 86400, 10);
                            var hours = parseInt((timer % 86400) / 3600, 10);
                            var minutes = parseInt((timer % 3600) / 60, 10);
                            var seconds = timer % 60;
                            hours = hours < 10 ? "0" + hours : hours;
                            minutes = minutes < 10 ? "0" + minutes : minutes;
                            seconds = seconds < 10 ? "0" + seconds : seconds;
                        } catch (e) {
                            alert(e);
                        }
                        $(this).data('duration', --timer);
                        if (timer < 0) {
                            $scope.$confirm_method_box.find('.send-pin-button').removeClass('disabled').show();
                            $scope.$confirm_method_box.find('.pin_code').hide();
                            $scope.pin_send_result = '';
                        } else {
                            $(this).empty().append((days > 0 ? days + ' ' + morph(days, 'день', 'дня', 'дней') + '<br>' : '') + hours + ":" + minutes + ":" + seconds);
                        }
                    });
                }, 1000);
                return CustomUI.centerForms();
            }, function(result) {
                $scope.$confirm_method_box.find('.pin-send-result').empty().html('<span class="red2-text"><i class="fa fa-exclamation-triangle"></i> ' + result.message + '</span>').stop().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
                $scope.$confirm_method_box.find('.send-pin-button').removeClass('disabled').show();
                return CustomUI.centerForms();
            });

            return false;
        };
    };
    durationCtrl.$inject = injectParams;
    app.controller('durationCtrl', durationCtrl);

});