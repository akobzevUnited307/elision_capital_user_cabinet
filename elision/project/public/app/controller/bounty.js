'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var bountyCtrl = function ($scope, utilsSrvc, $state) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();

    };

    bountyCtrl.$inject = injectParams;
    app.controller('bountyCtrl', bountyCtrl);

});