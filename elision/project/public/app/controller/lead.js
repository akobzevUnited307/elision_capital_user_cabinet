'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$timeout', '$stateParams'];
    var leadCtrl = function ($scope, utilsSrvc, $state, $timeout, $stateParams) {

        $scope.pages.setURL($state.current.name);
        $scope.lang = getApp().lang;
        $scope.BTC_SATOCHI = 100000000;
        $scope.app = getApp();

        $scope.lead = {
			data: {},
            loading: false,
            form: {
				fio: '',
				phone: '',
				lead_source_id: 1,
				comment: ''
            },
        	fio: '',
        	delete_id: '',
            init: function () {
            	var that = this;
				Highcharts.setOptions({
        			lang:{
            			rangeSelectorZoom: ''
					},
					global: {
						useUTC: false
					}
				});
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/lead/getleadsources/', {}, function(result) {
                	that.data.lead_sources = result.data;
                	
	                utilsSrvc.api.call('/app/lead/getlist/', {}, function(result) {
                		that.data.grow = result.data;
	                                                            
                		// построение pie графиков
						that.lead_pie_chart = Highcharts.chart('lead-pie-chart', {
							chart: {
								type: 'pie',
								backgroundColor: '#3f424b'
							},
							colors: ["#bda076", "#8c7758", "#6d5d44", "#504432", "#352e22", "#000000"],
							title: {
								text: ''
							},
							plotOptions: {
								pie: {
								    allowPointSelect: true,
								    cursor: 'pointer',
								    dataLabels: {
								        enabled: false,
								    },
								    showInLegend: true
								}
							},
							legend: {
								align: 'left',
								layout: 'vertical',
								verticalAlign: 'middle',
								labelFormatter: function () {
								    return this.name + ' (' + this.y + ')';
								},
								itemStyle: {
      								color: '#bda076'
								},
								itemHoverStyle: {
      								color: 'white'
								}
							},
							series: [
								{
									name: ' ',
									colorByPoint: true
								}
							],
							tooltip: {
								pointFormat: '{point.percentage:.1f}%'
							}
						});
	                    
	                    that.lead_grow_chart = Highcharts.stockChart('lead-grow-chart', {
							chart: {
								type: 'column',
								backgroundColor: '#3f424b',
							    height: 300
							},
							colors: ['#bda076'],
							title: {
							    text: ''
							},
							subtitle: {
					            align: 'left'
					        },
							xAxis: {
								type: 'datetime',
								max: new Date().getTime(),
								min: new Date().setFullYear(new Date().getFullYear() - 1),
						        tickInterval: 24 * 3600 * 1000 * 7
							},
							yAxis: {
								title: {
									text: ' '
								},
								min: 0
							},
							tooltip: {
								xDateFormat: '%d.%m.%Y',
								pointFormat: '{point.y}',
								shared: true
							},
							series: [{
							    name: ' ',
							    type: 'column',
							    threshold: null
							}],
							responsive: {
							    rules: [{
							        condition: {
							            maxWidth: 500
							        },
							        chartOptions: {
							            chart: {
							                height: 300
							            },
							            subtitle: {
							                text: null
							            },
							            navigator: {
							                enabled: false
							            }
							        }
							    }]
							},
							rangeSelector: {
      							buttonTheme: {
              						width: 45,
              						style: {
						                color: '#bda076',
						                fontWeight: 'white'
						            },
						            states: {
						                hover: {
						                },
						                select: {
						                    fill: '#bda076',
						                    style: {
						                        color: 'white'
						                    }
						                }
						            }
						        },
								buttons: [{
									type: 'all',
									text: 'неделя',
									events: {
										click: function() {
											$scope.safeApply(that.setSeriesData('week'));
										}
									}
								}, {
									type: 'all',
									text: 'месяц',
									events: {
										click: function() {
											$scope.safeApply(that.setSeriesData('month'));
										}
									}
								}, {
									type: 'all',
		                            text: 'год',
									events: {
										click: function() {
											$scope.safeApply(that.setSeriesData('year'));
										}
									}
		                        }],
								allButtonsEnabled: true,
              					inputEnabled: false
							},
							navigator: {
								enabled: false
							}
						});
						that.setSeriesData('week');
						that.setPieSeriesData('all');
	                }, null, null, function(){
		                that.loading = false;
		            });
	            }, null, null, null);
			},
			setSeriesData: function(interval) {
            	var that = this;
            	var data = that.data.grow.filter(function(item) {
					return !item.is_arch;
                });
            	
				var intervalData = [];
				data.map(function(value, index, array) {
					var d = new Date(value['dt']);
					if (interval == 'year') {
					    d = (d.getFullYear());
					} else if (interval == 'month') {
						d = (d.getFullYear()-1970)*12 + d.getMonth();
					} else if (interval == 'week') {
					    var day = d.getDay() || 7;  
						if( day !== 1 ) d = new Date(d.setHours(-24 * (day - 1)));
						d = new Date(d.setHours(0,0,0,0));
						d = Math.floor(d.getTime()/(1000*60*60*24*7));
					}
					intervalData[d] = intervalData[d]||[];
					intervalData[d].push(value);
				});
				var tmpIntervalData = [];
				var start = '';
				var finish = '';
				that.cur_cnt = 0;
				intervalData.forEach(function(intervalItem) {
					if (intervalItem.length) {
						var dt = new Date(intervalItem[0].dt);
						if (interval == 'year') {
						    dt = new Date(new Date(intervalItem[0].dt).getFullYear(), 0, 1).setHours(0,0,0,0);
						} else if (interval == 'month') {
							dt = new Date(dt.setDate(1)).setHours(0,0,0,0);
						} else if (interval == 'week') {
						    var day = dt.getDay() || 7;  
						    if( day !== 1 ) dt = new Date(dt.setHours(-24 * (day - 1)));
						    dt = dt.setHours(0,0,0,0);
						}
						start = start || dt;
						finish = dt || finish;
						var count = intervalItem.length;
						tmpIntervalData.push([new Date(dt).getTime(), count]);						
						that.cur_cnt = intervalItem.length;
					}
				});
				
				if (tmpIntervalData.length) {
					var i = 0;
					tmpIntervalData = tmpIntervalData.filter(function(){return true;});
					var dt = tmpIntervalData[i][0];
					while(dt < new Date().getTime()) {
						i++;
						if (tmpIntervalData.length == i) {
							break;
						}
						var date = new Date(dt);
						if (interval == 'year') {
							date = new Date(date.setFullYear(date.getFullYear() + 1));
						} else if (interval == 'month') {
							date = new Date(date.setMonth(date.getMonth() + 1));
						} else if (interval == 'week') {
							date = new Date(date.setDate(date.getDate() + 7));
						}
						var nextDate = new Date(tmpIntervalData[i][0]);
        				if (date.getDate() == nextDate.getDate() &&
        					date.getMonth() == nextDate.getMonth() &&
        					date.getFullYear() == nextDate.getFullYear()) {
								
        				} else {
        					i++;
							tmpIntervalData.splice(i, 0, [date.getTime(), 0]);
        				}
        				dt = tmpIntervalData[i][0];
					}
				}
				
				that.lead_grow_chart.series[0].setData(tmpIntervalData);
				var tickInterval = null;
				if (interval == 'year') {
					tickInterval = 24 * 3600 * 1000 * 365;
				} else if (interval == 'month') {
					tickInterval = 24 * 3600 * 1000 * 30;
				} else if (interval == 'week') {
					tickInterval = 24 * 3600 * 1000 * 7;
				}
				if (data.length) {
					that.lead_grow_chart.xAxis[0].update({min: new Date(start).getTime(), max: new Date(finish).getTime(), tickInterval: tickInterval});
				}
            },
            setPieSeriesData: function(interval) {
            	var that = this;
            	that.pie_interval = interval;
            	var data = that.data.grow.filter(function(item) {
					return !item.is_arch;
                });
            	
				var intervalData = [];
				data.map(function(value, index, array) {
					var d = new Date(value['dt']);
					if (interval == 'year') {
					    d = (d.getFullYear());
					} else if (interval == 'month') {
						d = (d.getFullYear()-1970)*12 + d.getMonth();
					} else if (interval == 'week') {
					    var day = d.getDay() || 7;  
						if( day !== 1 ) d = new Date(d.setHours(-24 * (day - 1)));
						d = new Date(d.setHours(0,0,0,0));
						d = Math.floor(d.getTime()/(1000*60*60*24*7));
					} else {
						d = 0;
					}
					intervalData[d] = intervalData[d]||[];
					intervalData[d].push(value);
				});
				var tmpIntervalData = [];
				var start = '';
				var finish = '';
				that.cur_cnt = 0;
				intervalData.forEach(function(intervalItem) {
					if (intervalItem.length) {
						var dt = new Date(intervalItem[0].dt);
						if (interval == 'year') {
						    dt = new Date(new Date(intervalItem[0].dt).getFullYear(), 0, 1).setHours(0,0,0,0);
						} else if (interval == 'month') {
							dt = new Date(dt.setDate(1)).setHours(0,0,0,0);
						} else if (interval == 'week') {
						    var day = dt.getDay() || 7;  
						    if( day !== 1 ) dt = new Date(dt.setHours(-24 * (day - 1)));
						    dt = dt.setHours(0,0,0,0);
						} else {
						    dt = new Date(new Date(intervalItem[0].dt).getFullYear(), 0, 1).setHours(0,0,0,0);
						}
						start = start || dt;
						finish = dt || finish;
						var count = intervalItem.length;
						tmpIntervalData.push([new Date(dt).getTime(), count]);						
						that.cur_cnt = intervalItem.length;
					}
				});
				
				// pie
				var lastIntarval = [];
				if (intervalData.length) {
					lastIntarval = intervalData[intervalData.length - 1];
				}
				var pieSeriesData = [];
				that.data.lead_sources.forEach(function(item) {
					pieSeriesData.push({
						name: item.title,
						y: lastIntarval.filter(function (interval) {
							return interval.lead_source_id == item.id;
						}).length
					});
				});
				that.lead_pie_chart.series[0].setData(pieSeriesData);
            },
            openAddLead: function(){
                var that = this;
                that.form = {
					fio: '',
					phone: '',
					lead_source_id: 1,
					comment: ''
	            };
                return CustomUI.showForm('form-addlead');
            },
            addLeadIsOK: function() {
				var that = this;
				return that.form.fio && that.form.phone && that.form.lead_source_id;
            },
            addlead: function() {
				var that = this;
				utilsSrvc.api.call('/app/lead/addlead/', that.form, function(result) {
					CustomUI.hideForm();
					if (result.data.duplicated) {
						CustomUI.showForm('form-addleadduplicated')
					}
					else {
						gmaback.js.showMessage('Лид добавлен', 5000);
                		that.load();
					}
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            hideForm: function() {
				CustomUI.hideForm();
            },
            setisarch: function(item) {
				var that = this;
				utilsSrvc.api.call('/app/lead/setisarch/', {id: item.id}, function(result) {
					gmaback.js.showMessage('Лид перемещен в ' + (item.is_arch ? 'активные' : 'архив'), 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            openDeleteLead: function(id){
                var that = this;
                that.delete_id = id;
                return CustomUI.showForm('form-deletelead');
            },
            deleteLead: function() {
				var that = this;
				if (!that.delete_id) {
					return;
				}
				utilsSrvc.api.call('/app/lead/deletelead/', {id: that.delete_id}, function(result) {
					that.delete_id = '';
					CustomUI.hideForm();
					gmaback.js.showMessage('Лид удален', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            }
        };
        
        $scope.safeApply = function(fn) {
   			var phase = this.$root.$$phase;
   			if(phase == '$apply' || phase == '$digest') {
   				if(fn && (typeof(fn) === 'function')) {
   					fn();
   				}
   			} else {
   				this.$apply(fn);
   			}
		};
		
		$scope.search = function (item) {
            if (!item.is_arch && (!$scope.lead.fio || (item.fio.toLowerCase().indexOf($scope.lead.fio) != -1))) {
			    return true;
			}
			return false;
		};
		
		$scope.archsearch = function (item) {
            if (item.is_arch && (!$scope.lead.fio || (item.fio.toLowerCase().indexOf($scope.lead.fio) != -1))) {
			    return true;
			}
			return false;
		};
		
		$scope.leadlist = {
			data: {},
            loading: false,
            form: {
				fio: '',
				phone: '',
				comment: ''
            },
        	fio: '',
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/lead/getleadlist/', {}, function(result) {
                	that.data.grow = result.data;	                       
	            }, null, null, function(){
		            that.loading = false;
		        });
			},
            openAddLead: function(){
                var that = this;
                that.form = {
					fio: '',
					phone: '',
					comment: ''
	            };
                return CustomUI.showForm('form-addleadlist');
            },
            openEditLeadList: function(lead){
                var that = this;
                that.form = {
                	id: lead.id,
					fio: lead.fio,
					phone: lead.phone,
					comment: lead.comment
	            };
                return CustomUI.showForm('form-editleadlist');
            },
            addLeadIsOK: function() {
				var that = this;
				return that.form.fio;
            },
            addlead: function() {
				var that = this;
				utilsSrvc.api.call('/app/lead/addleadlist/', that.form, function(result) {				
					CustomUI.hideForm();
					if (result.data.duplicated) {
						CustomUI.showForm('form-addleadlistduplicated')
					}
					gmaback.js.showMessage('Контакт добавлен', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            addLeadFromLeadList: function(item) {
				var that = this;
				utilsSrvc.api.call('/app/lead/addleadfromleadlist/', {id: item.id}, function(result) {
					gmaback.js.showMessage('Лид перемещен в активные', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            editLeadList: function() {
				var that = this;
				utilsSrvc.api.call('/app/lead/editleadlist/', that.form, function(result) {
					CustomUI.hideForm();
					gmaback.js.showMessage('Лид изменен', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            }
        };
        
        $scope.searchleadlist = function (item) {
            if (!$scope.leadlist.fio || (item.fio.toLowerCase().indexOf($scope.leadlist.fio) != -1)) {
			    return true;
			}
			return false;
		};
		
		$scope.lead_view = {
			data: {},
			form: {
				lead_id: '',
				lead_touch_type_id: 2,
				dt: '',
				comment: ''
			},
            loading: false,
            load: function() {
                var that = this;
                that.loading = true;
                that.initDatepicker();
                that.form = {
					lead_id: '',
					lead_touch_type_id: 2,
					dt: '',
					comment: ''
				};
                if (!$stateParams.id) {
                    return;
                }
                utilsSrvc.api.call('/app/lead/getleadbyid/', {id : $stateParams.id}, function(result) {
                	that.data = result.data;
                	if (!that.lead_touch_types) {
		                utilsSrvc.api.call('/app/lead/getleadtouchtypes/', {}, function(result) {
                			that.lead_touch_types = result.data;
		                }, null, null, function(){
			                that.loading = false;
			            });
					}
	            }, null, null, function(){
		            that.loading = false;
		        });
			},
			initDatepicker: function(value) {
				var that = this;
                // max date
                var dt = new Date();
                dt.setYear(dt.getFullYear() - 18);
                // start date
                var dt_start = new Date();
                dt_start.setYear(dt_start.getFullYear() - 18);
                var startDate = value ? new Date(value) : dt_start;
                $('#input-dt').datepicker({
                    language: 'ru', 
                    autoClose : true,
                    timepicker: true,
                    dateFormat: 'yyyy-mm-dd',
                    onSelect: function(fd, d, picker) {
                        if(d - startDate == 0) {
                            return;
                        }
                        $timeout(function(){
						    that.form.dt = fd;
						});
                    }
                }).data('datepicker').selectDate(startDate);
            },
            openAddComment: function(){
                var that = this;
                return CustomUI.showForm('form-addcomment');
            },
            addcomment: function() {
				var that = this;
                that.loading = true;
                if (!that.data.id) {
					return;
                }
                utilsSrvc.api.call('/app/lead/editleadcomment/', {id : that.data.id, comment: that.data.comment}, function(result) {
                	CustomUI.hideForm();
                	that.load();
	            }, null, null, function(){
		            that.loading = false;
		        });
            },
            openAddLeadTouch: function(){
                var that = this;
                that.form.lead_id = that.data.id;
                return CustomUI.showForm('form-addleadtouch');
            },
            addLeadTouchIsOK: function() {
				var that = this;
				return that.form.lead_id && that.form.lead_touch_type_id && that.form.dt;
            },
            addleadtouch: function() {
				var that = this;
                that.loading = true;
                if (!that.data.id) {
					return;
                }
                utilsSrvc.api.call('/app/lead/addleadtouch/', that.form, function(result) {
                	CustomUI.hideForm();
                	that.form = {
						lead_id: that.data.id,
						lead_touch_type_id: 2,
						dt: '',
						comment: ''
					};
                	that.load();
	            }, null, null, function(){
		            that.loading = false;
		        });
            },
            setisarch: function(item) {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('/app/lead/setisarch/', {id: item.id}, function(result) {
					gmaback.js.showMessage('Лид перемещен в ' + (item.is_arch ? 'активные' : 'архив'), 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            deletelead: function(id) {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('/app/lead/deletelead/', {id: id}, function(result) {
					gmaback.js.showMessage('Лид удален', 5000);
                	$state.go('index.lead.archive');
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            deleteLeadTouch: function(id) {
				var that = this;
				that.loading = true;
				utilsSrvc.api.call('/app/lead/deleteleadtouch/', {id: id}, function(result) {
					gmaback.js.showMessage('Касание удалено', 5000);
                	that.load();
                }, null, null, function(){
	                that.loading = false;
	            });
            },
        };
    };
            
    leadCtrl.$inject = injectParams;
    app.controller('leadCtrl', leadCtrl);

});