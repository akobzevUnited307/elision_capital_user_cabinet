'use strict';

define(['app', 'lang', 'service/utils'], function (app, lang) {

    var injectParams = ['$scope', 'utilsSrvc', '$window', '$timeout', '$interval', '$stateParams', '$state'];

    var officesCtrl = function ($scope, utilsSrvc, $window, $timeout, $interval, $stateParams, $state) {

        $scope.conf = getApp();
        $scope.lang = lang.values;
        $scope.loading = false;

        // Офисная программа
        $scope.program = {
            loading: true,
            data: null,
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/office/getprogram/', {office_id: $state.params.id}, function (result) {
                    that.data = result.data;
                }, null, null, function () {
                    that.loading = false;
                });
            }
        };

        // Заявки на открытие офиса
        $scope.requests = {
            loading: false,
            list: [],
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/office/getrequests/', {}, function(result) {
                    that.list = result.data.list;
                }, null, null, function () {
                    that.loading = false;
                });
            }
        };

        // Офисы trustera
        $scope.info = {
            loading: true,
            offices: [],
            office: {},
            load: function(only_my) {
                var that = this;
                only_my = !!only_my;
                that.loading = true;
                utilsSrvc.api.call('/app/office/getlist/', {only_my: only_my}, function (result) {
                    that.offices = that.buildOfficeObject(result.data.result);
                }, null, null, function () {
                    that.loading = false;
                });
            },
            myCount: function() {
                var cnt = 0;
                for(var o of $scope.requests.list) {
                    cnt++;
                }
                for(var o of $scope.info.offices) {
                    if(o.i_can_edit) {
                        cnt++;
                    }
                }
                return cnt;
            },
            loadItem: function(only_my) {
                var that = this;
                only_my = !!only_my;
                that.loading = true;
                utilsSrvc.api.call('/app/office/getlist/', {id: $stateParams.id, only_my: only_my}, function(result) {
                    $timeout(function() {
                        var officeArr = that.buildOfficeObject(result.data.result);
                        that.office = officeArr[0];
                        var myMap, layer;
                        var points_data = [];
                        var points = [[parseFloat(that.office.lot), parseFloat(that.office.lan)]];
                        var points_raw = [];
                        var points_title_data = [];
                        points_data[points_data.length] = '<div class="point-baloon"><h2>Адрес: Ямашева, д. 102А, п. 1</h2><table class="point-baloon-table"><tr><td><a class="fancybox" href="/data/imedia_kaz/01081049-10003650.jpg"><img src="/data/imedia_kaz/01081049-10003650.jpg" /></a><span class="code128">2112142331112</span></td><td>Код лифта:<br><strong>10003650</strong><br><br>Район:<br><strong>Элитка Ново</strong><br><br>Код тиража:<br><strong>01081049</strong><br><br>Дата:<br><strong>1 Апреля 2019, 08:02</strong></td></tr></table></div>';
                        points_raw[points_raw.length] = {"id":"12769","code":"10003650","photo":null,"desc":null,"dist":"\u042d\u043b\u0438\u0442\u043a\u0430 \u041d\u043e\u0432\u043e","street":"\u042f\u043c\u0430\u0448\u0435\u0432\u0430","house":"102\u0410","porch":"1","way_id":"77","way_step":"100","type":null,"cord_x":that.office.lot,"cord_y":that.office.lan,"status_id":0,"msg":null,"last_action_time":"1 \u0410\u043f\u0440\u0435\u043b\u044f 2019, 08:02"};
                        /**
                         * http://openlayers.org/en/latest/examples/
                         * http://openlayers.org/en/latest/apidoc/
                         * http://korona.geog.uni-heidelberg.de/tiles/roads/x=41719&y=20476&z=16
                         **/
                        myMap = new ol.Map({
                            target: 'map',
                            layers: [
                                new ol.layer.Tile({
                                    source: new ol.source.OSM()
                                })
                            ],
                            view: new ol.View({
                                // projection: 'EPSG:4326',
                                center: ol.proj.fromLonLat([parseFloat(that.office.lot), parseFloat(that.office.lan)]),
                                zoom: 15
                            })
                        });
                        const overlay = new ol.Overlay({
                            element: document.getElementById('popup-container'),
                            positioning: 'bottom-center',
                            offset: [0, -10],
                            autoPan: true
                        });
                        myMap.addOverlay(overlay);
                        var styles = {
                            'route': new ol.style.Style({
                                stroke: new ol.style.Stroke({
                                    width: 3, color: [237, 212, 0, 0.8]
                                })
                            }),
                            'red': new ol.style.Style({image: new ol.style.Icon({anchor: [0.5, 0.5], src: '/assets/img/map_point.png'})}),
                            'geoMarker': new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 7,
                                    snapToPixel: false,
                                    fill: new ol.style.Fill({color: 'black'}),
                                    stroke: new ol.style.Stroke({
                                        color: 'white', width: 1
                                    })
                                })
                            })
                        };
                        var layers = [];
                        // Зелёные
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                /*// hide geoMarker if animation is active
                                if (animating && feature.get('type') === 'geoMarker') {
                                    return null;
                                }*/
                                return styles[feature.get('type')];
                            }
                        }));
                        // Красные
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                return styles[feature.get('type')];
                            }
                        }));
                        // Жёлтые
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                return styles[feature.get('type')];
                            }
                        }));
                        // Серые
                        layers.push(new ol.layer.Vector({
                            source: new ol.source.Vector({features: []}),
                            style: function(feature) {
                                return styles[feature.get('type')];
                            }
                        }));
                        for(var i = 0, len = points.length; i < len; i++) {
                            var s = points_raw[i].status_id;
                            var type = 'red';
                            var marker = new ol.Feature({
                                type: type,
                                index: i,
                                data: points_raw[i],
                                geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(points_raw[i].cord_x), parseFloat(points_raw[i].cord_y)]))
                            });
                            layers[s].getSource().addFeature(marker);
                        }
                        myMap.addLayer(layers[1]);
                        myMap.addLayer(layers[2]);
                        myMap.addLayer(layers[3]);
                        myMap.addLayer(layers[0]);
                        myMap.on('click', function(e) {
                            let markup = '';
                            myMap.forEachFeatureAtPixel(e.pixel, function(feature) {
                                if(markup == '') {
                                    markup += `${markup && '<hr>'}<table>`;
                                    const properties = feature.getProperties();
                                    console.log(properties);
                                    var index = properties.index;
                                    markup += points_data[index];
                                }
                            }, {hitTolerance: 1});
                            if (markup) {
                                document.getElementById('popup-content').innerHTML = markup;
                                overlay.setPosition(e.coordinate);
                            } else {
                                overlay.setPosition();
                            }
                        });
                    });

                }, null, null, function(){
                    that.loading = false;
                });
            },
            selectEvent: function() {
                this.isEvent = true;
            },
            changeBool: function (param) {
                if (param == 'isEvent') {
                    this.isEvent = !this.isEvent;
                    this.isOffice = false;
                    this.isTeam = false;
                    this.isContact = false;
                }
                if (param == 'isOffice') {
                    this.isOffice = !this.isOffice;
                    this.isEvent = false;
                    this.isTeam = false;
                    this.isContact = false;
                }
                if (param == 'isTeam') {
                    this.isTeam = !this.isTeam;
                    this.isOffice = false;
                    this.isEvent = false;
                    this.isContact = false;
                }
                if (param == 'isContact') {
                    this.isContact = !this.isContact;
                    this.isOffice = false;
                    this.isTeam = false;
                    this.isEvent = false;
                }
                
            },
            buildOfficeObject: function (officesArr) {
                officesArr.forEach(function(of) {
                    if(of.contact_list) {
                        of.contact_list.forEach(function (contact) {
                            if (contact.office_contact_type_id == 1) {
                                // of.address = contact.value;
                            } else if (contact.office_contact_type_id == 2) {
                                of.number = contact.value;
                            } else if (contact.office_contact_type_id == 3) {
                                of.email = contact.value;
                            } else if (contact.office_contact_type_id == 4) {
                                of.telegram = contact.value;
                            } else if (contact.office_contact_type_id == 5) {
                                of.whatsapp = contact.value;
                            } else if (contact.office_contact_type_id == 6) {
                                of.contact_person = contact.value;
                            }
                        });
                    }
                    if(of.social_list) {
                        of.social_list.forEach(function (social) {
                            if (social.social_type_id == 1) {
                                of.vk = social.value;
                            } else if (social.social_type_id == 2) {
                                of.facebook = social.value;
                            } else if (social.social_type_id == 5) {
                                of.twitter = social.value;
                            } else if (social.social_type_id == 7) {
                                of.youtube = social.value;
                            } else if (social.social_type_id == 8) {
                                of.instagram = social.value;
                            }
                        });
                    }
                });

                return officesArr;
            },
            cities: function () {
                utilsSrvc.api.call('/app/office/getlist/', {}, function (result) {
                    // that.result = result.data;
                    that.offices = result.data.result;
                    that.offices.forEach(function(of) {
                    });
                }, null, null, function () {
                    that.loading = false;
                });
            },
            goToOfficeItem: function (office_id) {
                gmaback.index_scope.pages.gotoURL('index.offices.view', {id: office_id})
            },
            goToOfficeManage: function (office_id) {
                gmaback.index_scope.pages.gotoURL('index.offices.manage', {id: office_id})
            },
            loadEvents: function() {
                var that = this;  
                that.loading = true;                
                utilsSrvc.api.call('/app/events/getofficialeventcaselist', {}, function(result) {
                    that.data = result.data.list;
                    that.event_cases_visible_count = result.data.visible_count;
                    that.data = result.data.list.filter(function(item){
                        return item.in_future && item.is_active && item.office_id==that.office.id;
                    });
                    that.dataHist = result.data.list.filter(function(item){
                        return !item.in_future && item.is_active && item.office_id==that.office.id;
                    });
                    that.currentWeekDate = {
                        dt: new Date()
                    };
                    that.currentWeekDate.dt.setHours(0, 0, 0, 0);
                    that.currentWeekDate.dt = new Date(that.currentWeekDate.dt);
                }, null, null, function(){   
                    that.loading = false;
                });
            },
            loadoffices: function() {
                var that = this;  
                that.office_loading = true;                
                utilsSrvc.api.call('/app/office/getlist', { }, function(result){
                    that.offices = result.data.result;
                }, null, null, function(){
                    that.office_loading = false;
                });
            }
        };

        // Управление
        $scope.manage = {
            photo: {
                loading: false,
                /*form: {
                    image: ''
                },*/
                processCFAttachImage: function(files) {
                    var that = this;
                    console.log('files', files[0]);
                    var file = files[0];
                    if(!file.type.startsWith('image/')) {
                        return gmaback.js.showError('Необходимо выбрать изображение', 3000);
                    }

                    var form = new FormData();
                    form.append('office_id', $stateParams.id);
                    form.append('photo', file);
                    that.loading = true;                
                    utilsSrvc.api.call('/app/office/setphoto', form, function(result) {
                        // that.reset();
                        $scope.info.loadItem();
                        // CustomUI.hideForm();
                    }, null, null, function(){   
                        that.loading = false;
                    });
                },
                select: function(){
                    $('#input-manage-photo').trigger('click');
                }
            },
            contacts: {
                add: {
                    loading: false,
                    form: {
                        office_id: null,
                        office_contact_type_id: '1',
                        value: '',
                        person: {
                            fio: '',
                            phone: '',
                            position: ''
                        }
                    },
                    reset: function() {
                        this.form.office_contact_type_id = '1';
                        this.form.person.fio = '';
                        this.form.person.phone = '';
                        this.form.person.position = '';
                        this.form.value = '';
                    },
                    open: function() {
                        return CustomUI.showForm('form-addcontact');
                    },
                    canSubmit: function() {
                        if(this.form.office_contact_type_id == '6') {
                            return this.form.person.fio && this.form.person.phone && this.form.person.position;
                        } else {
                            return !!this.form.value;
                        }
                    },
                    submit: function() {
                        var that = this;
                        that.form.office_id = $stateParams.id;
                        that.loading = true;                
                        utilsSrvc.api.call('/app/office/addcontact', that.form, function(result) {
                            that.reset();
                            $scope.info.loadItem();
                            CustomUI.hideForm();
                        }, null, null, function(){   
                            that.loading = false;
                        });
                    }
                }
            },
            employee: {
                // поиск
                search: {
                    result: null,
                    form: {
                        office_id: '',
                        q: ''
                    },
                    reset: function() {
                        this.result = null;
                    },
                    submit: function() {
                        var that = this;
                        that.form.office_id = $stateParams.id;
                        that.loading = true;                
                        utilsSrvc.api.call('/app/office/searchuserforaddemployee', that.form, function(result) {
                            if(result.data) {
                                $scope.manage.employee.add.form.user_login = result.data.user_login;
                                $scope.manage.employee.add.form.fio = result.data.fio;
                                $scope.manage.employee.add.form.phone = result.data.phone;
                            }
                            that.result = result.data;
                        }, null, null, function(){   
                            that.loading = false;
                        });
                    }
                },
                // добавление
                add: {
                    loading: false,
                    form: {
                        office_id: '',
                        user_login: '',
                        fio: '',
                        phone: '',
                        position: '',
                    },
                    reset: function() {
                        this.form.user_login = '';
                        this.form.fio = '';
                        this.form.phone = '';
                        this.form.position = '';
                        $scope.manage.employee.search.form.q = '';
                        $scope.manage.employee.search.result = null;
                    },
                    open: function() {
                        return CustomUI.showForm('form-addemployee');
                    },
                    submit: function() {
                        var that = this;
                        that.form.office_id = $stateParams.id;
                        that.loading = true;                
                        utilsSrvc.api.call('/app/office/addemployee', that.form, function(result) {
                            that.reset();
                            $scope.info.loadItem();
                            CustomUI.hideForm();
                        }, null, null, function(){   
                            that.loading = false;
                        });
                    }
                }
            },
            // перевод денег
            sendmoney: {
                loading: false,
                form: {
                    office_id: '',
                    amount: 0
                },
                reset: function() {
                    this.form.amount = 0;
                },
                open: function() {
                    return CustomUI.showForm('form-sendmoney');
                },
                submit: function() {
                    var that = this;
                    that.form.office_id = $stateParams.id;
                    that.loading = true;                
                    utilsSrvc.api.call('/app/office/sendmoney', that.form, function(result) {
                        that.reset();
                        $scope.info.loadItem();
                        CustomUI.hideForm();
                    }, null, null, function(){   
                        that.loading = false;
                    });
                }
            }
        };

    };

    officesCtrl.$inject = injectParams;

    app.controller('officesCtrl', officesCtrl);

});