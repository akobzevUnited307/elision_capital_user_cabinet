'use strict';

define(['app', 'lang', 'service/utils', 'filter/chat_message',  'service/user'], function(app, lang) {

    var headerCtrlInjectParams = ['$scope', 'playerSrvc', 'utilsSrvc', 'userSrvc', '$timeout', '$state', 'messengerSrvc', '$rootScope'];
    var headerCtrl = function ($scope, playerSrvc, utilsSrvc, userSrvc, $timeout, $state, messengerSrvc, $rootScope) {

        gmaback.header_scope = $scope;
        // $scope.messenger = messengerSrvc.connect();

        getApp().lang = lang.values;
        $scope.app = getApp();
        $scope.my = utilsSrvc.my;
        $scope.utilsSrvc = utilsSrvc;

        // View
        $scope.view = utilsSrvc.my.view.load();

        // Profile
        $scope.profile = userSrvc.profile.load();

        // redirect
        $scope.redirect = function (url, data) {
            if (!data) {
                data = {};
            }
            $state.go(url, data);
        };

        // showMyForm
        $scope.showMyForm = function($event) {
            $('#my_tab_profile').getScope().showMyForm($event, 'my_tab_profile');
        }

        // События
        $scope.events = utilsSrvc.events.init();

        //$scope.$watch(function () { return utilsSrvc.my; }, function(newVal, oldVal) {
        //    console.log('profile', newVal);
            // $scope.profile = newVal.profile;
            // $scope.current_profile = newVal.current_profile;
        //});
        $scope.$watchCollection(function () { return utilsSrvc.events; }, function(newVal, oldVal) {
            $scope.events.chat_invite_queue.list = newVal.chat_invite_queue.list;
            $scope.events.list = newVal.list;
            $scope.events.id_list = newVal.id_list;
        });
        
        

        // partnerStatus
        $scope.partnerStatus = {
            loading_check: false,
            loading_bepartner: false,
            accept: false,
            step: 0,
            check: function() {
                var that = this;
                that.loading_check = true;
                utilsSrvc.api.call('/app/buy/bepartnercheck/', {}, function(result) {
                    if(result.data) {
                        that.step = 1;
                    } else {
                        gmaback.js.showError('К сожалению, у Вас нет открытого инвестиционного счета в Trustera. Сначала откройте счет, а затем попробуйте снова.', 5000);
                    }
                }, null, null, function () {
                    that.loading_check = false;
                });
            },
            bepartner: function() {
                var that = this;
                that.loading_bepartner = true;
                utilsSrvc.api.call('/app/buy/bepartner/', {}, function(result) {
                    // that.load();
                    getApp().changeProfile(result.data.user);
                    that.step = 3;
                    CustomUI.hideForm();
                    gmaback.js.showMessage('Поздравляем!<br>Теперь Вы бренд-партнер Golden DAX.', 5000);
                }, null, null, function () {
                    that.loading_bepartner = false;
                });
            }
        };

        $timeout(function(){
            utilsSrvc.header.load();
        });

    };

    headerCtrl.$inject = headerCtrlInjectParams;
    app.controller('headerCtrl', headerCtrl);

});