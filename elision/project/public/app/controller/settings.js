'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var settingsCtrl = function ($scope, utilsSrvc, $state) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        
        $scope.profile = {
            loading: false,       
            data: false,
            load: function() {
                var that = this;                                               
                if (!$scope.app.isLogged()) {
                    return;
                }
                that.loading = true;                
                utilsSrvc.api.call('/app/user/getbycode', { user_login: $scope.app.user.user_login, system_type_id: 2 }, function(result){
                    that.data = result.data;   
                }, null, null, function(){
                    that.loading = false;
                });
            }, 
            savecity: function(item) {
                utilsSrvc.api.call('/app/settings/savecity', {
                    id: item.id
                }, function(result) {                  
                    gmaback.js.showMessage(getApp().lang.save_success, 4000);
                    CustomUI.hideForm();
                }, function(result) {
                    gmaback.js.showError(result.message, 4000);
                });
            }
        };  
        
        $scope.city = {
            loading: false,       
            data: false,
            query: '',
            load: function() {
                var that = this;                                               
                if (!$scope.app.isLogged()) {
                    return;
                }                                   
                utilsSrvc.api.call('/app/settings/searchcity', { query: that.query }, function(result){
                    that.data = result.data;   
                }, null, null, function(){          
                });
            }
        };
        
        $scope.settings = {
            loading: false,
            sessions: [],
            load: function(){
                var that = this;
                if (!$scope.app.isLogged()) {
                    return;
                }
                that.loading = true;
                utilsSrvc.api.call('/app/settings/getusersessions', {}, function(result){
                    that.sessions = result.data.sessions;
                    that.tg_phone = result.data.tg_phone;
                }, function(result){
                    gmaback.js.showError(result.message, 4000);
                }, null, function(){
                    that.loading = false;
                });
            },
            deleteusersession: function(id) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/settings/deleteusersession', {id: id}, function(result) {
                    that.load();
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

    };

    settingsCtrl.$inject = injectParams;
    app.controller('settingsCtrl', settingsCtrl);

});