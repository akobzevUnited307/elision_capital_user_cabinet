'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams', '$timeout'];
    var leaderBonusCtrl = function ($scope, utilsSrvc, $state, $stateParams, $timeout) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        $scope.BTC_SATOCHI = 100000000;
        
        $scope.leader_level = [
        	{level: 1, career_id: 12,  percent: [{exists:true},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false}]},
        	{level: 2, career_id: 13,  percent: [{exists:true},{exists:true},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false}]},
        	{level: 3, career_id: 14, percent: [{exists:true},{exists:true},{exists:true},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false}]},
        	{level: 4, career_id: 15, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false}]},
        	{level: 5, career_id: 16, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:false},{exists:false},{exists:false},{exists:false},{exists:false}]},
        	{level: 6, career_id: 17, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:false},{exists:false},{exists:false},{exists:false}]},
        	{level: 7, career_id: 18, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:false},{exists:false},{exists:false}]},
        	{level: 8, career_id: 19, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:false},{exists:false}]},
        	{level: 9, career_id: 20, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:false}]},
        	{level: 10, career_id: 21, percent: [{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true},{exists:true}]}
        ];
        
        $scope.bonus_leader_summary = {
			loading: false,
			data: [],
			load: function() {
				var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/leaderbonus/getsummarylist', {}, function (result) {
                    that.data = result.data;
                    that.initChart();
                }, null, null, function () {
                    that.loading = false;
                });
			},
			initChart: function() {
				var that = this;
				if (!that.data.length) {
					$('#chart').hide();
					return;
				}
				that.chart = Highcharts.stockChart('chart', {
					chart: {
						type: 'column',
						backgroundColor: '#33353d',
						height: 300
					},
					colors: ['#bda076'],
					title: {
						text: ''
					},
					subtitle: {
				        align: 'left'
				    },
					xAxis: {
						type: 'datetime',
						max: newDate().getTime(),
						min: newDate().setFullYear(newDate().getFullYear() - 1),
					    tickInterval: 24 * 3600 * 1000 * 7
					},
					yAxis: {
						title: {
							text: ' '
						},
						min: 0
					},
					tooltip: {
						xDateFormat: '%d.%m.%Y',
						pointFormat: '{point.y:.2f} USDT',
						shared: true
					},
					series: [{
						name: ' ',
						type: 'column',
						threshold: null,
						tooltip: {
						    valueDecimals: 2
						}
					}],
					responsive: {
						rules: [{
						    condition: {
						        maxWidth: 500
						    },
						    chartOptions: {
						        chart: {
						            height: 300
						        },
						        subtitle: {
						            text: null
						        },
						        navigator: {
						            enabled: false
						        }
						    }
						}]
					},
					rangeSelector: {
      					buttonTheme: {
              				width: 60,
              				style: {
					            color: '#bda076',
					            fontWeight: 'white'
					        },
					        states: {
					            hover: {
					            },
					            select: {
					                fill: '#bda076',
					                style: {
					                    color: 'white'
					                }
					            }
					        }
					    },
						buttons: [{
							type: 'all',
							text: 'все время',							
							events: {
								click: function() {
									$scope.safeApply(that.setSeriesData('all', that.data, that.chart));
								}
							}
						}, {
							type: 'all',
							text: 'месяц',
							events: {
								click: function() {
									$scope.safeApply(that.setSeriesData('month', that.data, that.chart));
								}
							}
						}, {
							type: 'all',
	                        text: 'год',
							events: {
								click: function() {
									$scope.safeApply(that.setSeriesData('year', that.data, that.chart));
								}
							}
	                    }],
						allButtonsEnabled: true,
              			inputEnabled: false
					},
					navigator: {
						enabled: false
					}
				});
				that.setSeriesData('all', that.data, that.chart);
			},
			setSeriesData: function(interval, seriesData, chart) {
            	var that = this;
				if (interval == 'all') {
					var start = null;
					var finish = null;
					var seriesData = seriesData.map(function(item) {
						if (start == null) {
							start = newDate(item.dt).getTime();
							finish = newDate(item.dt).getTime();
						}
						start = newDate(item.dt).getTime() < start ? newDate(item.dt).getTime() : start;
						finish = newDate(item.dt).getTime() > finish ? newDate(item.dt).getTime() : finish;
						return [newDate(item.dt).getTime(), item.amount_usd / $scope.BTC_SATOCHI];
					});
					chart.series[0].setData(seriesData);
					chart.xAxis[0].update({min: start, max: finish});
				}
				else if (interval == 'month') {
					var start = null;
					var finish = null;
					var seriesData = seriesData.slice(Math.max(seriesData.length - 1, 0)).map(function(item) {
						if (start == null) {
							start = newDate(item.dt).getTime();							
							finish = newDate(item.dt).getTime();
						}
						start = newDate(item.dt).getTime() < start ? newDate(item.dt).getTime() : start;
						finish = newDate(item.dt).getTime() > finish ? newDate(item.dt).getTime() : finish;
						return [newDate(item.dt).getTime(), item.amount_usd / $scope.BTC_SATOCHI];
					});
					chart.series[0].setData(seriesData);
					chart.xAxis[0].update({min: start, max: finish});
				}
				else if (interval == 'year') {
					var start = null;
					var finish = null;
					var seriesData = seriesData.slice(Math.max(seriesData.length - 12, 0)).map(function(item) {
						if (start == null) {
							start = newDate(item.dt).getTime();
							finish = newDate(item.dt).getTime();
						}
						start = newDate(item.dt).getTime() < start ? newDate(item.dt).getTime() : start;
						finish = newDate(item.dt).getTime() > finish ? newDate(item.dt).getTime() : finish;
						return [newDate(item.dt).getTime(), item.amount_usd / $scope.BTC_SATOCHI];
					});
					chart.series[0].setData(seriesData);
					chart.xAxis[0].update({min: start, max: finish});
				}
            }
        };
        
        $scope.bonus_leader = {
			loading: false,
			data: [],
			load: function() {
				var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/leaderbonus/getsummary', {id: $stateParams.id}, function (result) {
                    that.data = result.data;
                }, null, null, function () {
                    that.loading = false;
                });
			}
        };
        
        $scope.safeApply = function(fn) {
   			var phase = this.$root.$$phase;
   			if(phase == '$apply' || phase == '$digest') {
   				if(fn && (typeof(fn) === 'function')) {
   					fn();
   				}
   			} else {
   				this.$apply(fn);
   			}
		};
    };

    leaderBonusCtrl.$inject = injectParams;
    app.controller('leaderBonusCtrl', leaderBonusCtrl);

});