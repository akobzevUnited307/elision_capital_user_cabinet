'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state'];
    var teamCtrl = function ($scope, utilsSrvc, $state) {

        // Constants
        $scope.lang = getApp().lang;
        $scope.BTC_SATOCHI = 100000000;
        $scope.app = getApp();
        $scope.date = newDate();                 
        $scope.pages.setURL($state.current.name);
        $scope.my = utilsSrvc.my;

        if($scope.app.user.user_status_id < 3) {
            return $state.go('index.user', {});
        }
        
        // Team
        $scope.team = {
            data: {},
            loading: false,
            init: function () {
				Highcharts.setOptions({
        			lang:{
            			rangeSelectorZoom: ''
					},
					global: {
						useUTC: false
					}
				});
            },
            loadProfit: function () {					
				var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadforpartnerdata/', {command_profit: true}, function(result) {
                    that.data.command_profit = result.data.command_profit;
                    that.list = that.data.command_profit.list;
                    that.setData();
                    
                    
                new Highcharts.Chart({
			        chart: {
			            renderTo: 'turnover-chart',
			            type: 'pie',
			            backgroundColor: '#343740'
			        },
			        title: {
			            text: ''
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            pie: {
			                shadow: false,
			                borderColor: '#42454e',
			                colors: ['#304483', '#997e2a']
			            }
			        },
			        legend: {
		            	enabled: false
			        },
			        tooltip: {
			            formatter: function() {
			                return '<b>'+ this.point.name +'</b>: '+ this.y.toFixed(2) +' %';
			            }
			        },
			        series: [{
			            name: '',
			            data: [["Trustera", that.data.command_profit.trustera_turnover / that.data.command_profit.total_turnover * 100],["Проавто", 100 - that.data.command_profit.trustera_turnover / that.data.command_profit.total_turnover * 100]],
			            size: '100%',
			            innerSize: '70%',
			            showInLegend:true,
			            dataLabels: {
			                enabled: false
			            }
			        }]
			    });
			    new Highcharts.Chart({
			        chart: {
			            renderTo: 'trustera-chart',
			            type: 'pie',
			            backgroundColor: '#343740'
			        },
			        title: {
			            text: ''
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            pie: {
			                shadow: false,
			                borderColor: '#42454e',
			                colors: ['#8f3a41', '#607e3e']
			            }
			        },
			        legend: {
		            	enabled: false
			        },
			        tooltip: {
			            formatter: function() {
			                return '<b>'+ this.point.name +'</b>: '+ this.y.toFixed(2) +' USDT';
			            }
			        },
			        series: [{
			            name: '',
			            data: [["Сумма коридоров", that.data.command_profit.trustera_turnover / $scope.BTC_SATOCHI],["ОИК", that.data.command_profit.oik_sum / $scope.BTC_SATOCHI]],
			            size: '100%',
			            innerSize: '70%',
			            showInLegend:true,
			            dataLabels: {
			                enabled: false
			            }
			        }]
			    });
			    new Highcharts.Chart({
			        chart: {
			            renderTo: 'wallet-chart',
			            type: 'pie',
			            backgroundColor: '#343740'
			        },
			        title: {
			            text: ''
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            pie: {
			                shadow: false,
			                borderColor: '#42454e',
			                colors: ['#8f3a41', '#607e3e', '#197a98']
			            }
			        },
			        legend: {
		            	enabled: false
			        },
			        tooltip: {
			            formatter: function() {
			                return '<b>'+ this.point.name +'</b>: '+ this.y;
			            }
			        },
			        series: [{
			            name: '',
			            data: [["Стандартные", that.data.command_profit.standart_cnt],["Трастовые", that.data.command_profit.trust_cnt],["Intro", that.data.command_profit.intro_cnt]],
			            size: '100%',
			            innerSize: '70%',
			            showInLegend:true,
			            dataLabels: {
			                enabled: false
			            }
			        }]
			    });
			    new Highcharts.Chart({
			        chart: {
			            renderTo: 'people-chart',
			            type: 'pie',
			            backgroundColor: '#343740'
			        },
			        title: {
			            text: ''
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            pie: {
			                shadow: false,
			                borderColor: '#42454e',
			                colors: ['#8f3a41', '#607e3e']
			            }
			        },
			        legend: {
		            	enabled: false
			        },
			        tooltip: {
			            formatter: function() {
			                return '<b>'+ this.point.name +'</b>: '+ this.y;
			            }
			        },
			        series: [{
			            name: '',
			            data: [["Инвесторы", that.data.command_profit.investor_cnt],["Пустые", that.data.command_profit.empty_cnt]],
			            size: '100%',
			            innerSize: '70%',
			            showInLegend:true,
			            dataLabels: {
			                enabled: false
			            }
			        }]
			    });
                new Highcharts.Chart({
			        chart: {
			            renderTo: 'proauto-chart',
			            type: 'pie',
			            backgroundColor: '#343740'
			        },
			        title: {
			            text: ''
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            pie: {
			                shadow: false,
			                borderColor: '#42454e',
			                colors: ['#8f3a41', '#607e3e', '#197a98', '#997e2a']
			            }
			        },
			        legend: {
		            	enabled: false
			        },
			        tooltip: {
			            formatter: function() {
			                return '<b>'+ this.point.name +'</b>: '+ this.y;
			            }
			        },
			        series: [{
			            name: '',
			            data: [["Базовая", that.data.command_profit.base_car_cnt],["Стандартная", that.data.command_profit.standart_car_cnt],["Премьер", that.data.command_profit.premier_car_cnt],["Накопительная", that.data.command_profit.cumulative_car_cnt]],
			            size: '100%',
			            innerSize: '70%',
			            showInLegend:true,
			            dataLabels: {
			                enabled: false
			            }
			        }]
			    });
                    
                }, null, null, function(){
	                that.loading = false;
	            });
            },
            changeProfitPeriod: function () {
				var that = this;
				that.profit_all_time = !that.profit_all_time;
				if (that.profit_all_time) {
					if (that.data.command_profit.list && that.data.command_profit.list.length) {
						var period = {
							dt: that.data.command_profit.list[that.data.command_profit.list.length - 1].dt,
							amount: 0,
							amount_career: 0,
							amount_main: 0,
							amount_world: 0,
							amount_office: 0,
							amount_rub: 0,
							amount_proauto: 0,
							amount_total: 0
						}; 
						that.data.command_profit.list.forEach(function (item) {
							period.amount += item.amount;
							period.amount_career += item.amount_career;
							period.amount_main += item.amount_main;
							period.amount_world += item.amount_world;
							period.amount_office += item.amount_office;
							period.amount_rub += item.amount_rub;
							period.amount_proauto += item.amount_proauto;
							period.amount_total += item.amount_total;
						});
						that.list = [period];
						that.setData();
					}
				} else {
					that.list = that.data.command_profit.list;
						that.setData();
				}
            },
        	setData: function () {
        		var that = this;
				if (that.list && that.list.length) {
                    that.profitChartItemClick(that.list[that.list.length - 1]);
                    $(document).ready(function() {
                        $('.command-partner-chart-body')[0].scrollLeft = 1000;
                    });
                }
        	},
            getDt: function (dt) {
                if (!dt) {
                    return;
                }
                return ('0' + (newDate(dt).getMonth() + 1)).slice(-2) + '.' + newDate(dt).getFullYear().toString().slice(-2);
            },
            profitChartItemClick: function (item) {
                var that = this;
                that.profitChartItem = item;
            }
        };
        
        // getRefURL
        $scope.getRefURL = function() {
            // return window.location.origin + '/r/' + getApp().user.user_login + '/';
            return 'https://trustera.global/r/' + getApp().user.user_login + '/';
        };

        // Клуб GoldenDAX
        $scope.career = utilsSrvc.career.load();

        // Реферальная структура
        $scope.message = {
            goToChat: function(user_login) {
                var that = this;
                that.loading = true;
                var CONTACT_TYPE_USER = 1
                var form = {
                    contact_type_id: CONTACT_TYPE_USER,
                    user_login: user_login
                }
                utilsSrvc.api.call('/app/messenger/addcontact', form, function(result){
                    if (result.data) {
                        $state.go('navigation.messenger', {url: result.data });
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

        $scope.checkCurWeek = function (d) {
        	var monday = newDate();
        	var day = monday.getDay() || 7;  
			if( day !== 1 ) monday = newDate(monday.setHours(-24 * (day - 1)));
			monday = monday.setHours(0,0,0,0);
        	return newDate(d).getTime() >= monday;
		}
   
   		$scope.safeApply = function(fn) {
   			var phase = this.$root.$$phase;
   			if(phase == '$apply' || phase == '$digest') {
   				if(fn && (typeof(fn) === 'function')) {
   					fn();
   				}
   			} else {
   				this.$apply(fn);
   			}
		};
        
        if(getApp().is_logged) {
            $scope.career.load();
        }

    };

    teamCtrl.$inject = injectParams;
    app.controller('teamCtrl', teamCtrl);

});