'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc', '$state', '$stateParams'];
    var profileCtrl = function ($scope, utilsSrvc, $state, $stateParams) {

        $scope.pages.setURL($state.current.name);
        $scope.app = getApp(); 

        $scope.profilebycode = {
            loading: false,          
            data: false,
            isInTree: false,
            load: function() {
                var that = this;
                if (!$stateParams.user_login) {
                    return;
                }
                that.loading = true;                
                utilsSrvc.api.call('/app/user/getbycode', { user_login: $stateParams.user_login, system_type_id: 2 }, function(result){
                    that.data = result.data;
                    that.getPremiumDate();
                    var currentDate = new Date();
                    var regDate = new Date(that.data.dt.replace(/-/g,"/"));
                    that.data.regDate = regDate;
                    var diffTime = Math.abs(currentDate.getTime() - regDate.getTime());
                    that.data.regDayAgo = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                    that.data.contactObj = that.buildContactObject(that.data.contact_list);
                    
                    if ($scope.app.user.user_login == $stateParams.user_login) {
                        that.isInTree = true;
                        return;
                    }
                    
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/loadforpartnerdata/', {}, function(result) {
                        var tree = result.data;
                        if ( tree.referals && tree.referals.tree) {
                            that.isInTree = that.searchTree($stateParams.user_login, tree.referals.tree);
                        }
                        
                    }, null, null, function(){
                        that.loading = false;
                    });
                    
                }, null, null, function(){
                    that.loading = false;
                });
                
                
            },
            searchTree: function(user_login, tree) {
                var that = this;
                for (var i = 0; i < tree.length; i++) {
                    var item = tree[i];
                    if (item.user) {
                        if (item.user.user_login == user_login) {
                            return true;
                        }
                    }
                    if (item.referal_list.length) {
                        var res = that.searchTree(user_login, item.referal_list);
                        if (res == true) {
                            return res;
                        }
                    }
                }
                return false;
            },
            buildContactObject: function (contactArr) {
                var contactObj = {};
                /*contactArr.forEach(function(item) {
                    if (item.user_contact_type_id == 1) {
                        contactObj.vk = 'https://vk.com/' + item.value;
                    } else if (item.user_contact_type_id == 2) {
                        contactObj.facebook = 'https://www.facebook.com/' + item.value;
                    } else if (item.user_contact_type_id == 5) {
                        contactObj.twitter = 'https://twitter.com/' + item.value;
                    } else if (item.user_contact_type_id == 8) {
                        contactObj.instagram = 'https://www.instagram.com/' + item.value;
                    }
                });*/
                return contactObj;
            },
            GoToVisitedEvents: function() {
                if(this.data.official_event_count == 0 || !this.data.invited_by_me) {
                    return;
                }
                gmaback.index_scope.pages.gotoURL('index.userevent', {id: this.data.id});
            },
            copyToClipboard: function () {
                var that = this;
                var str = 'https://trustera.global/r/' + that.data.user_login + '/';
                const el = document.createElement('textarea');
                el.value = str;               
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                document.body.removeChild(el);
                gmaback.js.showMessage('Текст скопирован', 4000);
            },
            getPremiumDate: function() {
            	var that = this;
				if (that.data.goldendax_ticket_id) {
					if (that.data.goldendax_ticket_dt_end) {
						var dt = new Date(that.data.goldendax_ticket_dt_end);
						var now = new Date();
						if (dt > now) {
							
							// year
							var diff = Math.abs(dt.getTime() - now.getTime()) / 1000;
						    diff /= (60 * 60 * 24);
						    if (diff >= 365) {
								that.premium_date = Math.floor(diff / 365) + 'г';
								return;
						    }						
							// month
							if (diff >= 30) {
								that.premium_date = Math.floor(diff / 30) + 'м';
								return;
						    }					    
						    // day
							if (diff > 1) {
								that.premium_date = Math.floor(diff) + 'д';
								return;
						    }
						    // hour
						    diff = Math.abs(dt.getTime() - now.getTime()) / 1000;
						    diff /= (60 * 60);
							if (diff >= 0) {
								that.premium_date = Math.floor(diff) + 'ч';
								return;
						    }
						}
					}
				}
				that.premium_date = false;
			}
        };
        
    };

    profileCtrl.$inject = injectParams;
    app.controller('profileCtrl', profileCtrl);

});