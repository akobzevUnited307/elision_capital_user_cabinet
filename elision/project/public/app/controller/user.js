'use strict';

define(['app', 'service/player', 'service/utils'], function(app) {

    var injectParams = ['$scope', 'utilsSrvc','userSrvc', '$state', '$timeout'];
    var userCtrl = function ($scope,  utilsSrvc, userSrvc, $state, $timeout) {

        $scope.BTC_SATOCHI = 100000000;
        $scope.pages.setURL($state.current.name);
        $scope.app = getApp();
        $scope.MIN_WITHDRAWAL_BTC_SATOSHI = 1000000;
        $scope.MIN_WITHDRAWAL_USDT = getApp().constant.MIN_WITHDRAWAL_USDT;

        // View
        $scope.view = utilsSrvc.my.view.load();
        
        // Profile
        $scope.profile = userSrvc.profile.load();

        // News
        // $scope.news = newsSrvc.news.loadList();

        // getRefURL
        $scope.getRefURL = function() {
            // return window.location.origin + '/r/' + getApp().user.user_login + '/';
            return getApp().domains.web + '/r/' + getApp().user.user_login + '/';
        };

        // hello ...
        $scope.hello = {
            loading: false,
            loading_activate_demo: false,
            candidat: {
                loading: false,
                loading_becandidat: false,
                result: null,
                form: {
                    accept: false,
                },
                check: function(send_msg_if_not) {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('app/user/checktelegrampartnerrequirements/', {send_msg_if_not: send_msg_if_not}, function(result) {
                        that.result = result.data;
                        if(!that.result.ok) {
                            $timeout(function() {
                                that.check(false);
                            }, 1000);
                        }
                    }, null, null, function(){
                        that.loading = false;
                    });
                },
                beCandidat: function() {
                    var that = this;
                    that.loading_becandidat = true;
                    // that.loading_activate_demo = true;
                    utilsSrvc.api.call('app/user/becandidat/', {}, function(result) {
                        $scope.hello.load();
                        getApp().changeProfile(result.data.user);
                    }, null, null, function(){
                        that.loading_becandidat = false;
                    });
                }
            },
            requirements: {
                visible: false,
                ok: false,
                user_status_id: null,
                i_can_testing: null,
                list: {
                    is_min_amount: true,
                    is_visit_event: false,
                    is_get_training: false,
                    is_get_testing: false,
                },
                show: function() {
                    this.visible = true;
                }
            },
            showVisitEventDetails: function() {
                return CustomUI.showForm('form-visit_event-details');
            },
            showTrainingDetails: function() {
                return CustomUI.showForm('form-training-details');
            },
            showTestingDetails: function() {
                return CustomUI.showForm('form-testing-details');
            },
            showBePartner: function() {
                return CustomUI.showForm('form-bepartner');
            },
            /*bePartner: function() {
                var that = this;
                that.loading_activate_demo = true;
                utilsSrvc.api.call('/app/buy/bepartner/', {}, function(result) {
                    that.load();
                    getApp().changeProfile(result.data.user);
                    CustomUI.hideForm();
                }, null, null, function(){
                    that.loading_activate_demo = false;
                });
            },*/
            openTesting: function() {
                var that = this;
                return CustomUI.showForm('form-testing', true, true, {
                    onclose: function(){
                        that.load();
                    }
                });
            },
            load: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/user/loadgoldendatxinfo/', {}, function(result) {
                    that.requirements.list = result.data.requirements.list;
                    that.requirements.i_can_testing = result.data.requirements.i_can_testing;
                    that.requirements.min_amount_usd = result.data.requirements.min_amount_usd;
                    that.requirements.amount_usd = result.data.requirements.amount_usd;
                    that.requirements.ok = result.data.requirements.ok;
                    that.requirements.user_status_id = result.data.requirements.user_status_id;
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

        // Страница кошелька
        $scope.walletpage = {
            loading: false,
            data: {},
            callback_list: [],
            is_closed: false,
            in_portfolios: '',
            opened_title: 'Закрытые счета',
            list: {
                main: [],
                invest: [],
                partner: [],
            },
            operations: {
                total: 0,
                visible_count: 0,
                showMore: function() {
                    this.visible_count += 50;
                    if(this.visible_count > this.total) {
                        this.visible_count = this.total;
                    }
                }
            },
            init: function() {
                this.load();
            },
            deleteETROrder: function(item) {
                var that = this;
                utilsSrvc.api.call('app/wallet/deleteetrorder/', {amount: item.amount}, function(result) {
                    // that.data = result.data;
                    // $scope.buy.init();
                    that.load();
                }, null, null, function(){
                    that.loading = false;
                });
            },
            load: function(callback) {
                var that = this;
                that.loading = true;
                if (typeof callback === 'function') {
                    that.callback_list.push(callback);
                }

                utilsSrvc.api.call('app/wallet/loadelisionwallets', {}, function(result) {
                    that.elision_list = result.data;
                }, null, null, function(){
                    // that.loading = false;
                });

                utilsSrvc.api.call('app/user/loadwalletpagedata', {dax_wallet: {operations: true}}, function(result) {
                    that.data = result.data;
                    if(!that.data) {
                        return;
                    }
                    that.list.main = [];
                    that.list.invest = [];
                    that.list.partner = [];
                    that.list.etr_order = that.data.dax_wallets_etr_order;
                    that.data.is_accepted = 1;
                    for(var w of that.data.dax_wallets) {
                        if (w.dax_wallet_type_id == 2 || w.dax_wallet_type_id == 24) {
                            if (!w.is_accepted) {
                                that.data.is_accepted = 0;
                            }
                        }
                        w.percent = 0;
                        for(var i = w.operations.length - 2; i >= 0; i--) {
                            var item = w.operations[i];
                            if (item.dax_wallet_operation_type_id == 28 ) {
                                w.percent += item.amount / (item.balance - item.amount) * 100;
                            }
                        }
                        if(w.dax_wallet_type_id == 1) {
                            that.list.main.push(w);
                        } else if(w.dax_wallet_type_id == 2) {
                            that.list.invest.push(w);
                        } else if(w.dax_wallet_type_id == 3) {
                            that.list.partner.push(w);
                        }
                    }
                    that.operations.total = that.data.user_operation_list.length;
                    that.operations.visible_count = 0;
                    that.operations.showMore();
                    if(that.callback_list.length > 0) {
                        for(var cb of that.callback_list) {
                            cb(that.list.main);
                            cb(that.list.invest);
                            cb(that.list.partner);
                        }
                        that.callback_list = [];
                    }
                   //  $scope.walletView.init();
                }, null, null, function(){
                    that.loading = false;
                    that.calculateTotalAmount();
                });
            },
            calculateTotalAmount: function (sec = 1000) {
                setTimeout(function () {
                    var that = this;
                    let base_balance = $('.base-company-balance .balance');
                    let total_amount = base_balance.data('total_amount');
                    let balance = base_balance.data('balance');
                    let total_amount_in_portfolios = total_amount - balance;
                    if (total_amount > 0) {
                       //  that.in_portfolios = total_amount_in_portfolios;
                        $('.base-in-portfolios .balance').html(myCurrencyType(total_amount_in_portfolios, 2) + ' $');
                    }
                }, sec);
                //  wallet.list.main.forEach( item => item.amount ) / $scope.BTC_SATOCHI
            },
            view_opened: function () {
                var that = this;
                that.is_closed = !that.is_closed;
                if (!that.is_closed) {
                    $("#opened-wallet").addClass("active");
                    $("#closed-wallet").removeClass("active");
                } else {
                    $("#opened-wallet").removeClass("active");
                    $("#closed-wallet").addClass("active");
                }
                that.opened_title = that.is_closed ? 'Открытые счета' : 'Закрытые счета';
            }
        };

        function myCurrencyType(amount, fraction) {
            if (!isNaN(parseFloat(amount)) && isFinite(amount)) {
                amount = parseFloat(amount);
                if(isNaN(fraction)) {
                    fraction = 2;
                }
                if(fraction == 0) {
                    amount = Math.floor(amount);
                } else {
                    amount = Math.floor(amount * Math.pow(10, fraction)) / Math.pow(10, fraction);
                }
                return amount.toLocaleString('ru-RU');
            } else {
                return 0; // amount;
            }
        }

        if(getApp().is_logged) {
            // Draw charts
            var d = new Date();
            var form = {
                timeZoneOffset: d.getTimezoneOffset()
            }
/*            $.getJSON('app/currency/daxratemonth/', form, function(data) {
                if (data.length == 0) {
                    data = []
                }
                $scope.chart_data = data.years;
                $scope.last_year_percent = data.last_year_percent;
                $scope.last_month_percent = data.last_month_percent;
                $scope.year_before_last_year_percent = data.year_before_last_year_percent;

            });*/

            var d = new Date();
            var form = {
                timeZoneOffset: d.getTimezoneOffset()
            }

            $.getJSON('app/currency/daxrate/', form, function(data) {
                // Create the chart
                var chart = Highcharts.stockChart('container', {
                    chart: {
                        height: 400
                    },
                    title: {
                        text: getApp().lang.chart_dax_price
                    },
                    subtitle: {
                        text: '' // '1 DAX = 1 USD'
                    },
                    rangeSelector: {
                        inputEnabled: false,
                        selected: 1
                    },
                    series: [{
                        name: getApp().lang.token_price,
                        data: data,
                        type: 'area',
                        threshold: null,
                        tooltip: {
                            valueDecimals: 2
                        }
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                chart: {
                                    height: 300
                                },
                                subtitle: {
                                    text: null
                                },
                                navigator: {
                                    enabled: false
                                }
                            }
                        }]
                    }
                });
            });

            // Tools
            $scope.tools = {
                loading:        false,
                cat:            null,
                years:          null,
                tool:           null,
                total_sum_usd:  0,
                chart_data:     [],
                table_data:     [],
                list:           [],
                data_years:     [],
                clean_capital_growth_usd_total: '...',
                openTool: function (tool) {
                    // var url = '/admin/fund/category/' + tool.fund_category_id + '/tool/' + tool.id + '/transaction/';
                    // location.href = url;
                },
                loadPerc: function(year) {
                    $.getJSON('app/currency/daxratemonth/', form, function(data) {
                        if (data.length == 0) {
                            data = []
                        }
                        data.years.forEach(function (item) {
                            if (item.year != year) {
                                return;
                            }
                            Highcharts.chart('container_per_month' + item.year, {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: ''
                                },
                                plotOptions: {
                                    series: {
                                        allowPointSelect: true,
                                        stacking: 'normal'
                                    }
                                },
                                xAxis: {
                                    categories: item.axis,
                                    labels: {
                                        rotation: -90,
                                        style: {
                                            fontSize: '13px',
                                            fontFamily: 'Verdana, sans-serif'
                                        }
                                    },
                                    formatter: function () {
                                        if (this.value) {
                                            return '<span style="fill: red;">' + this.value + '</span>';
                                        } else {
                                            return this.value;
                                        }
                                    }
                                },
                                yAxis: {
                                    title: false
                                },
                                credits: {
                                    enabled: true
                                },
                                series: [{
                                    data: item.series.data,
                                    color: '#929bcc',
                                    name: 'Процент доходности',
                                    stack: 'total',
                                    states: {
                                        hover: {
                                            color: '#253899',
                                            borderColor: 'gray'
                                        }
                                    }
                                }
                                ]
                            });
                        });
                    });
                },
                loadArchive: function(callback) {
                    this.load(null, {is_archive: 1});
                },
                filterData: function(dat, year) {
                    dat.filter(function() {
                        return this.year === year;
                    })
                },
                load: function(callback, filter) {
                    if(!filter) {
                        filter = {is_archive: 0};
                    }
                    var that = this;
                    this.loading = true;
                    utilsSrvc.api.call('app/fund/loadtools', {filter: filter}, function (resp) {
                        that.loading = false;

                        utilsSrvc.api.call('app/user/getmain/', {}, function(result) {
                            getApp().changeVerify(result.data.is_verificated);
                            getApp().changeAvatar(result.data.avatar_url);
                        });

                        $('.chartone').html(JSON.stringify(
                            resp.data.tools.map(function(item){
                                return { name: item.title, percent: item.amount_usd_percent + '%', data: [item.amount_usd_percent] }
                            }))
                        );

                        updateChartOne();

                        // $('.chartone').html('some data ');

                        // $scope.app.charts(1, data,  "doughnut");

                        //$scope.app.charts(1, data,  "doughnut");

                        //$scope.app.charts(1, data,  "doughnut");


                        that.list = resp.data.tools;
                        that.total_sum_usd = 0;
                        for (var cat of that.list) {
                            that.total_sum_usd += cat.amount_usd;
                        }
                        that.total_sum_usd = Math.round(that.total_sum_usd * 100) / 100;
                        if (callback instanceof Function) {
                            callback();
                        }
                    });

                    // Draw charts
                    var d = new Date();
                    var form = {
                        timeZoneOffset: d.getTimezoneOffset()
                    }
                    utilsSrvc.api.call('app/currency/daxratemonth/?timeZoneOffset=-180', form, function(data) {
                        if (data.length == 0) {
                            data = []
                        }
                        $scope.chart_data = data.years;
                        $scope.last_year_percent = data.last_year_percent;
                        $scope.last_month_percent = data.last_month_percent;
                        $scope.year_before_last_year_percent = data.year_before_last_year_percent;

                        $('.charthree').html(JSON.stringify(
                            data.years.map(function(item){
                                return { month: item.axis, data: item.series.data };
                            })
                        ));
                        updateChartThree();

                    });

                    // init.createCharts('maybe A');
                },
                loadTool: function () {
                    if(!getApp().var.page_capitalization_enable) {
                        return false;
                    }
                    var that = this;
                    this.load(function () {
                        for(var cat of that.list) {
                            for(var tool of cat.list) {
                                if (tool.id == $state.params.id) {
                                    that.tool = tool;
                                    that.cat = cat;
                                    var d = new Date();
                                    var form = {
                                        timeZoneOffset: d.getTimezoneOffset()
                                    };
                                    $.getJSON('/app/currency/toolcapitalgrowthsummary/', {
                                        tool_id: that.tool.id,
                                        real_data: 0,
                                        timeZoneOffset: d.getTimezoneOffset(),
                                        limit: -1,
                                        stock: 0,
                                        absolute_values: 1
                                    }, function (data) {
                                        $scope.$apply(function () {
                                            that.chart_data = data;
                                            // Create the chart
                                            var chart = Highcharts.stockChart('chart1', {
                                                chart: {
                                                    height: 400
                                                },
                                                title: {
                                                    text: getApp().lang.profit_text5 // Доходность команды за весь период
                                                },
                                                subtitle: {
                                                    text: '' // '1 DAX = 1 USD'
                                                },
                                                rangeSelector: {
                                                    inputEnabled: false,
                                                    selected: 1
                                                },
                                                series: [{
                                                    name: getApp().lang.profit_text6, // Доходность, %
                                                    data: data,
                                                    type: 'area',
                                                    threshold: null,
                                                    tooltip: {
                                                        valueDecimals: 2
                                                    },
                                                    color: '#919bcc'
                                                }],
                                                responsive: {
                                                    rules: [{
                                                        condition: {
                                                            maxWidth: 500
                                                        },
                                                        chartOptions: {
                                                            chart: {
                                                                height: 300
                                                            },
                                                            subtitle: {
                                                                text: null
                                                            },
                                                            navigator: {
                                                                enabled: false
                                                            }
                                                        }
                                                    }]
                                                }
                                            });
                                            $('#small').click(function () {
                                                chart.setSize(400);
                                            });
                                            $('#large').click(function () {
                                                chart.setSize(800);
                                            });
                                            $('#auto').click(function () {
                                                chart.setSize(null);
                                            });
                                        });
                                    });
                                    $.getJSON('/app/currency/toolcapitalgrowthsummary/', {
                                        tool_id: that.tool.id,
                                        real_data: 1,
                                        limit: -1
                                    }, function (data) {
                                        $scope.$apply(function () {
                                            that.clean_capital_growth_usd_total = 0;
                                            for (var row of data) {
                                                that.clean_capital_growth_usd_total += row.clean_capital_growth_usd;
                                            }
                                            that.table_data = data.reverse();
                                        });
                                    });
                                    break;
                                }
                            }
                        }
                    }, {});
                },
                calcToolCountInCat: function(cat) {
                    var resp = 0;
                    for (var tool of cat.list) {
                        if (tool.amount_usd > 0) {
                            resp++;
                        }
                    }
                    return resp;
                },
                makeChart: function (tool) {
                    $.getJSON('app/currency/toolcapitalgrowthsummary/', {
                        timeZoneOffset: window.timeZoneOffset,
                        tool_id: tool.id,
                        absolute_values: 1
                    }, function (data) {
                        $scope.$apply(function () {
                            tool.summary_percent = data.length > 0 ? data[data.length - 1][1] : '0';
                        });
                        Highcharts.chart('chart_' + tool.id, {
                            chart: {type: 'spline'},
                            title: {text: ''},
                            subtitle: {text: ''},
                            yAxis: {
                                title: {
                                    text: ''
                                },
                                gridLineWidth: 0,
                                labels: {enabled: false}
                            },
                            xAxis: {labels: {enabled: false}},
                            legend: {
                                enabled: false,
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle'
                            },
                            credits: {enabled: false},
                            tooltip: {
                                useHTML: true,
                                formatter: function () {
                                    var title = '';
                                    var index = this.point.index;
                                    if (index > 0) {
                                        var prev = this.series.data[index - 1].y;
                                        var dt = new Date(this.point.x);
                                        dt = dt.toLocaleString('ru-RU', {
                                            year: 'numeric',
                                            month: 'numeric',
                                            day: 'numeric',
                                            hour: 'numeric',
                                            minute: 'numeric'
                                        });
                                        // title = Number.parseFloat(this.y - prev).toFixed(2) + '%';
                                        title = Number.parseFloat(this.y).toFixed(2) + '%';
                                        title = title + '<br><b>' + dt + '</b>';
                                    } else {
                                        title = '0%';
                                    }
                                    // console.log(this.point.x);
                                    // var prevPoint = this.point.x == 0 ? null : this.series.data[this.point.x - 1];
                                    return title;
                                }
                                // headerFormat: '<b>{point.x:%e. %b}</b><br>',
                                // pointFormat: '{point.y:.2f} %'
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        enabled: true
                                    }
                                },
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                    pointWidth: 35,
                                    color: '#d9cdc1'
                                },
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    }
                                }
                            },
                            series: [{
                                name: 'Значение',
                                data: data
                            }],
                        });
                    });
                },
                collapse: function (cat) {
                    cat.expanded = !cat.expanded;
                }
            };

            function authorizedcheck() {
                utilsSrvc.api.call('app/user/authorizedcheck', {}, function(){
                }, null, null, function(){
                });
            };
        }

        $scope.walletpage.load();
        $scope.tools.load();

        $scope.remanagement = {
            loading: false,
            confirm_flag: false,
            data: {
                
            },            
            load: function(){
                var that = this;
                that.loading = true;
                that.loading = false;
            },
            confirm: function() {
                that.confirm_flag = true;
            }
        };


        // Партнерский рублевой счёт
        $scope.partnerRub = {
            wallet: null,
            // внесение средств
            fill: {
                open: function() {
                    this.setPage(1);
                    return CustomUI.showForm('form-partnerRub-fill');
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                setPage: function(number) {
                    this.page = number;
                },
            },
            // Перевод другому
            transfer: {
                loading: false,
                min_usd: 1,
                show_telegram: false,
                show_password: false,
                telegram_exists: false,
                searchUser: null,
                form: {
                    wallet_number: '',
                    number: '',
                    password: '',
                    amount_dax: ''
                },
                open: function() {
                    this.setPage(1);
                    var that = this;
                    that.loading = true;
                    that.searchUser = null;
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    that.form.number = '';
                    that.form.comment = '';
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                        that.form.amount_dax = ''; //$scope.walletView.wallet.amount / $scope.BTC_SATOCHI;
                        that.form.wallet_number = $scope.wallet.item.number;
                        CustomUI.showForm('form-partnerRub-transfer');
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                telegramprepareconfirm: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {}, function(result) {
                        that.telegram_exists = false;
                        that.show_telegram = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                setPage: function(number) {
                    this.page = number;
                },
                getInUSD: function() {
                    var resp = this.form.amount_dax * $scope.view.info.dax_rate_current;
                    if(resp > this.min_usd - 1 && resp < this.min_usd) {
                        resp = this.min_usd - 1;
                    }
                    return resp;
                },
                numberChange: function() {
                    var that = this;
                    that.searchUser = null;
                    if (!that.form.number) {
                        return;
                    }

                    utilsSrvc.api.call('/app/qwertypay/getgduserbynumberloginphone/', { number: that.form.number }, function(result) {
                        that.searchUser = result.data;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                checkMin: function() {
                    return this.getInUSD() >= this.min_usd;
                },
                isOk: function() {
                    return this.checkMin() && (this.form.amount_dax <= $scope.wallet.item.amount / $scope.BTC_SATOCHI) && this.searchUser && (this.form.password || this.form.pin);
                },
                isOkTelegram: function() {
                    return this.checkMin() && (this.form.amount_dax <= $scope.wallet.item.amount / $scope.BTC_SATOCHI) && this.searchUser;
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    that.form.password = that.form.pin || that.form.password;
                    utilsSrvc.api.call('/app/user/createtransfer/', {
                        wallet_number: that.form.wallet_number,
                        to_user_id: that.searchUser.id,
                        password: that.form.password,
                        amount_dax: Math.round(that.form.amount_dax * $scope.BTC_SATOCHI),
                        comment: that.form.comment,
                        project_id: 2
                    }, function(result) {
                        gmaback.js.showMessage('Перевод успешно осуществлён', 4000);
                        CustomUI.hideForm();
                        $scope.wallet.data = null;
                        $scope.wallet.load();
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                }
            },
            // Перевод в Trustera
            partnerToMain: {
                loading: false,
                wallet: null,
                max_amount: 0,
                form: {
                    wallet_number: '',
                    amount_dax: ''
                },
                open: function () {
                    var that = this;
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                        that.setWallet();
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                telegramprepareconfirm: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = false;
                        that.show_telegram = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                setWallet: function() {
                    var w = $scope.wallet.item;
                    this.max_amount = w.unlocked_amount_dax / $scope.BTC_SATOCHI;
                    this.wallet = w;
                    this.form.wallet_number = w.number;
                    this.form.amount_dax = '';
                },
                allAmount: function() {
                    this.form.amount_dax = this.wallet.unlocked_amount_dax / $scope.BTC_SATOCHI;
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    that.form.password = that.form.pin || that.form.password;
                    utilsSrvc.api.call('/app/user/movepartnertomain/', {
                        wallet_number: that.form.wallet_number,
                        amount_dax: Math.round(that.form.amount_dax * $scope.BTC_SATOCHI),
                        password: that.form.password,
                        project_id: 2,
                    }, function(result) {
                        gmaback.js.showMessage(getApp().lang.paid_success, 4000);
                        CustomUI.hideForm();
                        $scope.wallet.data = null;
                        $scope.wallet.load();
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                }
            },
            // Вывод средств
            withdrawalBtc: {
                open: function() {
                    var that = this;
                    that.loading = false;
                    that.setPage(1);
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    that.form.dax_wallet_id = $scope.wallet.item.id;
                    CustomUI.showForm('form-partnerRub-withdrawalBtc');
                    if(!this.current) {
                        this.check();
                    }
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                    // that.confirm();
                    that.loading = false;
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                setPage: function(number) {
                    this.page = number;
                },
                max_dax:        0,
                rates:          [],
                currency:       {},
                info:           false,
                loading:        false,
                sent:           false,
                current:        false,
                enabled:        false,
                check_enabled:  true,
                form: {
                    amount: 0,
                    target_amount: 0, // сколько человек получит выбранной валюты
                    currency_id: 0,
                    dax_rate: 0,
                    withdrawal_wallet_id: '',
                    password: '',
                    dax_wallet_id: 0
                },
                init: function () {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/getmain/', {}, function(result) {
                        that.info = result.data;
                        that.form.dax_rate = that.info.dax_rate_current;
                        //$scope.buyincity.load();
                        that.sent = false;
                        that.max_dax = that.info.withdrawal_available / $scope.BTC_SATOCHI;
                        that.max_dax = Math.round(that.max_dax * 100) / 100;
                        that.form.amount = 0;
                        that.rates = [];
                        for (var cur of that.info.dax_rates) {
                            that.rates.push({
                                id: cur.id,
                                code: cur.code,
                                title: cur.title,
                                value: cur.rate // cur.amount / that.max_dax
                            });
                        }
                        that.setCurrency(4);
                        utilsSrvc.api.call('/app/user/getwithdrawalwalletlist/', {system_type_id: 2}, function(result) {
                            that.withdrawal_wallets = result.data;
                        }, null, null, function (err) {
                            console.log('err', err);
                        });
                        utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function(result) {
                            if (result.data.current && result.data.current.length) {
                                that.current = result.data.current[0];
                            } else {
                                that.current = result.data.current;
                            }
                            if (!that.current) {
                                if (result.data.enabled == 1) {
                                    that.enabled = true;
                                } else {
                                    that.check_enabled = false;
                                }
                                $timeout(function() {
                                    $('#withdrawal-wallet').select2({
                                        templateResult: that.formatSelection,
                                        templateSelection: that.formatSelection,
                                        minimumResultsForSearch: -1
                                    });
                                });
                            } else {
                                if(that.current.currency_id == 4){
                                    that.current.currency_short_title = 'USDT';
                                    that.current.explorerHref = 'https://www.blockchain.com/ru/btc/address/';
                                    that.current.wallet_type_number = $scope.wallet.item.number;
                                    that.current.main_sum = $scope.wallet.item.amount;
                                }
                            }
                        });
                    }, null, null, function (err) {
                        that.loading = false;
                        console.log('err', err);
                    });
                },
                setCurrency: function(currency_id) {
                    this.form.currency_id = currency_id;
                    for (var r of this.rates) {
                        if (r.id == currency_id) {
                            this.currency = r;
                        }
                    }
                    this.changeAmount();
                },
                addressIsCorrect: function () {
                    var valid = false;
                    if (this.form.currency_id == 4) {
                        // BTC
                        valid = (/^(1|3)[0-9a-zA-Z]{32,33}$/.test(this.form.address));
                        // valid = (/^(1)[0-9a-zA-Z]{32,33}$/.test(this.form.address));
                    } else if (this.form.currency_id == 5) {
                        // ETH
                        valid = (/^0x[0-9a-f]{40}$/.test(this.form.address) || /^0x?[0-9A-F]{40}$/.test(this.form.address));
                    }
                    return valid;
                },
                changeAmount: function () {
                    this.form.amount = (this.form.amount || '0').toString().replace(',', '.');
                    for (var r of this.rates) {
                        if (r.id == this.form.currency_id) {
                            this.form.target_amount = this.form.amount * r.value;
                        }
                    }
                },
                changeWallet: function () {
                    var that = this;
                    that.withdrawal_wallet = that.withdrawal_wallets.find(function(item) {
                        return item.id == that.form.withdrawal_wallet_id;
                    });
                },
                getRate: function() {
                    for (var r of this.rates) {
                        if (r.id == this.form.currency_id) {
                            return 1 / r.value;
                        }
                    }
                },
                confirm: function () {
                    var that = this;
                    //that.loading = true;
                    that.show_telegram = false;
                    that.show_password = false;
                    that.telegram_exists = false;
                    that.form.password = '';
                    that.form.pin = '';
                    CustomUI.showForm('form-withdrawal');
                    utilsSrvc.api.call('/app/user/prepareconfirm/', {project_id: 2}, function(result) {
                        that.telegram_exists = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                telegramprepareconfirm: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/telegramprepareconfirm/', {}, function(result) {
                        that.telegram_exists = false;
                        that.show_telegram = result.telegram_exists;
                        that.show_password = !result.telegram_exists;
                    }, null, null, function(err) {
                        that.loading = false;
                    });
                },
                submit: function () {
                    var that = this;
                    that.form.password = that.form.pin || that.form.password;
                    utilsSrvc.api.call('/app/user/withdrawal/', this.form, function (result) {
                        that.close();
                        CustomUI.showMessage('Заявка успешно зарегистрирована', getApp().lang.withdrawal_funds);
                        utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function(result) {
                            that.sent = false;
                            that.current = false;
                            that.enabled = false;
                            that.check_enabled = true;
                            that.init();
                        });
                    });
                },
                check: function () {
                    var that = this;
                    utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function (result) {
                        console.info('Withdrawal check result', result);
                        if (result.data.current && result.data.current.length) {
                            that.current = result.data.current[0];
                        } else {
                            that.current = result.data.current;
                        }
                        if (result.data.enabled == 1) {
                            that.enabled = true;
                        } else {
                            that.check_enabled = false;
                        }
                    });
                },
                // партнерская программа
                partner_program: {
                    loading: false,
                    start: function () {
                        // Пользователь запустил перерасчет партнерской программы
                        var that = this;
                        that.loading = true;
                        utilsSrvc.api.call('/app/user/withdrawalpartnercalc/', {}, function (result) {
                            that.loading = false;
                            $scope.withdrawal.enabled = true;
                        }, function (result) {
                            // error
                            that.loading = false;
                            gmaback.js.showError('Произошла ошибка: ' + result.message, 4000);
                        });
                    }
                },
                formatSelection: function (optionElement) {
                    try { JSON.parse(optionElement.text) }
                    catch (ex) {
                        var $state = $('<div style="padding-top: 20px;text-transform: uppercase;">' + optionElement.text + '</div>');
                        return $state;
                    }
                    var item = JSON.parse(optionElement.text);
                    var html = '';
                    if (item.icon) {
                        html = '<img src="' + item.icon + '" style="margin-top: 10px;margin-right: 15px;width: 40px;height: 40px;">';
                    }
                    var $state = $('<div style="display:flex;align-items: center;">' + html + '<div><div style="padding-top: 10px;text-transform: uppercase;">Мой кошелек ' + item.title + '</div><strong style="text-transform: none;">' + item.address + '</strong></div></div>');
                    return $state;
                }
            },
            // вывод
            withdrawal: {
                page: 1,
                loading: false,
                form: {
                    amount: '',
                    ip_title: '',
                    inn: '',
                    rs_number: '',
                },
                open: function() {
                    this.setPage(1);
                    return CustomUI.showForm('form-partnerRub-withdrawal');
                },
                close: function() {
                    return CustomUI.hideForm('');
                },
                reset: function() {
                    this.form.amount = '';
                    this.form.ip_title = '';
                    this.form.inn = '';
                    this.form.rs_number = '';
                },
                setPage: function(number) {
                    this.page = number;
                },
                isAllow: function() {
                    return $scope.partnerRub.wallet && $scope.partnerRub.wallet.amount_rub > 300000000000;
                },
                formFilled: function() {
                    return false;
                    /*
                    return  this.form.amount &&
                            this.form.ip_title &&
                            this.form.inn &&
                            this.form.rs_number;
                    */
                },
                submit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/wallet/withdrawalpartnerrub/', that.form, function(result) {
                        that.reset();
                        that.close();
                    }, null, null, function(){
                        that.loading = false;
                    });
                }
            }
        };

        // wallet
        $scope.wallet = {
            loading: false,
            operations_loading: false,
            data: null,
            slider: null,
            item: null, // текущий открытый кошелёк
            ref_history_list: null,
            op_more: {
                all: 0,
                visible: 0,
                reset: function(all) {
                    this.all = all;
                    this.visible = 0;
                    this.show();
                },
                enabled: function() {
                    return this.visible < this.all;
                },
                show: function() {
                    this.visible += 100;
                    if(this.visible > this.all) {
                        this.visible = this.all;
                    }
                }
            },
            open: function(){
                var that = this;
                if(this.data) {
                    for(var w of that.data.dax_wallets) {
                        if(w.number == $stateParams.id) {
                            that.item = w;
                            break;
                        }
                    }
                } else {
                    this.load(function(){
                        that.open();
                    });
                }
            },
            slider_callback: function (index) {
                var that = this;
                if (that.data && that.data.dax_wallets && that.data.dax_wallets.length > 0) {
                    that.item = that.data.dax_wallets[index];
                    that.operations_loading = true;
                    utilsSrvc.api.call('/app/user/loadwalletpagedata/', {dax_wallet: {number: that.item.number, operations: true}}, function(result) {
                        for(var w of result.data.dax_wallets) {
                            if(w.number == that.item.number) {
                                that.item.allOperations = w.operations;
                                that.item.operations = w.operations;
                                that.op_more.reset(that.item.allOperations.length);
                                $scope.operationsfilter.init();
                            }
                        }
                    }, null, null, function(){
                        that.operations_loading = false;
                    });
                }
            },
            load: function(callback){
                var that = this;
                console.log('wallets', that.data);
                if(that.data) {
                    $timeout(function(){
                        that.slider = multiItemSlider('.slider', that.slider_callback);
                    });
                    return;
                }
                that.loading = true;
                utilsSrvc.api.call('/app/user/loadwalletpagedata/', {}, function(result) {
                    if (result.data && result.data.dax_wallets && result.data.dax_wallets.length > 0) {
                        result.data.dax_wallets = result.data.dax_wallets.filter(function(item) {
                            if (item.dax_wallet_type_id == 1) {
                                that.main_number = item.number;
                            }
                            return item.dax_wallet_type_id == 3;
                        });
                    }
                    that.data = result.data;
                    that.slider_callback(0);

                    for(var w of that.data.dax_wallets) {
                        if(w.dax_wallet_type_id == 3) {
                            $scope.partnerRub.wallet = w;
                        }
                    }
                    // wallet.data.dax_wallets

                    utilsSrvc.api.call('/app/user/getRate', {}, function(result){
                        $scope.app.rate = result.data;
                    }, null, null, null);

                    $timeout(function(){
                        try {
                            that.slider = multiItemSlider('.slider', that.slider_callback);
                        } catch(e) {
                            // do nothing
                            console.error(e);
                        }
                    });
                    if (typeof callback === 'function') {
                        callback(that.data)
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },

            showMoreInfo: function (ref_history_list) {
                this.ref_history_list = ref_history_list;
                return CustomUI.showForm('form-more-by-ref');
            },
            showMoreInfoClose: function () {
                return CustomUI.hideForm('');
            }
        };


        // Страница выбранного одного кошелька
        $scope.walletView = {
            loading: false,
            wallet: null,
            debt: null,
            operations: {
                sz: 100,
                len: 0,
                visible_count: 0,
                reset: function() {
                    this.len = $scope.walletView.wallet.operations.length;
                    this.visible_count = this.sz;
                    if(this.visible_count > this.len) {
                        this.visible_count = this.len;
                    }
                },
                moreAvailable: function() {
                    return this.visible_count < this.len;
                },
                more: function() {
                    this.visible_count += this.sz;
                    if(this.visible_count > this.len) {
                        this.visible_count = this.len;
                    }
                }
            },
            init: function(load_debt) {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/user/loadwalletpagedata', {dax_wallet: {number: $stateParams.number, operations: true}, load_debt: load_debt}, function(result) {
                    that.debt = result.data.debt;
                    for(var w of result.data.dax_wallets) {
                        if(w.number == $stateParams.number) {
                            that.wallet = w;
                            $scope.wallet_setting.form.wallet_id = w.id;
                            $scope.wallet_setting.form.title_orig = w.title;
                            $scope.wallet_setting.form.title = w.title;
                            that.wallet.allOperations = that.wallet.operations;
                            that.percent = 0;
                            that.all_percent = 0;
                            for(var i = w.operations.length - 1; i >= 0; i--) {
                                var item = w.operations[i];
                                if (item.dax_wallet_operation_type_id == 28 ) {
                                    that.percent = Math.round(item.amount / (item.balance - item.amount) * 100 * 100) / 100;
                                    item.percent = that.percent;
                                    that.all_percent += that.percent;
                                } else {
                                    item.percent = 0;
                                }
                            }
                            utilsSrvc.api.call('/app/user/withdrawalcheck/', {}, function(result) {
                                that.loading = false;
                                if (result.data.current && result.data.current.length) {
                                    that.withdrawal = result.data.current[0];
                                } else {
                                    that.withdrawal = result.data.current;
                                }
                            });

                            $interval(function () {
                                var date1 = newDate();
                                var date2 = newDate('12-01-2020');
                                var diff = date2.getTime() - date1.getTime();
                                that.days = diff / (1000 * 3600 * 24) + 1;
                            }, 1000);

                            that.operations.reset();
                            $scope.investToMain.setWallet(w);
                            $scope.trustSettings.init();
                            that.loadoffer();
                        }
                    }
                    if(that.wallet) {
                        $scope.operationsfilter.init();
                        that.makeChart();
                    }
                }, null, null, function(){
                    that.loading = false;
                });
            },
            loadoffer: function () {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/common/loadoffer/', {dax_wallet_invest_type_id: that.wallet.dax_wallet_invest_type_id}, function(result) {
                    if ($state.current.name == 'navigation.wallet_view_offer' && that.wallet.is_offer && that.wallet.is_condition) {
                        $state.go('navigation.wallet_view', {number: that.wallet.number});
                        return;
                    }
                    that.offer_wallet_en = result.data.offer_wallet_en;
                    that.offer_wallet_ru = result.data.offer_wallet_ru;
                    that.condition_wallet_en = result.data.condition_wallet_en;
                    that.condition_wallet_ru = result.data.condition_wallet_ru;
                    $(document).ready(function(){
                        $('.tabs').tabs();
                    });
                }, null, null, function(err) {
                    that.loading = false;
                });
            },
            makeChart: function() {
                var that = this;
                that.seriesData = that.wallet.operations.filter(function (item) {
                    return item.dax_wallet_operation_type_id == 28;
                }).sort((a,b) => (a.dt > b.dt) ? 1 : ((b.dt > a.dt) ? -1 : 0));
                that.setSeriesData();

                $scope.chart = Highcharts.chart('chart-profit', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        series: {
                            allowPointSelect: true,
                            stacking: 'normal'
                        }
                    },
                    xAxis: {
                        categories: that.tmpIntervalCategories,
                        labels: {
                            autoRotation: [-90],
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Montserrat, sans-serif, Arial'
                            },
                            formatter: function () {
                                return this.value;
                            }
                        }
                    },
                    yAxis: {
                        title: false
                    },
                    tooltip: {
                        shared: true,
                        useHTML: true,
                        headerFormat: '',
                        pointFormatter: function () {
                            var percent = this.y.toFixed(2);
                            return '<div style="display:flex;text-transform: uppercase;align-items: center;font-weight: bold;font-family: Montserrat, sans-serif, Arial;"><div style="flex: 1;font-size:11px;"><div style="color:#384db9">' + this.category + '</div><div>Процент доходности</div></div><div style="font-size:30px;margin-left: 10px;">' + percent + '</div></div>';
                        },
                        valueDecimals: 2
                    },
                    credits: {
                        enabled: true
                    },
                    series: [{
                        data: that.tmpIntervalData,
                        color: '#929bcc',
                        name: 'Процент доходности',
                        stack: 'total',
                        showInLegend: false,
                        states: {
                            hover: {
                                color: '#253899',
                                borderColor: 'gray'
                            }
                        }
                    }
                    ]
                });
                if (that.wallet.dax_wallet_type_id != 2 ||
                    !that.wallet.operations.filter(function (item) {
                        return item.dax_wallet_operation_type_id == 28;
                    }).length) {
                    $('#chart-profit-container').remove();
                } else {
                    $('#chart-profit-container').css({"visibility": "visible"});
                }
            },
            setSeriesData: function(interval) {
                var that = this;
                var intervalData = [];
                that.seriesData.map(function(value, index, array) {
                    d = newDate(value['dt']);
                    d = (d.getFullYear()-1970)*12 + d.getMonth();
                    intervalData[d] = intervalData[d]||[];
                    intervalData[d].push(value);
                });
                that.tmpIntervalData = [];
                that.tmpIntervalCategories = [];
                intervalData.forEach(function(intervalItem) {
                    if (intervalItem.length) {
                        var dt = intervalItem[0].dt;
                        var percent = 0;
                        intervalItem.forEach(function (item) {
                            percent += item.percent;
                        });
                        that.tmpIntervalData.push(percent);
                        that.tmpIntervalCategories.push(newDate(dt).toLocaleString($scope.app.languages.current.code, {
                            year: 'numeric',
                            month: 'long'
                        }));
                    }
                });
            },
            getTrustDtUnlock: function () {
                var dt_unlock = newDate();
                dt_unlock = dt_unlock.setDate(dt_unlock.getDate() + 365);
                var dt = newDate(this.wallet.dax_wallet_trust_settings.capital_withdrawal_date);

                if(dt > dt_unlock) {
                    dt_unlock = dt;
                }
                return newDate(dt_unlock).toLocaleDateString();
            },
            confirmAccept: function() {
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('app/wallet/confirmaccept', {id: that.wallet.id}, function (resp) {
                    that.init();
                }, null, null, function () {
                    that.loading = false;
                });
            },
            initOffer: function() {
                var that = this;
                $timeout(function() {
                    $(window).scrollTop(0);
                }, 1000);
            },
            initBuy: function () {
                mercuryoWidget.run({
                    widgetId: '85251fd9-8602-452b-b90b-6e897ad2c129',
                    host: document.getElementById('mercuryo-widget'),
                    type: 'buy',
                    currencies: ['BTC'],
                    fixCurrency: true,
                    height: '1500px',
                    fiatCurrencyDefault: 'USD',
                    address: $scope.wallet.data.buy_wallet.address,
                    fiatAmount: 500,
                    hideAddress: true,
                    lang: $scope.app.languages.current.code
                });
            }
        }

        // transfer
        $scope.transfer = {
            loading: false,
            confirm_flag: false,
            data: {
                amount: '',
                comm: '',
            },
            load: function(){
                var that = this;
                that.loading = true;
                utilsSrvc.api.call('/app/user/paidtxn/', {}, function(result) {

                }, null, null, function(){
                    that.loading = false;
                });
            },
            confirm: function() {
                var that = this;
                if (that.data.amount > 0) {
                    that.confirm_flag = true;
                    that.data.comm = that.data.amount * 0.03;
                    that.data.full_amount = that.data.amount - that.data.comm;
                } else {
                    gmaback.js.showError('Некорректная сумма', 4000);
                }
            },
            submit: function() {
                $state.go('index.transfer', {id: 378});
            }
        };

        /**
         * operationsfilter
         */
        $scope.operationsfilter = {
            start_date: null,
            finish_date: null,
            dax_wallet_operation_type_id: [],
            dax_wallet_operation_types: [],
            init: function() {
                var that = this;
                $scope.wallet.item.operations.forEach(function(operation) {
                    if (!that.dax_wallet_operation_types.find(function(item) {
                        return operation.dax_wallet_operation_type_id == item.dax_wallet_operation_type_id;
                    })) {
                        that.dax_wallet_operation_types.push({
                            dax_wallet_operation_type_id: operation.dax_wallet_operation_type_id,
                            dax_wallet_operation_type_title: operation.dax_wallet_operation_type_title,
                        });
                    }
                });
                setTimeout(function() {
                    $('.myfiltercheckbox:checkbox').each(function () {
                        $(this).prop('checked', true);
                    });
                }, 1000);
                that.start_date = null;
                that.finish_date = null;
            },
            clear: function() {
                var that = this;
                $('.myfiltercheckbox:checkbox').each(function () {
                    $(this).prop('checked', true);
                });

                that.start_date = null;
                that.finish_date = null;
                that.filter();
            },
            changeDate: function() {
                var that = this;
                if (that.start_date && that.finish_date &&
                    that.start_date.getTime() > that.finish_date.getTime()) {
                    that.start_date = that.finish_date;
                }
            },
            filter: function() {
                var that = this;

                that.dax_wallet_operation_type_id = [];
                /*$('.myfiltercheckbox:checkbox:checked').each(function () {
                   if (this.checked && $(this).val()) {
                       that.dax_wallet_operation_type_id.push(parseInt($(this).val()));
                   }
                  });*/

                $scope.wallet.item.operations = $scope.wallet.item.allOperations;
                $scope.wallet.item.operations = $scope.wallet.item.operations.filter(function(item) {
                    if (that.start_date && that.start_date.getTime() >= newDate(item.dt).getTime()) {
                        return false;
                    }
                    if (that.finish_date && that.finish_date.getTime() <= newDate(item.dt).getTime()) {
                        return false;
                    }
                    if (that.dax_wallet_operation_type_id.length) {
                        if (that.dax_wallet_operation_type_id.indexOf(item.dax_wallet_operation_type_id) < 0) {
                            return false;
                        }
                    }

                    return true;
                });
                $scope.wallet.op_more.reset();
                CustomUI.hideForm();
            },
            open: function() {
                CustomUI.showForm('form-operationsfilter');
            }
        };

        //$scope.walletView.init();

    };

    userCtrl.$inject = injectParams;
    app.controller('userCtrl', userCtrl);

});