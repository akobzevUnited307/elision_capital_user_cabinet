'use strict';

define(['app'], function(app) {
    
    var injectParams = ['$scope', '$state', '$interval', 'utilsSrvc', '$stateParams', 'userSrvc', '$timeout', '$templateCache', '$sce', 'newsSrvc'];
    var foundationworkCtrl = function ($scope, $state, $interval, utilsSrvc, $stateParams, userSrvc, $timeout, $templateCache, $sce, newsSrvc) {
        
        // Constants
        $scope.lang = getApp().lang;
        $scope.app = getApp();
        $scope.BTC_SATOCHI = 100000000;
        $scope.date = new Date();
        $scope.profit_last_month = '...';

        // News
        $scope.news = newsSrvc.loadList();
        // Dax rate
        $scope.daxrate = {
            mode: 2,
            setMode: function (mode_id) {
                this.mode = mode_id;
            }
        }

        if(getApp().is_logged) {
            // Draw charts
            var d = new Date();
            var form = {
                timeZoneOffset: d.getTimezoneOffset()
            }
            $.getJSON('/app/currency/daxratemonth/', form, function(data) {
                 if (data.length == 0) {
                    data = []
                }
                $scope.chart_data = data.years;
                $scope.last_year_percent = data.last_year_percent;
                $scope.last_month_percent = data.last_month_percent;
                $scope.year_before_last_year_percent = data.year_before_last_year_percent;

                // data.years.forEach(function (item) {
                // 	// if (item.year != 2018) {
				// 	// 	return;
                // 	// }
				// 	Highcharts.chart('container_per_month' + item.year, {
	            //         chart: {
	            //             type: 'column'
	            //         },
	            //         title: {
	            //             text: ''
	            //         },
	            //         plotOptions: {
	            //             series: {      
	            //                 allowPointSelect: true,
	            //                 stacking: 'normal'
	            //             }
	            //         },
	            //         xAxis: {
	            //             categories: item.axis,
	            //             labels: {
	            //                 rotation: -90,
	            //                 style: {
	            //                     fontSize: '13px',
	            //                     fontFamily: 'Verdana, sans-serif'
	            //                 }
	            //             },
	            //             formatter: function () {
	            //                 if (this.value) {
	            //                     return '<span style="fill: red;">' + this.value + '</span>';
	            //                 } else {
	            //                     return this.value;
	            //                 }
	            //             }
	            //         },
	            //         yAxis: {
				// 		    title: false
				// 		},
	            //         credits: {
	            //             enabled: true
	            //         },
	            //         series: [{
	            //             data: item.series.data,
	            //             color: '#929bcc',
	            //             name: 'Процент доходности', 
	            //             stack: 'total',
	            //             states: { 
	            //                 hover: {
	            //                     color: '#253899',
	            //                     borderColor: 'gray'
	            //                 }
	            //             }
	            //             }
	            //         ]
	            //     });
                // });
                
            });
            var d = new Date();
            var form = {
                timeZoneOffset: d.getTimezoneOffset()
            }
            $.getJSON('/app/currency/daxrate/', form, function(data) {
                // Create the chart
                var chart = Highcharts.stockChart('container', {
                    chart: {
                        height: 400
                    },
                    title: {
                        text: getApp().lang.chart_dax_price
                    },
                    subtitle: {
                        text: '' // '1 DAX = 1 USD'
                    },
                    rangeSelector: {
                        inputEnabled: false,
                        selected: 1
                    },
                    series: [{
                        name: getApp().lang.token_price,
                        data: data,
                        type: 'area',
                        threshold: null,
                        tooltip: {
                            valueDecimals: 2
                        }
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                chart: {
                                    height: 300
                                },
                                subtitle: {
                                    text: null
                                },
                                navigator: {
                                    enabled: false
                                }
                            }
                        }]
                    }
                });
            });
            
            authorizedcheck();

            // Tools
            $scope.tools = {
                loading:        false,
                cat:            null,
                years:          null,
                tool:           null,
                total_sum_usd:  0,
                chart_data:     [],
                table_data:     [],
                list:           [],
                data_years:     [],
                clean_capital_growth_usd_total: '...',
                openTool: function (tool) {
                    // var url = '/admin/fund/category/' + tool.fund_category_id + '/tool/' + tool.id + '/transaction/';
                    // location.href = url;
                },
                loadPerc: function(year) {
                    $.getJSON('/app/currency/daxratemonth/', form, function(data) {
                        if (data.length == 0) {
                           data = []
                       }  
                    data.years.forEach(function (item) {
                        if (item.year != year) {
                        	return;
                        }
                        Highcharts.chart('container_per_month' + item.year, {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: ''
                            },
                            plotOptions: {
                                series: {      
                                    allowPointSelect: true,
                                    stacking: 'normal'
                                }
                            },
                            xAxis: {
                                categories: item.axis,
                                labels: {
                                    rotation: -90,
                                    style: {
                                        fontSize: '13px',
                                        fontFamily: 'Verdana, sans-serif'
                                    }
                                },
                                formatter: function () {
                                    if (this.value) {
                                        return '<span style="fill: red;">' + this.value + '</span>';
                                    } else {
                                        return this.value;
                                    }
                                }
                            },
                            yAxis: {
                                title: false
                            },
                            credits: {
                                enabled: true
                            },
                            series: [{
                                data: item.series.data,
                                color: '#929bcc',
                                name: 'Процент доходности', 
                                stack: 'total',
                                states: { 
                                    hover: {
                                        color: '#253899',
                                        borderColor: 'gray'
                                    }
                                }
                                }
                            ]
                        });
                    });
                    });
                },
                loadArchive: function(callback) {
                    this.load(null, {is_archive: 1});
                },
                filterData: function(dat, year) {
                    dat.filter(function() {
                        return this.year === year;
                    })
                },
                load: function(callback, filter) {
                    if(!filter) {
                        filter = {is_archive: 0};
                    }
                    if(!getApp().var.page_capitalization_enable) {
                        return false;
                    }
                    var that = this;
                    this.loading = true;
                    utilsSrvc.api.call('app/fund/loadtools', {filter: filter}, function (resp) {
                        that.loading = false;
                        
                        utilsSrvc.api.call('/app/user/getmain/', {}, function(result) {
                            getApp().changeVerify(result.data.is_verificated);
                            getApp().changeAvatar(result.data.avatar_url);
                        });
                        
                        if ($('#chart-pie').length ) {
                            // Create the chart
                            Highcharts.chart('chart-pie', {
                                chart: {
                                    type: 'pie'
                                },
                                colors: ["#f15c80", "#7cb5ec", "#90ed7d", "#f7a35c", "#8085e9", "#e4d354", "#91e8e1", "#2b908f", "#434348", "#f45b5b"],
                                title: {
                                    text: ''
                                },
                                plotOptions: {
                                    pie: {
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: false,
                                        },
                                        showInLegend: true
                                    }
                                },
                                legend: {
                                    align: 'left',
                                    layout: 'vertical',
                                    verticalAlign: 'middle',
                                    labelFormatter: function () {
                                        return this.title + ' - ' + this.y + '%';
                                    }
                                },
                                tooltip: {
                                    pointFormat: '<span style="color:{point.color}">{point.title}</span>: <b>{point.y:.2f}%</b><br/>'
                                },
                                series: [
                                    {
                                        name: ' ',
                                        colorByPoint: true,
                                        data: resp.data.tools.map(function(item){
                                            return { title: item.title, y: item.amount_usd_percent }
                                        })
                                    }
                                ]
                            });
                        }
                        
                        if ($('#chart-bar').length ) {
                            // Create the chart
                            Highcharts.chart('chart-bar', {
                                chart: {
                                    type: 'bar'
                                },
                                colors: ["#008000", "#ff9900", "#295cda", "#30b558", "#8085e9", "#e4d354", "#91e8e1", "#2b908f", "#434348", "#f45b5b"],
                                title: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: resp.data.tools.map(function(item){ 
                                        return item.title ;
                                    }),
                                    visible: false, 
                                    reversed: true           
                                },
                                yAxis: {
                                    min: 0,
                                    visible: false,
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal'
                                    }
                                },
                                legend: {
                                       reversed: false
                                },
                                tooltip: {
                                    pointFormat: '<span style="color:{point.color}">{point.title}</span>: <b>{point.y:.2f}%</b><br/>',
                                    enabled: false
                                },
                                series: resp.data.tools.map(function(item){
                                    return { name: item.title + ' ' + item.amount_usd_percent + '%', data: [item.amount_usd_percent] }
                                })
                            });
                        }
                        
                        that.list = resp.data.tools;
                        that.total_sum_usd = 0;
                        for (var cat of that.list) {
                            that.total_sum_usd += cat.amount_usd;
                        }
                        that.total_sum_usd = Math.round(that.total_sum_usd * 100) / 100;
                        if (callback instanceof Function) {
                            callback();
                        }
                    });
                },
                loadTool: function () {
                    if(!getApp().var.page_capitalization_enable) {
                        return false;
                    }
                    var that = this;
                    this.load(function () {
                        for(var cat of that.list) {
                            for(var tool of cat.list) {
                                if (tool.id == $state.params.id) {
                                    that.tool = tool;
                                    that.cat = cat;
                                    var d = new Date();
                                    var form = {
                                        timeZoneOffset: d.getTimezoneOffset()
                                    };
                                    $.getJSON('/app/currency/toolcapitalgrowthsummary/', {
                                        tool_id: that.tool.id,
                                        real_data: 0,
                                        timeZoneOffset: d.getTimezoneOffset(),
                                        limit: -1,
                                        stock: 0,
                                        absolute_values: 1
                                    }, function (data) {
                                        $scope.$apply(function () {
                                            that.chart_data = data;
                                            // Create the chart
                                            var chart = Highcharts.stockChart('chart1', {
                                                chart: {
                                                    height: 400
                                                },
                                                title: {
                                                    text: getApp().lang.profit_text5 // Доходность команды за весь период
                                                },
                                                subtitle: {
                                                    text: '' // '1 DAX = 1 USD'
                                                },
                                                rangeSelector: {
                                                    inputEnabled: false,
                                                    selected: 1
                                                },
                                                series: [{
                                                    name: getApp().lang.profit_text6, // Доходность, %
                                                    data: data,
                                                    type: 'area',
                                                    threshold: null,
                                                    tooltip: {
                                                        valueDecimals: 2
                                                    },
                                                    color: '#919bcc'
                                                }],
                                                responsive: {
                                                    rules: [{
                                                        condition: {
                                                            maxWidth: 500
                                                        },
                                                        chartOptions: {
                                                            chart: {
                                                                height: 300
                                                            },
                                                            subtitle: {
                                                                text: null
                                                            },
                                                            navigator: {
                                                                enabled: false
                                                            }
                                                        }
                                                    }]
                                                }
                                            });
                                            $('#small').click(function () {
                                                chart.setSize(400);
                                            });
                                            $('#large').click(function () {
                                                chart.setSize(800);
                                            });
                                            $('#auto').click(function () {
                                                chart.setSize(null);
                                            });
                                        });
                                    });
                                    $.getJSON('/app/currency/toolcapitalgrowthsummary/', {
                                        tool_id: that.tool.id,
                                        real_data: 1,
                                        limit: -1
                                    }, function (data) {
                                        $scope.$apply(function () {
                                            that.clean_capital_growth_usd_total = 0;
                                            for (var row of data) {
                                                that.clean_capital_growth_usd_total += row.clean_capital_growth_usd;
                                            }
                                            that.table_data = data.reverse();
                                        }); 
                                    });
                                    break;
                                }
                            }
                        }
                    }, {});
                },
                calcToolCountInCat: function(cat) {
                    var resp = 0;
                    for (var tool of cat.list) {
                        if (tool.amount_usd > 0) {
                            resp++;
                        }
                    }
                    return resp;
                },
                makeChart: function (tool) {
                    $.getJSON('/app/currency/toolcapitalgrowthsummary/', {
                        timeZoneOffset: window.timeZoneOffset,
                        tool_id: tool.id,
                        absolute_values: 1
                    }, function (data) {
                        $scope.$apply(function () {
                            tool.summary_percent = data.length > 0 ? data[data.length - 1][1] : '0';
                        });
                        Highcharts.chart('chart_' + tool.id, {
                            chart: {type: 'spline'},
                            title: {text: ''},
                            subtitle: {text: ''},
                            yAxis: {
                                title: {
                                    text: ''
                                },
                                gridLineWidth: 0,
                                labels: {enabled: false}
                            },
                            xAxis: {labels: {enabled: false}},
                            legend: {
                                enabled: false,
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle'
                            },
                            credits: {enabled: false},
                            tooltip: {
                                useHTML: true,
                                formatter: function () {
                                    var title = '';
                                    var index = this.point.index;
                                    if (index > 0) {
                                        var prev = this.series.data[index - 1].y;
                                        var dt = new Date(this.point.x);
                                        dt = dt.toLocaleString('ru-RU', {
                                            year: 'numeric',
                                            month: 'numeric',
                                            day: 'numeric',
                                            hour: 'numeric',
                                            minute: 'numeric'
                                        });
                                        // title = Number.parseFloat(this.y - prev).toFixed(2) + '%';
                                        title = Number.parseFloat(this.y).toFixed(2) + '%';
                                        title = title + '<br><b>' + dt + '</b>';
                                    } else {
                                        title = '0%';
                                    }
                                    // console.log(this.point.x);
                                    // var prevPoint = this.point.x == 0 ? null : this.series.data[this.point.x - 1];
                                    return title;
                                }
                                // headerFormat: '<b>{point.x:%e. %b}</b><br>',
                                // pointFormat: '{point.y:.2f} %'
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        enabled: true
                                    }
                                },
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                    pointWidth: 35,
                                    color: '#d9cdc1'
                                },
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    }
                                }
                            },
                            series: [{
                                name: 'Значение',
                                data: data
                            }],
                        });
                    });
                },
                collapse: function (cat) {
                    cat.expanded = !cat.expanded;
                }
            };
            
            function authorizedcheck() {
                utilsSrvc.api.call('app/user/authorizedcheck', {}, function(){
                }, null, null, function(){
                });
            };
        }

    };

    foundationworkCtrl.$inject = injectParams;
    app.controller('foundationworkCtrl', foundationworkCtrl);

});