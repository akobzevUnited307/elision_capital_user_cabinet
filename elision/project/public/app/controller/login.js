'use strict';

define([
        'app',
        'lang',
        // 'service/utils',
        'directive/myenter'
    ], function (app, lang) {

    var injectParams = ['$scope', '$stateParams', 'utilsSrvc', 'userSrvc', '$state', '$timeout'];

    var loginCtrl = function ($scope, $stateParams, utilsSrvc, userSrvc, $state, $timeout) {

        $scope.lang = lang.values;
        $scope.loading = false;
        $scope.show_telegram = false;
        $scope.show_password = false;
        $scope.show_sendtelegrampin = true;

        $scope.form = {
            auth_method: 'login',
            email: '',
            pin: '',
            password: '',
            project_id: 2
        };

        // Login
        $scope.login = function() {
            $scope.loading = true;
            utilsSrvc.api.call('/app/user/login/', this.form, function(result) {
                try {
                    getApp().loginCallback(result);
                    $scope.pages.setLogged(true);
                    utilsSrvc.my.view.load();
                    userSrvc.profile.load();
                    $scope.pages.gotoURL('index.user');
                } catch(e) {
                    gmaback.js.showError(e, 4000);
                }
            }, function(reason) {
                console.log('reason', reason);
                gmaback.js.showError(reason.message, 4000);
            }, null, function(){
                $scope.loading = false;
            });
        };

        // sendtelegrampin
        $scope.sendtelegrampin = function() {
            $scope.loading = true;
            this.form.telegram_bot_id = 8;
            utilsSrvc.api.call('/app/user/sendtelegrampin/', this.form, function(result) {                	
                $scope.show_sendtelegrampin = false;
                $scope.show_telegram = result.telegram_exists;
                $scope.show_password = !result.telegram_exists;
                $scope.in_system = result.in_system;
            }, null, null, function(){
                $scope.loading = false;
            });
        };             
        
        $scope.openTelegram = function() {
            var win = window.open('tg://resolve?domain=goldendaxbot', '_blank');
            win.focus();
            $scope.in_system = false;
            $scope.show_telegram = true;
            $scope.show_password = false
        };
        
        $scope.back = function() {                
            $scope.in_system = false;
            $scope.show_telegram =  false;
            $scope.show_password = false;
            $scope.show_sendtelegrampin = true;
            $scope.form = {
                auth_method: 'email',
                email: '',
                pin: '',
                phone: '',
                password: ''
            };
        }

        // loginbypin
        $scope.loginbypin = function() {
            $scope.loading = true;
            utilsSrvc.api.call('/app/user/loginbypin/', this.form, function(result) {
                try {
                    getApp().loginCallback(result);
                    $scope.pages.setLogged(true);
                    utilsSrvc.my.view.load();
                    userSrvc.profile.load();
                    $scope.pages.gotoURL('index.user');
                } catch(e) {
                    gmaback.js.showError(e, 4000);
                }
            }, function(reason) {
	            console.log('reason', reason);
	            gmaback.js.showError(reason.message, 4000);
            }, null, function(){
                $scope.loading = false;
            });
        };        

        $scope.reset = function() {
			if ($scope.show_telegram || $scope.show_password) {
				$scope.show_telegram =  false;
		        $scope.show_password = false;
		        $scope.show_sendtelegrampin = true;
			}
        };
        
        $scope.showMe = function(){
            $timeout(function(){
                CustomUI.showForm('form-login', true, true, {onclose: function(){
                    $scope.pages.gotoURL('index.splash');
                }});
            });
        }

    };

    loginCtrl.$inject = injectParams;
    app.controller('loginCtrl', loginCtrl);

});