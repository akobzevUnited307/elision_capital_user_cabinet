'use strict';

define([
    'app', 'lang', 'service/utils', 'footerCtrl'
], function (app, lang) {
    'use strict';

    var injectParams = ['$scope', '$state', 'utilsSrvc', '$timeout', '$location'];

    var ctrl = function($scope, $state, utilsSrvc, $timeout, $location) {

        utilsSrvc.switchTheme('default');

        $scope.lang = lang.values;
        $scope.app = getApp();

        $scope.$state = $state;
        $scope.conf = getApp();
        $scope.is_logged = getApp().isLogged();

        var sid = localStorage.getItem('session_id');

        $scope.goLogin = function(){
            return getApp().logout();
        };

        $scope.logout = function() {
            localStorage.setItem('session_id', '');
            location.href = 'login.html';
        };

    };

    ctrl.$inject = injectParams;
    app.controller('splashCtrl', ctrl);
    
});




