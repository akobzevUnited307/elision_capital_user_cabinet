'use strict';

define(['app', 'service/utils', 'filter/chat_message',  'service/user'], function(app) {

    var injectParams = ['$scope', 'playerSrvc', 'utilsSrvc', 'userSrvc', '$timeout', '$state', 'messengerSrvc', '$rootScope'];
    var referralCtrl = function ($scope, playerSrvc, utilsSrvc, userSrvc, $timeout, $state, messengerSrvc, $rootScope) {

        $scope.app = getApp();
        
		// Referal URL
        if($state.params.user_login) {
            localStorage.setItem('user_login', $state.params.user_login);
        }

        // Отправить заявку
        $scope.ref = {
            loading: false,
            form: {
                fio: '',
                phone: '',
                user_login: ''
            },
            reset: function() {
                this.form.fio = '';
                this.form.phone = '';
            },
            submit: function() {
                // Тут отправка заявки
                var that = this;
                that.loading = true;
                var user_login = localStorage.getItem('user_login')
                if(user_login) {
                    that.form.user_login = user_login;
                }
                utilsSrvc.api.call('/app/lead/goldendaxref/', that.form, function(result) {
                    that.reset();
                    that.is_sended = true;
                }, null, null, function(){
                    that.loading = false;
                });
            }
        };

    };

    referralCtrl.$inject = injectParams;
    app.controller('referralCtrl', referralCtrl);

});