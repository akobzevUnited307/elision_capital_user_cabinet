'use strict';

define(['app', 'service/utils', 'scripts/audiovisualizer'], function(app, utilsSrvc, audiovisualizer) {

	var injectParams = ['utilsSrvc', '$timeout'];
    
    var PlayerFileFormProcess = function(player, $timeout, playlist_id) {
        this.ID = Routines.guid();
        this.player = player;
        this.playlist_id = playlist_id;
        this.$timeout = $timeout;
        this.fileList = [];
        this.formList = [];
        this.sent = false;
    };

    PlayerFileFormProcess.prototype.appendFiles = function(files) {
        var process = this;
        process.fileList = files;
        process.formList = [];
        var index = 0;
        for (var file of process.fileList) {
            var item = {
                loading: true,
                file: file,
                index: index++,
                form: new FormData(),
                request: new XMLHttpRequest(),
                bytes_total: 0,
                bytes_loaded : 0,
                bytes_percent : 0,
                processing: function () {
                    var that = this;
                    this.form.append('playlist_id', process.playlist_id);
                    this.form.append('file', that.file);
                    this.form.append('use_proxy', isDesktop() ? 0 : 1);
                    this.request.onreadystatechange = function () {
                        if (that.request.readyState == 4 && that.request.status == 200) {
                            var data = angular.fromJson(that.request.responseText);
                            console.log(data);
                            if (typeof data.error != 'undefined') {
                                process.$timeout(function(){ that.loading = false; });
                                gmaback.js.showError(data.error.message, 4000);
                            } else if (typeof data.status != 'undefined') {
                                if (data.status == 'success') {
                                    // console.log('loading = false', that.file.file_name);
                                    that.loading = false;
                                    process.$timeout(function(){
                                        for(var item of data.data.list) {
                                            var track = new PlayerTrack(item);
                                            track.visible = true;
                                            track.removed = false;
                                            process.player.obj.add(track);
                                        }
                                    });
                                } else {
                                    process.$timeout(function(){ that.loading = false; });
                                    gmaback.js.showError(data.message, 4000);
                                }
                            } else {
                                process.$timeout(function(){ that.loading = false; });
                                gmaback.js.showError(data.result, 4000);
                            }
                        }
                    };
                    this.request.open(
                        'POST',
                        getApp().domains.api + '/app/player/uploadfile/',
                        true
                    );
                    // this.request.setRequestHeader('X-Xsrftoken', getApp().XSRF);
                    this.request.setRequestHeader('xsrf', getApp().token_id);
                    this.request.setRequestHeader('X-Session-ID', getApp().session_id);
                    this.request.upload.onprogress  = function(evt) {
                        process.$timeout(function(){
                            console.log('loading evt', that.index, evt.loaded, evt.total);
                            that.bytes_total = evt.total;
                            that.bytes_loaded = evt.loaded;
                            that.bytes_percent = evt.total > 0 ? (evt.loaded / evt.total) * 100 : 100;
                        });
                    };
                    this.request.send(this.form);
                }
            };
            this.formList.push(item);
            item.processing();
        }
        return false;
    };

	var service = function(utilsSrvc, $timeout) {
        var playerSrvc = {
            state: 'stop',
            call: function(method, data, callback, callback_error, callback_progress, callback_finaly) {
                var url = '/app/player/' + method;
                utilsSrvc.api.call(url, data, callback, callback_error, callback_progress, callback_finaly);
            },
            play: {
            	inited: false,
                state: 'stop',
                // player: null,
                // title: null,
                // duration: null
            },
            toggle: function() {
                this.player.visible = !this.player.visible;
                this.player.audiovisualizer.visible = this.player.visible;
                if(!this.play.inited) {
                    this.play.inited = true;
                    this.loadPlayLists();
                }
            },
            show: function() {
                this.toggle();
            },
            // Load All playlists
            loadPlayLists: function() {
                this.player.playlists.load();
            },
            // Setup our new audio player class and pass it the playlist.
            player: {
                obj: false,
                state: 'new',
                search_string: '',
                start_volume: 1,
                loading: false,
                loading_radio_delete: false,
                active_index: -1,
                cached_html: '',
                visible: false,
                audiovisualizer: false,
                player_settings: {next_auto_play: true, repeat: true, playlist_mode: 1},
                setSettings: function(player_settings){
                    console.log('setPlayerSettings---');
                    this.player_settings = player_settings;
                },
                getCurrentTitle: function() {
                    if(this.obj && this.obj.current) {
                        return this.obj.current.item.title;
                    }
                    return '';
                },
                setVolume: function(vol) {
                    if(this.obj) {
                        this.obj.volume(vol);
                    } else {
                        this.start_volume = vol;
                    }
                },
                init: function(){
                    console.log('initPlayer---');
                    if(!this.obj) {
                        // Setup the audio animation
                        var options = {
                            container: '#waveform',
                            width: 960, // $('#waveform').outerWidth(),
                            height: 300
                            // height: window.innerHeight
                        };
                        var that = this;
                        this.audiovisualizer = audiovisualizer.init(options);
                        this.obj = new Player(this.player_settings);
                        this.obj.volume(that.start_volume);
                        $(this.obj).on('stateChange', function(e){
                            if(that.obj.state == 'play') {
                                // $('#player-object .controls').css({'background-image': 'url(' + that.obj.current.item.logo + ')'});
                                that.audiovisualizer.start(that.obj);
                            } else if(that.obj.state == 'stop') {
                                that.audiovisualizer.stop(that.obj);
                            }
                            $timeout(function(){
                                that.state = that.obj.state;
                                playerSrvc.state = that.obj.state;
                            });
                        });
                        $(this.obj).on('settingsChanged', function(e){
                            var settings = that.obj.getSettings();
                            playerSrvc.call('savesettings', {settings: settings}, function(resp){});
                        });
                        this.obj.init();
                    }
                    this.drop.init();
                    return this;
                },
                playlists: {
                    list: [],
                    current: false,
                    loading: false,
                    loading_current: false,
                    load: function() {
                        var that = this;
                        that.loading = true;
                        playerSrvc.call('getplaylists', {}, function(result) {
                            playerSrvc.player.filter.setDictionaries(result.data.dictionaries);
                            that.list = [];
                            for(var item of result.data.list) {
                                item.active = false;
                                that.list.push(item);
                            }
                            if(that.list.length > 0) {
                                that.select(that.list[0]);
                            }
                        }, null, null, function(){
                            that.loading = false;
                        });
                    },
                    select: function(playlist) {
                        var that = this;
                        if(this.current) {
                            if(this.current.id == playlist.id) {
                                return false;
                            }
                            this.current.active = false;
                        }
                        playlist.active = true;
                        that.current = playlist;
                        that.loading_current = true;
                        playerSrvc.call('getplaylist', {id: playlist.id, use_proxy: isDesktop() ? 0 : 1}, function(result){
                            if(that.current && that.current.id == playlist.id) {
                                playerSrvc.player.obj.clear();
                                for(var item of result.data.list) {
                                    var track = new PlayerTrack(item);
                                    track.visible = true;
                                    track.removed = false;
                                    playerSrvc.player.obj.add(track);
                                }
                                playerSrvc.player.find();
                            }
                        }, null, null, function(){
                            that.loading_current = false;
                        });
                    },
                    create: {
                        loading: false,
                        form: {
                            title: ''
                        },
                        show: function() {
                            this.form.title = '';
                            return CustomUI.showForm('form-player-pl-create');
                        },
                        submit: function(){
                            var that = this;
                            that.loading = true;
                            playerSrvc.call('createplaylist', this.form, function(result){
                                var item = result.data.new_item;
                                playerSrvc.player.playlists.list.push(item);
                                playerSrvc.player.playlists.select(item);
                                CustomUI.hideForm();
                            }, null, null, function(){
                                that.loading = false;
                            });
                        }
                    }
                },
                removeRadio: function(track) {
                    var that = this;
                    that.loading_radio_delete = true;
                    playerSrvc.call('removetrack', {playlist_id: that.playlists.current.id, radio_id_list: [track.item.id]}, function(result){
                        track.removed = true;
                    }, null, null, function(){
                        that.loading_radio_delete = false;
                    });
                },
                click: function(e) {
                    if(e.offsetY < 0) {
                        playerSrvc.toggle();
                    }
                },
                filter: {
                    form: {
                        genre: false,
                        country: false,
                    },
                    dic: {
                        genre: [],
                        country: [],
                        bitrate: [],
                    },
                    setDictionaries: function(dic) {
                        this.dic = dic;
                    },
                    setGenre: function(item) {
                        if(this.form.genre) {
                            this.form.genre.active = false;
                        }
                        if(item) {
                            item.active = true;
                        }
                        this.form.genre = item;
                    },
                    setCountry: function(item) {
                        if(this.form.country) {
                            this.form.country.active = false;
                        }
                        if(item) {
                            item.active = true;
                        }
                        this.form.country = item;
                    }
                },
                getRadioPlaylist: function(){
                    var resp = [];
                    for(var index in this.obj.playlist) {
                        var item = this.obj.playlist[index];
                        item.index = index;
                        var ok = item.visible && !item.removed;
                        if(ok) {
                            if(this.filter.form.genre !== false) {
                                if(this.filter.form.genre.id > 0) {
                                    if(!item.info || item.info.genre_id_list.indexOf(this.filter.form.genre.id) < 0) {
                                        ok = false;
                                    }
                                }
                            }
                            if(ok) {
                                if(this.filter.form.country !== false) {
                                    if(this.filter.form.country.id > 0) {
                                        if(!item.info || (item.info.country_id != this.filter.form.country.id)) {
                                            ok = false;
                                        }
                                    }
                                }
                                if(ok) {
                                    resp.push(item);
                                }
                            }
                        }
                    }
                    return resp;
                },
                seek: function(event) {
                    this.obj.seek(event.clientX / (window.innerWidth * (gmaback.index_scope.mobileview ? 1 : .5)));
                },
                volumeSliderMove: function(event){
                    if (window.sliderDown) {
                        event = window.event;
                        var x = event.clientX || event.touches[0].clientX;
                        var startX = window.innerWidth * 0.05;
                        var layerX = x - startX;
                        var per = Math.min(1, Math.max(0, layerX / parseFloat(barEmpty.scrollWidth)));
                        this.obj.volume(per);
                    }
                },
                volumeSliderTouch: function(){
                    window.sliderDown = true;
                },
                volumeSliderEnd: function(){
                    console.log('volumeSliderEnd');
                    window.sliderDown = false;
                },
                togglePlaylist: function(event) {
                    // $scope.show_playlist = !$scope.show_playlist;
                },
                hidePlaylist: function(){
                    // $scope.show_playlist = false;
                },
                find: function() {
                    var re = this.search_string.toLowerCase().replace(new RegExp(' ', 'g'), '.*');
                    for(var song of this.obj.playlist) {
                        song.visible = song.item.title.toLowerCase().replace(/<\/?[^>]+(>|$)/g, '').match(re);
                    }
                },
                recordVideo: {
                    record_in_process: false,
                    context: false,
                    gainNode: false,
                    stop: function() {
                        this.record_in_process = false;
                    },
                    start: function(audioStream) {
                        var that = this;
                        that.record_in_process = true;
                        that.context = new AudioContext();
                        that.gainNode = that.context.createGain();
                        that.gainNode.connect(that.context.destination);
                        that.gainNode.gain.value = 0; // don't play for self
                        var f = function(audioStream) {
                            var canvas = document.querySelector('#waveform canvas');
                            var canvasStream = canvas.captureStream(60);
                            var finalStream = new MediaStream();
                            audioStream.getAudioTracks().forEach(function(track) {
                                console.log('track', track);
                                finalStream.addTrack(track);
                            });
                            canvasStream.getVideoTracks().forEach(function(track) {
                                finalStream.addTrack(track);
                            });
                            var recorder = RecordRTC(finalStream, {
                                type: 'video',
                                // mimeType: 'video/webm;codecs=vp8', // webm
                                // mimeType: 'video/webm;codecs=vp9,vp8', // webm vp8/vp9
                                mimeType: 'video/webm;codecs=h264', // mp4
                                // mimeType: 'video/x-matroska;codecs=avc1', // mkv
                                // video: { width: 960, height: 462 },
                                bitsPerSecond: 12800000,
                                // frameRate: 60,
                                // frameInterval: 1,
                                quality: 100
                            });
                            recorder.startRecording();
                            (function looper() {
                                if(!that.record_in_process) {
                                    recorder.stopRecording(function() {
                                        var blob = recorder.getBlob();
                                        // document.body.innerHTML = '<video controls src="' + URL.createObjectURL(blob) + '" autoplay loop></video>';
                                        audioStream.stop();
                                        canvasStream.stop();
                                        var a = document.createElement("a");
                                        document.body.appendChild(a);
                                        a.style = "display: none";
                                        var url = window.URL.createObjectURL(blob);
                                        a.href = url;
                                        a.download = 'myvideo.webm';
                                        a.click();
                                        window.URL.revokeObjectURL(url);
                                    });
                                    return;
                                }
                                setTimeout(looper, 100);
                            })();
                        };
                        if(audioStream) {
                            f(audioStream);
                        } else {
                            that.getMp3Stream(f);
                        }
                    },
                    getMp3Stream: function(callback) {
                        var that = this;
                        var selector = new FileSelector();
                        selector.accept = '*.mp3';
                        selector.selectSingleFile(function(file) {
                            if(!file || !file.size) {
                                alert('Please try again by selecting a valid mp3 file.');
                                return;
                            }
                            var reader = new FileReader();
                            reader.file = file;
                            reader.onload = (function(e) {
                                // Import callback function
                                // provides PCM audio data decoded as an audio buffer
                                that.context.decodeAudioData(e.target.result, function(buffer) {
                                    that.createSoundSource(buffer, callback);
                                });
                            });
                            reader.readAsArrayBuffer(reader.file);
                        });
                    },
                    createSoundSource: function(buffer, callback) {
                        var that = this;
                        var soundSource = that.context.createBufferSource();
                        soundSource.loop = true;
                        soundSource.buffer = buffer;
                        soundSource.start(0, 0 / 1000);
                        soundSource.connect(that.gainNode);
                        var destination = that.context.createMediaStreamDestination();
                        soundSource.connect(destination);
                        // durtion=second*1000 (milliseconds)
                        callback(destination.stream);
                    }
                },
                upload: {
                    show: function() {
                        $('#upload_audiofiles').click();
                    },
                    onchange: function(input) {
                        if(!input.files || !input.files[0]) {
                            return false;
                        }
                        var fd = new FormData();
                        for(var file of input.files) {
                            fd.append('file', file);
                        }
                        $('#upload_audiofiles').val('');
                        var proc = new PlayerFileFormProcess(playerSrvc.player, $timeout, playerSrvc.player.playlists.current.id);
                        proc.appendFiles(fd.getAll('file'));
                        $timeout(function(){
                            playerSrvc.player.drop.ProcessList.push(proc);
                        });
                        // return this.ProcessFiles(fd.getAll('file'));
                    }
                },
                add: {
                    radio_list: [],
                    loading: false,
                    loading_add: false,
                    filter: {
                        form: {
                            search_string: '',
                            genre: false,
                            country: false,
                            reset: function() {
                                this.search_string = '';
                                this.genre = false;
                                this.country = false;
                            }
                        },
                        setGenre: function(item) {
                            if(this.form.genre) {
                                this.form.genre.active = false;
                            }
                            if(item) {
                                item.active = true;
                            }
                            this.form.genre = item;
                        },
                        setCountry: function(item) {
                            if(this.form.country) {
                                this.form.country.active = false;
                            }
                            if(item) {
                                item.active = true;
                            }
                            this.form.country = item;
                        }
                    },
                    show: function(){
                        var that = this;
                        CustomUI.showForm('form-player-add');
                        if(that.radio_list.length == 0) {
                            that.loading = true;
                            playerSrvc.call('getcatalog', {use_proxy: isDesktop() ? 0 : 1}, function(result){
                                that.radio_list = [];
                                for(var item of result.data.radio_list) {
                                    item.visible = true;
                                    that.radio_list.push(item);
                                }
                            }, null, null, function(){
                                that.loading = false;
                            });
                        }
                    },
                    addRadio: function(item) {
                        var that = this;
                        that.loading_add = true;
                        playerSrvc.call('addradio', {playlist_id: playerSrvc.player.playlists.current.id, use_proxy: isDesktop() ? 0 : 1, radio_id_list: [item.id]}, function(result){
                            for(var item of result.data.list) {
                                var track = new PlayerTrack(item);
                                track.visible = true;
                                track.removed = false;
                                // playerSrvc.player.obj.addToBegin(track);
                                playerSrvc.player.obj.add(track);
                            }
                            CustomUI.hideForm();
                            that.filter.form.reset();
                        }, null, null, function(){
                            that.loading_add = false;
                        });
                    },
                    find: function() {
                        console.log('find');
                        var re = this.filter.form.search_string.toLowerCase().replace(new RegExp(' ', 'g'), '.*');
                        for(var song of this.radio_list) {
                            song.visible = song.title.toLowerCase().replace(/<\/?[^>]+(>|$)/g, '').match(re);
                        }
                    },
                    getRadioPlaylist: function(){
                        var resp = [];
                        for(var index in this.radio_list) {
                            var item = this.radio_list[index];
                            item.index = index;
                            var ok = item.visible;
                            if(ok) {
                                if(this.filter.form.genre !== false) {
                                    if(this.filter.form.genre.id > 0) {
                                        if(!item.info || item.info.genre_id_list.indexOf(this.filter.form.genre.id) < 0) {
                                            ok = false;
                                        }
                                    }
                                }
                                if(ok) {
                                    if(this.filter.form.country !== false) {
                                        if(this.filter.form.country.id > 0) {
                                            if(!item.info || (item.info.country_id != this.filter.form.country.id)) {
                                                ok = false;
                                            }
                                        }
                                    }
                                }
                                if(ok) {
                                    resp.push(item);
                                }
                            }
                        }
                        return resp;
                    }
                },
                /**
                * Drag&Drops
                **/
                drop: {
                    id: 'player-object',
                    chatRight: false,
                    ProcessList: [],
                    getUploadFiles: function() {
                        var resp = [];
                        for(var proc of this.ProcessList) {
                            for(var file of proc.formList) {
                                if(file.loading) {
                                    resp.push(file);
                                }
                            }
                        }
                        return resp;
                    },
                    // FileDragHover ...
                    FileDragHover: function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        var $target = $(e.target);
                        if ($target.attr('id') != this.id) {
                            $target = $target.closest('#' + this.id);
                        }
                        if (e.type == 'dragover') {
                            var dt = e.dataTransfer;
                            if (dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('Files'))) {
                                // $("#dropzone").show();
                                // window.clearTimeout(dragTimer);
                            } else {
                                e.preventDefault();
                                // Set the dropEffect to move
                                e.dataTransfer.dropEffect = 'none';
                                return false;
                            }
                            $target.addClass('hover');
                            if (window.removeHoverTimer != null) {
                                clearTimeout(window.removeHoverTimer);
                            }
                        } else {
                            // RemoveCoinListHover ...
                            window.removeHoverTimer = setTimeout(function () {
                                console.log('remove');
                                $target.removeClass('hover');
                            }, 25);
                        }
                    },
                    // FileSelectHandler ...
                    FileSelectHandler: function (e) {
                        this.FileDragHover(e);
                        this.DropEvent(e);
                    },
                    DropEvent: function (e) {
                        // process all File objects
                        var files = e.target.files || e.dataTransfer.files;
                        if (files.length > 0) {
                            console.log('drop file', files);
                            var proc = new PlayerFileFormProcess(playerSrvc.player, $timeout, playerSrvc.player.playlists.current.id);
                            proc.appendFiles(files);
                            $timeout(function(){
                                playerSrvc.player.drop.ProcessList.push(proc);
                            });
                        }
                    },
                    init: function() {
                        'use strict'
                        // Disabled drag & drop on entire window
                        $(document.body).bind('dragover', function (e) {
                            e.preventDefault();
                            e.originalEvent.dataTransfer.effectAllowed = 'none';
                            e.originalEvent.dataTransfer.dropEffect = 'none';
                            return false;
                        });
                        $(document.body).unbind('drop').bind('drop', function (e) {
                            e.preventDefault();
                            return false;
                        });
                        // Chat Drop files
                        this.chatRight = document.getElementById(this.id);
                        var that = this;
                        // file drop
                        this.chatRight.addEventListener('dragover', function (e) {
                            return that.FileDragHover(e);
                        }, false);
                        this.chatRight.addEventListener('dragleave', function (e) {
                            return that.FileDragHover(e);
                        }, false);
                        this.chatRight.addEventListener('drop', function (e) {
                            return that.FileSelectHandler(e);
                        }, false);
                    }
                }
            }
        };
        return playerSrvc;
    };
    service.$inject = injectParams;
    app.factory('playerSrvc', service);
});