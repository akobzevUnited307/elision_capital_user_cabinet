'use strict';

define(['app', 'service/utils'], function (app) {

    // Сервис (посредник, чтобы из директивы вызвать обработчик в контроллере)
    var service = function (utilsSrvc) {
        return {
            elements: null,
            index: null,
            inline: {
                $container: false,
                elements: [],
                index: 0,
                init: function($container, scope) {
                    this.$container = $container;
                    var that = this;
                    that.index = 0;
                    that.elements = [];
                    $container.find('[data-filesource]').each(function (i) {
                        var path = $(this).data('filesource');
                        $(this).data('imageview_index', i);
                        var item = {
                            path: path,
                            item: $(this)
                        };
                        that.elements.push(item);
                    });
                    return that.elements;
                },
                move: function(add) {
                    this.show(this.index += add)
                },
                show: function(add) {
                    this.index = add;
                    var cnt = this.elements.length;
                    if(this.index < 0) {
                        this.index = cnt - 1;
                    }
                    if(this.index >= cnt) {
                        this.index = 0;
                    }
                    for(var i in this.elements) {
                        var item = this.elements[i].item;
                        if(i != this.index) {
                            item.removeClass('active');
                        } else {
                            item.addClass('active');
                        }
                    }
                    return this.index;
                }
            },
            reset: function () {
                this.elements = null;
                this.index = null;
            },
            show: function (elements, index) {
                this.elements = elements;
                this.index = index;
            }
        };
    };
    service.$inject = ['utilsSrvc'];
    app.factory('imageviewSrvc', [service]);

    // Контроллер
    var imageviewCtrl = function ($scope, utilsSrvc, imageviewSrvc) {

        $scope.state = {
            items: null,
            count: 0,
            active_item: null,
            index: null
        };

        $scope.reset = function(){
            imageviewSrvc.reset();
        };

        $scope.$watch(function () {
            return imageviewSrvc.elements;
        }, function (newVal, oldVal) {
            if (newVal) {
                $scope.state.items = newVal;
                $scope.state.count = newVal.length;
                $scope.view(imageviewSrvc.index);
                $('#form-imageview #form-imageview-img').hide();
                CustomUI.showForm('form-imageview', true, false, {
                    onclose: function() {
                        $scope.reset();
                    }
                });
            }
        });
        $scope.imgLoaded = function() {
            var e = window.event || $scope._evt;
            var $el = $(e.target);
            $el.fadeIn(100);
        };
        $scope.imgClick = function() {
            var e = window.event || $scope._evt;
            var $el = $(e.target);
            $el.closest('.view').toggleClass('active');
        };
        $scope.close = function () {
            CustomUI.hideForm();
        };
        $scope.go = function (m) {
            this.view($scope.state.index + m);
        };
        $scope.view = function (index) {
            var prev_index = $scope.state.index;
            if ($scope.state.items.length < 1) {
                return;
            }
            if (index < 0) {
                index = $scope.state.items.length - 1;
            }
            if (index + 1 > $scope.state.items.length) {
                index = 0;
            }
            $scope.state.index = index;
            // иначе не произойдет onload
            if (prev_index != index) {
                $('#form-imageview img').hide();
            }
            $scope.state.active_item = $scope.state.items[index];
        };
        $scope.onPan = function (event) {
            // console.log(event.type);
            /*if(event.type === 'panup') {
                    if(event.deltaY < -200) {
                        this.close();
                    } else {
                        $('#form-imageview').css('-webkit-transform', 'translateY(' + (event.deltaY) + 'px)');
                    }
                }*/
            /*if(event.type == 'panleft') {
                $('#form-imageview img').css('-webkit-transform', 'translate(' + (-event.distance) + 'px, -50%)');
            } else if(event.type == 'panup') {
                $('#form-imageview img').css('-webkit-transform', 'translate(-50%, -50%)');
            }*/
            //console.log(event.type);
        };
        $scope.onSwipe = function (event) {
            // console.log(event.event);
            if (event.type === 'swipeleft') {
                this.go(1);
            } else if (event.type === 'swiperight') {
                this.go(-1);
            }
        };
    };
    imageviewCtrl.$inject = ['$scope', 'utilsSrvc', 'imageviewSrvc'];
    app.controller('imageviewCtrl', imageviewCtrl);

    // Директива
    app.directive('imageView', ['$timeout', 'imageviewSrvc', function($timeout, imageviewSrvc) {
        return {
            restrict : 'A',
            transclude: false,
            link: function(scope, element, attrs) {
                $timeout(function(){
                    imageviewSrvc.reset();
                    scope.dots = {
                        index: 0,
                        list: [],
                        moveTo: function(index) {
                            this.index = imageviewSrvc.inline.show(index);
                        }
                    };
                    var elements = imageviewSrvc.inline.init($(element), scope);
                    scope.dots.list = elements;
                    $(element).on('click', '.arrow-left,.arrow-right', function () {
                        var add = $(this).hasClass('arrow-left') ? -1 : 1;
                        imageviewSrvc.inline.move(add);
                        scope.$apply(function () {
                            scope.dots.index = imageviewSrvc.inline.index;
                        });
                    });
                    $(element).on('click', '[data-filesource]', function () {
                        var active_path = $(this).data('filesource');
                        var elements = [];
                        var index = 0;
                        $(element).find('[data-filesource]').each(function (i) {
                            var path = $(this).data('filesource');
                            if (active_path == path) {
                                index = i;
                            }
                            elements.push({
                                path: path,
                                item: $(this)
                            });
                        });
                        scope.$apply(function () {
                            $('#form-imageview').css('-webkit-transform', 'translateY(0px)');
                            imageviewSrvc.show(elements, index);
                        });
                        return false;
                    });
                });
            }
        };
    }]);

});