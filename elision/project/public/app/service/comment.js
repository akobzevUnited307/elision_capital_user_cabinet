'use strict';

define(['app', 'service/utils', 'gmaback'], function (app) {

    app.directive('commentstree', function ($compile) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                family: '=',
                islogged: '=',
                reply: '='
            },
            template: '<div class="comments_list_tree comment_item" ng-repeat="item in family" data-comment-id="{{item.id}}">' +
                '<div class="animated zoomIn">' +
                '<div class="blog-comment-text">' +
                '<div>' +
                '<a ng-href="{{item.link}}">' +
                '<span class="fio chip" ng-if="!item.organization_title"><img ng-src="{{item.ava}}">{{item.fio}}</span>' +
                '<span class="fio chip" ng-if="item.organization_title"><img ng-src="{{item.ava}}">{{item.organization_title}}</span>' +
                '</a>' +
                '</div>' +
                '{{item.comment}}' +
                '</div>' +
                '<div class="reply_box hide-on-small-and-down">' +
                '<span class="date grey-text left">{{item.dt | datetime}}</span>' +
                '<a ng-if="islogged" class="reply grey-text" data-id="{{item.id}}" ng-click="reply($event)" href="#">' + getApp().lang.reply + '</a> ' +
                '<span class="like" show-like-users entity_name="comment" entity_id="{{item.id}}" count_like="{{item.count_like}}"><span class="count_like">{{item.count_like}}</span> <span ' +
                '    class="fa"' +
                '    ng-class="{\'likes-blog\' : islogged, \'fa-thumbs-o-up\': (item.i_like == 0), \'fa-thumbs-up\': (item.i_like == 1), \'disabled\': (item.i_dislike == 1)}" ' +
                '    entity_id="{{item.id}}" ' +
                '    entity_name="comment">&nbsp;</span></span>' +
                '<span class="dislike red-text"><span class="count_like">{{item.count_dislike}}</span> <span ' +
                '    class="fa"' +
                '    ng-class="{\'dislikes-blog\' : islogged, \'fa-thumbs-o-down\': (item.i_dislike == 0), \'fa-thumbs-down\': (item.i_dislike == 1), \'disabled\': (item.i_like == 1)}" ' +
                '    entity_id="{{item.id}}" ' +
                '    entity_name="comment">&nbsp;</span></span>' +
                '</div>' +
                '<commentstree family="item.children" islogged="islogged" reply="reply"></commentstree>' +
                '</div>' +
                '</div>',
            compile: function (tElement, tAttr, transclude) {
                var contents = tElement.contents().remove();
                var compiledContents;
                return function (scope, iElement, iAttr) {
                    if (!compiledContents) {
                        compiledContents = $compile(contents, transclude);
                    }
                    compiledContents(scope, function (clone, scope) {
                        iElement.append(clone);
                    });
                };
            }
        };
    });

    var injectParams = ['utilsSrvc'];
    var service = function (utilsSrvc) {
        return {
            call: function (method, data, callback) {
                var url = '/app/comment/' + method;
                utilsSrvc.api.call(url, data, callback);
            }
        };
    };
    service.$inject = injectParams;
    app.factory('commentSrvc', ['utilsSrvc', service]);

});