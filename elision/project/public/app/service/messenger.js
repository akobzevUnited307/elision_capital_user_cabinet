'use strict';

define(['app', 'service/utils', 'gmaback'], function(app) {
	var injectParams = ['utilsSrvc'];
	var service = function(utilsSrvc) {
        var resp = {
            contacts: [],
            callbacks: {},
            chat: {
                url: '',
                messages: []
            },
            missedMessages: 0,
            needToScroll: false,
            call: function(method, data, callback, callback_error) {
                callback_error = callback_error || null;
                var url = '/app/messenger/' + method;
                utilsSrvc.api.call(url, data, callback, callback_error);
            },
            get: function() {
                return this;
            },
            connect: function() {
                gmaback.enter(null);
                return this;
            },
            enterToChat: function(channel_url) {
                this.chat.messages = [];
                this.chat.url = channel_url;
                gmaback.send(gmaback.options.event.command_connect_channel, {channel_url: channel_url});
            },
            onEvent: function(event, data) {
                var that = this;
                if(gmaback.messenger_scope) {
                    gmaback.messenger_scope.$apply(function(){
                        if(event == gmaback.options.event.command_channels) {
                            that.contacts = data;
                        } else if(event == gmaback.options.event.command_msg_in) {
                            console.log('messages', data);
                            that.needToScroll = false;
                            for(var msg of data) {
                                var len = that.chat.messages.length;
                                if(len > 0) {
                                    var added = false;
                                    for(var index in that.chat.messages) {
                                        console.log(index);
                                        // console.log(len, len - index - 1, that.chat.messages);
                                        if(msg.id > that.chat.messages[len - index - 1].id) {
                                            that.chat.messages.splice(len - index, 0, msg);
                                            added = true;
                                            break;
                                        } else if(msg.id == that.chat.messages[len - index - 1].id) {
                                            added = true;
                                            break;
                                        }
                                    }
                                    if(!added) {
                                        that.chat.messages.unshift(msg);
                                    }
                                } else {
                                    that.chat.messages.push(msg);
                                }
                            }
                            that.needToScroll = true;
                            /*
                            var story = [];
                            if (that.chat.messages.length && data.length && data[data.length - 1].id < that.chat.messages[0].id) {
                                for(var msg of data) {
                                    if(msg.channel_url == that.chat.url && msg.id >= 0 && msg.user_id >= 0) {
                                        story.push(msg);
                                    }
                                }
                            } else {                                        
                                for(var msg of data) {
                                    if(msg.channel_url == that.chat.url && msg.id >= 0 && msg.user_id >= 0) {
                                        that.needToScroll = true;
                                        that.chat.messages.unshift(msg);
                                    }
                                }
                            }
                            if (story.length) {
                                that.chat.messages = story.concat(that.chat.messages);
                            }*/
                        } else if(event == gmaback.options.event.command_missed_update) {
                            for(var item of data) {
                                for(var contact of that.contacts) {
                                    if (contact.url == item.channel_url) {
                                        contact.missed_messages = item.count;
                                        break;
                                    }
                                }
                            }
                        }
                    });
                }
                if(gmaback.navigation_scope) {
                    gmaback.navigation_scope.$apply(function(){
                        if (event == gmaback.options.event.command_msg_in) {
                            for(var msg of data) {
                                if(msg.channel_url != that.chat.url) {
                                    that.missedMessages++;
                                    gmaback.js.showMessage('У Вас новое сообщение', 3000);
                                    var i = 0;
                                    var tmpContact = null;
                                    for(var contact of that.contacts) {
                                        if (contact.url == msg.channel_url) {
                                            contact.missed_messages++;
                                            i++;
                                            tmpContact = contact;
                                            break;
                                        }
                                    }
                                    if (tmpContact) {
                                        that.contacts.splice(i, 1);
                                        that.contacts.unshift(tmpContact);
                                    }
                                }
                            }
                        } else if(event == gmaback.options.event.command_missed_update) {
                            that.missedMessages = 0;
                            for(var item of data) {
                                that.missedMessages += item.count;
                            }
                        } else if(event == gmaback.options.event.command_channels) {
                            that.contacts = data;
                            that.missedMessages = 0;
                            for(var item of data) {
                                that.missedMessages += item.missed_messages;
                            }
                        }
                    });
                }
            },
	        sendMessage: function(message) {
                var that = this;
				gmaback.send(gmaback.options.event.command_msg_send, {channel_url: this.chat.url, message: message});
                var i = 0;
                var tmpContact = null;
                for(var contact of that.contacts) {
                    if (contact.url == this.chat.url) {
                        i++;
                        tmpContact = contact;
                        break;
                    }
                }
                if (tmpContact) {
                    that.contacts.splice(i, 1);
                    that.contacts.unshift(tmpContact);
                }
	        },
	        getPreviousMessages: function(channel_url, message_id) {
				gmaback.send(gmaback.options.event.command_msg_get_prev, {channel_url: channel_url, message_id: parseInt(message_id)});
	        }
	    };
        
        
        
        gmaback.messenger_service = resp;
        return resp;
    };
    service.$inject = injectParams;
    app.factory('messengerSrvc', ['utilsSrvc', service]);
});