'use strict';

define(['app', 'service/utils', 'gmaback'], function(app) {
	var injectParams = ['utilsSrvc'];
    // Plugins
	var service = function(utilsSrvc) {
        return {
            // Просмотр нестандартных типов дополнительного содержимого сообщения
            MessageDataViewer: {
                list: {},
                register: function(type, func) {
                    this.list[type] = func;
                },
                run: function(message) {
                    var type = message.data.type;
                    if(this.list.hasOwnProperty(type)) {
                        return this.list[type](message);
                    }
                }
            }
	    };
    };
    service.$inject = injectParams;
    app.factory('pluginsSrvc', ['utilsSrvc', service]);
});