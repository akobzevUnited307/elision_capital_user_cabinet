'use strict';

define(['app', 'service/utils', 'gmaback'], function(app) {
	var injectParams = ['utilsSrvc'];
	var service = function(utilsSrvc) {
        return {
            call: function(method, data, callback) {
                var url = '/app/settings/' + method;
                utilsSrvc.api.call(url, data, callback);
            }
	    };
    };
    service.$inject = injectParams;
    app.factory('settingsSrvc', ['utilsSrvc', service]);
});