'use strict';

/**
 * Утилиты
 */
define(['app', 'gmaback'], function (app) {
    var injectParams = ['AppConfig', '$http', '$q', '$timeout', '$location'];
    var service = function(AppConfig, $http, $q, $timeout, $location) {
        var utilsSrvc = {
            markdown: {
                // https://github.com/showdownjs/showdown/wiki/Showdown-options
                converter: null,
                init: function() {
                    if(this.converter) {
                        return true;
                    }
                    showdown.extension('htmlescape', function() {
                        return [{
                            type: 'lang',
                            filter: function(text, converter, options) {
                                var codeBlocks = []
                                function hashCodeBlock (code) {
                                    code = '~C' + (codeBlocks.push(code) - 1) + 'C'
                                    return code
                                }
                                text += '~0'
                                text = text.replace(/(^[ \t]*>([ \t]*>)*)(?=.*?$)/gm, function(wholeMatch) {
                                    wholeMatch = wholeMatch.replace(/>/g, '~Q')
                                    return wholeMatch
                                });
                                if (options.ghCodeBlocks) {
                                    text = text.replace(/(^|\n)(```(.*)\n([\s\S]*?)\n```)/g,
                                    function (wholeMatch, m1, m2) {
                                        return m1 + hashCodeBlock(m2)
                                    }
                                    )
                                }
                                text = text.replace(
                                    /((?:(?:(?: |\t|~Q)*?~Q)?\n){2}|^(?:(?: |\t|~Q)*?~Q)?)((?:(?:(?: |\t|~Q)*?~Q)?(?:[ ]{4}|\t).*\n+)+)((?:(?: |\t|~Q)*?~Q)?\n*[ ]{0,3}(?![^ \t\n])|(?=(?:(?: |\t|~Q)*?~Q)?~0))/g,
                                    function (wholeMatch, m1, m2, m3) {
                                    return m1 + hashCodeBlock(m2) + m3
                                })
                                text = text.replace(/(^|[^\\])((`+)([^\r]*?[^`])\3)(?!`)/gm,
                                    function(wholeMatch) {
                                        return hashCodeBlock(wholeMatch)
                                    }
                                );
                                text = text.replace(/&/g, '&amp;')
                                text = text.replace(/</g, '&lt;')
                                text = text.replace(/>/g, '&gt;')
                                while (text.search(/~C(\d+)C/) >= 0) {
                                    var codeBlock = codeBlocks[RegExp.$1]
                                    codeBlock = codeBlock.replace(/\$/g, '$$$$') // Escape any dollar signs
                                    text = text.replace(/~C\d+C/, codeBlock)
                                }
                                text = text.replace(/~Q/g, '>');
                                text = text.replace(/~0$/, '');
                                return text
                            }
                        }]
                    });
                    showdown.extension('codehighlight', function() {
                        function htmlunencode(text) {
                            return (
                                text
                                .replace(/&amp;/g, '&')
                                .replace(/&lt;/g, '<')
                                .replace(/&gt;/g, '>')
                            );
                        }
                        return [
                            {
                                type: 'output',
                                filter: function(text, converter, options) {
                                    // use new shodown's regexp engine to conditionally parse codeblocks
                                    var left  = '<pre><code\\b[^>]*>',
                                    right = '</code></pre>',
                                    flags = 'g',
                                    replacement = function(wholeMatch, match, left, right) {
                                        // unescape match to prevent double escaping
                                        match = htmlunencode(match);
                                        var cls = $(wholeMatch).find('code').attr('class');
                                        if(cls) {
                                            var lang = cls.split('-')[1];
                                            var lng = Prism.languages[lang];
                                            if(lng) {
                                                return left + Prism.highlight(match, lng) + right; //left + hljs.highlightAuto(match).value + right;
                                            }
                                        }
                                        return wholeMatch; // left + match + right;
                                    };
                                    return showdown.helper.replaceRecursiveRegExp(text, replacement, left, right, flags);
                                }
                            }
                        ];
                    });
                    this.converter = new showdown.Converter({
                        /*
                            'omitExtraWLInCodeBlocks': true,
                            'noHeaderId': false,
                            'parseImgDimensions': true,
                            'simplifiedAutoLink': true,
                            'literalMidWordUnderscores': true,
                            'strikethrough': true,
                            'tables': true,
                            'tablesHeaderId': false,
                            'ghCodeBlocks': true,
                            'tasklists': true,
                            'smoothLivePreview': true,
                            'prefixHeaderId': false,
                            'disableForced4SpacesIndentedSublists': false,
                            'ghCompatibleHeaderId': true,
                            'smartIndentationFix': false
                        */
                        extensions: ['codehighlight', 'htmlescape'],
                        excludeTrailingPunctuationFromURLs: true,
                        tasklists: true,
                        literalMidWordUnderscores: true,
                        tables: true,
                        strikethrough: true,
                        ghCodeBlocks: true,
                        openLinksInNewWindow: true,
                        simpleLineBreaks: true,
                        simplifiedAutoLink: true
                    });
                },
                makeHTML: function(inputMD) {
                    this.init();
                    var resultHTML = this.converter.makeHtml(inputMD);
                    return resultHTML;
                }
            },
            my: {
                profile: getApp().user,
                current_profile: getApp().profile,
                view: {
                    info: false,
                    loading: false,
                    rate: {
                        value: false,
                        viewInCurrency: function(currency) {
                            this.value = currency;
                        }
                    },
                    load: function () {
                        var that = this;
                        if(!that.loading && ['/', '/login/', '/registration/'].indexOf($location.$$url) == -1 && !that.info) {
                            that.loading = true;
                            utilsSrvc.api.call('/app/user/getmain/', {system_type_id: 2}, function (result) {
                                getApp().changeAvatar(result.data.avatar_url);
                                if (!result.data.country_id && gmaback.header_scope.setcountry) {
                                    gmaback.header_scope.setcountry.show();
                                }
                                that.info = result.data;
                                that.rate.viewInCurrency(that.info.dax_rates[0])
                            }, function (err) {
                                console.log('err', err);
                            }, null, function(){
                                that.loading = false;
                            });
                        }
                        return that;
                    },
                    getRegionID: function () {
                        return this.info ? this.info.region_id : false; // 5246
                    }
                }
            },
            switchTheme: function (code) {
                $('body').attr('data-theme', code);
                getApp().settings.values.theme = code;
                getApp().settings.save();
            },
            getSmiles: function() {
                var list = [];
                for (var code of ['acute', 'aggressive', 'air_kiss', 'angel', 'bad', 'bb', 'beach', 'beee', 'biggrin', 'big_boss', 'blum2', 'blush', 'boast', 'bomb', 'boredom', 'bye', 'censored', 'clapping', 'cray', 'crazy', 'curtsey', 'dance4', 'dash1', 'diablo', 'dirol', 'drinks', 'feminist', 'flirt', 'focus', 'fool', 'friends', 'gamer4', 'girl_cray2', 'girl_crazy', 'girl_drink4', 'girl_haha', 'girl_hospital', 'girl_impossible', 'girl_in_love', 'girl_pinkglasses', 'girl_sigh', 'give_heart2', 'give_rose', 'good', 'hang2', 'heart', 'help', 'hi', 'hunter', 'hysteric', 'i-m_so_happy', 'ireful1', 'king', 'kiss2', 'kiss3', 'lazy', 'lol', 'mail1', 'mamba', 'mega_shock', 'mocking', 'moil', 'music', 'nea', 'new_russian', 'ok', 'paint2', 'pardon', 'party2', 'pleasantry', 'popcorn1', 'prankster2', 'preved', 'punish', 'rofl', 'sad', 'sarcastic', 'scare', 'scratch_one-s_head', 'search', 'secret', 'shock', 'shout', 'slow', 'smile', 'smoke', 'sorry2', 'spiteful', 'spruce_up', 'stop', 'tease', 'tender', 'thank_you2', 'this', 'training1', 'unknown', 'vampire', 'vava', 'victory', 'wacko2', 'wink', 'wizard', 'yahoo', 'yes3', 'yess']) {
                    list.push({
                        code: code,
                        path: './assets/smiles/' + getApp().settings.values.smiles + '/dark/' + code + '.gif'
                    });
                }
                return {
                    dark: list
                };
            }, 
            career: {
                loading: false,
                current: {
                    dt_to: '2019-05-12'
                },
                load: function() {
                    var that = this;
                    if(!that.loading) {
                        that.loading = true;
                        that.buy.reset();
                        utilsSrvc.api.call('/app/buy/loadgoldendaxinfo', {}, function(result) {
                            that.current = result.data.current;
                            that.buy.plan_list = result.data.plan_list;
                            that.buy.user_main_amount = result.data.user_main_amount;
                        }, null, null, function () {
                            that.loading = false;
                        });
                    }
                    return that;
                },
                buy: {
                    price_list: [],
                    plan_list: [],
                    current_price: false,
                    show_confirm: false,
                    user_main_amount: 0,
                    loading: false,
                    wallet: false,
                    form: {
                        plan_id: false,
                        price_id: false
                    },
                    reset: function () {
                        // reset state
                        this.loading = false;
                        this.wallet = false;
                        this.show_confirm = false;
                        // reset form
                        this.form.plan_id = false;
                        this.form.price_id = false;
                    },
                    selectPlan: function (plan) {
                        this.price_list = plan.price_list;
                        this.form.plan_id = plan.id;
                    },
                    selectPrice: function (price) {
                        if (this.current_price) {
                            this.current_price.active = false;
                        }
                        price.active = true;
                        this.current_price = price;
                        this.form.price_id = price.id;
                        $('.career_month_list').find('input[type=radio]').each(function (index, element) {
                            $(element).prop("checked", false);
                        });                    
                        $('.career_month_list').find('input#' + price.id).each(function (index, element) {
                            $(element).prop("checked", true);
                        });  
                    },
                    rejectConfirm: function () {
                        this.show_confirm = false;
                    },
                    showConfirm: function () {
                        this.show_confirm = true;
                    },
                    getPrice: function () {
                        return this.current_price;
                    },
                    submit: function () {
                        var that = this;
                        that.loading = true;
                        utilsSrvc.api.call('/app/buy/buygoldendax', this.form, function (result) {
                            if (result.data.is_complete == 1) {
                                utilsSrvc.career.load();
                                that.reset();
                                CustomUI.hideForm();
                            } else {
                                that.wallet = result.data.wallet;
                            }
                        }, null, null, function () {
                            that.loading = false;
                        });
                    },
                    show: function () {
                        this.reset();
                        return CustomUI.showForm('form-buy-goldendax');
                        /*
                        if($scope.career.current) {
                            $scope.career.current = false;
                        } else {
                            $scope.career.current = {
                                dt_to: '2019-05-12'
                            };
                        }*/
                    }
                }
            },
            // шапка
            header: {
                load: function () {
                }
            },
            // события
            data: {
                process: function (data) {
                    console.log('ws.cmd_data', data.code, data.data.profile);
                    if(data.code == 'user/avatar' || data.code == 'user/profile') {
                        getApp().changeProfile(data.data.profile);
                    }
                }
            },
            // события
            events: {
                list: [],
                id_list: [],
                loading: false,
                loading_clear: false,
                chat_invite_queue: {
                    list: [],
                },
                init: function() {
                    return this;
                },
                organization_invite_queue: {
                    list: [],
                },
                // очередь приглашений в чат
                chat_invite_queue: {
                    list: [],
                    action: function(action_name, event) {
                        utilsSrvc.api.call('/app/messenger/inviteaction/', {action: action_name, event_id: event.id}, function(result){
                            utilsSrvc.events.removeById(event.id);
                            if(action_name == 'accept') {
                                // that.select(chat_id);
                                if(gmaback.messenger_scope) {
                                    gmaback.messenger_scope.channels.select(result.data.url);
                                }
                                // $scope.redirect('messenger.channel', {channelUrl: result.data.url});
                            }
                        });
                    }
                },
                // очередь приглашений в друзья
                friend_invite_queue: {
                    list: [],
                    action: function(action_name, event) {
                        utilsSrvc.api.call('/app/user/actioninvite/', {action: action_name, event_id: event.id}, function(result){
                            utilsSrvc.events.removeById(event.id);
                            if(action_name == 'accept') {
                                $scope.redirect('user_view', {code: result.data.user_login});
                            }
                        });
                    }
                },
                loadFullList: function () {
                    // console.log('Events.loadFullList');
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/events/loadlist/', {}, function(result) {
                        that.list = result.data;
                    }, null, null, function(){
                        that.loading = false;
                    });
                },
                show: function ($event) {
                    $('#events_button').removeClass('active');
                    $('#my_tab_profile').getScope().showMyForm($event, 'my_tab_events');
                },
                closePanel: function () {
                    return false;
                },
                //show: function($event){
                //    $('#modalCtrl').getScope().events.show($event);
                //},
                clear: function() {
                    var that = this;
                    that.loading_clear = true;
                    utilsSrvc.api.call('/app/user/clearevent/', {}, function (result) {
                        if (result.data.list && result.data.list.length > 0) {
                            result.data.list.forEach(function(event_id, index, object) {
                                that.removeById(event_id);
                            });
                        }
                    }, null, null, function(){
                        that.loading_clear = false;
                    });
                },
                gotochat: function(event) {
                    $scope.redirect('messenger', {channelUrl: event.data.chat.url});
                },
                remove: function(event) {
                    utilsSrvc.api.call('/app/events/markasread/', {event_id: event.id}, function(result){});
                    this.removeById(event.id);
                },
                removeById: function (event_id) {
                    this.list.forEach(function (event, index, object) {
                        if (event.id == event_id) {
                            object.splice(index, 1);
                        }
                    });
                    this.sort();
                },
                sort: function () {
                    function compare(a, b) {
                        if (a.id <= b.id) {
                            return 1;
                        }
                        if (a.id > b.id) {
                            return -1;
                        }
                        return 0;
                    }
                    this.list.sort(compare);
                },
                process: function (event_list) {
                    var chat_invite_queue_added = 0;
                    var chat_invite_queue_message = 0;
                    var has_new_invite = false;
                    var need_load_friends = false;
                    var count_profile = 0;
                    for(var event of event_list) {
                        if ($.inArray(event.id, utilsSrvc.events.id_list) < 0) {
                            if (event.event_category_message_id == 17) {
                                // Список друзей и приглашений в друзья обновлены
                                need_load_friends = true;
                                if (event.format_data) {
                                    var format_data = JSON.parse(event.format_data);
                                    if (format_data.deleted_event_id) {
                                        utilsSrvc.events.removeById(format_data.deleted_event_id);
                                    }
                                }
                            } else if (event.event_category_message_id == 37) {
                                // Баланс обновлен
                                var balance_info = JSON.parse(event.data);
                                utilsSrvc.my.current_profile.sum = balance_info['amount_title'];
                            } else {
                                event.data = event.data ? angular.fromJson(event.data) : null;
                                utilsSrvc.events.id_list.push(event.id);
                                utilsSrvc.events.list.push(event);
                                if (event.event_category_message_id == 11) {
                                    // "%username% пригласил Вас на беседу в Бизнес-мессенджер"
                                    has_new_invite = true;
                                    utilsSrvc.events.chat_invite_queue.list.push(event);
                                    chat_invite_queue_added++;
                                    chat_invite_queue_message = event.message;
                                }
                                if (event.event_category_message_id == 28) {
                                    count_profile = count_profile + 1;
                                    $('[data-event-selector="my_settings"] .new').text(count_profile);
                                }
                            }
                        }
                    }
                    if(chat_invite_queue_added == 1) {
                        gmaback.js.showMessage(chat_invite_queue_message, 4000, 'rounded');
                    }
                    // $('[data-event-selector="my_friends"] .new').text(count_profile);
                    this.sort();
                    if (need_load_friends) {
                        // Список друзей и приглашений в друзья обновлены
                        utilsSrvc.friend.load();
                    }
                    if (has_new_invite) {
                        if (gmaback.isMasterTab()) {
                            audio.plop2.play();
                        }
                    }
                }
            },
            str: {
                linkify: function (inputText) {
                    var replacedText, replacePattern1, replacePattern2, replacePattern3;
                    //URLs starting with http://, https://, or ftp://
                    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Zа-яА-ЯёЁ0-9+&@$#\/%?=~_|!:,.;]*[-A-Zа-яА-ЯёЁ0-9+&@#\/%=~_|])/gim;
                    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
                    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
                    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
                    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
                    //Change email addresses to mailto:: links.
                    replacePattern3 = /(([a-zA-Zа-яА-ЯёЁ0-9\-\_\.])+@[a-zA-Zа-яА-ЯёЁ\_]+?(\.[a-zA-Zа-яА-ЯёЁ]{2,6})+)/gim;
                    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');
                    return replacedText;
                }
            },
            user: {
                getSessionId: function () {
                    var sid = localStorage.getItem('session_id');
                    return sid;
                }
            },
            api: {
                // Организует вызов API, обработку ответа и вызов callback-функции
                call: function (url, data, callback, callback_error, callback_progress, callback_final) {
                    if ( !url.match(/^\//g)){
                        url = '/'+url;
                    }
                    url = AppConfig.domains.api + url.replace(/\/\//g, '');
                    callback_error = callback_error || null;
                    callback_progress = callback_progress || null;
                    callback_final = callback_final || null;
                    var deferred = $q.defer();
                    var options = {
                        method: 'POST',
                        url: url,
                        headers: {
                            'X-Session-ID': getApp().session_id // ,
                            // 'xsrf': getApp().token_id || (window.localStorageWrapper.getItem('reg_token_id') || null)
                        },
                        data: data instanceof FormData ? data : JSON.parse(JSON.stringify(data)),
                        uploadEventHandlers: {}
                    };
                    var xsrf = getApp().token_id || (window.localStorageWrapper.getItem('reg_token_id') || null);
                    if(xsrf) {
                        options.headers.xsrf = xsrf;
                    }
                    if (callback_progress instanceof Function) {
                        options.uploadEventHandlers.progress = callback_progress;
                    }
                    if (data instanceof FormData) {
                        options.headers['Content-Type'] = undefined;
                    }
                    var promise = $http(options);
                    promise.then(function(result) {
                        result = result.data;
                        if (result.error) {
                            result = result.error;
                            result.status = 'error';
                        }
                        if (result.status == 'error') {
                            if (callback_error && callback_error instanceof Function) {
                                callback_error(result);
                            } else {
                                if (result.code == 401) {
                                    if (result, gmaback.index_scope.pages.url == 'index.login') {
                                        gmaback.js.showError(result.message, 4000);
                                    } else {
                                        getApp().logout();
                                    }
                                } else {
                                    gmaback.js.showError(result.message, 4000);
                                }
                            }
                        } else {
                            callback(result);
                        }
                        if (callback_final && callback_final instanceof Function) {
                            callback_final(result);
                        }
                    }, function(result) {
                        console.log('result', result);
                        if(result.status == 401) {
                            if (result, gmaback.index_scope.pages.url == 'index.login') {
                                gmaback.js.showError(result.message, 4000);
                            } else {
                                getApp().logout();
                            }
                        } else {
                            result = result.data;
                            if (callback_error && callback_error instanceof Function) {
                                callback_error(result);
                            } else {
                                gmaback.js.showError(result.message, 4000);
                            }
                            if (callback_final && callback_final instanceof Function) {
                                callback_final(result);
                            }
                        }
                        /*
                        console.log(reason);
                        gmaback.js.showError('Failed: ' + reason, 4000);
                        if (callback_final && callback_final instanceof Function) {
                            callback_final(reason);
                        }*/
                    }, function (update) {
                        alert('Got notification: ' + update);
                    });
                }
            },
            // my friends
            friend: {
                loading: false,
                list: [],
                invite_list: [],
                my_requests: [],
                // Список друзей и приглашений в друзья обновлены
                load: function () {
                    if (getApp().isLogged()) {
                        utilsSrvc.friend.loading = true;
                        utilsSrvc.api.call('/app/user/getmyfriends/', {}, function (result) {
                            utilsSrvc.friend.list = result.data.list;
                            utilsSrvc.friend.invite_list = result.data.invite_list;
                            utilsSrvc.friend.my_requests = result.data.my_requests;
                            utilsSrvc.friend.loading = false;
                            if (utilsSrvc.friend.invite_list.length > 0) {
                                $('[data-event-selector="my_friends"] .badge').text(utilsSrvc.friend.invite_list.length);
                            } else {
                                $('[data-event-selector="my_friends"] .badge').text('');
                            }
                        });
                    }
                }
            },
            morph: function (n, f1, f2, f5) {
                return Routines.morph(n, f1, f2, f5);
            }
        };
        gmaback.utils_service = utilsSrvc;
        return utilsSrvc;
    };
    service.$inject = injectParams;
    app.factory('utilsSrvc', service);
});