'use strict';

define(['app', 'service/utils', 'gmaback'], function(app) {
	var injectParams = ['utilsSrvc'];
	var service = function(utilsSrvc) {

        var srvc = {
            // Profile
            profile: {
                _id: -1,
                loading: false,
                data: false,
                flag: true,
                readmore: false,
                form: {
                    telegram_code: ''
                },
                load: function() {
                    var that = this;                                              
                    if (!getApp().isLogged()) {
                        return that;
                    }
                    that.loading = true;                
                    utilsSrvc.api.call('/app/user/getbycode', { user_login: getApp().user.user_login, system_type_id: 2 }, function(result){
                        that.data = result.data;
                        var currentDate = new Date();
                        var regDate = new Date(that.data.dt.replace(/-/g,"/"));
                        that.data.regDate = regDate;
                        var diffTime = Math.abs(currentDate.getTime() - regDate.getTime());
                        that.data.regDayAgo = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    }, null, null, function(){
                        that.loading = false;
                    });
                    return that;
                },
                checkbot: function() {
                    var that = this;
                    // that.loading = true;
                    utilsSrvc.api.call('/app/user/checkbotusergoldendax/', {}, function(resp) {   
                        that.checkbotuser = resp.data;
                        if (!that.checkbotuser) {
                            that.telegrambotmessage();
                        }
                    }, null, null, function () {
                        // that.loading = false;
                    });
                },
                telegrambotmessage: function() {
                        return CustomUI.showForm('form-tg_login_bot', false);
                },
                       // Привязка Telegram бота
                tgLoginBotsubmit: function() {
                    var that = this;
                    that.loading = true;
                    utilsSrvc.api.call('/app/user/linktgloginbotgoldendax/', that.form, function(result) {
                        that.flag=false;
                        CustomUI.hideForm();
                        gmaback.js.showMessage('✅ Спасибо.<br>Вы успешно подключили Telegram бот к своему аккаунту.', 4000);
                    }, null, null, function(){
                        that.loading = false;
                    });
                },
	            GoToTeam: function() {
	                gmaback.index_scope.pages.gotoURL('index.team', {});
	            },
                GoToVisitedEvents: function() {
                    if(this.data.official_event_count == 0 || !this.data.invited_by_me) {
                        return;
                    }
                    gmaback.index_scope.pages.gotoURL('index.userevent', {id: this.data.id});
                },
                copyToClipboard: function () {
                    var that = this;
                    var str = 'https://trustera.global/r/' + that.data.user_login + '/';
                    const el = document.createElement('textarea');
                    el.value = str;               
                    document.body.appendChild(el);
                    el.select();
                    document.execCommand('copy');
                    document.body.removeChild(el);
                    gmaback.js.showMessage('Текст скопирован', 4000);
                },
                savefio: function() {
                    var that = this;
                    utilsSrvc.api.call('/app/settings/updatenames', {
                        fio: that.data.fio,
                        second_name: that.data.second_name,
                        nickname: that.data.nickname
                    }, function(result) {
                        gmaback.js.showMessage(getApp().lang.save_success, 4000);
                        CustomUI.hideForm();
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    });
                },
                savephone: function() {
                    var that = this;
                    utilsSrvc.api.call('/app/settings/updatephoneandnames', {
                        first_name: that.data.first_name,
                        last_name: that.data.last_name,
                        phone: that.data.phone
                    }, function(result) {
                        gmaback.js.showMessage(getApp().lang.save_success, 4000);
                        CustomUI.hideForm();
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    });
                },
                saveemail: function() {
                    var that = this;
                    utilsSrvc.api.call('/app/settings/updateemail', {
                        email: that.data.email
                    }, function(result) {
                        gmaback.js.showMessage(getApp().lang.save_success, 4000);
                        CustomUI.hideForm();
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    });
                },
                showabout: function() {
                    return CustomUI.showForm('form-about');
                },
                saveabout: function(item) {
                    var that = this;
                    utilsSrvc.api.call('/app/settings/saveabout', {
                        about: that.data.about
                    }, function(result) {                  
                        gmaback.js.showMessage(getApp().lang.save_success, 4000);
                        CustomUI.hideForm();
                    }, function(result) {
                        gmaback.js.showError(result.message, 4000);
                    });
                }
            }
        };
        return srvc;


        /*return {
            call: function(method, data, callback) {
                var url = '/app/user/' + method;
                utilsSrvc.api.call(url, data, callback);
            }
	    };*/

    };
    service.$inject = injectParams;
    app.factory('userSrvc', ['utilsSrvc', service]);
});