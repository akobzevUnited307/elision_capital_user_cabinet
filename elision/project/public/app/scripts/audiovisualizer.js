'use strict';

define({
    draw_ctx: null,
    ctx: null,
    source: null,
    analyser: null,
    visible: false,
    node: null,
    fbc_array: null,
    vars: {
        // give vars an initial real value to validate
        bars: 200,
        react_x: 0,
        react_y: 0,
        center_x: 0,
        center_y: 0,
        radius: 0,
        deltarad: 0,
        shockwave: 0,
        rot: 0,
        intensity: 0,
        pause: 1,
        isSeeking: 0
    },
    init: function(options) {
        // default configuration properties
        var defaults = {
            container: 'body',
            cover: false,
            ration: 1,
            width: '320px',
            height: '240px'
        };
        var options = $.extend(defaults, options);
        // Canvas
        this.canvas = document.createElement('canvas');
        this.canvas.width = options.width;
        this.canvas.height = options.height;
        if (options.cover) {
            this.canvas.style.width = this.canvas.style.height = '100%';
        } else {
            this.canvas.style.width = (options.width / options.ratio) + 'px';
            this.canvas.style.height = (options.height / options.ratio) + 'px';
        };
        this.container = $(options.container || 'body');
        this.container.append(this.canvas);
        this.draw_ctx = this.canvas.getContext('2d');
        return this;
    },
    start: function(player){
        console.log('audiovisualizer start params', player.audio);
        if(!this.analyser) {
            this.node = player.audio;
            this.ctx = this.ctx || new AudioContext();
            this.analyser = this.ctx.createAnalyser();
            this.source = this.ctx.createMediaElementSource(this.node);
            this.source.connect(this.analyser);
            this.analyser.connect(this.ctx.destination);
            this.fbc_array = new Uint8Array(this.analyser.frequencyBinCount);
            this.frameLooper();
        }
        this.show();
    },
    stop: function(){
        console.log('stopppp');
        this.hide();
        if(this.analyser) {
            this.analyser.disconnect();
            this.analyser = null;
        }
        if(this.source) {
            this.source.disconnect();
            this.source = null;
        }
    },
    show: function(){
        this.container.show();
    },
    hide: function(){
        // this.container.hide();
    },
    frameLooper: function(){

        if(this.visible) {

            var that = this;

            var ctx = that.draw_ctx;
            var canvas = that.canvas;
            
            canvas.width = $('#waveform').outerWidth();
            canvas.height = $('#waveform').outerHeight();

            var grd = ctx.createLinearGradient(0, 0, 0, canvas.height);
            // grd.addColorStop(0, "rgba(180, 140, 230, 0)");
            // grd.addColorStop(1, "rgba(102, 102, 255, 1)");
            grd.addColorStop(0, "rgba(17, 88, 167, 1)");
            grd.addColorStop(1, "rgba(17, 88, 167, 1)");

            ctx.fillStyle = grd;
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            if(!this.source) {
                console.log('Exit from frameLooper');
                return;
            }

            // ctx.fillStyle = "rgba(255, 255, 255, " + (that.vars.intensity * 0.0000125 - 0.4) + ")";
            // ctx.fillRect(0, 0, canvas.width, canvas.height);

            that.vars.rot = that.vars.rot + that.vars.intensity * 0.0000001;

            that.vars.react_x = 0;
            that.vars.react_y = 0;

            that.vars.intensity = 0;

            that.analyser.getByteFrequencyData(that.fbc_array);
            // console.log('looper', that.fbc_array);

            for (var i = 0; i < that.vars.bars; i++) {
                var rads = Math.PI * 2 / that.vars.bars;

                var bar_x = that.vars.center_x;
                var bar_y = that.vars.center_y;
                var bar_height = Math.min(99999, Math.max((that.fbc_array[i] * 2.5 - 200), 0));
                var bar_width = bar_height * 0.02;
                var bar_x_term = that.vars.center_x + Math.cos(rads * i + that.vars.rot) * (that.vars.radius + bar_height);
                var bar_y_term = that.vars.center_y + Math.sin(rads * i + that.vars.rot) * (that.vars.radius + bar_height);

                ctx.save();

                var lineColor = "rgb(" + (that.fbc_array[i]).toString() + ", " + 255 + ", " + 255 + ")";
                // lineColor = "rgba(255, 255, 255, .5)";

                ctx.strokeStyle = lineColor;
                ctx.lineWidth = bar_width;
                ctx.beginPath();
                ctx.moveTo(bar_x, bar_y);
                ctx.lineTo(bar_x_term, bar_y_term);
                ctx.stroke();

                that.vars.react_x += Math.cos(rads * i + that.vars.rot) * (that.vars.radius + bar_height);
                that.vars.react_y += Math.sin(rads * i + that.vars.rot) * (that.vars.radius + bar_height);

                that.vars.intensity += bar_height;
            }

            that.vars.center_x = canvas.width / 2 - (that.vars.react_x * 0.007);
            that.vars.center_y = canvas.height / 2 - (that.vars.react_y * 0.007);

            var radius_old = that.vars.radius;
            that.vars.radius =  25 + (that.vars.intensity * 0.002);
            that.vars.deltarad = that.vars.radius - radius_old;

            ctx.fillStyle = "rgb(255, 255, 255)";
            ctx.beginPath();
            ctx.arc(that.vars.center_x, that.vars.center_y, that.vars.radius + 2, 0, Math.PI * 2, false);
            ctx.fill();

            // shockwave effect            
            that.vars.shockwave += 60;

            ctx.lineWidth = 15;
            ctx.strokeStyle = "rgb(255, 255, 255)";
            ctx.beginPath();
            ctx.arc(that.vars.center_x, that.vars.center_y, that.vars.shockwave + that.vars.radius, 0, Math.PI * 2, false);
            ctx.stroke();
       
            if (that.vars.deltarad > 15) {
                that.vars.shockwave = 0;
                ctx.fillStyle = "rgba(255, 255, 255, 0.7)";
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                that.vars.rot = that.vars.rot + 0.4;
            }
            
        }

        //if (!isSeeking) {
        //    document.getElementById("audioTime").value = (100 / audio.duration) * audio.currentTime;
        //}
        // document.getElementById("time").innerHTML = Math.floor(audio.currentTime / 60) + ":" + (Math.floor(audio.currentTime % 60) < 10 ? "0" : "") + Math.floor(audio.currentTime % 60);

        var self = this;
        window.requestAnimationFrame(function(){
            self.frameLooper();
        });
    }
});