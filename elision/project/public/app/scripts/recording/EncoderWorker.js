// manually rewritten from CoffeeScript output
// (see dev-coffee branch for original source)

// importScripts('/app/scripts/recording/OggVorbisEncoder.js');
importScripts((isDesktop() ? '.' : '') + '/app/scripts/recording/OggVorbisEncoder.min2.js');

var buffers = undefined,
encoder = undefined;

self.onmessage = function(event) {
	var data = event.data;
	console.log('->', data.command);
	try {
		switch (data.command) {
			case 'start':
				encoder = new OggVorbisEncoder(data.sampleRate, data.numChannels,
				data.quality);
				buffers = data.process === 'separate' ? [] : undefined;
				return 'worked';
				break;
			case 'record':
				if (buffers != null) {
					buffers.push(data.buffers);
				} else {
					encoder.encode(data.buffers);
				}
				break;
			case 'finish':
				if (buffers != null) {
					while (buffers.length > 0) {
						encoder.encode(buffers.shift());
					}
				}
				self.postMessage({ blob: encoder.finish() });
				encoder = undefined;
				break;
			case 'cancel':
				encoder.cancel();
				encoder = undefined;
		}
	} catch(e) {
		console.log(e);
	}
};
