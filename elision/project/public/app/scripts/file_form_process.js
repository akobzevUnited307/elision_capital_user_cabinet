// MessengerFileFormProcess

'use strict';

var MessengerFileFormProcess = function ($scope, $timeout, channelID) {
    this.ID = gmaback.js.guid();
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.channelID = channelID;
    this.fileList = [];
    this.formList = [];
    this.fileNeedLoad = 0;
    this.message = null; // comment to file(s)
    this.contentType = null;
    this.originalFile = null;
    this.compressedFile = null;
    this.request = null;
    this.documentFile = null;
    this.compressedAudio = null;
    this.sendCompressedImage = true;
    this.confirm = false; // Send process confirmed by press [Send] button
    this.sent = false;
    this.documentFileName = null;
    this.digital_signature_sms_code = '';
    this.image_extensions = [
        'png', 'jpg', 'jpeg', 'gif'
    ];
    this.fileicons = [
        'ai', 'avi', 'css', 'csv', 'dbf', 'doc', 'docx',
        'dwg', 'exe', 'fla', 'htm', 'html', 'iso', 'js', 'json', 'mp3',
        'mp4', 'pdf', 'ppt', 'psd', 'rtf', 'svg',
        'txt', 'xls', 'xlsx', 'xml', 'zip',
        'jpg', 'jpeg', 'png'
    ];
};

MessengerFileFormProcess.prototype.setPreviewImage = function (img) {
    $('#image-preview').html(img);
};

MessengerFileFormProcess.prototype.showPreview = function (img) {
    return CustomUI.showForm('form-fileForm');
};

MessengerFileFormProcess.prototype.pasteText = function (text) {
    // do nothing
};

MessengerFileFormProcess.prototype.keyDown = function (e) {
    if (e.keyCode == 13 && (!e.ctrlKey && !e.altKey && !e.shiftKey)) {
        return this.Send();
    }
};

MessengerFileFormProcess.prototype.Send = function () {
    var process = this;
    var sendCompressed = true; // || $('#input-sendCompressedImage').is(':checked');
    this.formList = [];
    this.confirm = true;
    this.message = $('#input-imageFile-comment').val() || '';
    $('#input-imageFile-comment').val('');
    CustomUI.hideForm('form-fileForm');
    var index = 0;
    for (var file of process.fileList) {
        var item = {
            loading: true,
            file: file,
            index: index++,
            form: new FormData(),
            request: new XMLHttpRequest(),
            processing: function () {
                this.form.append('chat_id', process.channelID);
                this.form.append('digital_signature_sms_code', 0);
                this.form.append('message', this.index == 0 ? process.message : '');
                if (sendCompressed) {
                    // console.log('send', this.file.compressedFile ? 1 : 0)
                    if (this.file.compressedAudio) {
                        var blob = new Blob([this.file.compressedAudio], {
                            type: 'application/octet-binary'
                        });
                        // form.append('audio_compressed', blob, 'audio_compressed.mp3');
                        this.form.append('audio_compressed', blob, 'audio_compressed.ogg');
                    } else if (this.file.compressedFile) {
                        var blob = new Blob([this.file.compressedFile], {
                            type: 'application/octet-binary'
                        });
                        this.form.append('image_compressed', blob, 'image_compressed.jpg');
                    } else if (this.file.content_type == 'document') {
                        // var blob = new Blob([this.file.file], {type: 'application/octet-binary'});
                        // this.form.append('document', blob, this.file.file_name);
                        this.form.append('document', this.file.file);
                    }
                } else {
                    // var fileOfBlob = new File(this.compressedFile, 'image_original');
                    // form.append('image_original', fileOfBlob);
                    // form.append('image_original', this.originalFile);
                }
                var that = this;
                this.request.onreadystatechange = function () {
                    if (that.request.readyState == 4 && that.request.status == 200) {
                        var data = angular.fromJson(that.request.responseText);
                        console.log(data);
                        // $(btn).removeClass('loading');
                        if (typeof data.error != 'undefined') {
                            gmaback.js.showError(data.error.message, 4000);
                        } else if (typeof data.status != 'undefined') {
                            if (data.status == 'success') {
                                // console.log('loading = false', that.file.file_name);
                                that.loading = false;
                                process.$scope.$apply();
                            } else {
                                gmaback.js.showError(data.message, 4000);
                            }
                        } else {
                            gmaback.js.showError(data.result, 4000);
                        }
                    }
                };
                this.request.open(
                    'POST',
                    getApp().domains.api + '/app/messenger/uploadfile/',
                    true
                );
                // this.request.setRequestHeader('X-Xsrftoken', getApp().XSRF);
                this.request.setRequestHeader('xsrf', getApp().token_id);
                this.request.setRequestHeader('X-Session-ID', getApp().session_id);
                this.request.send(this.form);
            }
        };
        this.formList.push(item);
        item.processing();
    }
    return false;
}

MessengerFileFormProcess.prototype.check = function () {
    var ready = this.fileNeedLoad == 0
    this.$scope.$apply();
}

MessengerFileFormProcess.prototype.appendCompressedAudio = function (blob) {
    var process = this;
    // proc.appendCompressedAudio(blob);
    // compressedAudio = blob;
    var content_type = 'document';
    // New file object
    var fileObj = {
        content_type: content_type,
        // file_preview_html:  file_preview_html,
        // file_icon:          file_icon,
        // extension:          extension,
        // file_name:          CurrentFile.name,
        // file:               CurrentFile,
        // preview_image:      null,
        compressedFile: null,
        compressedAudio: blob,
        fileNeedLoad: 0,
        loading: false,
        form_process: process,
        isImage: false,
        canvas: null,
        ctx: null,
        file_preview_html: getApp().lang.upload_image + '...'
    };
    process.fileList.push(fileObj);
    process.check();
    process.Send();
    // CustomUI.showForm('form-fileForm', false);
}

MessengerFileFormProcess.prototype.appendFiles = function (files) {
    var that = this;
    var process = this;
    $('#wizard-messenger-document').scWizard('showPage', 0);
    $('#wizard-messenger-document .loading').show();
    $('#wizard-messenger-document .loaded').hide();
    $('#wizard-messenger-document input[type="text"]').val('');
    this.$timeout(function () {
        var contentTypes = [];
        that.fileNeedLoad = files.length;
        // for(var i = 0; i < files.length; i++) {
        for (var CurrentFile of files) {
            var extension = CurrentFile.name.split('.').pop().toLowerCase();
            var isImage = that.image_extensions.indexOf(extension) !== -1;
            var file_icon = '/assets/img/file.png';
            var content_type = 'unknown';
            var file_preview_html = null;
            if (that.fileicons.indexOf(extension) >= 0) {
                file_icon = '/assets/img/extensions/' + extension + '.png';
            }
            if (isImage) {
                content_type = 'image';
            } else {
                content_type = 'document';
                file_preview_html = '<div style="padding-top: 20px; word-break: break-all;"><img src="' + file_icon + '" style="height: 110px; box-shadow: none;" /><br>' + CurrentFile.name + '</div>';
            }
            if (contentTypes.indexOf(content_type) === -1) {
                contentTypes.push(content_type)
            }
            // New file object
            var fileObj = {
                content_type: content_type,
                file_preview_html: file_preview_html,
                file_icon: file_icon,
                extension: extension,
                file_name: CurrentFile.name,
                file: CurrentFile,
                preview_image: null,
                compressedFile: null,
                loading: true,
                form_process: process,
                isImage: isImage,
                canvas: null,
                ctx: null,
                file_preview_html: getApp().lang.upload_image + '...',
                reader: new FileReader(),
                processing: function () {
                    console.log('file', this.file_name);
                    if (this.content_type == 'document') {
                        this.loading = false;
                        process.fileNeedLoad--;
                        process.check();
                    } else if (this.content_type == 'image') {
                        var that = this;
                        this.reader.onload = function (evt) {
                            var fileOrBlob = evt.target.result; // base64 encoded image
                            loadImage.parseMetaData(
                                that.file,
                                function (options) {
                                    var orientation = 0;
                                    if (typeof options != 'undefined' && typeof options.exif != 'undefined') {
                                        // console.log('loadImage.parseMetaData', data, data.exif);
                                        orientation = options.exif.get('Orientation');
                                    }
                                    // console.log('orientation', orientation);
                                    var pastedImage = new Image();
                                    pastedImage.onload = function () {
                                        var w = pastedImage.width;
                                        var h = pastedImage.height;
                                        var resized = false;
                                        if (w > h) {
                                            if (w > getApp().messenger.max_upload_image_size) {
                                                w = getApp().messenger.max_upload_image_size;
                                                h *= w / pastedImage.width;
                                                resized = true;
                                            }
                                        } else {
                                            if (h > getApp().messenger.max_upload_image_size) {
                                                h = getApp().messenger.max_upload_image_size;
                                                w *= h / pastedImage.height;
                                                resized = true;
                                            }
                                        }
                                        that.canvas = document.createElement('canvas');
                                        that.ctx = that.canvas.getContext('2d');
                                        that.canvas.width = w;
                                        that.canvas.height = h;
                                        that.ctx.beginPath();
                                        that.ctx.rect(0, 0, w, h);
                                        that.ctx.fillStyle = 'white';
                                        that.ctx.fill();
                                        if (orientation > 0) {
                                            loadImage.transformCoordinates(that.canvas, {
                                                orientation: orientation
                                            });
                                        }
                                        if (resized) {
                                            that.ctx.drawImage(pastedImage, 0, 0, pastedImage.width, pastedImage.height, 0, 0, w, h);
                                        } else {
                                            that.ctx.drawImage(pastedImage, 0, 0);
                                        }
                                        var photo = that.canvas.toDataURL('image/jpeg');
                                        that.compressedFile = photo;
                                        // Create an image element for preview
                                        that.preview_image = document.createElement('img');
                                        that.preview_image.onload = function () {
                                            // CustomUI.centerForms();
                                        };
                                        that.preview_image.src = photo;
                                        that.loading = false;
                                        that.form_process.fileNeedLoad--;
                                        that.form_process.check();
                                    };
                                    pastedImage.src = fileOrBlob; // base64 encoded image
                                }
                            );
                        };
                        this.reader.readAsDataURL(this.file);
                    }
                }
            };
            that.fileList.push(fileObj);
            fileObj.processing();
        }
        // console.log('contentTypes', contentTypes);
        if (contentTypes.length < 0) {
            return false;
        } else if (contentTypes.length > 1) {
            that.contentType = 'mixed';
        } else {
            that.contentType = contentTypes.shift();
        }
        that.check();
        CustomUI.showForm('form-fileForm', false);
    });
}