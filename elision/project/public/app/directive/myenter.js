'use strict';

define(['app'], function(app) {
    var injectParams = ['$q'];
    var directive = function($q) {
        return function(scope, element, attrs) {
            element.bind('keydown keypress', function (event) {
                if(event.which === 13) {
                    if(!event.shiftKey) {
                        scope.$apply(function (){
                            scope.$eval(attrs.myEnter);
                        });
                        event.preventDefault();
                    }
                }
            });
        };
    };
    directive.$inject = injectParams;
    app.directive('myEnter', directive);
});