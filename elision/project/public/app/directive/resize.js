'use strict';

define(['app'], function(app) {
    var injectParams = ['$window'];
    var directive = function($window) {
        return function (scope, element) {
            /*return false;*/
            var w = angular.element($window);
            scope.getWindowDimensions = function () {
                return {
                    'w': w.width(),
                    'h': w.height()
                };
            };
            scope.$watch(scope.getWindowDimensions, function(newValue, oldValue) {
                scope.windowWidth = newValue.w;
                scope.minWindowWidth = 600; // минимальная ширина окна браузера, при котором список контакта остается видимым
                scope.mobileview = scope.windowWidth <= scope.minWindowWidth;
                scope.smartphoneview = scope.windowWidth <= 600;
                scope.windowHeight = newValue.h;
                scope.style = function () {
                    return {
                        'height': (newValue.h - 100) + 'px',
                        'width': (newValue.w - 100) + 'px'
                    };
                };
            }, true);
            w.bind('resize', function () {
                scope.$apply();
            });
        }
    };
    directive.$inject = injectParams;
    app.directive('resize', directive);
});